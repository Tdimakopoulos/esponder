/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.operationcenter;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredOperationsCentreDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.type.CrisisTypeDTO;
import eu.esponder.jsfutils.JSFUtils;
import eu.esponder.objects.Logistic;
import eu.esponder.objects.OperationCenter;
import eu.esponder.objects.ResourcePlanInfo;
import eu.esponder.objects.person;
import eu.esponder.personnel.*;
import eu.esponder.ws.client.logic.CrisisResourceManager;
import eu.esponder.ws.client.logic.ResourcePlanInfoQueries;
import eu.esponder.ws.client.query.QueryManager;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.primefaces.event.DragDropEvent;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class OperationCentersManager {

    private List<OperationCenter> personnel;
    private List<OperationCenter> selectedPersonnel;
  private List<ResourcePlanInfo> pPersonnelPlanInfo;
    private String octoremove;
    private String crisiscontextparam;
  
        
     
     private void PopulateOCs()
    {
//        personnel.add(new OperationCenter("Messi", 10, "po.jpg", "MEOC"));
//        personnel.add(new OperationCenter("Villa", 7, "po.jpg", "MEOC"));
//        personnel.add(new OperationCenter("Pedro", 17, "po.jpg", "MEOC"));
//        personnel.add(new OperationCenter("Bojan", 9, "po.jpg", "MEOC"));
//        personnel.add(new OperationCenter("Xavi", 6, "po.jpg", "EOC"));
//        personnel.add(new OperationCenter("Iniesta", 8, "ff.jpg", "EOC"));
//        personnel.add(new OperationCenter("Mascherano", 16, "ff.jpg", "EOC"));
//        personnel.add(new OperationCenter("Puyol", 5, "ff.jpg", "EOC"));
//        personnel.add(new OperationCenter("Alves", 2, "ff.jpg", "EOC"));
//        personnel.add(new OperationCenter("Valdes", 1, "ff.jpg", "EOC"));
        try {
            ResourcePlanInfoQueries pFind = new ResourcePlanInfoQueries();
            QueryManager pQuery = new QueryManager();
            ResultListDTO pResults = pQuery.getRegisterdOCAll("1");
            for (int i = 0; i < pResults.getResultList().size(); i++) {
                RegisteredOperationsCentreDTO pItem = (RegisteredOperationsCentreDTO) pResults.getResultList().get(i);
                // fix me data need update   
                OperationCenter pperson = new OperationCenter(pItem.getTitle(), pItem.getId().intValue(), "NoPhoto", pFind.GetresourceCategoryforRegOC(pItem.getOperationsCentreCategoryId()));
                // pperson.setLat(pItem.getOrganisation().getLocation().getLatitude());
                //pperson.setLon(pItem.getOrganisation().getLocation().getLongitude());
                //pperson.setTitle(pItem.getTitle());
                //pperson.setOrganization(pItem.getOrganisation().getTitle());




                if(pItem.getStatus()==ResourceStatusDTO.AVAILABLE)
                personnel.add(pperson);
            }
        } catch (JsonParseException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JsonMappingException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void PopulateActionPlanInfo()
    {
      //  pPersonnelPlanInfo.add(new ResourcePlanInfo("MEOC",1,0));
//        pPersonnelPlanInfo.add(new ResourcePlanInfo("EOC",1,0));
        String plantitle="0";
        crisiscontextparam = (String) JSFUtils.getParameter("TestParam");
        QueryManager pq=new QueryManager();
        try {
            CrisisContextDTO pcc=pq.getCrisisContextID("1", this.crisiscontextparam);
            System.out.print(pcc.getTitle());
            CrisisTypeDTO[] pct=pcc.getCrisisTypes().toArray(new CrisisTypeDTO[pcc.getCrisisTypes().size()]);
            if(pct.length>=0)
                plantitle=pct[0].getTitle();
            
        } catch (JsonParseException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JsonMappingException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        }
         try {
            ResourcePlanInfoQueries pinfoQuery=new ResourcePlanInfoQueries();
            pPersonnelPlanInfo=pinfoQuery.GetQueryLists(3,plantitle);
            //getpPersonnelPlanInfo().add(new ResourcePlanInfo("Police Officer",10,0));
            //getpPersonnelPlanInfo().add(new ResourcePlanInfo("Fire Fighter Special",5,0));
            //getpPersonnelPlanInfo().add(new ResourcePlanInfo("Fire Fighter Special",5,0));
        } catch (IOException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public OperationCentersManager() {
        personnel = new ArrayList<OperationCenter>();
        selectedPersonnel = new ArrayList<OperationCenter>();
    pPersonnelPlanInfo = new ArrayList<ResourcePlanInfo>();
        
        PopulateActionPlanInfo();
        PopulateOCs();
        
    }

    public List<OperationCenter> getPersonnel() {
        return personnel;
    }

    public List<OperationCenter> getSelectedPersonnel() {
        return selectedPersonnel;
    }

     private void UpdateSelectedInfo(String szPosition,int iadd)
    {
        for (int i=0;i<pPersonnelPlanInfo.size();i++)
        {
            ResourcePlanInfo pElement=pPersonnelPlanInfo.get(i);
            if (pElement.getPtype().equalsIgnoreCase(szPosition))
            {
                if (iadd==1)
                {
                    int iselected=pElement.getIselected();
                    int ineeded=pElement.getIneeded();
                    pElement.setIselected(iselected+1);
                    pElement.setIneeded(ineeded-1);
                }
                if (iadd==-1)
                {
                    int iselected=pElement.getIselected();
                    int ineeded=pElement.getIneeded();
                    pElement.setIselected(iselected-1);
                    pElement.setIneeded(ineeded+1);
                }
            }
        }
    }
     
      private void clearlist()
    {
        for (int i=0;i<selectedPersonnel.size();i++)
        {
            for (int d=0;d<personnel.size();d++)
            {
                if(selectedPersonnel.get(i).getName().equalsIgnoreCase(personnel.get(d).getName()))
                {
                    System.out.println("Remove : "+personnel.get(d).getName());
                    personnel.remove(d);
                }
            }
        }
    }
      private int FindPersonPosition(String szTitle)
    {
        for(int i=0;i<selectedPersonnel.size();i++)
        {
            if (szTitle.equalsIgnoreCase(selectedPersonnel.get(i).getName()))
                return i;
        }
        return -1;
    }
    public void onPersonDrop(DragDropEvent event) {
        OperationCenter player = (OperationCenter) event.getData();
UpdateSelectedInfo(player.getPosition(),1);
        selectedPersonnel.add(player);
        personnel.remove(player);
        CrisisResourceManager pMan = new CrisisResourceManager();
        try {
            OperationsCentreDTO pOC=pMan.AssociateOC(crisiscontextparam, String.valueOf(player.getNumber()), "1");
             int ipos=FindPersonPosition(player.getName());//selectedPersonnel.indexOf(player);
                player.setNumber2(pOC.getId().intValue());
                selectedPersonnel.set(ipos, player);
        } catch (JsonGenerationException ex) {
            Logger.getLogger(OperationCentersManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JsonMappingException ex) {
            Logger.getLogger(OperationCentersManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(OperationCentersManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        clearlist();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(player.getName() + " added", "Position:" + event.getDropId()));
    }
     public String removeperson() {
        int ifind=Integer.parseInt(octoremove);
        for(int i=0;i<selectedPersonnel.size();i++)
        {
            if(selectedPersonnel.get(i).getNumber()==ifind)
            {
                OperationCenter pelement=selectedPersonnel.get(i);
                UpdateSelectedInfo(pelement.getPosition(),-1);
                //selectedPersonnel.remove(pelement);
                OperationCenter pocc=selectedPersonnel.get(i);
                selectedPersonnel.remove(i);
                personnel.add(pelement);
                CrisisResourceManager pMan = new CrisisResourceManager();
                try {
                    pMan.deAssociateoc(String.valueOf(pocc.getNumber2()), "1");
                } catch (JsonGenerationException ex) {
                    Logger.getLogger(OperationCentersManager.class.getName()).log(Level.SEVERE, null, ex);
                } catch (JsonMappingException ex) {
                    Logger.getLogger(OperationCentersManager.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(OperationCentersManager.class.getName()).log(Level.SEVERE, null, ex);
                }
                System.out.print("Find oc to remove"+pelement.getName());
            }
         } 
        
        //selectedPersonnel.add(player);
        //personnel.remove(player);
        
        System.out.print("removeperson "+octoremove);
                return "CrisisOperationCenters?faces-redirect=true";
        }
    /**
     * @return the pPersonnelPlanInfo
     */
    public List<ResourcePlanInfo> getpPersonnelPlanInfo() {
        return pPersonnelPlanInfo;
    }

    /**
     * @param pPersonnelPlanInfo the pPersonnelPlanInfo to set
     */
    public void setpPersonnelPlanInfo(List<ResourcePlanInfo> pPersonnelPlanInfo) {
        this.pPersonnelPlanInfo = pPersonnelPlanInfo;
    }

    /**
     * @return the octoremove
     */
    public String getOctoremove() {
        
        return octoremove;
    }

    /**
     * @param octoremove the octoremove to set
     */
    public void setOctoremove(String octoremove) {
        
        this.octoremove = octoremove;
    }

    /**
     * @return the crisiscontextparam
     */
    public String getCrisiscontextparam() {
        return crisiscontextparam;
    }

    /**
     * @param crisiscontextparam the crisiscontextparam to set
     */
    public void setCrisiscontextparam(String crisiscontextparam) {
        this.crisiscontextparam = crisiscontextparam;
    }

    

   
}
