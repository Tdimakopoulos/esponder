/*
 * 
 */
package eu.esponder.rest.datafusion;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlTransient;

import eu.esponder.df.ruleengine.controller.DatafusionControllerRemoteService;
import eu.esponder.df.ruleengine.utilities.ruleresults.object.RuleResultsXML;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.datafusion.DatafusionResultsDTO;
import eu.esponder.event.model.datafusion.DatafusionResultsGeneratedEvent;
import eu.esponder.event.voip.ESponderVOIPCallRequestEvent;
import eu.esponder.event.voip.ESponderVOIPConferenceRequestEvent;
import eu.esponder.eventadmin.ESponderEventPublisher;
import eu.esponder.rest.ESponderResource;
import eu.esponder.test.ResourceLocator;

// TODO: Auto-generated Javadoc
/**
 * The Class DatafusionRepositoryManager.
 */
@Path("/datafusion")
@XmlTransient
public class DatafusionRepositoryManager extends ESponderResource {

	@POST
	@Path("/makecall")
	@Consumes({ MediaType.APPLICATION_JSON })
	public String makecall(
			ESponderVOIPCallRequestEvent callRequestEvent,
			@QueryParam("userID") @NotNull(message = "userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message = "sessionID may not be null") String sessionID) {

		ESponderEventPublisher<ESponderVOIPCallRequestEvent> publisher = 
				new ESponderEventPublisher<ESponderVOIPCallRequestEvent>(ESponderVOIPCallRequestEvent.class);
		
		try {
			publisher.publishEvent(callRequestEvent);
		} catch (Exception e) {
			return e.getMessage();
		}
		publisher.CloseConnection();
		
		return "NoErrors";
	}

	@POST
	@Path("/makeconference")
	@Consumes({ MediaType.APPLICATION_JSON })
	public String makeconference(
			ESponderVOIPConferenceRequestEvent conferenceRequestEvent,
			@QueryParam("userID") @NotNull(message = "userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message = "sessionID may not be null") String sessionID) {

		ESponderEventPublisher<ESponderVOIPConferenceRequestEvent> publisher = 
				new ESponderEventPublisher<ESponderVOIPConferenceRequestEvent>(ESponderVOIPConferenceRequestEvent.class);
		
		try {
			publisher.publishEvent(conferenceRequestEvent);
		} catch (Exception e) {
			return e.getMessage();
		}
		publisher.CloseConnection();
		
		return "NoErrors";
	}
	
	@POST
	@Path("/dropcall")
	@Consumes({ MediaType.APPLICATION_JSON })
	public String dropcall(
			ESponderVOIPCallRequestEvent callRequestEvent,
			@QueryParam("userID") @NotNull(message = "userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message = "sessionID may not be null") String sessionID) {

		ESponderEventPublisher<ESponderVOIPCallRequestEvent> publisher = 
				new ESponderEventPublisher<ESponderVOIPCallRequestEvent>(ESponderVOIPCallRequestEvent.class);
		
		try {
			publisher.publishEvent(callRequestEvent);
		} catch (Exception e) {
			return e.getMessage();
		}
		publisher.CloseConnection();
		
		return "NoErrors";
	}

	@POST
	@Path("/dropconference")
	@Consumes({ MediaType.APPLICATION_JSON })
	public String dropconference(
			ESponderVOIPConferenceRequestEvent conferenceRequestEvent,
			@QueryParam("userID") @NotNull(message = "userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message = "sessionID may not be null") String sessionID) {

		ESponderEventPublisher<ESponderVOIPConferenceRequestEvent> publisher = 
				new ESponderEventPublisher<ESponderVOIPConferenceRequestEvent>(ESponderVOIPConferenceRequestEvent.class);
		
		try {
			publisher.publishEvent(conferenceRequestEvent);
		} catch (Exception e) {
			return e.getMessage();
		}
		publisher.CloseConnection();
		
		return "NoErrors";
	}

	// ESponderVOIPCallRequestEvent
	@GET
	@Path("/StartEventServer")
	public String startEventServer() {

		this.getEventServerRemoteService().StartServer();
		return "ok";
	}

	@GET
	@Path("/StopEventServer")
	public String stopEventServer() {

		this.getEventServerRemoteService().StopServer();
		return "ok";
	}

	/**
	 * Remotetolocal.
	 * 
	 * @param userID
	 *            the user id
	 * @param sessionID
	 *            the session id
	 * @return the string
	 */
	@GET
	@Path("/repository/remotetolocal")
	public String remotetolocal(
			@QueryParam("userID") @NotNull(message = "userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message = "sessionID may not be null") String sessionID) {

		String szReturnStatus = "NoErrors";

		DatafusionControllerRemoteService pservice = ResourceLocator
				.lookup("esponder/DatafusionControllerBean/remote");

		pservice.ReinitializeRepository();

		return szReturnStatus;
	}

	/**
	 * Ruleresults.
	 * 
	 * @param userID
	 *            the user id
	 * @param sessionID
	 *            the session id
	 * @return the rule results
	 * @throws Exception
	 *             the exception
	 */
	@GET
	@Path("/repository/ruleresults")
	@Produces({ MediaType.APPLICATION_JSON })
	public RuleResults ruleresults(
			@QueryParam("userID") @NotNull(message = "userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message = "sessionID may not be null") String sessionID)
			throws Exception {


		DatafusionControllerRemoteService pservice = ResourceLocator
				.lookup("esponder/DatafusionControllerBean/remote");

		List<RuleResultsXML> pResults = pservice.GetRuleResults();
		return new RuleResults(pResults);
	}

	/**
	 * Deleterepo.
	 * 
	 * @param userID
	 *            the user id
	 * @param sessionID
	 *            the session id
	 * @return the string
	 */
	@GET
	@Path("/repository/deleterepo")
	public String deleterepo(
			@QueryParam("userID") @NotNull(message = "userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message = "sessionID may not be null") String sessionID) {

		String szReturnStatus = "NoErrors";

		DatafusionControllerRemoteService pservice = ResourceLocator
				.lookup("esponder/DatafusionControllerBean/remote");

		pservice.DeleteRepository();

		return szReturnStatus;
	}

	/**
	 * Switchtolocal.
	 * 
	 * @param userID
	 *            the user id
	 * @param sessionID
	 *            the session id
	 * @return the string
	 */
	@GET
	@Path("/repository/switchtolocal")
	public String switchtolocal(
			@QueryParam("userID") @NotNull(message = "userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message = "sessionID may not be null") String sessionID) {
		String szReturnStatus = "NoErrors";

		DatafusionControllerRemoteService pservice = ResourceLocator
				.lookup("esponder/DatafusionControllerBean/remote");

		pservice.SetToLocal();

		return szReturnStatus;
	}

	/**
	 * Switchtoguvnor.
	 * 
	 * @param userID
	 *            the user id
	 * @param sessionID
	 *            the session id
	 * @return the string
	 */
	@GET
	@Path("/repository/switchtoguvnor")
	public String switchtoguvnor(
			@QueryParam("userID") @NotNull(message = "userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message = "sessionID may not be null") String sessionID) {

		String szReturnStatus = "NoErrors";

		DatafusionControllerRemoteService pservice = ResourceLocator
				.lookup("esponder/DatafusionControllerBean/remote");

		pservice.SetToLive();

		return szReturnStatus;
	}
}
