package eu.esponder.fru.helper;

import android.util.Log;

public class FRULogger {
	
	public static int logError(Object obj, String message) {
		return Log.e(obj.getClass().getCanonicalName(), message);
	}
	
	public static int logInfo(Object obj, String message) {
		return Log.i(obj.getClass().getCanonicalName(), message);
	}
	
	public static int logVerbose(Object obj, String message) {
		return Log.v(obj.getClass().getCanonicalName(), message);
	}
	
	public static int logWarn(Object obj, String message) {
		return Log.w(obj.getClass().getCanonicalName(), message);
	}
	
	public static int logDebug(Object obj, String message) {
		return Log.d(obj.getClass().getCanonicalName(), message);
	}

}
