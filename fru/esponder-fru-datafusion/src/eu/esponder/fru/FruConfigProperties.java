package eu.esponder.fru;

public class FruConfigProperties {
	
	
	public static final String FRU_PROPERTIES_CONFIGURATION_FILE = "fruConfiguration.properties";
	public static final String FRU_PROPERTIES_CONFIGURATION_PATH = "./config";
	public static final String FRU_PROPERTIES_CONFIGURATION_SEPARATOR = "/";
	
	public static final String FRU_PROPERTIES_CONFIGURATION_FULLNAME = 
			FRU_PROPERTIES_CONFIGURATION_PATH + FRU_PROPERTIES_CONFIGURATION_SEPARATOR + FRU_PROPERTIES_CONFIGURATION_FILE;
	

	public static final String FRU_PROPERTIES_KEY = "eu.esponder.fru";
	
	public static final String FRU_PROPERTIES_SEPARATOR_KEY = ".";
	public static final String FRU_DATAFUSION_MEASUREMENT_ENVELOPE_TX_PERIOD_KEY = 
			FRU_PROPERTIES_KEY + FRU_PROPERTIES_SEPARATOR_KEY + "datafusion.sensor.measurement.tx.period.seconds";
}
