/**
 * Part of The Security Library
 * 
 * Export Certificate
 * 
 * Functions
 * ---------------
 * Export : Export the certificate passed as argument using the path and name passed as arg as well
 * 
 * @author tdi
 */
package org.certificate.export;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.security.cert.CertificateEncodingException;
import sun.misc.BASE64Encoder;
import sun.security.provider.X509Factory;

public class exportcertificate {

    public void export(java.security.cert.X509Certificate[] chain, String path, String name) throws FileNotFoundException, UnsupportedEncodingException, IOException, CertificateEncodingException {
        OutputStream fout = new FileOutputStream(path + name);
        OutputStream bout = new BufferedOutputStream(fout);
        OutputStreamWriter out = new OutputStreamWriter(bout, "8859_1");
        BASE64Encoder encoder = new BASE64Encoder();
        out.write(X509Factory.BEGIN_CERT);
        out.flush();
        encoder.encodeBuffer(chain[0].getEncoded(), bout);
        bout.flush();
        out.write(X509Factory.END_CERT);
        out.flush();
        out.close();
    }
}
