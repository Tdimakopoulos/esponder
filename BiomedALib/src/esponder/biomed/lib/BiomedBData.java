/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package esponder.biomed.lib;


/**
 *
 * @author afa
 */
 public class BiomedBData extends BiomedAData {

    public BiomedBData(int flags, int channel, byte stream[], int payloadStartIndex, int sizeOfPayload) {
        super(flags, channel, stream, payloadStartIndex, sizeOfPayload);
    }

    /**
     * Get the subsequent value in the stream. This value can either be 8, 12 or 16 bits.
     * The flags attribute will make the decision about how to parse the value
     * @return 
     */
    @Override
    protected int getValue(int sample) {
        int value = 0;

        switch (this.sampleSize) {
            case 0: /*8bits*/
                value = this.buffer[this.index++];
                if (this.isunsigned) {
                    value &= 0xff;
                }
                break;
            case 1: /*12bits*/
                if ((sample % 2) == 0) {
                    value = (((int) buffer[this.index + 0]) & 0xff) | ((((int) buffer[this.index + 1]) & 0x0f) << 8);
                    ++this.index;//increment only for the first byte, the second byte will be used again
                } else {
                    value = ((((int) buffer[this.index + 0]) & 0xf0) << 4) | (((int) buffer[this.index + 1]) & 0xff);
                    this.index += 2;//increment with 2 after 
                }
                if (!this.isunsigned) {
                    value = ((value & (1 << 11)) != 0) ? value - 4096 : value;
                }
                break;
            case 2: /*16bits*/
                //MSB is stored before, instead of LSB as in the SEW
                value = (((int) buffer[this.index + 0]) << 8);
                value += (((int) buffer[this.index + 1]) & 0xff);
                this.index += 2;
                if (this.isunsigned) {
                    value &= 0xffff;
                }
                break;
            case 3:
                value = (((int) buffer[this.index + 0]) << 16);
                value += ((((int) buffer[this.index + 1])& 0xff) << 8);
                value += (((int) buffer[this.index + 2]) & 0xff);
                this.index += 3;
                if (this.isunsigned) {
                    value &= 0xffffff;
                }
                break;
            case 4:
                value = (((int) buffer[this.index + 0]) << 24);
                value += ((((int) buffer[this.index + 1])& 0xff) << 16);
                value += ((((int) buffer[this.index + 2])& 0xff) << 8);
                value += (((int) buffer[this.index + 3]) & 0xff);
                this.index += 4;
                if (this.isunsigned) {
                    value &= 0xffffffff;
                }
                break;
        }
        return value;
    }

    /**
     * Encapsulate the information and the data in RegularDataBlocks instances
     * 
     * @return 
     */
    public RegularDataBlock[] getDataBlocks() {
        float values[] = null;
        RegularDataBlock rdbs[] = null;

        //find the entry of the dictionary corresponding to the channel
//channel=0;
        BiomedBUtils pp = new BiomedBUtils();
        BiomedBDictionaryEntry entry = pp.dictionary.getEntry(this.channel);
        if(entry == null) return null;
        int numberOfBlocks = entry.blocks.length;
        rdbs = new RegularDataBlock[numberOfBlocks];
        //construct rdbs
        for (int blockIdx = 0; blockIdx < numberOfBlocks; blockIdx++) {
            BiomedBDictionaryBlock block = entry.blocks[blockIdx];
            this.isunsigned = !block.signed;
            this.sampleSize = block.sampleSize;
            rdbs[blockIdx] = new RegularDataBlock("", block.channel);
            values = new float[block.sampleCount]; 
            for (int i = 0; i < values.length; i++) {
                //values[i] = this.getValue(i, block.sampleSize, block.signed);
                values[i] = this.getValue(i);
            }
            switch (block.timeStampIncrementation) {
                case BiomedBDictionaryBlock.FROM_PACKET_HEADER:
                    int ts = this.timeStamp + block.timeStampOffset * block.timeStampMultiplier;
                    rdbs[blockIdx].setStartTimeInMillis(ts);
                    break;
                case BiomedBDictionaryBlock.FROM_PREVIOUS_SAMPLE:

                    break;
            }
            rdbs[blockIdx].setValues(values);
        }
        return rdbs;
    }
}
