/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package esponder.biomed.lib;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * This class contains the definition of the names and ids of the signals by the device protocol
 * @author afa
 */
public class BiomedUtils {

    public final static int ECG = 1;
    public final static int ACC_X = 6;
    public final static int ACC_Y = 7;
    public final static int ACC_Z = 8;
    public final static int RESP = 13;
    public final static int SKIN_BODY_TEMP = 105;
    public final static int ALG_ECG_Q = 129;
    public final static int ALG_ECG_HR = 132;
    public final static int ALG_ECG_RR = 133;
    public final static int ALG_ACTIVITY_ENERGY = 138;
    public final static int ALG_ACTIVITY_CLASS = 139;
    public final static int ALG_RESPIRATION_BR = 141;
    public final static int ALG_RESPIRATION_BA = 142;
    public final static int ALG_ECG_HRV = 143;
    public final static int ALG_RESPIRATION_Q = 144;
    public final static int ALG_RESPIRATION_AMPLITUDE = 145;
    public final static int ALG_ACTIVITY_AMPLITUDE = 148;
    public final static int ALG_ACTIVITY_PACEPERIOD = 149;
    public final static int ALG_ACTIVITY_PACE = 150;
    public final static int ALG_ENERGY_EXPENDITURE_FINE = 151;
    public final static int ALG_ACTIVITY_CLASS2 = 160;
    public final static int ALG_ACTIVITY_POSTURE = 161;
    //signals names
    public final static String ECG_TXT = "ecg";
    public final static String ACC_X_TXT = "acc_x";
    public final static String ACC_Y_TXT = "acc_y";
    public final static String ACC_Z_TXT = "acc_z";
    public final static String RESP_TXT = "resp";
    public final static String SBT_TXT = "skinBodyTemp";
    public final static String ECG_QI_TXT = "ecg_qi";
    public final static String HR_TXT = "hr";
    public final static String RR_TXT = "rr";
    public final static String EE_TXT = "ee";
    public final static String CLASS_TXT = "class";
    public final static String BR_TXT = "br";
    public final static String BA_TXT = "ba";
    public final static String HRV_TXT = "hrv";
    public final static String RESP_QI_TXT = "resp_qi";
    public final static String RA_TXT = "ra";
    public final static String AA_TXT = "aa";
    public final static String AP_TXT = "ap";
    public final static String PACE_TXT = "pace";
    public final static String EE_F_TXT = "eef";
    public final static String CLASS2_TXT = "class2";
    public final static String POSTURE_TXT = "posture";
    public final static int OTHER = 0;
    public final static int LYING = 1;
    public final static int SITTING_STANDING = 2;
    public final static int WALKING = 3;
    public final static int RUNNING = 4;

    private final static Map<Integer, String> sigMap;

    static {
        Map<Integer, String> map = new HashMap<Integer, String>();

        map.put(ECG, ECG_TXT);
        map.put(ACC_X, ACC_X_TXT);
        map.put(ACC_Y, ACC_Y_TXT);
        map.put(ACC_Z, ACC_Z_TXT);
        map.put(RESP, RESP_TXT);
        map.put(ALG_ECG_Q, ECG_QI_TXT);
        map.put(ALG_ECG_HR, HR_TXT);
        map.put(ALG_ECG_RR, RR_TXT);
        map.put(ALG_ACTIVITY_ENERGY, EE_TXT);
        map.put(ALG_ACTIVITY_CLASS, CLASS_TXT);
        map.put(ALG_RESPIRATION_BR, BR_TXT);
        map.put(ALG_RESPIRATION_BA, BA_TXT);
        map.put(ALG_ECG_HRV, HRV_TXT);
        map.put(ALG_RESPIRATION_Q, RESP_QI_TXT);
        map.put(ALG_RESPIRATION_AMPLITUDE, RA_TXT);
        map.put(ALG_ACTIVITY_AMPLITUDE, AA_TXT);
        map.put(ALG_ACTIVITY_AMPLITUDE, AP_TXT);
        map.put(ALG_ACTIVITY_PACE, PACE_TXT);
        map.put(ALG_ENERGY_EXPENDITURE_FINE, EE_F_TXT);
        map.put(ALG_ACTIVITY_CLASS2, CLASS2_TXT);
        map.put(ALG_ACTIVITY_POSTURE, POSTURE_TXT);
        sigMap = Collections.unmodifiableMap(map);
    }

    /**
     * Gets the name of the signal defined by its unique id
     * @param id the unique id of the signal
     * @return the signal name
     */
    public static String getName(int id) {
        String name = sigMap.get(id);
        return name;
    }

    /**
     * Gets the id of the signal defined by a given name
     * @param name the name of the signal
     * @return id of the signal
     */
    public static int getID(String name) {
        int id = -1;
        Iterator it = sigMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            if (entry.getValue().equals(name)) {
                id = (Integer) entry.getKey();
                break;
            }
        }
        return id;
    }
}
