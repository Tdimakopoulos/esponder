

package exodus.com.version;

import exodus.com.objects.RestInterf;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;

/**
 * REST Web Service

 */

@Stateless
@Path("/Version")
public class VersionResource {

    @EJB
    private NameStorageBean nameStorage;
    /**
     * Retrieves representation of an instance of helloworld.HelloWorldResource
     * @return an instance of java.lang.String
     */
    @GET
    @Path("/VersionNumber")
    @Produces("text/html")
    public String getXml() {
        return "<html><body><h1>"+nameStorage.getName()+"</h1></body></html>";
    }

    @GET
    @Path("/interfFindAll")
    @Produces("application/xml")
    public RestInterf getInterfXml() {
        //TODO return proper representation object
        //throw new UnsupportedOperationException();
        return null;
    }
}
