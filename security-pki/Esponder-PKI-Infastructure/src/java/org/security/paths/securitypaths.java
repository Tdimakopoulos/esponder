/**
 * Part of The Security Library
 * 
 * Manage paths and names for all security elements
 * 
 * Functions
 * ---------------
 * 
 * todo
 * 
 * @author tdi
 */
package org.security.paths;

public class securitypaths {

    private String KeyStorePath = "c:\\nephron\\keystore\\";
    private String KeyStoreName = "nephron.keystore";
    private String KeyPath = "c:\\nephron\\key\\";
    private String PrivateKeyname = "private.key";
    private String PublicKeyname = "public.key";
    private String CerPath = "c:\\nephron\\cer\\";
    private String CerName = "test.cer";

    public String getKeyStorePath() {
        return KeyStorePath;
    }

    public void setKeyStorePath(String KeyStorePath) {
        this.KeyStorePath = KeyStorePath;
    }

    public String getKeyStoreName() {
        return KeyStoreName;
    }

    public void setKeyStoreName(String KeyStoreName) {
        this.KeyStoreName = KeyStoreName;
    }

    public String getKeyPath() {
        return KeyPath;
    }

    public void setKeyPath(String KeyPath) {
        this.KeyPath = KeyPath;
    }

    public String getPrivateKeyname() {
        return PrivateKeyname;
    }

    public void setPrivateKeyname(String PrivateKeyname) {
        this.PrivateKeyname = PrivateKeyname;
    }

    public String getPublicKeyname() {
        return PublicKeyname;
    }

    public void setPublicKeyname(String PublicKeyname) {
        this.PublicKeyname = PublicKeyname;
    }

    public String getCerPath() {
        return CerPath;
    }

    public void setCerPath(String CerPath) {
        this.CerPath = CerPath;
    }

    public String getCerName() {
        return CerName;
    }

    public void setCerName(String CerName) {
        this.CerName = CerName;
    }
}
