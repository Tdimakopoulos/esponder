/*
 * 
 */
package eu.esponder.event.model.login;

import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.FirstResponderActorDTO;
import eu.esponder.event.model.crisis.resource.ActorEvent;

// TODO: Auto-generated Javadoc
/**
 * The Class LoginEsponderUserEvent.
 */
public class LoginEsponderUserEvent extends ActorEvent<ActorDTO>
		implements LoginEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5408128404989456038L;

	// Media variables
	private String sIPusername;
	private String sIPpassword;
	private String sIPaddressMEOC;
	private String sIPaddressEOC;
	private String sIPaddressFRC;
	private String sIPaddress;
	private String sIPaddresssrv;

	// webcam ip
	private String webcamip;

	// rasberry ip
	private String rasberryip;

	private int iRole;
	
String IMEI;
	
public String getIMEI() {
	return IMEI;
}

public void setIMEI(String iMEI) {
	IMEI = iMEI;
}
	
	public int getiRole() {
		return iRole;
	}

	public void setiRole(int iRole) {
		this.iRole = iRole;
	}

	public String getsIPusername() {
		return sIPusername;
	}

	public void setsIPusername(String sIPusername) {
		this.sIPusername = sIPusername;
	}

	public String getsIPpassword() {
		return sIPpassword;
	}

	public void setsIPpassword(String sIPpassword) {
		this.sIPpassword = sIPpassword;
	}

	public String getsIPaddressMEOC() {
		return sIPaddressMEOC;
	}

	public void setsIPaddressMEOC(String sIPaddressMEOC) {
		this.sIPaddressMEOC = sIPaddressMEOC;
	}

	public String getsIPaddressEOC() {
		return sIPaddressEOC;
	}

	public void setsIPaddressEOC(String sIPaddressEOC) {
		this.sIPaddressEOC = sIPaddressEOC;
	}

	public String getsIPaddressFRC() {
		return sIPaddressFRC;
	}

	public void setsIPaddressFRC(String sIPaddressFRC) {
		this.sIPaddressFRC = sIPaddressFRC;
	}

	public String getsIPaddress() {
		return sIPaddress;
	}

	public void setsIPaddress(String sIPaddress) {
		this.sIPaddress = sIPaddress;
	}

	public String getWebcamip() {
		return webcamip;
	}

	public void setWebcamip(String webcamip) {
		this.webcamip = webcamip;
	}

	public String getRasberryip() {
		return rasberryip;
	}

	public void setRasberryip(String rasberryip) {
		this.rasberryip = rasberryip;
	}

	public String getsIPaddresssrv() {
		return sIPaddresssrv;
	}

	public void setsIPaddresssrv(String sIPaddresssrv) {
		this.sIPaddresssrv = sIPaddresssrv;
	}

	/**
	 * Instantiates a new login esponder user event.
	 */
	public LoginEsponderUserEvent() {
		setJournalMessageInfo("LoginEsponderUserEvent");
	}

}
