package eu.esponder.rest.optimizer;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.resource.WeatherStationDTO;
import eu.esponder.rest.ESponderResource;
import eu.esponder.weatherstation.network.comm.SNMP4JHelper;
import eu.esponder.weatherstation.network.comm.readValues;

@Path("/optimizer/weatherstation")
public class WeatherStation extends ESponderResource {

	@GET
	@Path("/getlatestweather")
	@Produces({MediaType.APPLICATION_JSON})
	public WeatherStationDTO getWeatherConditions(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		SNMP4JHelper snmp=new SNMP4JHelper();
		readValues pp = new readValues();
		pp.read();
		snmp.readvalues();
		WeatherStationDTO pWeather=new WeatherStationDTO();
		pWeather.setRelativehumidity(String.valueOf((Integer.valueOf(snmp.getHum().trim())/10)));
		pWeather.setTemperature(String.valueOf((Integer.valueOf(snmp.getTemp().trim())/10)));
		pWeather.setAnemometer(String.valueOf(pp.getTemp()/10));
		pWeather.setWinddirection(pp.getsDir());
		return pWeather;
	}
	
	@GET
	@Path("/gethistoryweather")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO getWeatherHistoricalConditions(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return null;//this.getWeatherStatioRemoteService().findWeatherStationByTitleRemote("old", userID);
	}
}
