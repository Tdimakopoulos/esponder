package eu.esponder.df.ruleengine.controller.bean;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class LockManager {

	public static void main(String [ ] args) throws IOException
	{
		LockManager pm= new LockManager();
		pm.enableLock();
		System.out.println(pm.isLocked());
		pm.disableLock();
		System.out.println(pm.isLocked());
	}
	
	/** The sz properties file name unix. */
	private String szPropertiesFileNameUnix = "//home//exodus//osgi//";

	// comment for unix, uncomment for windows
	/** The sz properties file name windows. */
	private String szPropertiesFileNameWindows = "C://Development//";

	
	/**
	 * Checks if is unix.
	 *
	 * @return true, if is unix
	 */
	private String GetPathForFile() {
		if (File.separatorChar == '/')
			return szPropertiesFileNameUnix;
		else
			return szPropertiesFileNameWindows;
	}

	
	public void enableLock()
	{
		try  
		{
		    FileWriter fstream = new FileWriter(GetPathForFile()+"df.loc", false); //true tells to append data.
		    BufferedWriter out = new BufferedWriter(fstream);
		    out.write("1");
		    out.close();
		}
		catch (Exception e)
		{
		    System.err.println("Error: " + e.getMessage());
		}
	}
	public void disableLock()
	{
		try  
		{
		    FileWriter fstream = new FileWriter(GetPathForFile()+"df.loc", false); //true tells to append data.
		    BufferedWriter out = new BufferedWriter(fstream);
		    out.write("0");
		    out.close();
		}
		catch (Exception e)
		{
		    System.err.println("Error: " + e.getMessage());
		}
	}
	
	public boolean isLocked() throws IOException
	{
		  BufferedReader br;
			br = new BufferedReader(new FileReader(GetPathForFile()+"df.loc"));
	
		    try {
		        StringBuilder sb = new StringBuilder();
		        String line = br.readLine();

		        while (line != null) {
		            sb.append(line);
		            
		            line = br.readLine();
		        }
		        String everything = sb.toString();
		        if(everything.equalsIgnoreCase("1"))
		        {
		        	br.close();
		        	return true;
		        }
		        	
		        if(everything.equalsIgnoreCase("0"))
		        {
		        	br.close();
		        	return false;
		        }
		        	
		    } finally {
		        br.close();
		    }
			return false;
	}
}
