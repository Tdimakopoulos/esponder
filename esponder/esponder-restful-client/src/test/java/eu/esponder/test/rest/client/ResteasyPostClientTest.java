package eu.esponder.test.rest.client;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.testng.annotations.Test;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.criteria.EsponderCriteriaCollectionDTO;
import eu.esponder.dto.model.criteria.EsponderCriterionDTO;
import eu.esponder.dto.model.criteria.EsponderCriterionExpressionEnumDTO;
import eu.esponder.dto.model.criteria.EsponderIntersectionCriteriaCollectionDTO;
import eu.esponder.dto.model.criteria.EsponderNegationCriteriaCollectionDTO;
import eu.esponder.dto.model.criteria.EsponderQueryRestrictionDTO;
import eu.esponder.dto.model.criteria.EsponderUnionCriteriaCollectionDTO;
import eu.esponder.rest.client.ResteasyClient;
import eu.esponder.util.jaxb.Parser;

public class ResteasyPostClientTest {

	String GENERIC_QUERY_SERVICE_URI = "http://localhost:8080/esponder-restful/crisis/generic/post";

	@Test
	public void genericXmlResteasyPostClientTest() throws RuntimeException, Exception {

		
		Parser criteriaParser = new Parser(new Class[] {
				EsponderCriterionDTO.class,
				EsponderIntersectionCriteriaCollectionDTO.class,
				EsponderUnionCriteriaCollectionDTO.class,
				EsponderNegationCriteriaCollectionDTO.class });

		ResteasyClient postClient = new ResteasyClient(GENERIC_QUERY_SERVICE_URI, "application/xml");

		System.out.println("\n\nClient created successfully...\n\n");

		EsponderQueryRestrictionDTO criteria = getCriteria();
		Map<String, String> params = this.getGenericServiceParameters();
		String criteriaStrXml = criteriaParser.marshall(criteria);
		
		printXML(criteriaStrXml);

		String resultXML = postClient.post(params, criteriaStrXml);

		Parser resultsUnmarshaller = new Parser(new Class[] { ResultListDTO.class });
		ResultListDTO resultsDTO = (ResultListDTO) resultsUnmarshaller.unmarshal(resultXML);
		System.out.println(resultsDTO.toString());

	}
	
	
	@Test
	public void genericJsonResteasyPostClientTest() throws RuntimeException, Exception {

		ResteasyClient postClient = new ResteasyClient(GENERIC_QUERY_SERVICE_URI, "application/json");

		EsponderQueryRestrictionDTO criteria = getCriteria();
		Map<String, String> params = this.getGenericServiceParameters();
		
		ObjectMapper mapper = new ObjectMapper();
		String criteriaStrJSON = mapper.writeValueAsString(criteria);
		
		printJSON(criteriaStrJSON);
		String resultJSON = postClient.post(params, criteriaStrJSON);
		
		printJSON(resultJSON);

	}

	private Map<String, String> getGenericServiceParameters() {
		Map<String, String> params = new HashMap<String, String>();
		params.put("userID", "1");
		params.put("pageNumber", "0");
		params.put("pageSize", "5");
		params.put("queriedEntityDTO", ActorDTO.class.getName());
		return params;
	}

//	

	private EsponderQueryRestrictionDTO getCriteria() {

		EsponderIntersectionCriteriaCollectionDTO criteriaDTO = new EsponderIntersectionCriteriaCollectionDTO(
				new HashSet<EsponderQueryRestrictionDTO>());
		EsponderCriterionDTO ecriterion = new EsponderCriterionDTO();
		EsponderCriterionDTO ecriterion2 = new EsponderCriterionDTO();

		ecriterion.setField("id");
		ecriterion.setValue((new Long(0)).toString());
		ecriterion.setExpression(EsponderCriterionExpressionEnumDTO.GREATER_THAN);
		((EsponderCriteriaCollectionDTO) criteriaDTO).add(ecriterion);

		ecriterion2.setField("id");
		ecriterion2.setValue((new Long(5)).toString());
		ecriterion2.setExpression(EsponderCriterionExpressionEnumDTO.LESS_THAN_OR_EQUAL);
		((EsponderCriteriaCollectionDTO) criteriaDTO).add(ecriterion2);

		EsponderUnionCriteriaCollectionDTO criteriaDTO2 = new EsponderUnionCriteriaCollectionDTO(
				new HashSet<EsponderQueryRestrictionDTO>());
		EsponderCriterionDTO ecriterion3 = new EsponderCriterionDTO();
		EsponderCriterionDTO ecriterion4 = new EsponderCriterionDTO();

		ecriterion3.setField("id");
		ecriterion3.setValue((new Long(3)).toString());
		ecriterion3.setExpression(EsponderCriterionExpressionEnumDTO.GREATER_THAN);

		ecriterion4.setField("id");
		ecriterion4.setValue((new Long(4)).toString());
		ecriterion4.setExpression(EsponderCriterionExpressionEnumDTO.GREATER_THAN);

		((EsponderCriteriaCollectionDTO) criteriaDTO2).add(ecriterion3);
		((EsponderCriteriaCollectionDTO) criteriaDTO2).add(ecriterion4);

		((EsponderCriteriaCollectionDTO) criteriaDTO).add(criteriaDTO2);

		return criteriaDTO;
	}
	
	
	private void printJSON(String jsonStr) {
		System.out.println("\n\n******* JSON START *******\n");
		System.out.println(jsonStr);
		System.out.println("\n******** JSON END ********\n\n");
	}
	
	private void printXML(String xmlStr) {
		System.out.println("\n\n******* XML START *******\n");
		System.out.println(xmlStr);
		System.out.println("\n******** XML END ********\n\n");
	}
	
//	@SuppressWarnings("unused")
//	private String createGetURI(String uri) {
//		HashMap<String, String> params = (HashMap<String, String>) buildParameters();
//		Iterator<?> it = params.entrySet().iterator();
//		StringBuilder builder = new StringBuilder(uri);
//		builder.append("?");
//		while (it.hasNext()) {
//			Map.Entry pairs = (Map.Entry) it.next();
//			builder.append(pairs.getKey() + "=" + pairs.getValue());
//			if (it.hasNext())
//				builder.append("&");
//		}
//		System.out.println("URI : " + builder.toString());
//		return builder.toString();
//	}
	
}