/*
 * 
 */
package eu.esponder.event.exception;

import eu.esponder.event.model.ESponderExceptionEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class GeneratedExceptionEvent.
 */
public class GeneratedExceptionEvent extends ESponderExceptionEvent{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5034902663430478055L;

	/**
	 * Instantiates a new generated exception event.
	 */
	public GeneratedExceptionEvent(){
		setJournalMessageInfo("GeneratedExceptionEvent");
	}
}
