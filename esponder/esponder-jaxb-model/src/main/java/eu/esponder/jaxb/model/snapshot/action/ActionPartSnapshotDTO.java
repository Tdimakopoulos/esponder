package eu.esponder.jaxb.model.snapshot.action;

import eu.esponder.jaxb.model.snapshot.SpatialSnapshotDTO;
import eu.esponder.jaxb.model.snapshot.status.ActionPartSnapshotStatusDTO;


public class ActionPartSnapshotDTO extends SpatialSnapshotDTO {
	
	private ActionPartSnapshotStatusDTO status;

	public ActionPartSnapshotStatusDTO getStatus() {
		return status;
	}

	public void setStatus(ActionPartSnapshotStatusDTO status) {
		this.status = status;
	}
	
		
}
