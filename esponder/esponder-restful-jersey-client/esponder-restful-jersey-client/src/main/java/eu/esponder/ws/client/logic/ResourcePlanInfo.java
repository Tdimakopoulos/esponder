/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.ws.client.logic;

/**
 *
 * @author Tom
 */
public class ResourcePlanInfo {
 
    private String ptype;
    private int ineeded;
    

    public ResourcePlanInfo(String ptype,int ineeded,int iselected)
    {
        this.ptype=ptype;
        this.ineeded=ineeded;
    
    }
    /**
     * @return the ptype
     */
    public String getPtype() {
        return ptype;
    }

    /**
     * @param ptype the ptype to set
     */
    public void setPtype(String ptype) {
        this.ptype = ptype;
    }

    /**
     * @return the ineeded
     */
    public int getIneeded() {
        return ineeded;
    }

    /**
     * @param ineeded the ineeded to set
     */
    public void setIneeded(int ineeded) {
        this.ineeded = ineeded;
    }

    
}
