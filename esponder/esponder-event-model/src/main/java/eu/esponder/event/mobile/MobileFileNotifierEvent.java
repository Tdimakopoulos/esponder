package eu.esponder.event.mobile;

import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.event.model.ESponderEvent;

public class MobileFileNotifierEvent extends ESponderEvent<ESponderUserDTO> {
	
	private static final long serialVersionUID = -5049321795600339854L;

	private String imei;
	private String fileURL;
	private String description;
	String fileid;
	String sourcemeoc;
	String sourceeoc;
	
	public String getFileid() {
		return fileid;
	}

	public void setFileid(String fileid) {
		this.fileid = fileid;
	}

	public String getSourcemeoc() {
		return sourcemeoc;
	}

	public void setSourcemeoc(String sourcemeoc) {
		this.sourcemeoc = sourcemeoc;
	}

	public String getSourceeoc() {
		return sourceeoc;
	}

	public void setSourceeoc(String sourceeoc) {
		this.sourceeoc = sourceeoc;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getFileURL() {
		return fileURL;
	}

	public void setFileURL(String fileURL) {
		this.fileURL = fileURL;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	

}
