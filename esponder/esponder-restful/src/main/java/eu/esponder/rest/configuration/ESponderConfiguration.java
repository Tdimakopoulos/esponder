/*
 * 
 */
package eu.esponder.rest.configuration;

import java.io.IOException;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import eu.esponder.dto.model.config.ESponderConfigParameterDTO;
import eu.esponder.dto.model.security.KeyStorageDTO;
import eu.esponder.rest.ESponderResource;

// TODO: Auto-generated Javadoc
/**
 * The Class ESponderConfiguration.
 */
@Path("/system/configuration")
public class ESponderConfiguration extends ESponderResource {



	/**
	 * Gets the configuration by id.
	 *
	 * @param configurationId the configuration id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the configuration by id
	 * @throws ClassNotFoundException the class not found exception
	 */
	@GET
	@Path("/getByID")
	@Produces({MediaType.APPLICATION_JSON})
	public ESponderConfigParameterDTO getConfigurationById (
			@QueryParam("configurationId") @NotNull(message="ConfigurationId may not be null") Long configurationId,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) throws ClassNotFoundException {

		return this.getConfigurationRemoteService().findESponderConfigByIdRemote(configurationId);
	}


	/**
	 * Gets the configuration by name.
	 *
	 * @param configurationName the configuration name
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the configuration by name
	 * @throws ClassNotFoundException the class not found exception
	 */
	@GET
	@Path("/getByName")
	@Produces({MediaType.APPLICATION_JSON})
	public ESponderConfigParameterDTO getConfigurationByName (
			@QueryParam("configurationName") @NotNull(message="ConfigurationId may not be null") String configurationName,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) throws ClassNotFoundException {

		return this.getConfigurationRemoteService().findESponderConfigByNameRemote(configurationName);
	}


	/**
	 * Creates the e sponder configuration.
	 *
	 * @param configurationDTO the configuration dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the e sponder config parameter dto
	 * @throws ClassNotFoundException the class not found exception
	 */
	@POST
	@Path("/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ESponderConfigParameterDTO createESponderConfiguration(
			@NotNull(message="entity to be created may not be null") ESponderConfigParameterDTO configurationDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) throws ClassNotFoundException {

		ESponderConfigParameterDTO configDTO = this.getConfigurationRemoteService().createESponderConfigRemote(configurationDTO, userID); 
		return configDTO;
	}


	/**
	 * Update configuration.
	 *
	 * @param configurationDTO the configuration dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the e sponder config parameter dto
	 */
	@PUT
	@Path("/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ESponderConfigParameterDTO updateConfiguration( 
			@NotNull(message="entity to be updated may not be null") ESponderConfigParameterDTO configurationDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return this.getConfigurationRemoteService().updateESponderConfigRemote(configurationDTO, userID);
	}


	/**
	 * Delete configuration.
	 *
	 * @param configurationId the configuration id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the long
	 */
	@DELETE
	@Path("/delete")
	public Long deleteConfiguration(
			@QueryParam("configurationId") @NotNull(message="ConfigurationId may not be null") Long configurationId,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		this.getConfigurationRemoteService().deleteESponderConfigRemote(configurationId, userID);
		return configurationId;
	}


}