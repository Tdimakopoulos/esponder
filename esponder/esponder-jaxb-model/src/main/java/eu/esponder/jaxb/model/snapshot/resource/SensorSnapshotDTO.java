package eu.esponder.jaxb.model.snapshot.resource;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;

import eu.esponder.jaxb.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.jaxb.model.snapshot.SnapshotDTO;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;
import eu.esponder.jaxb.model.snapshot.status.SensorSnapshotStatusDTO;

@XmlRootElement(name="sensorSnapshot")
@XmlType(name="SensorSnapshot")
@JsonPropertyOrder({"status", "value", "statisticType", "period", "sensor"})
public class SensorSnapshotDTO extends SnapshotDTO {

	private SensorSnapshotStatusDTO status;
	
	private MeasurementStatisticTypeEnumDTO statisticType;
	
	private String value;
	
	private SensorDTO sensor;

	public SensorSnapshotStatusDTO getStatus() {
		return status;
	}

	public SensorDTO getSensor() {
		return sensor;
	}

	public void setSensor(SensorDTO sensor) {
		this.sensor = sensor;
	}

	public void setStatus(SensorSnapshotStatusDTO status) {
		this.status = status;
	}

	public MeasurementStatisticTypeEnumDTO getStatisticType() {
		return statisticType;
	}

	public void setStatisticType(MeasurementStatisticTypeEnumDTO statisticType) {
		this.statisticType = statisticType;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "SensorSnapshotDTO [status=" + status 
				+ ", statisticType=" + statisticType + ", value=" + value 
				+ ", period=" + period + ", id=" + id + "]";
	}
	
}
