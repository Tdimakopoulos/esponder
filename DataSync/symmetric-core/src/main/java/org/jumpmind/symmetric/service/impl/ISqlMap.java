package org.jumpmind.symmetric.service.impl;

public interface ISqlMap {

    public String getSql(String... keys);
    
}
