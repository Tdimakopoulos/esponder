package esponder.biomed.lib;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * This class has a job of a proxy between the host and the device
 * Every request will be cached and linked to its own response before notifying the host
 * This class makes use of protected queues to store and restore payloads.
 * Once the thread starts, it will do all the job by checking the sending queue, sending command and getting reponses
 * Once a command response is received, the object blocked will be notiffied to process.
 * As for the data, it will be the job of the consumer to fetch the data packet in the queue
 * @author afa
 */
class DeviceProxy extends Thread {

    private final String TAG = "DeviceProxy";
//    BluetoothCommunicationAgent bluetoothAgent = null;
//    BluetoothDevice bluetoothDevice = null;
    private BlockingQueue<BiomedACommand> commandQueue = null;
    private BlockingQueue<RegularDataBlock> dataQueue = null;
    private BlockingQueue<BiomedACommand> sentCommandDeque = null;
    private boolean kill = false;
    private boolean terminated = false;
    private byte sequence = 0;
    private int frameNumber = 0;
    private BiomedAFrameParser parser = null;
    private byte savedStream[] = new byte[0];
    private int streamIdx = 0;

    /**
     * Constructor
     * @param address the address of the bluetooth device 
     */
    public DeviceProxy(String address) {
//        BiomedABluetoothDevice device = (BiomedABluetoothDevice) (DeviceFinder.getDevice(address));
//        this.bluetoothDevice = device.getRemoteDevice();
//        this.bluetoothAgent = new BluetoothCommunicationAgent(this.bluetoothDevice);
//        this.terminated = false;
//        this.commandQueue = new LinkedBlockingQueue<BiomedACommand>();
//        this.dataQueue = new LinkedBlockingQueue<RegularDataBlock>();
//        this.sentCommandDeque = new LinkedBlockingQueue<BiomedACommand>();
    }

    /**
     * Connect to the bleutooth device and subscribe to the serial service
     * @throws CommunicationException if device cannot be reached or serial service doesn't exisit
     */
//    public void connect() throws CommunicationException {
//        try {
//            this.bluetoothAgent.connect();
//        } catch (IOException ex) {
//            CommunicationException commex = new CommunicationException();
//            commex.setMessage("Unable to open bluetooth socket");
//            throw commex;
//        }
//    }

    /**
     * Disconnect from the device
     * @throws IOException if the input and output streams cannot be closed
     */
    public void disconnect() throws IOException {
//        this.bluetoothAgent.cancel();
    }

    /**
     * Add a frame to the outgoing queue, it will be sent when it is possible to do so
     * @param frame the frame to be sent
     * @throws InterruptedException when the protecting monitor is interrupted
     */
    public void addInCommandQueue(BiomedACommand cmd) throws InterruptedException {
        this.commandQueue.put(cmd);
    }

    /**
     * add the array of data payloads
     * @param data array of data payloads
     * @throws InterruptedException when the protecting monitor is interrupted
     */
    public void addInDataQueue(RegularDataBlock rdbs[]) throws InterruptedException {
        if (rdbs != null) {
            for (int i = 0; i < rdbs.length; i++) {
                this.dataQueue.put(rdbs[i]);
            }
        }
    }

    /**
     * Retreive all the payloads in the queue
     * @param dummy don't care
     * @return the array containing all data blocks received
     */
    public RegularDataBlock[] retreiveFromDataQueue(int dummy) {
        RegularDataBlock rdbs[] = null;

        if (this.dataQueue != null) {
            rdbs = new RegularDataBlock[this.dataQueue.size()];
            for (int i = 0; i < rdbs.length; i++) {
                rdbs[i] = this.dataQueue.poll();
            }
        }
        return rdbs;
    }

    /**
     * Flush the data queue. This should be done before every streaming restarting
     */
    public void flushDataQueue() {
        int i = 0;
        for (; i < this.dataQueue.size();) {
            RegularDataBlock rdb = this.dataQueue.poll();
            if (rdb != null) {
                i++;
            }
        }
    }

    /**
     * Check in the queue if there are some frames to be sent
     * Serialize the the frame as byte savedStream
     * Send the byte savedStream
     */
    private boolean sendNextCommand() {
        boolean sent = false;
        try {
            //check in the queue if there are some command to be sent
            if (this.commandQueue.size() <= 0) {
                return false;
            }
            BiomedACommand cmd = this.commandQueue.poll();
            if (cmd != null) {
                //inset the sequence number
                cmd.insertSequenceNumber(sequence++);
                //serialize the the frame as byte savedStream
                
                //tomd reomove
                //byte outgoing[] = FrameMaker.getFrame(cmd, this.frameNumber++);
                
                //tomd reomove
                //send the byte savedStream
                //this.bluetoothAgent.send(outgoing);
                
                
                //mark this command as sent
                this.sentCommandDeque.add(cmd);
                sent = true;
            }
        } catch (Exception ex) {
            //       MessageLogger.logger.severe(ex.getMessage());
        }
        return sent;
    }

    /**
     * Receive the byte savedStream till reaching the sync byte 0.
     * Decode the frame and parse it and the commands responses in it
     * The data payload will be added related to the streaming will be added in their own queue
     * @return the commands responses contained in the incomming frame
     */
    private BiomedACommand[] receivePayloads() {
        byte buffer[] = new byte[4096];
        int length = 0;
        byte b = -1;
        BiomedACommand cmds[] = null;
        RegularDataBlock dataBlocks[] = null;
        boolean receiving = false;
        int waitTime = 0;


        try {
            //check there are some received bytes
            do {
                if (this.streamIdx >= this.savedStream.length) {//did we consume all the packet?
                  //tomd reomove
                    //this.savedStream = this.bluetoothAgent.receive();
                    
                    
                    if (this.savedStream.length > 0) {
                        receiving = true;
                        waitTime = 0;
                        this.streamIdx = 0;
                    } else {
                        //once a byte is received the wait time will no more be incremented
                        receiving = false;
                        waitTime += 500;
                        Thread.sleep(500);
                    }
                } else {//consume the next byte
                    b = this.savedStream[this.streamIdx++];
                    buffer[length++] = b;
                }

            } while ((b != 0) && (waitTime <= 3000));
            //parse only if the frame received 
            if (receiving == true) {
                //reconstitute frame and get frame as results
                byte incoming[] = Cobs.decode(buffer, length);
                this.parser.setFrame(incoming);
                this.parser.process();
                cmds = this.parser.getCmds();
                dataBlocks = this.parser.getDataBlocks();
                //add data blocks in the queue,  it will be used by the streaming agent
                if (dataBlocks != null) {
                    if (dataBlocks.length > 0) {
                        this.addInDataQueue(dataBlocks);
                    }
                }
            } else {
  //              Log.w(TAG, "nothing in the queue");
            }
        } catch (Exception ex) {
    //        Log.e(TAG, "problem in addInDataQueue", ex);
        }
        return cmds;
    }

    /**
     * Check in the queue if there are some frames to be sent
     * Serialize the the frame as byte savedStream
     * Send the byte savedStream
     * Check there are some received bytes
     * Reconstitute frame and put in the queue
     * Save the remaining byte for the next run
     */
    @Override
    public void run() {
        BiomedACommand sentCmd = null;
        BiomedACommand cmds[] = null;

        while (kill == false) {
            boolean sent = this.sendNextCommand();
            //receive one data packet and/or poll till receiving response to command
            do {
                //response for commands are more important than data representation
                cmds = this.receivePayloads();
                //may be need some time out
            } while ((cmds == null) && (sent == true) && (kill == false));//did we receive the response of the command
            if (cmds != null) {
                //get the sent command without removing it
                for (int rcmdIdx = 0; rcmdIdx < cmds.length; rcmdIdx++) {
                    for (int scmdIdx = 0; scmdIdx < this.sentCommandDeque.size(); scmdIdx++) {
                        //get the sent command to be processed
                        sentCmd = this.sentCommandDeque.remove();
                        //check if this is the response of the current thread
                        if (cmds[rcmdIdx].isResponseOf(sentCmd)) {
                            //copy the response in the command
                            sentCmd.setSize(cmds[rcmdIdx].getSize());
                            sentCmd.setBuffer(cmds[rcmdIdx].getBuffer(), 0, cmds[rcmdIdx].getBuffer().length);
                            sentCmd.beginParameterSearch();
                            //notify only the concerned thread
                            synchronized (sentCmd) {
                                sentCmd.notifyAll();
                            }
                        } else {
                            //put it back in the same place
                            this.sentCommandDeque.add(sentCmd);
                        }
                    }
                }
            }//end if(cmds != null)
        }
        terminated = true;
//        Log.i(TAG, "device proxy thread is terminated");
    }

    public void kill() {
        this.kill = true;
    }

    /**
     * @return the terminated
     */
    public boolean isTerminated() {
        return terminated;
    }

    /**
     * @param parser the parser to set
     */
    public void setFrameParser(BiomedAFrameParser parser) {
        this.parser = parser;
    }
}
