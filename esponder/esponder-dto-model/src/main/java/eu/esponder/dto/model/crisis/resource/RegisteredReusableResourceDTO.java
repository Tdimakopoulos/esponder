/*
 * 
 */
package eu.esponder.dto.model.crisis.resource;

import java.math.BigDecimal;
import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.type.VehicleEquipmentTypeDTO;
import eu.esponder.dto.model.type.VehiclePurposeTypeDTO;
import eu.esponder.dto.model.type.VehicleTypeDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class RegisteredReusableResourceDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "type", "title",  "quantity", "registeredConsumables", "children","parent",  "reusableResourceCategoryId"
	,"vehicleEquipments"
	,"vehiclePurposes"
	,"vehicleType"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class RegisteredReusableResourceDTO extends ResourceDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1249171957154114291L;


	/** The quantity. */
	private BigDecimal quantity;
	

	/** The registered consumables. */
	private Set<RegisteredConsumableResourceDTO> registeredConsumables;
	
	/** The parent. */
	private RegisteredReusableResourceDTO parent;
	

	/** The children. */
	private Set<RegisteredReusableResourceDTO> children;
	

	/** The reusable resource category id. */
	private Long reusableResourceCategoryId;

	
	private Set<VehicleEquipmentTypeDTO> vehicleEquipments;
	
	
	private Set<VehiclePurposeTypeDTO> vehiclePurposes;
	
	
	private VehicleTypeDTO vehicleType;
	
	
	
	/**
	 * Gets the quantity.
	 *
	 * @return the quantity
	 */
	public BigDecimal getQuantity() {
		return quantity;
	}

	/**
	 * Sets the quantity.
	 *
	 * @param quantity the new quantity
	 */
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	/**
	 * Gets the registered consumables.
	 *
	 * @return the registered consumables
	 */
	public Set<RegisteredConsumableResourceDTO> getRegisteredConsumables() {
		return registeredConsumables;
	}

	/**
	 * Sets the registered consumables.
	 *
	 * @param registeredConsumables the new registered consumables
	 */
	public void setRegisteredConsumables(
			Set<RegisteredConsumableResourceDTO> registeredConsumables) {
		this.registeredConsumables = registeredConsumables;
	}

	/**
	 * Gets the parent.
	 *
	 * @return the parent
	 */
	public RegisteredReusableResourceDTO getParent() {
		return parent;
	}

	/**
	 * Sets the parent.
	 *
	 * @param parent the new parent
	 */
	public void setParent(RegisteredReusableResourceDTO parent) {
		this.parent = parent;
	}

	/**
	 * Gets the children.
	 *
	 * @return the children
	 */
	public Set<RegisteredReusableResourceDTO> getChildren() {
		return children;
	}

	/**
	 * Sets the children.
	 *
	 * @param children the new children
	 */
	public void setChildren(Set<RegisteredReusableResourceDTO> children) {
		this.children = children;
	}

	/**
	 * Gets the reusable resource category id.
	 *
	 * @return the reusable resource category id
	 */
	public Long getReusableResourceCategoryId() {
		return reusableResourceCategoryId;
	}

	/**
	 * Sets the reusable resource category id.
	 *
	 * @param reusableResourceCategoryId the new reusable resource category id
	 */
	public void setReusableResourceCategoryId(Long reusableResourceCategoryId) {
		this.reusableResourceCategoryId = reusableResourceCategoryId;
	}

	public Set<VehicleEquipmentTypeDTO> getVehicleEquipments() {
		return vehicleEquipments;
	}

	public void setVehicleEquipments(Set<VehicleEquipmentTypeDTO> vehicleEquipments) {
		this.vehicleEquipments = vehicleEquipments;
	}

	public Set<VehiclePurposeTypeDTO> getVehiclePurposes() {
		return vehiclePurposes;
	}

	public void setVehiclePurposes(Set<VehiclePurposeTypeDTO> vehiclePurposes) {
		this.vehiclePurposes = vehiclePurposes;
	}

	public VehicleTypeDTO getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(VehicleTypeDTO vehicleType) {
		this.vehicleType = vehicleType;
	}

	
}
