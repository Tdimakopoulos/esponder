/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.cache.measurments;

import eu.cache.database.CDWAKDMeasurments;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MEOC-WS1
 */
public class mesdb {

    ArrayList<CDWAKDMeasurments> pdb = new ArrayList<CDWAKDMeasurments>();
    int ipos;
    String imei;

    public void SetIMEI(String szimei) {
        imei = szimei;
    }

    public ArrayList<CDWAKDMeasurments> getList() {
        return pdb;
    }

    public void LoadFromFile() throws FileNotFoundException, IOException, ClassNotFoundException {

        String szPath;
        if (File.separatorChar == '/') {
            szPath = "/home/exodus/osgi/";
        } else {
            szPath = "e:\\dd\\";
        }


        pdb.clear();
        File f = new File(szPath + imei + "a");
        boolean bfind = true;
        int iloop = 0;
        while (bfind) {
            if (f.exists()) {
                FileInputStream fis = new FileInputStream(szPath + imei);
                ObjectInputStream ois = new ObjectInputStream(fis);
                pdb = (ArrayList<CDWAKDMeasurments>) (List<CDWAKDMeasurments>) ois.readObject();
                ois.close();
                ipos = pdb.size();
                bfind = false;
                if (pdb.size() == 0) {
                    bfind = true;
                    iloop++;
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(mesdb.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            } else {
                System.out.println("No File Exists");
                ipos = 0;
                iloop++;
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    Logger.getLogger(mesdb.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            if (iloop == 3) {
                System.out.println("No Results");
                bfind = false;
                ipos = 0;
                iloop++;
            }
        }

    }

    public CDWAKDMeasurments getItem(int indexpos) {
        return pdb.get(indexpos);
    }

    public int getpos() {
        return ipos;
    }
}
