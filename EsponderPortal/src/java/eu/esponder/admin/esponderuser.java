/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.admin;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.objects.*;
import eu.esponder.ws.client.create.CreateOperations;
import eu.esponder.ws.client.query.QueryManager;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import eu.esponder.ws.client.delete.DeleteOperations;
import eu.esponder.ws.client.update.UpdateOperations;
import javax.faces.bean.SessionScoped;
import org.codehaus.jackson.JsonGenerationException;

/**
 *
 * @author Tom
 */
@ManagedBean
@SessionScoped
public class esponderuser {

    private List<esponderuseracc> esusers;
    private String useridforedit;
    private String username;
    private String password;
    private String role;
    
    /**
     * Creates a new instance of esponderuser
     */
    public esponderuser() {
         esusers = new ArrayList<esponderuseracc>();
        QueryManager oquery = new QueryManager();
        try {
            ResultListDTO plist=oquery.getAllUsers("1");
            for (int i=0;i<plist.getResultList().size();i++)
            {
                ESponderUserDTO plistitem=(ESponderUserDTO) plist.getResultList().get(i);
                esponderuseracc pitem = new esponderuseracc();
//                pitem.setId(plistitem.getId());
//                pitem.setUsername(plistitem.getUserName());
//                pitem.setRole(plistitem.getRole());
//                if(i==0)
//                {
//                pitem.setId(plistitem.getId());
//                pitem.setUsername("Panos Zografopoulos");
//                pitem.setRole("Fire Brigade Athens");
//                }
//                if(i==1)
//                {
//                pitem.setId(plistitem.getId());
//                pitem.setUsername("Kostas Vagelidis");
//                pitem.setRole("Central Crisis Athens");
//                }
//                if(i==2)
//                {
//                pitem.setId(plistitem.getId());
//                pitem.setUsername("Andreas Kostopoulos");
//                pitem.setRole("Fire Brigade Athens II");
//                }
//                if(i==3)
//                {
//                pitem.setId(plistitem.getId());
//                pitem.setUsername("Antonis Ramfos");
//                pitem.setRole("Crisis Management Athens");
//                }
//                if(i==4)
//                {
                pitem.setId(plistitem.getId());
                //Edit for new model
        //puser.setRole(role);
        //puser.setUserName(username);
//                }
                esusers.add(pitem);
                
            }
        } catch (JsonParseException ex) {
            Logger.getLogger(esponderuser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JsonMappingException ex) {
            Logger.getLogger(esponderuser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(esponderuser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void queryuser()
    {
         esusers = new ArrayList<esponderuseracc>();
        QueryManager oquery = new QueryManager();
        try {
            ResultListDTO plist=oquery.getAllUsers("1");
            for (int i=0;i<plist.getResultList().size();i++)
            {
                ESponderUserDTO plistitem=(ESponderUserDTO) plist.getResultList().get(i);
                esponderuseracc pitem = new esponderuseracc();
//                if(i==0)
//                {
//                pitem.setId(plistitem.getId());
//                pitem.setUsername("Panos Zografopoulos");
//                pitem.setRole("Fire Brigade Athens");
//                }
//                if(i==1)
//                {
//                pitem.setId(plistitem.getId());
//                pitem.setUsername("Kostas Vagelidis");
//                pitem.setRole("Central Crisis Athens");
//                }
//                if(i==2)
//                {
//                pitem.setId(plistitem.getId());
//                pitem.setUsername("Andreas Kostopoulos");
//                pitem.setRole("Fire Brigade Athens II");
//                }
//                if(i==3)
//                {
//                pitem.setId(plistitem.getId());
//                pitem.setUsername("Antonis Ramfos");
//                pitem.setRole("Crisis Management Athens");
//                }
//                if(i==4)
//                {
                pitem.setId(plistitem.getId());
        //Edit for new model
        //puser.setRole(role);
        //puser.setUserName(username);
//                }
                esusers.add(pitem);
                
            }
        } catch (JsonParseException ex) {
            Logger.getLogger(esponderuser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JsonMappingException ex) {
            Logger.getLogger(esponderuser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(esponderuser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * @return the esusers
     */
    public List<esponderuseracc> getEsusers() {
        return esusers;
    }

    /**
     * @param esusers the esusers to set
     */
    public void setEsusers(List<esponderuseracc> esusers) {
        this.esusers = esusers;
    }

    /**
     * @return the useridforedit
     */
    public String getUseridforedit() {
        return useridforedit;
    }

    /**
     * @param useridforedit the useridforedit to set
     */
    public void setUseridforedit(String useridforedit) {
        this.useridforedit = useridforedit;
    }
    
    public String removeuser() {
        long ifind = Long.parseLong(useridforedit);
        System.out.println("user id remove : "+ifind);     
        DeleteOperations pDelete = new DeleteOperations();
        try {
            pDelete.deleteEsponderUser("1", useridforedit);
        } catch (JsonParseException ex) {
            System.out.println("Error on User Delete "+ex.getMessage());
        } catch (JsonMappingException ex) {
            System.out.println("Error on User Delete "+ex.getMessage());
        } catch (IOException ex) {
            System.out.println("Error on User Delete "+ex.getMessage());
        }
          queryuser();      
        return "User?faces-redirect=true";
    }
    
    public String edituser() {
        long ifind = Long.parseLong(useridforedit);
     
        System.out.println("user id edit : "+ifind);     
        
        for (int i=0;i<esusers.size();i++)
        {
            if(esusers.get(i).getId()==ifind)
            {
                System.out.println("user found : "+esusers.get(i).getUsername());  
                
                        username=esusers.get(i).getUsername();
                        role=esusers.get(i).getRole();
            }
        }
        return "UserEdit?faces-redirect=true";
    }
    public String adduser() {
        
      username="";
                        role="";
                        password="";
        return "UserAdd?faces-redirect=true";
    }
    
    public String saveadduser() {
        ESponderUserDTO puser = new ESponderUserDTO();
     
        //Edit for new model
        //puser.setRole(role);
        //puser.setUserName(username);
        CreateOperations pco = new CreateOperations();
        try {
            pco.createEsponderUser("1", puser);
        } catch (JsonGenerationException ex) {
            System.out.println("Error on add user : "+ex.getMessage());
        } catch (JsonMappingException ex) {
            System.out.println("Error on add user : "+ex.getMessage());
        } catch (IOException ex) {
            System.out.println("Error on add user : "+ex.getMessage());
        }
        queryuser();
        return "User?faces-redirect=true";
    }

    public String saveedituser() {
        ESponderUserDTO puser = new ESponderUserDTO();
     
        //Edit for new model
        //puser.setRole(role);
        //puser.setUserName(username);
        UpdateOperations pco = new UpdateOperations();
        try {
            //pco.createEsponderUser("1", puser);
            pco.updateEsponderUser("1", puser);
        } catch (JsonGenerationException ex) {
            System.out.println("Error on add user : "+ex.getMessage());
        } catch (JsonMappingException ex) {
            System.out.println("Error on add user : "+ex.getMessage());
        } catch (IOException ex) {
            System.out.println("Error on add user : "+ex.getMessage());
        }
        queryuser();
        return "User?faces-redirect=true";
    }
    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the role
     */
    public String getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(String role) {
        this.role = role;
    }
}
