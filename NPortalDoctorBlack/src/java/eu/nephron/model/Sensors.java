/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.model;

import eu.nephron.mestypes.mestypes;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author tdim
 */
public class Sensors implements Serializable {

    private String type;
    private double value;
    private Date time;
    private String typename;
    private String units="mmol/L";
    

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the value
     */
    public double getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(double value) {
        this.value = value;
    }

    /**
     * @return the time
     */
    public Date getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(Date time) {
        this.time = time;
    }

    /**
     * @return the typename
     */
    public String getTypename() {

        mestypes ptypes= new mestypes();
        String sname= ptypes.GetName(Long.parseLong(type));
        
        sname=sname.replace("Condactivity Dialysate","Conductivity Dialysate");

sname=sname.replace("Temp IN","Temperature Bloodline In");
sname=sname.replace("Temp OUT","Temperature Bloodline Out");
sname=sname.replace("Inlet PH","Inlet pH");
sname=sname.replace("Inlet Potasium","Inlet Potassium");

sname=sname.replace("Inlet Temp","Temperature Dialysate In");
sname=sname.replace("Inlet Uria","Oxidation State");
sname=sname.replace("Outlet PH","Outlet pH");
sname=sname.replace("Outlet Potasium","Outlet Potassium" );

sname=sname.replace("Outlet Temp","Temperature Dialysate Out");

sname=sname.replace("Diastolic","Blood pressure Diastolic");
sname=sname.replace("Systolic","Blood pressure Systolic");
        return sname;

    }

    /**
     * @param typename the typename to set
     */
    public void setTypename(String typename) {
        this.typename = typename;
    }

    /**
     * @return the units
     */
    public String getUnits() {
        long ptype=Long.parseLong(type);
        if(ptype==11)
            return "mS/cm";
        if(ptype==13)
            return "mm/Hg";
        if(ptype==15)
            return "mm/Hg";
        if(ptype==30)
            return "mm/Hg";
        if(ptype==32)
            return "mm/Hg";
        
        if(ptype==12)
            return "°C";
        if(ptype==17)
            return "°C";
        if(ptype==18)
            return "°C";
        if(ptype==4)
            return "°C";
        if(ptype==9)
            return "°C";
        if(ptype==31)
            return "bpm";
        if(ptype==1)
            return "";
        if(ptype==6)
            return "";
        if(ptype==5)
            return "";
        if(ptype==10)
            return "";
        
        return units;
    }

    /**
     * @param units the units to set
     */
    public void setUnits(String units) {
        this.units = units;
    }
}
