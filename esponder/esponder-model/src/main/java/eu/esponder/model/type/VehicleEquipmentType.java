/*
 * 
 */
package eu.esponder.model.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import eu.esponder.model.crisis.resource.RegisteredReusableResource;
import eu.esponder.model.crisis.resource.ReusableResource;



// TODO: Auto-generated Javadoc
/**
 * The Class ReusableResourceType.
 */
@Entity
@DiscriminatorValue("VehicleEquipment")
public class VehicleEquipmentType extends LogisticsResourceType {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3416073165515169747L;
	
	/** The container. */
	@ManyToOne
	@JoinColumn(name="REUSABLE_RESOURCE_ID")
	private ReusableResource vehiclequipments;


	public ReusableResource getVehiclequipments() {
		return vehiclequipments;
	}

	public void setVehiclequipments(ReusableResource vehiclequipments) {
		this.vehiclequipments = vehiclequipments;
	}
	
	
}
