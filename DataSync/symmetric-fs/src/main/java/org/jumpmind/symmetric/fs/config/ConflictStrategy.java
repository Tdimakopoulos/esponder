package org.jumpmind.symmetric.fs.config;

public enum ConflictStrategy {
    SERVER_WINS, CLIENT_WINS, REPORT_ERROR
}
