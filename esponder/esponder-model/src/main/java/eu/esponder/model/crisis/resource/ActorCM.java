/*
 * 
 */
package eu.esponder.model.crisis.resource;

import java.util.Set;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


// TODO: Auto-generated Javadoc
/**
 * The Class Actor Crisis Manager.
 */
@Entity
@DiscriminatorValue("CM")
@NamedQueries({
	@NamedQuery(name="ActorCM.findByTitle", query="select a from ActorCM a where a.title=:title"),
	@NamedQuery(name="ActorCM.findAll", query="select a from ActorCM a")
})
public class ActorCM extends Actor {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 70227517958595376L;

	/** The subordinates. If we are in meoc level this set describes the FR cheifs, If we are in EOC level this is the supervisors of Meoc*/
	@OneToMany(mappedBy="crisisManager")
	private Set<ActorIC> subordinates;

	/** The operations centre. */
	@OneToOne(mappedBy="crisisManager")
	private OCEoc operationsCentre;

	/**
	 * Gets the subordinates.
	 *
	 * @return the subordinates
	 */
	public Set<ActorIC> getSubordinates() {
		return subordinates;
	}

	/**
	 * Sets the subordinates.
	 *
	 * @param subordinates the new subordinates
	 */
	public void setSubordinates(Set<ActorIC> subordinates) {
		this.subordinates = subordinates;
	}

	/**
	 * Gets the operations centre.
	 *
	 * @return the operations centre
	 */
	public OCEoc getOperationsCentre() {
		return operationsCentre;
	}

	/**
	 * Sets the operations centre.
	 *
	 * @param operationsCentre the new operations centre
	 */
	public void setOperationsCentre(OCEoc operationsCentre) {
		this.operationsCentre = operationsCentre;
	}
	
}
