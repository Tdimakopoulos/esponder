/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package LPSLibExus;

import uilib.exus.*;
import exus.esponder.settings.settingsmanager;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;

/**
 *
 * @author tdim
 */
public class LPSLibEXUS {

    StreamConnection sc = null;
    UICommands pp = new UICommands();
    LPSReadThread rt = new LPSReadThread();

    public UICommands GetCommandModule()
    {
        return pp;
    }
    
    public void OpenConnection() throws IOException {
        settingsmanager pps = new settingsmanager();
        pps.ReadValues();
        System.out.println("Open Bluetooth Connection with the LPS... Please wait ....");
        sc = (StreamConnection) Connector.open(pps.getLPSB());//"btspp://000BCE0AE779:1;authenticate=false;encrypt=false;master=false");
        pp.OpenStream(sc);//pps.getbUI());//(
        System.out.println("Connection and streaming is open for LPS ....");
    }

    public void CloseConnection() throws IOException {
        System.out.println("Closing connection with LPS");
        pp.CloseStream();
        sc.close();
        System.out.println("Connection with LPS closed");
    }

    public void StartReadLPSThread() {

        rt.SetBTConnection(sc);
        rt.start();
    }

    public void StopReadLPSThread() {
        rt.StopReadThread();
    }

    
}
