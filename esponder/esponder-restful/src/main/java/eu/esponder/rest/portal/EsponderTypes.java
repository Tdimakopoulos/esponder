/*
 * 
 */
package eu.esponder.rest.portal;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.type.ActionTypeDTO;
import eu.esponder.dto.model.type.CrisisDisasterTypeDTO;
import eu.esponder.dto.model.type.CrisisFeatureTypeDTO;
import eu.esponder.dto.model.type.ESponderTypeDTO;
import eu.esponder.rest.ESponderResource;

// TODO: Auto-generated Javadoc
/**
 * The Class EsponderTypes.
 */
@Path("/portal/types")
public class EsponderTypes extends ESponderResource {



	/**
	 * Read all personnel.
	 *
	 * @param pkiKey the pki key
	 * @return the result list dto
	 * @throws ClassNotFoundException the class not found exception
	 */
	@GET
	@Path("/findAll")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResultListDTO readAllTypes(
			@QueryParam("pkiKey") @NotNull(message="pkiKey may not be null") String pkiKey) throws ClassNotFoundException {

		if(!SecurityCheck(pkiKey,1))
		{
			//throw security exception and stop
			return null;
		}
		Long userID=SecurityGetUserID(pkiKey);

		List<ESponderTypeDTO> pResults = this.getTypeRemoteService().findDTOAllTypes();
		return new ResultListDTO(pResults);

	}

	@GET
	@Path("/findAllDisasterTypes")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResultListDTO findAllDisasterTypes(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) throws ClassNotFoundException {
		List<ESponderTypeDTO> pResults = this.getTypeRemoteService().findDTOAllTypes();
		for(int i=0;i<pResults.size();i++)
		{
			if(pResults.get(i) instanceof CrisisDisasterTypeDTO)
			{
				
			}else
			{
				pResults.remove(i);
			}
		}
		return new ResultListDTO(pResults);

	}
	
	@GET
	@Path("/findAllFeatureTypes")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResultListDTO findAllFeatureTypes(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) throws ClassNotFoundException {
		List<ESponderTypeDTO> pResults = this.getTypeRemoteService().findDTOAllTypes();
		for(int i=0;i<pResults.size();i++)
		{
			if(pResults.get(i) instanceof CrisisFeatureTypeDTO)
			{
				
			}else
			{
				pResults.remove(i);
			}
		}
		return new ResultListDTO(pResults);

	}
	
	@GET
	@Path("/findAllActionType")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResultListDTO findAllActionTypes(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) throws ClassNotFoundException {
		List<ESponderTypeDTO> pResults = this.getTypeRemoteService().findDTOAllTypes();
		for(int i=0;i<pResults.size();i++)
		{
			if(pResults.get(i) instanceof ActionTypeDTO)
			{
				
			}else
			{
				pResults.remove(i);
			}
		}
		return new ResultListDTO(pResults);

	}
}