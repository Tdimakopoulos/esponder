/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exodus.com.Interfres;

import exodus.com.dbcontrol.DBControl;
import exodus.com.objects.RestInterf;
import java.util.List;
import javax.ejb.Stateless;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;

/**
 * REST Web Service
 *
 * @author tom
 */
@Stateless
@Path("/interf")
public class InterfResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of InterfResource
     */
    public InterfResource() {
    }

        
    /**
     * Retrieves representation of an instance of exodus.com.Interfres.InterfResource
     * @return an instance of exodus.com.objects.Interf
     */
    @GET
    @Path("/QueryAllInterf")
    @Produces("application/xml")
    public List<RestInterf> getQueryInterfAll() {
        DBControl pdb=new DBControl();
        return pdb.SelectFromInterf("");
    }

    /**
     * PUT method for updating or creating an instance of InterfResource
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Path("/CreateInterf")
    @Consumes("application/xml")
    public void putCreateInterf(RestInterf content) {
        DBControl pdb=new DBControl();
        pdb.createInterf(content);
    }
}
