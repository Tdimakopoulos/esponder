package eu.esponder.optimizerscenarios.first.logisticmanager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredReusableResourceDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.optimizerscenarios.first.objects.Logistic;
import eu.esponder.ws.client.logic.ResourcePlanInfoQueries;
import eu.esponder.ws.client.query.QueryManager;

public class LogisticsManager {

	private List<Logistic> logistics;
	
	public LogisticsManager()
	{
		logistics= new ArrayList<Logistic>();
		PopulateLogistics();
	}
	public List<Logistic> GetLogistics()
	{
		return logistics;
	}
	
	public void PrintAllLogistics() {
		System.out.println(" ");
		System.out.println(" ");
		for (int i = 0; i < logistics.size(); i++) {
			System.out.println("id: " + logistics.get(i).getNumber());
			System.out.println("Name: " + logistics.get(i).getName());
			System.out.println("Type: " + logistics.get(i).getType());
			System.out.println("Category: " + logistics.get(i).getCategory());
			
			System.out.println(" ");
			System.out.println(" ");

		}
	}
	
	private void PopulateLogistics() {

        try {
            ResourcePlanInfoQueries pFind = new ResourcePlanInfoQueries();
            QueryManager pQuery = new QueryManager();
            ResultListDTO pResults = pQuery.getRegisterdConsumablesAll("1");
            for (int i = 0; i < pResults.getResultList().size(); i++) {
                RegisteredConsumableResourceDTO pItem = (RegisteredConsumableResourceDTO) pResults.getResultList().get(i);
                Logistic pperson = new Logistic(pItem.getTitle(), pItem.getId().intValue(), "con", pFind.GetresourceCategoryforRegConsumables(pItem.getConsumableResourceCategoryId()));
                 if(pItem.getStatus()==ResourceStatusDTO.AVAILABLE)
                	 logistics.add(pperson);
            }
        } catch (JsonParseException ex) {
     
        } catch (JsonMappingException ex) {
     
        } catch (IOException ex) {
     
        }


        try {
            ResourcePlanInfoQueries pFind = new ResourcePlanInfoQueries();
            QueryManager pQuery = new QueryManager();
            ResultListDTO pResults = pQuery.getRegisterdReusablesAll("1");
            for (int i = 0; i < pResults.getResultList().size(); i++) {
                RegisteredReusableResourceDTO pItem = (RegisteredReusableResourceDTO) pResults.getResultList().get(i);
                Logistic pperson = new Logistic(pItem.getTitle(), pItem.getId().intValue(), "reu", pFind.GetresourceCategoryforRegResuable(pItem.getReusableResourceCategoryId()));
                     if(pItem.getStatus()==ResourceStatusDTO.AVAILABLE)
                    	 logistics.add(pperson);
            }
        } catch (JsonParseException ex) {
     
        } catch (JsonMappingException ex) {
     
        } catch (IOException ex) {
     
        }
    }
}
