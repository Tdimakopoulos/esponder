/*
 * 
 */
package eu.esponder.model.crisis.resource;


import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;





// TODO: Auto-generated Javadoc
/**
 * The Class Equipment.
 * Entity class the manage the Equipments
 */
@Entity
@Table(name="mobilephone")
@NamedQueries({
	@NamedQuery(name="MobilePhoneEquipment.findByTitle", query="select e from MobilePhoneEquipment e where e.title=:title"),
	@NamedQuery(name="MobilePhoneEquipment.findByIMEI", query="select e from MobilePhoneEquipment e where e.mobilephoneIMEI=:IMEI")
})
public class MobilePhoneEquipment extends Equipment {

	private static final long serialVersionUID = 4916633406886430439L;

	private String mobilephoneIMEI;

	public String getMobilephoneIMEI() {
		return mobilephoneIMEI;
	}

	public void setMobilephoneIMEI(String mobilephoneIMEI) {
		this.mobilephoneIMEI = mobilephoneIMEI;
	}
	
		
}
