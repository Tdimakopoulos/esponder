/*
 * 
 */
package eu.esponder.event.model.config;

import eu.esponder.dto.model.crisis.resource.ResourceDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class UpdateConfigurationEvent.
 */
public class UpdateConfigurationEvent extends ConfigurationEvent<ResourceDTO> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 9215480992618803998L;

	/**
	 * Instantiates a new update configuration event.
	 */
	public UpdateConfigurationEvent(){
		setJournalMessageInfo("Update Configuration Event");
	}
	
}
