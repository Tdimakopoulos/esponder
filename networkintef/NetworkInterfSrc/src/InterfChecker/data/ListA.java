package InterfChecker.data;

//Attributes for ListA

import javax.xml.bind.annotation.XmlElement;

public class ListA {
	protected int listAId;
        private int interfId;
	protected String name;
	protected String macAddress;
	protected String ipAddress;
	protected String netMask;
	protected String netAddress;
	protected String broadcastAddress;
	protected String gateway;
	protected Double maxTxRate;
	protected Double curTxRate;
	protected Double curPercLineRate;
	protected Double packetErrorRate;
	protected Double TxBytes;
	protected Double RxBytes;
	protected int rxPackets;
	protected int txPackets;
	protected int rxErrors;
	protected int txErrors;
	
	public ListA() {

	}
	
	public int getListAId() {
		return listAId;
	}


	public void setListAId(int listAId) {
		this.listAId = listAId;
	}


	public Double getMaxTxRate() {
		return maxTxRate;
	}

	public void setMaxTxRate(Double maxTxRate) {
		this.maxTxRate = maxTxRate;
	}

	public Double getCurTxRate() {
		return curTxRate;
	}

	public void setCurTxRate(Double curTxRate) {
		this.curTxRate = curTxRate;
	}

	public String getBroadcastAddress() {
		return broadcastAddress;
	}
	public void setBroadcastAddress(String broadcastAddress) {
		this.broadcastAddress = broadcastAddress;
	}
	public Double getCurPercLineRate() {
		return curPercLineRate;
	}
	public void setCurPercLineRate(Double curPercLineRate) {
		this.curPercLineRate = curPercLineRate;
	}
	public String getGateway() {
		return gateway;
	}
	public void setGateway(String gateway) {
		this.gateway = gateway;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getMacAddress() {
		return macAddress;
	}
	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNetMask() {
		return netMask;
	}
	public void setNetMask(String netMask) {
		this.netMask = netMask;
	}

	public String getNetAddress() {
		return netAddress;
	}

	public void setNetAddress(String netAddress) {
		this.netAddress = netAddress;
	}

	public Double getRxBytes() {
		return RxBytes;
	}

	public void setRxBytes(Double rxBytes) {
		RxBytes = rxBytes;
	}

	public Double getTxBytes() {
		return TxBytes;
	}

	public void setTxBytes(Double txBytes) {
		TxBytes = txBytes;
	}

	public Double getPacketErrorRate() {
		return packetErrorRate;
	}

	public void setPacketErrorRate(Double packetErrorRate) {
		this.packetErrorRate = packetErrorRate;
	}

	public int getRxErrors() {
		return rxErrors;
	}

	public void setRxErrors(int rxErrors) {
		this.rxErrors = rxErrors;
	}

	public int getRxPackets() {
		return rxPackets;
	}

	public void setRxPackets(int rxPackets) {
		this.rxPackets = rxPackets;
	}

	public int getTxErrors() {
		return txErrors;
	}

	public void setTxErrors(int txErrors) {
		this.txErrors = txErrors;
	}

	public int getTxPackets() {
		return txPackets;
	}

	public void setTxPackets(int txPackets) {
		this.txPackets = txPackets;
	}

    /**
     * @return the interfId
     */
    public int getInterfId() {
        return interfId;
    }

    /**
     * @param interfId the interfId to set
     */
    public void setInterfId(int interfId) {
        this.interfId = interfId;
    }
	
	
	
	
	
	
}
