/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.ws.client.delete;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import eu.esponder.dto.model.crisis.resource.category.PlannableResourceCategoryDTO;
import eu.esponder.ws.client.urlmanager.UrlManager;
import java.io.IOException;
import javax.ws.rs.core.MultivaluedMap;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author tdim
 */
public class DeleteOperations {
    ObjectMapper mapper = new ObjectMapper();
	UrlManager URL = new UrlManager();

        public  String deleteEsponderUser(String userID, String userID2)
			throws JsonParseException, JsonMappingException, IOException {

		// Variables
		
		Client client = Client.create();
		WebResource webResource = client.resource(URL.getSzpuserdelete());

		// Initialize Parameters
		MultivaluedMap queryParams = new MultivaluedMapImpl();
		queryParams.add("userID", userID);
		queryParams.add("deletedUserID", userID2);

		// Call Webservice
		String szReturn = webResource.queryParams(queryParams)
				.delete(String.class);

		

		return szReturn;
	}
}
