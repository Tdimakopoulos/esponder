package com.esponder.apps;

import java.util.Timer;
import java.util.TimerTask;
import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle; 
import android.app.AlertDialog;


public class MyAlertDialog extends Activity {
	AlertDialog alert;
	String alertMessage;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		overridePendingTransition(0, 0);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			alertMessage = extras.getString("alertMessage");
		}

		AlertDialog.Builder alertd = new AlertDialog.Builder(this);
		alertd.setTitle("ALARM");
		alertd.setMessage(alertMessage);
		alertd.setPositiveButton("Ok",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						alert.dismiss();
						finish();
						overridePendingTransition(0, 0);
					}
				});
		alert = alertd.create();
		alert.show();

		final Timer t = new Timer();
		t.schedule(new TimerTask() {
			public void run() {
				alert.dismiss();
				finish();
				overridePendingTransition(0, 0);
				t.cancel();
			}
		}, 5000);

	}

}
