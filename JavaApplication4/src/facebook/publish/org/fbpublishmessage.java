/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package facebook.publish.org;

import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.types.FacebookType;

/**
 *
 * @author tom
 */
public class fbpublishmessage {
    private String connection = "me/feed";
    private String sztype="message";
    private String szText="Hello.....";
    private FacebookClient facebookClient=null;
    private FacebookType publishMessageResponse=null;
    
    public void PublishText()
    {
     
        setPublishMessageResponse(getFacebookClient().publish(getConnection(), FacebookType.class,
                                    Parameter.with(getSztype(), getSzText())));
        System.out.println("Published message ID: " + publishMessageResponse.getId());
     
    }

    /**
     * @return the connection
     */
    public String getConnection() {
        return connection;
    }

    /**
     * @param connection the connection to set
     */
    public void setConnection(String connection) {
        this.connection = connection;
    }

    /**
     * @return the sztype
     */
    public String getSztype() {
        return sztype;
    }

    /**
     * @param sztype the sztype to set
     */
    public void setSztype(String sztype) {
        this.sztype = sztype;
    }

    /**
     * @return the szText
     */
    public String getSzText() {
        return szText;
    }

    /**
     * @param szText the szText to set
     */
    public void setSzText(String szText) {
        this.szText = szText;
    }

    /**
     * @return the facebookClient
     */
    public FacebookClient getFacebookClient() {
        return facebookClient;
    }

    /**
     * @param facebookClient the facebookClient to set
     */
    public void setFacebookClient(FacebookClient facebookClient) {
        this.facebookClient = facebookClient;
    }

    /**
     * @return the publishMessageResponse
     */
    public FacebookType getPublishMessageResponse() {
        return publishMessageResponse;
    }

    /**
     * @param publishMessageResponse the publishMessageResponse to set
     */
    public void setPublishMessageResponse(FacebookType publishMessageResponse) {
        this.publishMessageResponse = publishMessageResponse;
    }
}
