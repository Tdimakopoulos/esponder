package org.exus.df.statistics.model;

import java.util.Date;

public class OutputStatistics {

	Date pStart;
	Date pEnd;
	Long dStart;
	Long dEnd;
	Double avgValue;
	Double maxValue;
	Double minValue;
	
	public Date getpStart() {
		return pStart;
	}
	public void setpStart(Date pStart) {
		this.pStart = pStart;
	}
	public Date getpEnd() {
		return pEnd;
	}
	public void setpEnd(Date pEnd) {
		this.pEnd = pEnd;
	}
	public Long getdStart() {
		return dStart;
	}
	public void setdStart(Long dStart) {
		this.dStart = dStart;
	}
	public Long getdEnd() {
		return dEnd;
	}
	public void setdEnd(Long dEnd) {
		this.dEnd = dEnd;
	}
	public Double getAvgValue() {
		return avgValue;
	}
	public void setAvgValue(Double avgValue) {
		this.avgValue = avgValue;
	}
	public Double getMaxValue() {
		return maxValue;
	}
	public void setMaxValue(Double maxValue) {
		this.maxValue = maxValue;
	}
	public Double getMinValue() {
		return minValue;
	}
	public void setMinValue(Double minValue) {
		this.minValue = minValue;
	}

}
