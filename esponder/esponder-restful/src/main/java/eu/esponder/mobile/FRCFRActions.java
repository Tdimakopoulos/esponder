package eu.esponder.mobile;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.event.mobile.MobileMessageEvent;
import eu.esponder.eventadmin.ESponderEventPublisher;
import eu.esponder.rest.ESponderResource;

@Path("/mobile/actions")
public class FRCFRActions extends ESponderResource {
	
	@GET
	@Path("/sendwarning")
	@Produces({MediaType.APPLICATION_JSON})
	public String readSubCrisisById(
			@QueryParam("UserID") @NotNull(message="subCrisisID may not be null") Long UserID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		return "Success";
	}
	
	@GET
	@Path("/sendmessage")
	@Produces({ MediaType.APPLICATION_JSON })
	public String SendMessage(
			@QueryParam("UserID") @NotNull(message="subCrisisID may not be null") Long UserID,
			@QueryParam("origin") String origin,
			@QueryParam("origintype") String origintype, 
			@QueryParam("message") String message,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		String imei="";
		
		if(origin!=null && message!=null) {
			ESponderEventPublisher<MobileMessageEvent> publisher = 
					new ESponderEventPublisher<MobileMessageEvent>(MobileMessageEvent.class);
			
			MobileMessageEvent event  = new MobileMessageEvent();
			event.setMobileimei(imei);
			
			if(origintype.equalsIgnoreCase("meoc"))
				event.setSourceMEoc(origin);
			else
				event.setSourceEoc(origin);
			
			event.setEventSeverity(SeverityLevelDTO.MEDIUM);
			event.setEventTimestamp(new Date());
			event.setJournalMessage("Notification generated from "+origin);
			event.setJournalMessageInfo("Notification generated from "+origin);
			event.setSzMessage(message);
			event.setEventAttachment(new ESponderUserDTO());
			
			try {
				publisher.publishEvent(event);
			} catch (Exception e) {
				return e.getMessage();
			}
			publisher.CloseConnection();
			return "Success";
		}
		else
			return "Something went wrong";
	}

}
