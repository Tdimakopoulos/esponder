/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exodus.com.listA;

import exodus.com.dbcontrol.DBControl;
import exodus.com.objects.RestListA;
import java.util.List;
import javax.ejb.Stateless;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;

/**
 * REST Web Service
 *
 * @author tom
 */
@Stateless
@Path("/listA")
public class ListAResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of ListAResource
     */
    public ListAResource() {
    }

    /**
     * Retrieves representation of an instance of exodus.com.listA.ListAResource
     * @return an instance of exodus.com.objects.ListA
     */
    @GET
    @Path("/QueryAllListA")
    @Produces("application/xml")
    public List<RestListA> getQueryListAAll() {
        DBControl pdb=new DBControl();
        return pdb.SelectFromListA("");
    }

    /**
     * PUT method for updating or creating an instance of ListAResource
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Path("/CreateListA")
    @Consumes("application/xml")
    public void putCreateListA(RestListA content) {
        DBControl pdb=new DBControl();
        pdb.createListA(content);
    }
}
