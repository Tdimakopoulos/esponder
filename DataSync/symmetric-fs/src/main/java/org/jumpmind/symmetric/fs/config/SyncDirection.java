package org.jumpmind.symmetric.fs.config;

public enum SyncDirection {

    CLIENT_TO_SERVER, SERVER_TO_CLIENT, BIDIRECTIONAL
    
}
