/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.events.popup;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
/**
 *
 * @author Thomas
 */
@ManagedBean
@ApplicationScoped
public class EventMonitorController {

    /**
     * Creates a new instance of EventMonitorController
     */
    public EventMonitorController() {
    }
    
    public void eventListenerMonitorSessionClosed(javax.faces.event.AjaxBehaviorEvent event) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, 
										"Your session is closed", "You have been idle for at least 5 seconds"));
		
		//invalidate session
	}
    
     public void eventListenerMonitor(javax.faces.event.AjaxBehaviorEvent event) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, 
										"Event", "FRU : Kostas Augerinos have Faulty Temperature Sensor."));
		
		//invalidate session
	}
}
