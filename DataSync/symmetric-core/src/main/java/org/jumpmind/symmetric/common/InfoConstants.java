package org.jumpmind.symmetric.common;

final public class InfoConstants {

    private InfoConstants() {
    }
    
    public final static String NODE_GROUP_IDS = "group.ids";

    public final static String NODE_ID = "node.id";
    
    public final static String DATABASE_TYPE = "database.type";    
    
    public final static String DATABASE_VERSION = "database.version";
    
    public final static String DEPLOYMENT_TYPE = "deployment.type";
    
    public final static String TIMEZONE_OFFSET = "timezone.offset";
    
    public final static String SYMMETRIC_VERSION = "symmetric.version"; 
    
    
}
