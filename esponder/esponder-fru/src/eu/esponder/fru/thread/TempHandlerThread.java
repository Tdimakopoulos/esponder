package eu.esponder.fru.thread;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.Queue;
import eu.esponder.Main;
import eu.esponder.RuleEngine;

import eu.esponder.fru.helper.math.MathHelper;
import eu.esponder.fru.measurement.TemperatureMeasurement;

public class TempHandlerThread extends Thread {
	
//	private final MathHelper mathHelper = new MathHelper();
	
	private Queue<Double> data = new LinkedList<Double>();
	RuleEngine rules = new RuleEngine();
	
	private int secs, datacource;
	
	private String command;
	
	private double AVG, MAX;

	public TempHandlerThread(int datacource, String command, int secs) {
		this.secs = secs;
		this.command = command;
		this.datacource = datacource;
	}

	public  synchronized void   run() {
		while (true) {

			try {
				Thread.sleep(secs*1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			Calendar cal = Calendar.getInstance();
		      
            int minute = cal.get(Calendar.MINUTE);
            int second = cal.get(Calendar.SECOND);
 
            Main.sensorTempQueue.get(datacource);
			if (command.equals("AVG")) {
				
				System.out.println("To size tou AVG  stis = "+minute +":"+second +" einai = "
						+ Main.sensorTempQueue.get(datacource).size());
				data = Main.sensorTempQueue.get(datacource);
				AVG = MathHelper.average(data);
				System.out.println("Ready to Put to Envelope AVG=" + AVG);
				if(rules.process(new BigDecimal(AVG), "Temperature", "AVG")){
					System.out.println("PASSED RULES AVG = " + AVG);
					TemperatureMeasurement temp1 = new TemperatureMeasurement(AVG);
					Main.envelopeToGo.add(temp1);
				}
				else{
					System.out.println("NOT PASSED RULES AVG = " + AVG);
				}
				
				 
				 
				Main.sensorTempQueue.get(datacource).clear();

			}

			if (command.equals("MAX")) {
				System.out.println("To size tou MAX stis = "+minute +":"+second +" einai = "
						+ Main.sensorTempQueue.get(datacource).size());
				data = Main.sensorTempQueue.get(datacource);
				MAX = MathHelper.getMaxValue(data);
				System.out.println("Ready to Put to Envelope MAX=" + MAX);
				
				if(rules.process(new BigDecimal(MAX), "Temperature", "MAX")){
					System.out.println("PASSED RULES MAX = " + MAX);
					TemperatureMeasurement temp1 = new TemperatureMeasurement(MAX);
					Main.envelopeToGo.add(temp1);
				}
				else{
					System.out.println("NOT PASSED RULES MAX = " + MAX);
				}
				
				Main.sensorTempQueue.get(datacource).clear(); 
			}
 

		}

	}

}
