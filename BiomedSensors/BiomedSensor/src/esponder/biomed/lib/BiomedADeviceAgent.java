/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package esponder.biomed.lib;

import java.util.concurrent.BlockingQueue;
import esponder.biomed.lib.BiomedACommandsDefinition.Classes;
import esponder.biomed.lib.BiomedACommandsDefinition.OpcodeFlag;
import java.util.Calendar;
import java.util.TimeZone;

/**
 *
 * @author afa
 */
class BiomedADeviceAgent {

    public static int CMD_WAIT_TIME = 5000; //5sec
    protected BlockingQueue<BiomedAFrame> incoming = null;
    protected BlockingQueue<BiomedAFrame> outgoing = null;
    protected DeviceProxy proxy = null;

    public BiomedADeviceAgent(DeviceProxy proxy) {
        this.proxy = proxy;
    }

     public String getDeviceID() throws CommunicationException {
        String id = null;
        try {
            BiomedACommand cmd = new BiomedACommand(Classes.SYSTEM, OpcodeFlag.COMMAND, BiomedACommandsDefinition.Class.SYSTEM.OPCODE.DEVICEID);
            this.proxy.addInCommandQueue(cmd);
            //wait for the notification from the proxy when response is received
            cmd.waitOnResponse(CMD_WAIT_TIME);
            //get the id of the device
            cmd.beginParameterSearch();
            long value = cmd.getParameterValue(BiomedACommandsDefinition.Class.SYSTEM.PARAMETER.STRING);
            id = Long.toHexString(value);
        } catch (Exception ex) {
            CommunicationException bce = new CommunicationException();
            bce.setCode(CommunicationException.API_ERROR_DATALOGGER_NOTCONNECTED);
            bce.setMessage("Cannot connect to the device : " + ex.getMessage());
        }
        return id;
    }

    public BatteryStatus getBatteryStatus() throws CommunicationException {
        BatteryStatus status = new BatteryStatus();
        try {
            BiomedACommand cmd = new BiomedACommand(Classes.POWER, OpcodeFlag.COMMAND, BiomedACommandsDefinition.Class.POWER.OPCODE.GETSTATUS);
            this.proxy.addInCommandQueue(cmd);
            //wait for the notification from the proxy when response is received
            cmd.waitOnResponse(CMD_WAIT_TIME);
            cmd.beginParameterSearch();
            status.percentage = (int) cmd.getParameterValue(BiomedACommandsDefinition.Class.POWER.PARAMETER.BATTERYREMAININGPERCENT);
            cmd.beginParameterSearch();
            status.volageInMV = (int) cmd.getParameterValue(BiomedACommandsDefinition.Class.POWER.PARAMETER.BATTERYVOLTAGE);
            cmd.beginParameterSearch();
            if ((int) cmd.getParameterValue(BiomedACommandsDefinition.Class.POWER.PARAMETER.ISCHARGING) == 1) {
                status.isCharging = true;
            }
        } catch (Exception ex) {
            CommunicationException bce = new CommunicationException();
            bce.setCode(CommunicationException.API_ERROR_COMMAND_FAILED);
            bce.setMessage("Cannot set the clock of the device : " + ex.getMessage());
            throw bce;
        }
        return status;
    }

    public void setTimeClock(int year, int month, int day, int hour, int minute, int second) throws CommunicationException {
        try {
            BiomedACommand cmd = new BiomedACommand(Classes.SYSTEM, OpcodeFlag.COMMAND, BiomedACommandsDefinition.Class.SYSTEM.OPCODE.SETCLOCK);
            //append calendar
            cmd.appendParameter(BiomedACommandsDefinition.Class.SYSTEM.PARAMETER.CLOCK_YEAR, true, year);
            cmd.appendParameter(BiomedACommandsDefinition.Class.SYSTEM.PARAMETER.CLOCK_MONTH, true, month);
            cmd.appendParameter(BiomedACommandsDefinition.Class.SYSTEM.PARAMETER.CLOCK_DAY, true, day);
            cmd.appendParameter(BiomedACommandsDefinition.Class.SYSTEM.PARAMETER.CLOCK_HOUR, true, hour);
            cmd.appendParameter(BiomedACommandsDefinition.Class.SYSTEM.PARAMETER.CLOCK_MINUTE, true, minute);
            cmd.appendParameter(BiomedACommandsDefinition.Class.SYSTEM.PARAMETER.CLOCK_SECOND, true, second);
            this.proxy.addInCommandQueue(cmd);
            //wait for the notification from the proxy when response is received
            cmd.waitOnResponse(CMD_WAIT_TIME);
        } catch (Exception ex) {
            CommunicationException bce = new CommunicationException();
            bce.setCode(CommunicationException.API_ERROR_COMMAND_FAILED);
            bce.setMessage("Cannot set the clock of the device : " + ex.getMessage());
            throw bce;
        }
    }

    public long getClockTime() throws CommunicationException {
        long time = 0;
        try {
            BiomedACommand cmd = new BiomedACommand(Classes.SYSTEM, OpcodeFlag.COMMAND, BiomedACommandsDefinition.Class.SYSTEM.OPCODE.GETCLOCK);
            this.proxy.addInCommandQueue(cmd);
            //wait for the notification from the proxy when response is received
            cmd.waitOnResponse(CMD_WAIT_TIME);
            //get the current date and time
            cmd.beginParameterSearch();
            int year = (int) cmd.getParameterValue(BiomedACommandsDefinition.Class.SYSTEM.PARAMETER.CLOCK_YEAR);
            cmd.beginParameterSearch();
            int month = (int) cmd.getParameterValue(BiomedACommandsDefinition.Class.SYSTEM.PARAMETER.CLOCK_MONTH);
            cmd.beginParameterSearch();
            int day = (int) cmd.getParameterValue(BiomedACommandsDefinition.Class.SYSTEM.PARAMETER.CLOCK_DAY);
            cmd.beginParameterSearch();
            int hour = (int) cmd.getParameterValue(BiomedACommandsDefinition.Class.SYSTEM.PARAMETER.CLOCK_HOUR);
            cmd.beginParameterSearch();
            int minute = (int) cmd.getParameterValue(BiomedACommandsDefinition.Class.SYSTEM.PARAMETER.CLOCK_MINUTE);
            cmd.beginParameterSearch();
            int second = (int) cmd.getParameterValue(BiomedACommandsDefinition.Class.SYSTEM.PARAMETER.CLOCK_SECOND);
            Calendar c = Calendar.getInstance(TimeZone.getDefault());
            c.set(year, month - 1, day, hour, minute, second);
            time = c.getTimeInMillis();
        } catch (Exception ex) {
            CommunicationException bce = new CommunicationException();
            bce.setCode(CommunicationException.API_ERROR_COMMAND_FAILED);
            bce.setMessage("Cannot get the clock of the device : " + ex.getMessage());
            throw bce;
        }
        return time;
    }

    public void startStreaming() throws CommunicationException {
        try {
            BiomedACommand cmd = new BiomedACommand(Classes.SYSTEM, OpcodeFlag.COMMAND, BiomedACommandsDefinition.Class.SYSTEM.OPCODE.STATE);
            cmd.appendParameter(BiomedACommandsDefinition.Class.SYSTEM.PARAMETER.STREAM, true, 1);
            this.proxy.flushDataQueue();
            this.proxy.addInCommandQueue(cmd);
            //wait for the notification from the proxy when response is received
            cmd.waitOnResponse(CMD_WAIT_TIME);
        } catch (Exception ex) {
            CommunicationException bce = new CommunicationException();
            bce.setCode(CommunicationException.API_ERROR_STREAMING_NOT_STARTED);
            bce.setMessage("Cannot start the streaming : " + ex.getMessage());
            throw bce;
        }
    }

    public void stopStreaming() throws CommunicationException {
        try {
            BiomedACommand cmd = new BiomedACommand(Classes.SYSTEM, OpcodeFlag.COMMAND, BiomedACommandsDefinition.Class.SYSTEM.OPCODE.STATE);
            cmd.appendParameter(BiomedACommandsDefinition.Class.SYSTEM.PARAMETER.STREAM, true, 0);
            this.proxy.addInCommandQueue(cmd);
            //wait for the notification from the proxy when response is received
            cmd.waitOnResponse(CMD_WAIT_TIME);
        } catch (Exception ex) {
            CommunicationException bce = new CommunicationException();
            bce.setCode(CommunicationException.API_ERROR_COMMAND_FAILED);
            bce.setMessage("Cannot stop the streaming : " + ex.getMessage());
            throw bce;
        }
    }

    public void startRecording() throws CommunicationException {
        try {
            BiomedACommand cmd = new BiomedACommand(Classes.SYSTEM, OpcodeFlag.COMMAND, BiomedACommandsDefinition.Class.SYSTEM.OPCODE.STATE);
            cmd.appendParameter(BiomedACommandsDefinition.Class.SYSTEM.PARAMETER.RECORD, true, 1);
            this.proxy.addInCommandQueue(cmd);
            //wait for the notification from the proxy when response is received
            synchronized (cmd) {
                cmd.wait(CMD_WAIT_TIME);
            }
        } catch (Exception ex) {
            CommunicationException bce = new CommunicationException();
            bce.setCode(CommunicationException.API_ERROR_COMMAND_FAILED);
            bce.setMessage("Cannot start the recording : " + ex.getMessage());
            throw bce;
        }
    }

    public void stopRecording() throws CommunicationException {
        try {
            BiomedACommand cmd = new BiomedACommand(Classes.SYSTEM, OpcodeFlag.COMMAND, BiomedACommandsDefinition.Class.SYSTEM.OPCODE.STATE);
            cmd.appendParameter(BiomedACommandsDefinition.Class.SYSTEM.PARAMETER.RECORD, true, 0);
            this.proxy.addInCommandQueue(cmd);
            //wait for the notification from the proxy when response is received
            synchronized (cmd) {
                cmd.wait(CMD_WAIT_TIME);
            }
        } catch (Exception ex) {
            CommunicationException bce = new CommunicationException();
            bce.setCode(CommunicationException.API_ERROR_COMMAND_FAILED);
            bce.setMessage("Cannot stop the recording : " + ex.getMessage());
            throw bce;
        }
    }

//    public int getDeviceStatus() throws CommunicationException {
//        int status = 0;
//        int value = -1;
//
//        try {
//            BiomedACommand cmd = new BiomedACommand(Classes.SYSTEM, OpcodeFlag.COMMAND, BiomedACommandsDefinition.Class.SYSTEM.OPCODE.STATE);
//            this.proxy.addInCommandQueue(cmd);
//            //wait for the notification from the proxy when response is received
//            cmd.waitOnResponse(CMD_WAIT_TIME);
//            cmd.beginParameterSearch();
//            value = (int) cmd.getParameterValue(BiomedACommandsDefinition.Class.SYSTEM.PARAMETER.STREAM);
//            if (value == 1) {
//                status = BiomedABluetoothDevice.STREAMING;
//            }
//            cmd.beginParameterSearch();
//            value = (int) cmd.getParameterValue(BiomedACommandsDefinition.Class.SYSTEM.PARAMETER.RECORD);
//            if (value == 1) {
//                if (status == BiomedABluetoothDevice.STREAMING) {
//                    status += BiomedABluetoothDevice.RECORDING;
//                } else {
//                    status = BiomedABluetoothDevice.RECORDING;
//                }
//            }
//            if (value != -1 && status == BiomedABluetoothDevice.DISCONNECTED) {//connected if there is a response to the command
//                status = BiomedABluetoothDevice.CONNECTED;
//            }
//        } catch (Exception ex) {
//            CommunicationException bce = new CommunicationException();
//            bce.setCode(CommunicationException.API_ERROR_COMMAND_FAILED);
//            bce.setMessage("Cannot get the device status : " + ex.getMessage());
//            throw bce;
//        }
//        return status;
//    }

    /**
     * Get the integer array filled with the signals channels enabled in the device.
     * This will provide the number of the signals and their respective channel id.
     * @return signal channels array
     */
    public int[] getSignalsChannels() throws CommunicationException {
        int numberOfSignals = 0;
        int channels[] = null;
        int intChannels[] = new int[64];
        int channel = -1;

        try {
            BiomedACommand cmd = new BiomedACommand(Classes.ACQUISITION, OpcodeFlag.COMMAND, BiomedACommandsDefinition.Class.ACQUISITION.OPCODE.ACQSIGNALINFO);
            this.proxy.addInCommandQueue(cmd);

            //wait for the notification from the proxy when response is received
            cmd.waitOnResponse(CMD_WAIT_TIME);
            //put the index at the first parameter
            cmd.beginParameterSearch();
            //get the values of the occurences of the paramaters "CHANNEL"
            do {
                channel = (int) cmd.getParameterValue(BiomedACommandsDefinition.Class.ACQUISITION.PARAMETER.CHANNEL);
                if (channel != -1) {
                    intChannels[numberOfSignals++] = (0xFF & channel);
                }
            } while (channel != -1);

        } catch (Exception ex) {
            CommunicationException bce = new CommunicationException();
            bce.setCode(CommunicationException.API_ERROR_COMMAND_FAILED);
            bce.setMessage("Cannot get the signals channels : " + ex.getMessage());
            throw bce;
        }
        channels = new int[numberOfSignals];
        System.arraycopy(intChannels, 0, channels, 0, numberOfSignals);
        return channels;
    }

    /**
     * Get the information concerning the signal specified by the channel
     * @param channel the channel of the signal (can be found in BiomedADefinition)
     * @return the signal object filled with it own information
     */
    public Signal getSignal(int channel) throws CommunicationException {
        Signal signal = new Signal();
        long value = 0;
        String name = null;

        try {
            BiomedACommand cmd = new BiomedACommand(Classes.ACQUISITION, OpcodeFlag.COMMAND, BiomedACommandsDefinition.Class.ACQUISITION.OPCODE.ACQSIGNALINFO);
            cmd.appendParameter(BiomedACommandsDefinition.Class.ACQUISITION.PARAMETER.CHANNEL, true, channel);
            this.proxy.addInCommandQueue(cmd);
            //wait for the notification from the proxy when response is received
            cmd.waitOnResponse(CMD_WAIT_TIME);
            //get the name of the signal
            //put the index at the first parameter
            cmd.beginParameterSearch();
            name = cmd.getStringParameterValue(BiomedACommandsDefinition.Class.ACQUISITION.PARAMETER.NAME);
            //signal.setName(Globals.convertHexToString(Long.toHexString(value)));
            signal.setName(name);
            //get the frquency of the signal
            cmd.beginParameterSearch();
            value = cmd.getParameterValue(BiomedACommandsDefinition.Class.ACQUISITION.PARAMETER.FREQUENCY);
            signal.setSamplingFrequency((float) (value & 0xFF));
            //is it enabled
            cmd.beginParameterSearch();
            value = cmd.getParameterValue(BiomedACommandsDefinition.Class.ACQUISITION.PARAMETER.ENABLE);
            //by default it is set to true by the constructor
            if (value == 0) {
                signal.setEnabled(false);
            }


        } catch (Exception ex) {
            CommunicationException bce = new CommunicationException();
            bce.setCode(CommunicationException.API_ERROR_COMMAND_FAILED);
            bce.setMessage("Cannot get the signal : " + ex.getMessage());
            throw bce;
        }

        return signal;
    }
}
