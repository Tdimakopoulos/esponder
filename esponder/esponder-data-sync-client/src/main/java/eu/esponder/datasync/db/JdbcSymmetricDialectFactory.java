package eu.esponder.datasync.db;

import org.jumpmind.db.platform.IDatabasePlatform;
import org.jumpmind.db.platform.db2.Db2DatabasePlatform;
import org.jumpmind.db.platform.derby.DerbyDatabasePlatform;
import org.jumpmind.db.platform.firebird.FirebirdDatabasePlatform;
import org.jumpmind.db.platform.greenplum.GreenplumPlatform;
import org.jumpmind.db.platform.h2.H2DatabasePlatform;
import org.jumpmind.db.platform.hsqldb.HsqlDbDatabasePlatform;
import org.jumpmind.db.platform.hsqldb2.HsqlDb2DatabasePlatform;
import org.jumpmind.db.platform.informix.InformixDatabasePlatform;
import org.jumpmind.db.platform.interbase.InterbaseDatabasePlatform;
import org.jumpmind.db.platform.mariadb.MariaDBDatabasePlatform;
import org.jumpmind.db.platform.mssql.MsSqlDatabasePlatform;
import org.jumpmind.db.platform.mysql.MySqlDatabasePlatform;
import org.jumpmind.db.platform.oracle.OracleDatabasePlatform;
import org.jumpmind.db.platform.postgresql.PostgreSqlDatabasePlatform;
import org.jumpmind.db.platform.sqlite.SqliteDatabasePlatform;
import org.jumpmind.db.platform.sybase.SybaseDatabasePlatform;
import org.jumpmind.symmetric.db.AbstractSymmetricDialect;
import org.jumpmind.symmetric.db.DbNotSupportedException;
import org.jumpmind.symmetric.db.ISymmetricDialect;
import org.jumpmind.symmetric.db.sqlite.SqliteSymmetricDialect;
import org.jumpmind.symmetric.service.IParameterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.esponder.datasync.db.db2.Db2SymmetricDialect;
import eu.esponder.datasync.db.db2.Db2v9SymmetricDialect;
import eu.esponder.datasync.db.derby.DerbySymmetricDialect;
import eu.esponder.datasync.db.firebird.FirebirdSymmetricDialect;
import eu.esponder.datasync.db.h2.H2SymmetricDialect;
import eu.esponder.datasync.db.hsqldb.HsqlDbSymmetricDialect;
import eu.esponder.datasync.db.hsqldb2.HsqlDb2SymmetricDialect;
import eu.esponder.datasync.db.informix.InformixSymmetricDialect;
import eu.esponder.datasync.db.interbase.InterbaseSymmetricDialect;
import eu.esponder.datasync.db.mariadb.MariaDBSymmetricDialect;
import eu.esponder.datasync.db.mssql.MsSqlSymmetricDialect;
import eu.esponder.datasync.db.mysql.MySqlSymmetricDialect;
import eu.esponder.datasync.db.oracle.OracleSymmetricDialect;
import eu.esponder.datasync.db.postgresql.GreenplumSymmetricDialect;
import eu.esponder.datasync.db.postgresql.PostgreSqlSymmetricDialect;
import eu.esponder.datasync.db.sybase.SybaseSymmetricDialect;

/**
 * Factory class that is responsible for creating the appropriate
 * {@link ISymmetricDialect} for the configured database.
 */
public class JdbcSymmetricDialectFactory {

    protected final Logger log = LoggerFactory.getLogger(getClass());

    private IParameterService parameterService;

    private IDatabasePlatform platform;

    public JdbcSymmetricDialectFactory(IParameterService parameterService, IDatabasePlatform platform) {
        this.parameterService = parameterService;
        this.platform = platform;
    }

    public ISymmetricDialect create() {

        AbstractSymmetricDialect dialect = null;

        if (platform instanceof MariaDBDatabasePlatform) {
            dialect = new MariaDBSymmetricDialect(parameterService, platform);
        } else if (platform instanceof MySqlDatabasePlatform) {
                dialect = new MySqlSymmetricDialect(parameterService, platform);
        } else if (platform instanceof OracleDatabasePlatform) {
            dialect = new OracleSymmetricDialect(parameterService, platform);
        } else if (platform instanceof MsSqlDatabasePlatform) {
            dialect = new MsSqlSymmetricDialect(parameterService, platform);
        } else if (platform instanceof GreenplumPlatform) {
            dialect = new GreenplumSymmetricDialect(parameterService, platform);
        } else if (platform instanceof PostgreSqlDatabasePlatform) {
            dialect = new PostgreSqlSymmetricDialect(parameterService, platform);
        } else if (platform instanceof DerbyDatabasePlatform) {
            dialect = new DerbySymmetricDialect(parameterService, platform);
        } else if (platform instanceof H2DatabasePlatform) {
            dialect = new H2SymmetricDialect(parameterService, platform);
        } else if (platform instanceof HsqlDbDatabasePlatform) {
            dialect = new HsqlDbSymmetricDialect(parameterService, platform);
        } else if (platform instanceof HsqlDb2DatabasePlatform) {
            dialect = new HsqlDb2SymmetricDialect(parameterService, platform);
        } else if (platform instanceof InformixDatabasePlatform) {
            dialect = new InformixSymmetricDialect(parameterService, platform);
        } else if (platform instanceof Db2DatabasePlatform) {
            int dbMajorVersion = platform.getSqlTemplate().getDatabaseMajorVersion();
            int dbMinorVersion = platform.getSqlTemplate().getDatabaseMinorVersion();
            if (dbMajorVersion < 9 || (dbMajorVersion == 9 && dbMinorVersion < 5)) {
                dialect = new Db2SymmetricDialect(parameterService, platform);
            } else {
                dialect = new Db2v9SymmetricDialect(parameterService, platform);
            }
        } else if (platform instanceof FirebirdDatabasePlatform) {
            dialect = new FirebirdSymmetricDialect(parameterService, platform);
        } else if (platform instanceof SybaseDatabasePlatform) {
            dialect = new SybaseSymmetricDialect(parameterService, platform);
        } else if (platform instanceof InterbaseDatabasePlatform) {
            dialect = new InterbaseSymmetricDialect(parameterService, platform);
        } else if (platform instanceof SqliteDatabasePlatform) {
            dialect = new SqliteSymmetricDialect(parameterService, platform);
        } else {
            throw new DbNotSupportedException();
        }
        return dialect;
    }

}