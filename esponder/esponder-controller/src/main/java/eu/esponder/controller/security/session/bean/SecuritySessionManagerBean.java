package eu.esponder.controller.security.session.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.controller.security.session.SecuritySessionManagerRemoteService;
import eu.esponder.controller.security.session.SecuritySessionManagerService;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.model.user.ESponderUser;
import eu.esponder.security.keymanager.X509CertImpl;
import eu.esponder.session.ESponderSession;
import eu.esponder.session.ESponderSessionDTO;



@Stateless
public class SecuritySessionManagerBean implements SecuritySessionManagerService, SecuritySessionManagerRemoteService {

	/** The user crud service. */
	@EJB
	private CrudService<ESponderSession> userCrudService;

	/** The mapping service. */
	@EJB
	private ESponderMappingService mappingService;
	
	// -------------------------------------------------------------------------

		/* (non-Javadoc)
		 * @see eu.esponder.controller.crisis.user.UserRemoteService#createUserRemote(eu.esponder.dto.model.user.ESponderUserDTO, java.lang.Long)
		 */
		@Override
		public ESponderSessionDTO createsessionRemote(ESponderSessionDTO userDTO, Long userID) {
	
			ESponderSession user = (ESponderSession) mappingService.mapESponderEntityDTO(
					userDTO, ESponderSession.class);
			user = createsession(user, userID);
			return (ESponderSessionDTO) mappingService.mapESponderEntity(user,
					ESponderSessionDTO.class);
		}

		/* (non-Javadoc)
		 * @see eu.esponder.controller.crisis.user.UserService#createUser(eu.esponder.model.user.ESponderUser, java.lang.Long)
		 */
		@Override
		@Interceptors(ActionAuditInterceptor.class)
		public ESponderSession createsession(ESponderSession user, Long userID) {
			
			userCrudService.create(user);
			return user;
		}

		// -------------------------------------------------------------------------

		/* (non-Javadoc)
		 * @see eu.esponder.controller.crisis.user.UserRemoteService#updateUserRemote(eu.esponder.dto.model.user.ESponderUserDTO, java.lang.Long)
		 */
		@Override
		public ESponderSessionDTO updatesessionRemote(ESponderSessionDTO userDTO, Long userID) {
			
			
			ESponderSession user = (ESponderSession) mappingService.mapESponderEntityDTO(userDTO, ESponderSession.class);
			user = updatesession(user, userID);
			return (ESponderSessionDTO) mappingService.mapESponderEntity(user, ESponderSessionDTO.class);
		}

		/* (non-Javadoc)
		 * @see eu.esponder.controller.crisis.user.UserService#updateUser(eu.esponder.model.user.ESponderUser, java.lang.Long)
		 */
		@Override
		@Interceptors(ActionAuditInterceptor.class)
		public ESponderSession updatesession(ESponderSession user, Long userID) {
						
			ESponderSession userPersisted = userCrudService.find(ESponderSession.class, user.getId());
			
			mappingService.mapEntityToEntity(user, userPersisted);
			return (ESponderSession) userCrudService.update(userPersisted);
		}

		// -------------------------------------------------------------------------

		/* (non-Javadoc)
		 * @see eu.esponder.controller.crisis.user.UserRemoteService#deleteUserRemote(java.lang.Long, java.lang.Long)
		 */
		@Override
		public Long deletesessionRemote(Long deletedUserId, Long userID) {
			return deletesession(deletedUserId, userID);
		}

		/* (non-Javadoc)
		 * @see eu.esponder.controller.crisis.user.UserService#deleteUser(long, java.lang.Long)
		 */
		@Override
		@Interceptors(ActionAuditInterceptor.class)
		public Long deletesession(long deletedUserId, Long userID) {
			ESponderSession resource = userCrudService.find(ESponderSession.class, deletedUserId);

			userCrudService.delete(resource);

			return deletedUserId;
			
		}

		
		
		
//    @Override
//	public ESponderSessionDTO findUserByUsernameRemote(String userName) {
//		ESponderUser user = findUserByPKI(userName);
//		if (user != null)
//			return (ESponderUserDTO) mappingService.mapESponderEntity(user,
//					ESponderUserDTO.class);
//		else
//			return null;
//	}
//
//	
//	@Override
//	public ESponderUser findUserByUsername(String userName) {
//
//		Map<String, Object> params = new HashMap<String, Object>();
//		params.put("username", userName);
//		return (ESponderUser) userCrudService.findSingleWithNamedQuery(
//				"ESponderUser.findByUsername", params);
//		
//	}
	
	// -----------------------------------------------------------------------------
	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.user.UserRemoteService#findAllUsersRemote()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<ESponderSessionDTO> findAllSessionsRemote() {
		return (List<ESponderSessionDTO>) mappingService.mapESponderEntity(findAllSessions(), ESponderSessionDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.user.UserService#findAllUsers()
	 */
	@Override
	public List<ESponderSession> findAllSessions() {
		return (List<ESponderSession>) userCrudService.findWithNamedQuery("ESponderSession.findAll");
	}

	// ------------------------------------------------------------------------- 

		
		
		
}
