/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.model;

import java.util.Date;
import java.util.List;

/**
 *
 * @author tdim
 */
public class DevicesModel {
   
    private String szCode;
    private String szName;
    private String Serial;
    private int iquantity;
    private Date dEffectiveDate;
    private Date dExpirationDate;
    private Date dManufacturedDate;
    private int irole;
    private Long dID;

    /**
     * @return the szCode
     */
    public String getSzCode() {
        if(szCode.equalsIgnoreCase("SP"))
            return "Smartphone";
        if(szCode.equalsIgnoreCase("WAKD"))
            return "Wearable Artificial Kidney";
        return szCode;
    }

    /**
     * @param szCode the szCode to set
     */
    public void setSzCode(String szCode) {
        this.szCode = szCode;
    }

    /**
     * @return the szName
     */
    public String getSzName() {
        return szName;
    }

    /**
     * @param szName the szName to set
     */
    public void setSzName(String szName) {
        this.szName = szName;
    }

    /**
     * @return the Serial
     */
    public String getSerial() {
        return Serial;
    }

    /**
     * @param Serial the Serial to set
     */
    public void setSerial(String Serial) {
        this.Serial = Serial;
    }

    /**
     * @return the iquantity
     */
    public int getIquantity() {
        return iquantity;
    }

    /**
     * @param iquantity the iquantity to set
     */
    public void setIquantity(int iquantity) {
        this.iquantity = iquantity;
    }

    /**
     * @return the dEffectiveDate
     */
    public Date getdEffectiveDate() {
        return dEffectiveDate;
    }

    /**
     * @param dEffectiveDate the dEffectiveDate to set
     */
    public void setdEffectiveDate(Date dEffectiveDate) {
        this.dEffectiveDate = dEffectiveDate;
    }

    /**
     * @return the dExpirationDate
     */
    public Date getdExpirationDate() {
        return dExpirationDate;
    }

    /**
     * @param dExpirationDate the dExpirationDate to set
     */
    public void setdExpirationDate(Date dExpirationDate) {
        this.dExpirationDate = dExpirationDate;
    }

    /**
     * @return the dManufacturedDate
     */
    public Date getdManufacturedDate() {
        return dManufacturedDate;
    }

    /**
     * @param dManufacturedDate the dManufacturedDate to set
     */
    public void setdManufacturedDate(Date dManufacturedDate) {
        this.dManufacturedDate = dManufacturedDate;
    }

    /**
     * @return the irole
     */
    public int getIrole() {
        if(szCode.equalsIgnoreCase("SP"))
            return 1;
        if(szCode.equalsIgnoreCase("WAKD"))
            return 2;
        return -1;
    }

    /**
     * @param irole the irole to set
     */
    public void setIrole(int irole) {
        this.irole = irole;
    }

    /**
     * @return the dID
     */
    public Long getdID() {
        return dID;
    }

    /**
     * @param dID the dID to set
     */
    public void setdID(Long dID) {
        this.dID = dID;
    }
    
     
}
