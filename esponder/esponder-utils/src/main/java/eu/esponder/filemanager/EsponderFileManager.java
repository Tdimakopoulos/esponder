package eu.esponder.filemanager;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class EsponderFileManager {

	FileInputStream fstream = null;
	DataInputStream in = null;
	BufferedReader br = null;
	String strLine;
	boolean hasErrors = false;
	String szErrormsg = "N/A";

	public String GetReadedLine() {
		return strLine;
	}

	public int GetReadedLineint() {
		return Integer.valueOf(strLine);
	}

	public Long GetReadedLineLong() {
		return Long.valueOf(strLine);
	}

	public Double GetReadedLineDouble() {
		return Double.valueOf(strLine);
	}

	public Float GetReadedLineFloat() {
		return Float.valueOf(strLine);
	}

	public boolean closeConnection() {
		try {
			br.close();
			in.close();
			fstream.close();
		} catch (IOException e) {
			hasErrors = true;
			szErrormsg = e.getMessage();
			return false;
		}
		return true;
	}

	public boolean OpenFileForReading(String szFilename) {
		try {
			fstream = new FileInputStream(szFilename);
		} catch (FileNotFoundException e) {
			hasErrors = true;
			szErrormsg = e.getMessage();
			return false;
		}

		// Get the object of DataInputStream
		in = new DataInputStream(fstream);
		br = new BufferedReader(new InputStreamReader(in));
		return true;
	}

	public boolean ReadNext() {
		try {
			strLine = br.readLine();
		} catch (IOException e) {
			hasErrors = true;
			szErrormsg = e.getMessage();
			return false;
		}
		if (strLine == null)
			return false;
		else
			return true;
	}

	public FileInputStream getFstream() {
		return fstream;
	}

	public void setFstream(FileInputStream fstream) {
		this.fstream = fstream;
	}

	public DataInputStream getIn() {
		return in;
	}

	public void setIn(DataInputStream in) {
		this.in = in;
	}

	public BufferedReader getBr() {
		return br;
	}

	public void setBr(BufferedReader br) {
		this.br = br;
	}

	public String getStrLine() {
		return strLine;
	}

	public void setStrLine(String strLine) {
		this.strLine = strLine;
	}

}
