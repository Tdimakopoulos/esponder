/*
 * 
 */
package eu.esponder.controller.mapping.bean;

import java.util.List;

import javax.ejb.Stateless;

import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.mapping.ESponderRemoteMappingService;
import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.model.ESponderEntity;
import eu.esponder.util.mapping.MappingService;
import eu.esponder.util.mapping.MappingServiceFactory;
import eu.esponder.util.mapping.MappingServiceFactory.MappingServiceType;

// TODO: Auto-generated Javadoc
// 
/**
 * The Class ESponderMappingBean.
 */
@Stateless
public class ESponderMappingBean implements ESponderMappingService, ESponderRemoteMappingService {

	/** The Constant MAPPER. */
	private final static MappingService MAPPER = MappingServiceFactory.getMappingService(MappingServiceType.CONFIG);

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderMappingService#mapESponderEntity(eu.esponder.model.ESponderEntity, java.lang.Class)
	 */
	public ESponderEntityDTO mapESponderEntity(ESponderEntity<Long> entity,	Class<? extends ESponderEntityDTO> targetClass) {
		if (targetClass != null) {
			ESponderEntityDTO e = (null != entity ? MAPPER.transform(entity, targetClass) : null);
			return  e;
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderMappingService#mapESponderEntity(java.util.List, java.lang.Class)
	 */
	public List<? extends ESponderEntityDTO> mapESponderEntity( List<? extends ESponderEntity<Long>> resultsList, Class<? extends ESponderEntityDTO> targetClass) {
		if (targetClass != null) {
			return (List<? extends ESponderEntityDTO>) (null != resultsList ? MAPPER.transform(resultsList, targetClass) : null);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderMappingService#mapESponderEntityDTO(java.util.List, java.lang.Class)
	 */
	public List<? extends ESponderEntity<?>> mapESponderEntityDTO(List<? extends ESponderEntityDTO> resultsList, Class<? extends ESponderEntity<?>> targetClass) {
		if (targetClass != null) {
			return (List<? extends ESponderEntity<?>>) (null != resultsList ? MAPPER.transform(resultsList, targetClass) : null);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderMappingService#mapESponderEntityDTO(eu.esponder.dto.model.ESponderEntityDTO, java.lang.Class)
	 */
	public ESponderEntity<?> mapESponderEntityDTO(ESponderEntityDTO entity,	Class<? extends ESponderEntity<?>> targetClass) {
		if (targetClass != null) {
			return (null != entity ? MAPPER.transform(entity, targetClass) : null);
		}
		return null;
	}
	
	/**
	 * Map e sponder entity dto test.
	 *
	 * @param entity the entity
	 * @param targetClass the target class
	 * @return the e sponder entity
	 */
	public ESponderEntity<?> mapESponderEntityDTOTest(ESponderEntityDTO entity,	Class<? extends ESponderEntity<?>> targetClass) {
		if (targetClass != null) {
			return (null != entity ? MAPPER.transform(entity, targetClass) : null);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderMappingService#mapObject(java.lang.Object, java.lang.Class)
	 */
	@Override
	public Object mapObject(Object obj, Class<?> targetClass) {
		if (targetClass != null) {
			return (null != obj ? MAPPER.transform(obj, targetClass) : null);
		}
		return null;
	}


	/**
	 * Map e sponder entity dto with object.
	 *
	 * @param src the src
	 * @param dest the dest
	 * @return the e sponder entity
	 */
	public ESponderEntity<?> mapESponderEntityDTOWithObject(ESponderEntityDTO src, ESponderEntity<Long> dest) {
		MAPPER.transform(src, dest);
		return dest;
	}



	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderMappingService#getEntityClass(java.lang.String)
	 */
	public Class<?> getEntityClass(String className) throws ClassNotFoundException {
		return Class.forName(className);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderMappingService#getManagedEntityName(java.lang.String)
	 */
	public String getManagedEntityName(String queriedEntityDTO) {
		String queriedEntity = queriedEntityDTO.replaceAll("dto.", "");
		queriedEntity = queriedEntity.replaceAll("DTO", "");
		return queriedEntity;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderMappingService#getDTOEntityName(java.lang.String)
	 */
	public String getDTOEntityName(String entityName) {
		String dtoEntityName = entityName.replaceAll("eu.esponder", "eu.esponder.dto");
		dtoEntityName = dtoEntityName.concat("DTO");
		return dtoEntityName;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderMappingService#mapESponderEnumDTO(java.lang.Object, java.lang.Class)
	 */
	@Override
	public Object mapESponderEnumDTO(Object entity, Class<?> targetClass) {
		if (targetClass != null) {
			return (null != entity ? MAPPER.transform(entity, targetClass) : null);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderMappingService#getManagedEntityClass(java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	public Class<? extends ESponderEntity<Long>> getManagedEntityClass(Class<? extends ESponderEntityDTO> clz) throws ClassNotFoundException{
		String className = this.getManagedEntityName(clz.getName()); 
		return (Class<? extends ESponderEntity<Long>>) this.getEntityClass(className);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderMappingService#getDTOEntityClass(java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	public Class<? extends ESponderEntityDTO> getDTOEntityClass(Class<? extends ESponderEntity<Long>> clz) throws ClassNotFoundException{
		String className = this.getDTOEntityName(clz.getName()); 
		return (Class<? extends ESponderEntityDTO>) this.getEntityClass(className);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.mapping.ESponderMappingService#mapEntityToEntity(eu.esponder.model.ESponderEntity, eu.esponder.model.ESponderEntity)
	 */
	@Override
	public void mapEntityToEntity(ESponderEntity<Long> src, ESponderEntity<Long> dest) {
		MAPPER.transform(src, dest);
	}

}
