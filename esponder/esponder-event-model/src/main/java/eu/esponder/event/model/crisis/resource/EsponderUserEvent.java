package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.event.model.crisis.resource.ActorEvent.ESponderActorType;

public abstract class EsponderUserEvent<T extends ESponderUserDTO> extends ResourceEvent<T>{
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8357301175649628740L;

	/**
	 * The Enum ESponderActorType.
	 */
	public enum ESponderActorType {

		/** The Incident commander. */
		IncidentCommander,
		/** The Crisis manager. */
		CrisisManager,
		/** The First responder. */
		FirstResponder,
		/** The First responder chief. */
		FirstResponderChief,
		/** The E sponder user. */
		ESponderUser
	}

	/** The Actor type. */
	ESponderActorType ActorType;

	/**
	 * Gets the actor type.
	 * 
	 * @return the actor type
	 */
	public ESponderActorType getActorType() {
		return ActorType;
	}

	/**
	 * Sets the actor type.
	 * 
	 * @param actorType
	 *            the new actor type
	 */
	public void setActorType(ESponderActorType actorType) {
		ActorType = actorType;
	}
}
