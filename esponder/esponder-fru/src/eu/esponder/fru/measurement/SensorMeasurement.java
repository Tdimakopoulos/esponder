package eu.esponder.fru.measurement;

public class SensorMeasurement {
  
	private String timestamp;


	public String getTimestamp(){
	 	return timestamp;
	}
	
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
}
