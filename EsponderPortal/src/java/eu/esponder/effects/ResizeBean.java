/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.effects;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.ResizeEvent;

/**
 *
 * @author Thomas
 */
@ManagedBean
@RequestScoped
public class ResizeBean {

    /**
     * Creates a new instance of ResizeBean
     */
    public ResizeBean() {
    }
    public void onResize(ResizeEvent event) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
                        "Image resized", "Width:" + event.getWidth() + ",Height:" + event.getHeight());

        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
}
