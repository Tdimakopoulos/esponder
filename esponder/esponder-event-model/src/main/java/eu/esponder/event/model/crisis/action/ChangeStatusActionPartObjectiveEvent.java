/*
 * 
 */
package eu.esponder.event.model.crisis.action;




import eu.esponder.dto.model.crisis.action.ActionObjectiveDTO;
import eu.esponder.dto.model.crisis.action.ActionPartObjectiveDTO;

import eu.esponder.event.model.CreateEvent;



// TODO: Auto-generated Javadoc
/**
 * The Class CreateActionEvent.
 */
public class ChangeStatusActionPartObjectiveEvent extends ActionPartObjectiveEvent<ActionPartObjectiveDTO> implements CreateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2006035559724305579L;

int newStatus;
	
	
	public int getNewStatus() {
		return newStatus;
	}


	public void setNewStatus(int newStatus) {
		this.newStatus = newStatus;
	}
	
	/**
	 * Instantiates a new creates the action event.
	 */
	public ChangeStatusActionPartObjectiveEvent(){
		setJournalMessageInfo("Create Action Event");
	}
}
