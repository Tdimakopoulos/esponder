/*
 * 
 */
package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.event.model.CreateEvent;



// TODO: Auto-generated Javadoc
/**
 * The Class CreateResourceEquipmentEvent.
 */
public class CreateResourceEquipmentEvent extends ResourceEquipmentEvent<EquipmentDTO> implements CreateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1002815036463481211L;
	
	/**
	 * Instantiates a new creates the resource equipment event.
	 */
	public CreateResourceEquipmentEvent(){
		setJournalMessageInfo("CreateResourceEquipmentEvent");
	}
}
