/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exus.esponder.filedatabase.handlers;

import exus.esponder.filedatabase.model.BiomedATable;
import exus.esponder.filedatabase.model.BiomedBTable;
import exus.esponder.filedatabase.model.ECGTable;
import exus.esponder.filedatabase.model.Pathmanager;
import exus.esponder.filedatabase.model.UITable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;

/**
 *
 * @author tdim
 */
public class PersistSensorsReadings {
    
    String fileUI="UIReading.bin";
    String fileECG="ECGReading.bin";
    String fileBIOA="BIOAReading.bin";
    String fileBIOB="BIOBReading.bin";
    
    public void SaveBiomedB(String type,float szReading) throws FileNotFoundException, IOException, ClassNotFoundException
    {
        String szPath;
        Pathmanager pmanager = new Pathmanager();
        szPath = pmanager.GetPath();
        
        BiomedBTable item=new BiomedBTable();
        item.setFvalue(szReading);
        item.setLvalue(Long.MIN_VALUE);
        item.setSvalue(String.valueOf(szReading));
        item.setStype(type);
        item.setItype(0);
        item.setTime((new Date()).getTime());
        item.setIfield1(0);
        item.setIfield2(0);
        item.setLfield1(Long.MIN_VALUE);
        item.setLfield2(Long.MIN_VALUE);
        item.setSzField1("");
        item.setSzField2("");
        item.setSzField3("");
        
        File f = new File(szPath+fileBIOB);
        if(f.exists()) 
        { 
            BiomedBDatabase pp=new BiomedBDatabase();
            pp.SetFilename(fileBIOB);
            pp.LoadFromFile();
            pp.Add(item);
            pp.SaveOnFile();
        }else
        {
            BiomedBDatabase pp=new BiomedBDatabase();
            pp.SetFilename(fileBIOB);
            pp.Add(item);
            pp.SaveOnFile();
        }
    }
    
    public void SaveBiomedA(String type,float szReading) throws FileNotFoundException, IOException, ClassNotFoundException
    {
        String szPath;
        Pathmanager pmanager = new Pathmanager();
        szPath = pmanager.GetPath();
        
        BiomedATable item=new BiomedATable();
        item.setFvalue(szReading);
        item.setLvalue(Long.MIN_VALUE);
        item.setSvalue(String.valueOf(szReading));
        item.setStype(type);
        item.setItype(0);
        item.setTime((new Date()).getTime());
        item.setIfield1(0);
        item.setIfield2(0);
        item.setLfield1(Long.MIN_VALUE);
        item.setLfield2(Long.MIN_VALUE);
        item.setSzField1("");
        item.setSzField2("");
        item.setSzField3("");
        
        File f = new File(szPath+fileBIOA);
        if(f.exists()) 
        { 
            BiomedADatabase pp=new BiomedADatabase();
            pp.SetFilename(fileBIOA);
            pp.LoadFromFile();
            pp.Add(item);
            pp.SaveOnFile();
        }else
        {
            BiomedADatabase pp=new BiomedADatabase();
            pp.SetFilename(fileBIOA);
            pp.Add(item);
            pp.SaveOnFile();
        }
    }
    
    
    public void SaveECG(float szReading) throws FileNotFoundException, IOException, ClassNotFoundException
    {
        String szPath;
        Pathmanager pmanager = new Pathmanager();
        szPath = pmanager.GetPath();
        
        ECGTable item=new ECGTable();
        item.setFvalue(szReading);
        item.setLvalue(Long.MIN_VALUE);
        item.setSvalue(String.valueOf(szReading));
        item.setTime((new Date()).getTime());
        item.setIfield1(0);
        item.setIfield2(0);
        item.setLfield1(Long.MIN_VALUE);
        item.setLfield2(Long.MIN_VALUE);
        item.setSzField1("");
        item.setSzField2("");
        item.setSzField3("");
        
        File f = new File(szPath+fileECG);
        if(f.exists()) 
        { 
            ECGDatabase pp=new ECGDatabase();
            pp.SetFilename(fileECG);
            pp.LoadFromFile();
            pp.Add(item);
            pp.SaveOnFile();
        }else
        {
            ECGDatabase pp=new ECGDatabase();
            pp.SetFilename(fileECG);
            pp.Add(item);
            pp.SaveOnFile();
        }
    }
    
    
    public void SaveUI(String szReading) throws FileNotFoundException, IOException, ClassNotFoundException
    {
        String szPath;
        Pathmanager pmanager = new Pathmanager();
        szPath = pmanager.GetPath();
        
        UITable item=new UITable();
        item.setCommandString(szReading);
        item.setTime((new Date()).getTime());
        item.setIfield1(0);
        item.setIfield2(0);
        item.setLfield1(Long.MIN_VALUE);
        item.setLfield2(Long.MIN_VALUE);
        item.setSzField1("");
        item.setSzField2("");
        item.setSzField3("");
        
        File f = new File(szPath+fileUI);
        if(f.exists()) 
        { 
            UIDatabase pp=new UIDatabase();
            pp.SetFilename(fileUI);
            pp.LoadFromFile();
            pp.Add(item);
            pp.SaveOnFile();
        }else
        {
            UIDatabase pp=new UIDatabase();
            pp.SetFilename(fileUI);
            pp.Add(item);
            pp.SaveOnFile();
        }
    }
    
    
    
}
