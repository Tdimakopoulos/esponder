package eu.esponder.jaxb.model.crisis.view;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;

@XmlRootElement(name="referencePOI")
@XmlType(name="ReferencePOI")
@JsonPropertyOrder({"referenceFile"})
public class ReferencePOIDTO extends ResourcePOIDTO {
	
	private String referenceFile;

	public String getReferenceFile() {
		return referenceFile;
	}

	public void setReferenceFile(String referenceFile) {
		this.referenceFile = referenceFile;
	}

}
