/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.objects;

import eu.esponder.dto.model.crisis.CrisisContextDTO;
import java.util.Date;

/**
 *
 * @author tdim
 */
public class CrisisContextDash {
    private String szTitle;
    private Date pDateStart;
    private Date pDateEnd;
    private Long id;
    private String szStatus;
    private String szAlert;
    
    private CrisisContextDTO pCrisisDTO;
    
    /**
     * @return the szTitle
     */
    public String getSzTitle() {
        return szTitle;
    }

    /**
     * @param szTitle the szTitle to set
     */
    public void setSzTitle(String szTitle) {
        this.szTitle = szTitle;
    }

    /**
     * @return the pDateStart
     */
    public Date getpDateStart() {
        return pDateStart;
    }

    /**
     * @param pDateStart the pDateStart to set
     */
    public void setpDateStart(Date pDateStart) {
        this.pDateStart = pDateStart;
    }

    /**
     * @return the pDateEnd
     */
    public Date getpDateEnd() {
        return pDateEnd;
    }

    /**
     * @param pDateEnd the pDateEnd to set
     */
    public void setpDateEnd(Date pDateEnd) {
        this.pDateEnd = pDateEnd;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the szStatus
     */
    public String getSzStatus() {
        return szStatus;
    }

    /**
     * @param szStatus the szStatus to set
     */
    public void setSzStatus(String szStatus) {
        this.szStatus = szStatus;
    }

    /**
     * @return the szAlert
     */
    public String getSzAlert() {
        return szAlert;
    }

    /**
     * @param szAlert the szAlert to set
     */
    public void setSzAlert(String szAlert) {
        this.szAlert = szAlert;
    }

    /**
     * @return the pCrisisDTO
     */
    public CrisisContextDTO getpCrisisDTO() {
        return pCrisisDTO;
    }

    /**
     * @param pCrisisDTO the pCrisisDTO to set
     */
    public void setpCrisisDTO(CrisisContextDTO pCrisisDTO) {
        this.pCrisisDTO = pCrisisDTO;
    }
    
}
