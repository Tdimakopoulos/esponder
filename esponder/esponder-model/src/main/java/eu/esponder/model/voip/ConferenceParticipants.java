package eu.esponder.model.voip;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;
import eu.esponder.model.crisis.resource.OperationsCentre;

@Entity
@Table(name="conferenceparticipants")
@NamedQueries({
		@NamedQuery(name = "ConferenceParticipants.findAll", query = "select u from ConferenceParticipants u"),
		@NamedQuery(name = "ConferenceParticipants.findByid", query = "select u from ConferenceParticipants u where u.id=:id"),
		@NamedQuery(name = "ConferenceParticipants.findByActorID", query = "select u from ConferenceParticipants u where u.actorID=:actorID"),
		@NamedQuery(name = "ConferenceParticipants.findByStatus", query = "select u from ConferenceParticipants u where u.ipstatus=:ipstatus")})
public class ConferenceParticipants extends ESponderEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2758387616684723634L;
	
	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(name="VOIPPAR_ID")
	protected Long id;
	

	@ManyToOne
	@JoinColumn(name = "VC_ID")
	private VoipConference voipConference;
	
	Long actorID;
	
	int ipstatus;
	
	
	
	public VoipConference getVoipConference() {
		return voipConference;
	}

	public void setVoipConference(VoipConference voipConference) {
		this.voipConference = voipConference;
	}

	public Long getActorID() {
		return actorID;
	}

	public void setActorID(Long actorID) {
		this.actorID = actorID;
	}

	public int getIpstatus() {
		return ipstatus;
	}

	public void setIpstatus(int ipstatus) {
		this.ipstatus = ipstatus;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}
}
