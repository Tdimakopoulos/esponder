package eu.esponder.fru.measurement;

import java.math.BigDecimal;

public class CarbonDioxideMeasurement extends DoubleValueSensorMeasurement {

	public CarbonDioxideMeasurement(BigDecimal value) {
		super(value);
	}

	public CarbonDioxideMeasurement(int value) {
		super(value);
	}

	
}
