/*
 * 
 */
package eu.esponder.controller.mapping;

import java.util.List;

import javax.ejb.Remote;

import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.model.ESponderEntity;

// TODO: Auto-generated Javadoc
// 
/**
 * The Interface ESponderRemoteMappingService.
 */
@Remote
public interface ESponderRemoteMappingService {


	/**
	 * Map e sponder entity.
	 *
	 * @param resultsList the results list
	 * @param targetClass the target class
	 * @return the list<? extends e sponder entity dt o>
	 */
	public List<? extends ESponderEntityDTO> mapESponderEntity(List<? extends ESponderEntity<Long>> resultsList, Class<? extends ESponderEntityDTO> targetClass);

	/**
	 * Map e sponder entity.
	 *
	 * @param entity the entity
	 * @param targetClass the target class
	 * @return the e sponder entity dto
	 */
	public ESponderEntityDTO mapESponderEntity(ESponderEntity<Long> entity, Class<? extends ESponderEntityDTO> targetClass);

	/**
	 * Map e sponder entity dto.
	 *
	 * @param resultsList the results list
	 * @param targetClass the target class
	 * @return the list<? extends e sponder entity<?>>
	 */
	public List<? extends ESponderEntity<?>> mapESponderEntityDTO(List<? extends ESponderEntityDTO> resultsList, Class<? extends ESponderEntity<?>> targetClass);

	/**
	 * Map e sponder entity dto.
	 *
	 * @param entity the entity
	 * @param targetClass the target class
	 * @return the e sponder entity
	 */
	public ESponderEntity<?> mapESponderEntityDTO(ESponderEntityDTO entity, Class<? extends ESponderEntity<?>> targetClass);

	//***************************************************************************************************************	

	/**
	 * Map e sponder enum dto.
	 *
	 * @param entity the entity
	 * @param targetClass the target class
	 * @return the object
	 */
	public Object mapESponderEnumDTO(Object entity, Class<?> targetClass);

	/**
	 * Gets the managed entity class.
	 *
	 * @param clz the clz
	 * @return the managed entity class
	 * @throws ClassNotFoundException the class not found exception
	 */
	public Class<? extends ESponderEntity<Long>> getManagedEntityClass(Class<? extends ESponderEntityDTO> clz) throws ClassNotFoundException;

	/**
	 * Gets the dto entity class.
	 *
	 * @param clz the clz
	 * @return the DTO entity class
	 * @throws ClassNotFoundException the class not found exception
	 */
	public Class<? extends ESponderEntityDTO> getDTOEntityClass(Class<? extends ESponderEntity<Long>> clz) throws ClassNotFoundException;

	/**
	 * Gets the entity class.
	 *
	 * @param className the class name
	 * @return the entity class
	 * @throws ClassNotFoundException the class not found exception
	 */
	public Class<?> getEntityClass(String className) throws ClassNotFoundException;

	/**
	 * Gets the managed entity name.
	 *
	 * @param queriedEntityDTO the queried entity dto
	 * @return the managed entity name
	 */
	public String getManagedEntityName(String queriedEntityDTO);

	/**
	 * Gets the dTO entity name.
	 *
	 * @param entityName the entity name
	 * @return the dTO entity name
	 */
	public String getDTOEntityName(String entityName);

	/**
	 * Map entity to entity.
	 *
	 * @param src the src
	 * @param dest the dest
	 */
	public void mapEntityToEntity(ESponderEntity<Long> src, ESponderEntity<Long> dest);

	/**
	 * Map object.
	 *
	 * @param obj the obj
	 * @param targetClass the target class
	 * @return the object
	 */
	public Object mapObject(Object obj, Class<?> targetClass);



}
