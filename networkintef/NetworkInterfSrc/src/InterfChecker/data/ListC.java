package InterfChecker.data;


//Attributes for ListC
public class ListC {
	protected int listCId;
	protected String AcPoMAC;
	protected String Essid;
	protected String Channel;
	protected String Status;
	protected String SignalLevel;
        protected String device;
	
	public ListC() {
	}
	
	
	public int getListCId() {
		return listCId;
	}


	public void setListCId(int listCId) {
		this.listCId = listCId;
	}


	public String getAcPoMAC() {
		return AcPoMAC;
	}

	public void setAcPoMAC(String acPoMAC) {
		AcPoMAC = acPoMAC;
	}

	public String getChannel() {
		return Channel;
	}

	public void setChannel(String channel) {
		Channel = channel;
	}

	public String getEssid() {
		return Essid;
	}

	public void setEssid(String essid) {
		Essid = essid;
	}

	public String getSignalLevel() {
		return SignalLevel;
	}

	public void setSignalLevel(String signalLevel) {
		SignalLevel = signalLevel;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

        public String getDevice() {
            return device;
        }

        public void setDevice(String device) {
            this.device = device;
        }
	
        
}
