package eu.esponder.util.jaxb;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class Parser {

	private Marshaller marshaller;
	private Unmarshaller um;

	public Parser(Class<?>[] classes) throws JAXBException {
		JAXBContext ctx = JAXBContext.newInstance(classes);
		marshaller = ctx.createMarshaller();
		um = ctx.createUnmarshaller();
	}

	public String marshall(Object xmlFile) throws JAXBException {
		StringWriter stringWriter = new StringWriter(); 
		this.marshaller.marshal(xmlFile, stringWriter);
		return stringWriter.toString();
	}

	public Object unmarshal(String xmlFile) throws JAXBException {
		InputStream is = new ByteArrayInputStream(xmlFile.getBytes());
		return this.um.unmarshal(is);
	}

}
