package eu.esponder.controller.security.session;

import java.util.List;

import javax.ejb.Remote;

import eu.esponder.session.ESponderSessionDTO;

@Remote
public interface SecuritySessionManagerRemoteService {

	ESponderSessionDTO createsessionRemote(ESponderSessionDTO userDTO,
			Long userID);

	ESponderSessionDTO updatesessionRemote(ESponderSessionDTO userDTO,
			Long userID);

	Long deletesessionRemote(Long deletedUserId, Long userID);

	List<ESponderSessionDTO> findAllSessionsRemote();

}
