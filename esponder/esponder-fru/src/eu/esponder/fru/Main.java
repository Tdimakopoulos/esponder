package eu.esponder.fru;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import eu.esponder.fru.data.DataHandler;
import eu.esponder.fru.data.DataSender;
import eu.esponder.fru.measurement.MeasurementsEnvelope;
import eu.esponder.fru.thread.TempHandlerThread;

public class Main {

	public static MeasurementsEnvelope envelope;

	public static List<Queue<Double>> sensorTempQueue;
	
	public static List<Queue<Double>> sensorCarbonQueue;

	public static void main(String args[]) {
		// Random rand;
		envelope = new MeasurementsEnvelope();

		final List<String> Sensor_Temp_Commands = new ArrayList<String>();
		sensorTempQueue = new ArrayList<Queue<Double>>();

		final List<String> Sensor_Carbon_Commands = new ArrayList<String>();
		sensorCarbonQueue = new ArrayList<Queue<Double>>();

//		final List<Integer> temperatures = new ArrayList<Integer>();
//		final List<Integer> carbon_diox = new ArrayList<Integer>();
//		final MathHelper mathHelper = new MathHelper();
		String[] temp = null;

		try {
			// Open the file that is the first
			// command line parameter
			FileInputStream fstream = new FileInputStream("config/config.txt");
			// Get the object of DataInputStream
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			// Read File Line By Line
			while ((strLine = br.readLine()) != null) {
				// Print the content on the console
				temp = strLine.split("=");
				// System.out.println (temp[0]);
				if (temp[0].equals("Sensor_Temperature")) {
					Sensor_Temp_Commands.add(temp[1]);
				} else if (temp[0].equals("Sensor_Carbon")) {
					Sensor_Carbon_Commands.add(temp[1]);
				}
			}
			// Close the input stream
			in.close();
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}

		for (int i = 0; i < Sensor_Temp_Commands.size(); i++) {
			Queue<Double> qe = new LinkedList<Double>();
			sensorTempQueue.add(qe);
		}

		for (int i = 0; i < Sensor_Temp_Commands.size(); i++) {
			Queue<Double> qe = new LinkedList<Double>();
			sensorCarbonQueue.add(qe);
		}

		Thread data_sender = new DataSender();
		data_sender.start();
		try {
			Thread.sleep(1);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Thread data_Handler = new DataHandler(envelope);
		data_Handler.start();

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int i = 0; i < Sensor_Temp_Commands.size(); i++) {
			String[] temp2;
			temp2 = Sensor_Temp_Commands.get(i).split("_");
			Thread temp_thread = new TempHandlerThread(i, temp2[0],
					Integer.parseInt(temp2[1]));
			temp_thread.start();
		}

		/*
		 * exec.scheduleAtFixedRate(new Runnable() {
		 * 
		 * @Override public void run() {
		 * 
		 * for(int i=0;i<envelope.size();i++){
		 * 
		 * if(envelope.get(i) instanceof TemperatureMeasurement){ //
		 * System.out.println("1"); temperatures.add(((TemperatureMeasurement)
		 * envelope.get(i)).getValue()); }
		 * 
		 * if(envelope.get(i) instanceof Carbon_Dioxide_Measurement){ //
		 * System.out.println("2");
		 * carbon_diox.add(((Carbon_Dioxide_Measurement)
		 * envelope.get(i)).getValue()); }
		 * 
		 * 
		 * } Calendar calendar = Calendar.getInstance(); int hour =
		 * calendar.get(Calendar.HOUR); int minute =
		 * calendar.get(Calendar.MINUTE); int second =
		 * calendar.get(Calendar.SECOND);
		 * 
		 * System.out.println("hour : " +hour+ " minute : "+ minute +
		 * " seconds "+second);
		 * System.out.println("Temperature AVG :"+mathe.average(temperatures));
		 * System
		 * .out.println("Temperature Max :"+mathe.getMaxValue(temperatures));
		 * System
		 * .out.println("Temperature Min :"+mathe.getMinValue(temperatures));
		 * 
		 * System.out.println("Carbon AVG :"+mathe.average(carbon_diox));
		 * System.out.println("Carbon Max :"+mathe.getMaxValue(carbon_diox));
		 * System.out.println("Carbon Min :"+mathe.getMinValue(carbon_diox));
		 * System
		 * .out.println("<--------------------------------------------------->"
		 * ); } }, 0, 6, TimeUnit.SECONDS);
		 */
	}
}
