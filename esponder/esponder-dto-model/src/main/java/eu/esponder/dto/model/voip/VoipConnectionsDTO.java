package eu.esponder.dto.model.voip;



import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;



@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id","connectionID"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class VoipConnectionsDTO extends ESponderEntityDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6749439888200814818L;

	/** The id. */
	
	protected Long id;
	

	Long connectionID;
	
	
	
	public Long getConnectionID() {
		return connectionID;
	}

	public void setConnectionID(Long connectionID) {
		this.connectionID = connectionID;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

}
