package InterfChecker;

import InterfChecker.data.ListA;
import InterfChecker.data.MonitorData;

import netinterf.soap.clients.DatabaseManagerClient;
import netinterf.soap.clients.MonitorDataClient;
import interfaces.soap.ws.SoapMessageInterf;
import interfaces.soap.ws.SoapMessageListA;
import interfaces.soap.ws.SoapMessageListB;
import interfaces.soap.ws.SoapMessageListC;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DataCheckerThread extends Thread {

    private volatile boolean stop = false;//variable if thread needs to close--If true then we are stopping the thread
    private String info = " INFO ";
    protected static double totalTime;//Synolikos xronos dil.time3-time1
    protected Date time1;//Xronos stin arxi tou iteration tou thread
    protected Date time2;//Xronos meta tis leitourgies/elegxous tou thread
    protected Date time3;//Xronos meta to timeout
    protected boolean changed = false;
    protected boolean readyToInsert = false;
    protected String deviceName;

    public DataCheckerThread(String str) {
        super(str);
    }

    public String toString() {//print message
        return "[Thread-" + getId() + "] ";
    }

    public String monitorInterf() {//print message
        return this + info + "- " + this.getName() + " -DataCheckerThread- "
                + " TTL is " + (Application.TTL);
    }

    public void run() {
        time1 = new Date();//initializing timer
        System.out.println(monitorInterf());
        try {
            sleep(2000);
        } catch (InterruptedException ex) {
            Logger.getLogger(DataCheckerThread.class.getName()).log(Level.SEVERE, null, ex);
        }
        while (!stop) {//If stop is true then stop
            try {
                time2 = new Date();
                sleep(2000);
                if (changed) {
                    InterfaceThread.monitorData.setInterfVectorA(InterfaceThread.interfVectorA);
                    InterfaceThread.monitorData.setInterfVectorB(InterfaceThread.interfVectorB);
                    if (InterfaceThread.interfVectorC != null) //If list C has an element
                    {
                        InterfaceThread.monitorData.setInterfVectorC(InterfaceThread.interfVectorC);
                    }

                    InterfaceThread.monitorData.setVectorInterf(InterfaceThread.VectorInterf);
                    time1 = new Date();
                    for (int a = 0; a < InterfaceThread.monitorData.getInterfVectorA().size(); a++) {//Iterating Vestor A
                        ListA test = InterfaceThread.interfVectorA.elementAt(a);
                        System.out.println("Data Checker Interface:" + test.getListAId() + " Name=" + test.getName() + " |MAC=" + test.getMacAddress());
                    }
                    sleep(500);
                    System.out.println("Size : " + InterfaceThread.monitorData.getVectorInterf().size());

                    DBinsert(InterfaceThread.monitorData);
                }

                totalTime = time2.getTime() - time1.getTime();
                if (totalTime >= Application.TTL) {
                    DBclear();
                }

            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
            if (stop) {
                System.out.println("Stoppping thread " + this);
            }
        }
    }

    public synchronized void DBclear() {
        System.out.println("Time in ms" + totalTime);
        time1 = new Date();
        DatabaseManagerClient dbManager = new DatabaseManagerClient();
        dbManager.DeleteDatabase(Application.hostname);
    }

    public synchronized void DBinsert(MonitorData monitorData) {
        System.out.println("********CHANGE FOUND Initiating WS");
        //call Web service to save into the DatabaseSystem.out.println("*********************1******************");
        MonitorDataClient pClient = new MonitorDataClient();

        interfaces.soap.ws.SoapMessageMonitorData pMonitorData = new interfaces.soap.ws.SoapMessageMonitorData();
        for (int i = 0; i < monitorData.getInterfVectorA().size(); i++) {
            SoapMessageListA pelement = new SoapMessageListA();
            pelement.setListAId(monitorData.getInterfVectorA().get(i).getListAId());
            pelement.setBroadcastAddress(monitorData.getInterfVectorA().get(i).getBroadcastAddress());
            pelement.setCurPercLineRate(monitorData.getInterfVectorA().get(i).getCurPercLineRate());
            pelement.setCurTxRate(monitorData.getInterfVectorA().get(i).getCurTxRate());
            pelement.setGateway(monitorData.getInterfVectorA().get(i).getGateway());
            pelement.setInterfId(monitorData.getInterfVectorA().get(i).getInterfId());
            pelement.setIpAddress(monitorData.getInterfVectorA().get(i).getIpAddress());
            pelement.setMacAddress(monitorData.getInterfVectorA().get(i).getMacAddress());
            pelement.setMaxTxRate(monitorData.getInterfVectorA().get(i).getMaxTxRate());
            pelement.setNetAddress(monitorData.getInterfVectorA().get(i).getNetAddress());
            pelement.setNetMask(monitorData.getInterfVectorA().get(i).getNetMask());
            pelement.setPacketErrorRate(monitorData.getInterfVectorA().get(i).getPacketErrorRate());
            pelement.setRxBytes(monitorData.getInterfVectorA().get(i).getRxBytes());
            pelement.setRxErrors(monitorData.getInterfVectorA().get(i).getRxErrors());
            pelement.setRxPackets(monitorData.getInterfVectorA().get(i).getRxPackets());
            pelement.setTxBytes(monitorData.getInterfVectorA().get(i).getTxBytes());
            pelement.setTxErrors(monitorData.getInterfVectorA().get(i).getTxErrors());
            pelement.setTxPackets(monitorData.getInterfVectorA().get(i).getTxPackets());

            pMonitorData.getInterfVectorA().add(pelement);

        }
        for (int i = 0; i < monitorData.getInterfVectorB().size(); i++) {
            SoapMessageListB pelement = new SoapMessageListB();
            pelement.setListBId(monitorData.getInterfVectorB().get(i).getListBId());
            pelement.setAccessPointStatus(monitorData.getInterfVectorB().get(i).getStatus());
            pelement.setChannel(monitorData.getInterfVectorB().get(i).getChannel());
            pelement.setDiscardedPackets(monitorData.getInterfVectorB().get(i).getDiscardedPackets());
            pelement.setEssid(monitorData.getInterfVectorB().get(i).getEssid());
            pelement.setInterfId(monitorData.getInterfVectorB().get(i).getInterfId());
            pelement.setLinkLevelNoise(monitorData.getInterfVectorB().get(i).getLinkLevelNoise());
            pelement.setLinkQuality(monitorData.getInterfVectorB().get(i).getLinkQuality());
            pelement.setMacAddressAP(monitorData.getInterfVectorB().get(i).getMacAddressAP());
            pelement.setSignalLevel(monitorData.getInterfVectorB().get(i).getSignalLevel());
            pelement.setTxPower(monitorData.getInterfVectorB().get(i).getTxPower());

            pMonitorData.getInterfVectorB().add(pelement);

        }
        for (int i = 0; i < monitorData.getVectorInterf().size(); i++) {
            SoapMessageInterf pelement = new SoapMessageInterf();
            pelement.setInterfId(monitorData.getVectorInterf().get(i).getInterfId());
            pelement.setDevice(monitorData.getVectorInterf().get(i).getDevice());
            pelement.setName(monitorData.getVectorInterf().get(i).getName());

            pMonitorData.getVectorInterf().add(pelement);

        }
        if (monitorData.getInterfVectorC() != null) {
            for (int i = 0; i < monitorData.getInterfVectorC().size(); i++) {
                SoapMessageListC pelement = new SoapMessageListC();
                pelement.setAcPoMAC(monitorData.getInterfVectorC().get(i).getAcPoMAC());
                pelement.setChannel(monitorData.getInterfVectorC().get(i).getChannel());
                pelement.setDevice(monitorData.getInterfVectorC().get(i).getDevice());
                pelement.setEssid(monitorData.getInterfVectorC().get(i).getEssid());
                pelement.setListCId(monitorData.getInterfVectorC().get(i).getListCId());
                pelement.setSignalLevel(monitorData.getInterfVectorC().get(i).getSignalLevel());
                pelement.setStatus(monitorData.getInterfVectorC().get(i).getStatus());

                pMonitorData.getInterfVectorC().add(pelement);

            }
        }
        pClient.putXml(pMonitorData);

    }

    //Method to stop thread
    public void requestStop() {
        stop = true;
    }
}
