/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.session.db;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author tdim
 */
@Entity
public class sessiontable implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String sessionID;
    private Long lastaccess;
    private int itype;
    private String pkikey;
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof sessiontable)) {
            return false;
        }
        sessiontable other = (sessiontable) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eu.esponder.session.db.sessiontable[ id=" + id + " ]";
    }

    /**
     * @return the sessionID
     */
    public String getSessionID() {
        return sessionID;
    }

    /**
     * @param sessionID the sessionID to set
     */
    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    /**
     * @return the lastaccess
     */
    public Long getLastaccess() {
        return lastaccess;
    }

    /**
     * @param lastaccess the lastaccess to set
     */
    public void setLastaccess(Long lastaccess) {
        this.lastaccess = lastaccess;
    }

    /**
     * @return the itype
     */
    public int getItype() {
        return itype;
    }

    /**
     * @param itype the itype to set
     */
    public void setItype(int itype) {
        this.itype = itype;
    }

    /**
     * @return the pkikey
     */
    public String getPkikey() {
        return pkikey;
    }

    /**
     * @param pkikey the pkikey to set
     */
    public void setPkikey(String pkikey) {
        this.pkikey = pkikey;
    }
    
}
