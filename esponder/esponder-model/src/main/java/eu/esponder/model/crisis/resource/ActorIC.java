/*
 * 
 */
package eu.esponder.model.crisis.resource;

import java.util.Set;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


// TODO: Auto-generated Javadoc
/**
 * The Class Actor incident commander.
 */
@Entity
@DiscriminatorValue("IC")
@NamedQueries({
	@NamedQuery(name="ActorIC.findByTitle", query="select a from ActorIC a where a.title=:title"),
	@NamedQuery(name="ActorIC.findAll", query="select a from ActorIC a")
})
public class ActorIC extends Actor {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1921649479237772588L;


	/** The supervisor. This is a supervisor for Meoc or EOC*/
	@ManyToOne
	@JoinColumn(name="CrisisManager")
	private ActorCM crisisManager;
	
	/** The operations centre. */
	@OneToOne(mappedBy="incidentCommander")
	private OCMeoc operationsCentre;

	/** The subordinates. If we are in meoc level this set describes the FR chiefs, If we are in EOC level this is the supervisors of Meoc*/
	@OneToMany(mappedBy="incidentCommander")
	private Set<FRTeam> frTeams;

	/**
	 * Gets the crisis manager.
	 *
	 * @return the crisis manager
	 */
	public ActorCM getCrisisManager() {
		return crisisManager;
	}

	/**
	 * Sets the crisis manager.
	 *
	 * @param crisisManager the new crisis manager
	 */
	public void setCrisisManager(ActorCM crisisManager) {
		this.crisisManager = crisisManager;
	}

	/**
	 * Gets the fr teams.
	 *
	 * @return the fr teams
	 */
	public Set<FRTeam> getFrTeams() {
		return frTeams;
	}

	/**
	 * Sets the fr teams.
	 *
	 * @param frTeams the new fr teams
	 */
	public void setFrTeams(Set<FRTeam> frTeams) {
		this.frTeams = frTeams;
	}

	/**
	 * Gets the supervisor.
	 *
	 * @return the supervisor
	 */
	public ActorCM getSupervisor() {
		return crisisManager;
	}

	/**
	 * Sets the supervisor.
	 *
	 * @param crisisManager the new supervisor
	 */
	public void setSupervisor(ActorCM crisisManager) {
		this.crisisManager = crisisManager;
	}

	/**
	 * Gets the operations centre.
	 *
	 * @return the operations centre
	 */
	public OCMeoc getOperationsCentre() {
		return operationsCentre;
	}

	/**
	 * Sets the operations centre.
	 *
	 * @param operationsCentre the new operations centre
	 */
	public void setOperationsCentre(OCMeoc operationsCentre) {
		this.operationsCentre = operationsCentre;
	}
	
}
