/*
 * 
 */
package eu.esponder.rest.portal;

import java.util.Date;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.codehaus.enunciate.XmlTransient;

import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.datafusion.DatafusionResultsDTO;
import eu.esponder.event.model.datafusion.DatafusionResultsGeneratedEvent;
import eu.esponder.eventadmin.ESponderEventPublisher;
import eu.esponder.rest.ESponderResource;

// TODO: Auto-generated Javadoc
/**
 * The Class RunSimulator.
 */
@Path("/portal/alarms")
@XmlTransient
public class AlarmGenerator extends ESponderResource {



	/**
	 * Run simulation.
	 *
	 * @param pkiKey the pki key
	 * @return the string
	 */
	@GET
	@Path("/run")
	@Produces({ MediaType.APPLICATION_JSON })
	public String RunAlarmGenerator(
			@QueryParam("frID") Long frID,
			@QueryParam("imei") String imei
			) {
		
		if(frID!=null) {
			ESponderEventPublisher<DatafusionResultsGeneratedEvent> publisher = 
					new ESponderEventPublisher<DatafusionResultsGeneratedEvent>(DatafusionResultsGeneratedEvent.class);
			
			DatafusionResultsGeneratedEvent event = new DatafusionResultsGeneratedEvent();
			DatafusionResultsDTO dfResult = new DatafusionResultsDTO();
			dfResult.setId(frID);
			dfResult.setDetails1("");
			dfResult.setResultsText1("Temperature rising , please proceeed with caution");
			dfResult.setResultsText2(String.valueOf(frID));
			dfResult.setResultsText3(imei);
			event.setEventAttachment(dfResult);
			event.setEventSeverity(SeverityLevelDTO.SERIOUS);
			event.setEventTimestamp(new Date());
			event.setJournalMessage("Alarm generated for FR with id : "+frID);
			event.setJournalMessageInfo("Alarm generated for FR with id : "+frID);
			
			try {
				publisher.publishEvent(event);
			} catch (Exception e) {
				return e.getMessage();
			}
			publisher.CloseConnection();
			return "Success";
		}
		else
			return "Something went wrong";
	}

		

}