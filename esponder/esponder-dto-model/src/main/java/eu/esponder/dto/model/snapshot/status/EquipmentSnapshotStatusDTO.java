/*
 * 
 */
package eu.esponder.dto.model.snapshot.status;


// TODO: Auto-generated Javadoc
/**
 * The Enum EquipmentSnapshotStatusDTO.
 */
public enum EquipmentSnapshotStatusDTO {
	
	/** The working. */
	WORKING,
	
	/** The malfunctioning. */
	MALFUNCTIONING,
	
	/** The damaged. */
	DAMAGED
}
