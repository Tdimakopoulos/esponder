/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.role.bean;

import eu.esponder.entity.HL7connections;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class HL7connectionsFacade extends AbstractFacade<HL7connections> {
    @PersistenceContext(unitName = "Esponder-PKI-InfastructurePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public HL7connectionsFacade() {
        super(HL7connections.class);
    }
    
}
