package eu.esponder.jaxb.model.snapshot.sensor.measurement;

import java.math.BigDecimal;

public class ArithmeticSensorMeasurementDTO extends SensorMeasurementDTO {

	private BigDecimal measurement;

	public BigDecimal getMeasurement() {
		return measurement;
	}

	public void setMeasurement(BigDecimal measurement) {
		this.measurement = measurement;
	}

}
