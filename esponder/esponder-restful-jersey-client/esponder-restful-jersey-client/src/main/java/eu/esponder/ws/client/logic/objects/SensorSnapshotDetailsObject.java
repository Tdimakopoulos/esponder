package eu.esponder.ws.client.logic.objects;

import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;

public class SensorSnapshotDetailsObject {

	SensorSnapshotDTO pSensorSnapshot= new SensorSnapshotDTO();
	ActorDTO pActor = new ActorDTO();
	EquipmentDTO pEquipment = new EquipmentDTO();
        
	public SensorSnapshotDTO getpSensorSnapshot() {
		return pSensorSnapshot;
	}
	public void setpSensorSnapshot(SensorSnapshotDTO pSensorSnapshot) {
		this.pSensorSnapshot = pSensorSnapshot;
	}
	public ActorDTO getpActor() {
		return pActor;
	}
	public void setpActor(ActorDTO pActor) {
		this.pActor = pActor;
	}
	public EquipmentDTO getpEquipment() {
		return pEquipment;
	}
	public void setpEquipment(EquipmentDTO pEquipment) {
		this.pEquipment = pEquipment;
	}
}
