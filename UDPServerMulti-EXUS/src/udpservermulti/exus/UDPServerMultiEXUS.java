/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package udpservermulti.exus;

import java.net.DatagramSocket;
import udp.server.event.publish.PublishEventMeasurment;

/**
 *
 * @author tdim
 */
public class UDPServerMultiEXUS {

    private static int threads = 0;
//public static void main(String[] args) {
//                    PublishEventMeasurment pM = new PublishEventMeasurment();
//                pM.PublishM("11,11,11a", "N/A");
//}
                
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Connection Parameters
        DatagramSocket socket = null;
        int maxThreads = 15;             // Max threads at any time

       
        int serverPort = 9898;

        try {
            // Setup socket
            socket = new DatagramSocket(serverPort);
            System.out.println("Server Started");

            while (true) {
              //  System.out.println("****"+threads+" "+maxThreads);
                if (threads < maxThreads) {
                    
                    ServerThread server = new ServerThread(socket);
                    new Thread(server).start();
                    threads++;
                    //System.out.println("Number of threads active: " + threads);
                }
                if(threads==0)
                    {
                    
                    ServerThread server = new ServerThread(socket);
                    new Thread(server).start();
                    threads++;
                    //System.out.println("Number of threads active: " + threads);
                }
                
            }
        } catch (Exception e) {
            System.out.println("Error Main loop : " + e.getMessage());
        } finally {
            if (socket != null) {
                socket.close();
            }
        }
    }

    // Setter for number of active threads
    public static void decNumThreads() {
        threads--;
        //System.out.println(" -- Number of threads active: " + threads);
    }
}
