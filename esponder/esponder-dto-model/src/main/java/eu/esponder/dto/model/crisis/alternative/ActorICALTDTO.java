/*
 * 
 */
package eu.esponder.dto.model.crisis.alternative;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;


// TODO: Auto-generated Javadoc
/**
 * The Class ActorICALTDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "title",  "personnel", "voIPURL",
	"crisisManager", "frTeams","operationsCentre"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ActorICALTDTO extends ActorALTDTO{


	

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6367759504421845598L;


	/**
	 * Instantiates a new actor icaltdto.
	 */
	public ActorICALTDTO() {}

	/** The crisis manager. */
	private Long crisisManager;

	/** The fr teams. */
	private Set<FRTeamALTDTO> frTeams;
	
	/** The operations centre. */
	private Long operationsCentre;
	
	
	/**
	 * Gets the fr teams.
	 *
	 * @return the fr teams
	 */
	public Set<FRTeamALTDTO> getFrTeams() {
		return frTeams;
	}

	/**
	 * Sets the fr teams.
	 *
	 * @param frTeams the new fr teams
	 */
	public void setFrTeams(Set<FRTeamALTDTO> frTeams) {
		this.frTeams = frTeams;
	}

	/**
	 * Gets the crisis manager.
	 *
	 * @return the crisis manager
	 */
	public Long getCrisisManager() {
		return crisisManager;
	}

	/**
	 * Sets the crisis manager.
	 *
	 * @param crisisManager the new crisis manager
	 */
	public void setCrisisManager(Long crisisManager) {
		this.crisisManager = crisisManager;
	}

	/**
	 * Gets the operations centre.
	 *
	 * @return the operations centre
	 */
	public Long getOperationsCentre() {
		return operationsCentre;
	}

	/**
	 * Sets the operations centre.
	 *
	 * @param operationsCentre the new operations centre
	 */
	public void setOperationsCentre(Long operationsCentre) {
		this.operationsCentre = operationsCentre;
	}

	
	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.crisis.alternative.ActorALTDTO#toString()
	 */
	@Override
	public String toString() {
		return this.getClass().getName()+" [id=" + id + ", title=" + title + "]";
	}
	
}
