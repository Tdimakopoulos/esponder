/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.logistics;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.PersonnelDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredReusableResourceDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.crisis.resource.ReusableResourceDTO;
import eu.esponder.dto.model.type.CrisisTypeDTO;
import eu.esponder.jsfutils.JSFUtils;
import eu.esponder.objects.Logistic;
import eu.esponder.objects.OperationCenter;
import eu.esponder.objects.ResourcePlanInfo;
import eu.esponder.objects.person;
import eu.esponder.personnel.*;
import eu.esponder.ws.client.logic.CrisisResourceManager;
import eu.esponder.ws.client.logic.ResourcePlanInfoQueries;
import eu.esponder.ws.client.query.QueryManager;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.primefaces.event.DragDropEvent;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class LogisticsManager {

    private List<Logistic> personnel;
    private List<Logistic> selectedPersonnel;
    private List<ResourcePlanInfo> pPersonnelPlanInfo;
    private String logistictoremove;
    private String crisiscontextparam;

    private void PopulateLogistics() {

        try {
            ResourcePlanInfoQueries pFind = new ResourcePlanInfoQueries();
            QueryManager pQuery = new QueryManager();
            ResultListDTO pResults = pQuery.getRegisterdConsumablesAll("1");
            for (int i = 0; i < pResults.getResultList().size(); i++) {
                RegisteredConsumableResourceDTO pItem = (RegisteredConsumableResourceDTO) pResults.getResultList().get(i);
                // fix me data need update   
                Logistic pperson = new Logistic(pItem.getTitle(), pItem.getId().intValue(), "con", pFind.GetresourceCategoryforRegConsumables(pItem.getConsumableResourceCategoryId()));
                // pperson.setLat(pItem.getOrganisation().getLocation().getLatitude());
                //pperson.setLon(pItem.getOrganisation().getLocation().getLongitude());
                //pperson.setTitle(pItem.getTitle());
                //pperson.setOrganization(pItem.getOrganisation().getTitle());



            if(pItem.getStatus()==ResourceStatusDTO.AVAILABLE)
                personnel.add(pperson);
            }
        } catch (JsonParseException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JsonMappingException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        }


        try {
            ResourcePlanInfoQueries pFind = new ResourcePlanInfoQueries();
            QueryManager pQuery = new QueryManager();
            ResultListDTO pResults = pQuery.getRegisterdReusablesAll("1");
            for (int i = 0; i < pResults.getResultList().size(); i++) {
                RegisteredReusableResourceDTO pItem = (RegisteredReusableResourceDTO) pResults.getResultList().get(i);
                // fix me data need update   
                Logistic pperson = new Logistic(pItem.getTitle(), pItem.getId().intValue(), "reu", pFind.GetresourceCategoryforRegResuable(pItem.getReusableResourceCategoryId()));
                // pperson.setLat(pItem.getOrganisation().getLocation().getLatitude());
                //pperson.setLon(pItem.getOrganisation().getLocation().getLongitude());
                //pperson.setTitle(pItem.getTitle());
                //pperson.setOrganization(pItem.getOrganisation().getTitle());




                if(pItem.getStatus()==ResourceStatusDTO.AVAILABLE)
                personnel.add(pperson);
            }
        } catch (JsonParseException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JsonMappingException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void PopulateActionPlanInfo() {
        // pPersonnelPlanInfo.add(new ResourcePlanInfo("Tent",20,0));
//        pPersonnelPlanInfo.add(new ResourcePlanInfo("Water",500,0));
        String plantitle = "0";
        crisiscontextparam = (String) JSFUtils.getParameter("TestParam");
        QueryManager pq = new QueryManager();
        try {
            System.out.println("param->" + this.crisiscontextparam);
            CrisisContextDTO pcc = pq.getCrisisContextID("1", this.crisiscontextparam);
            System.out.print(pcc.getTitle());
            CrisisTypeDTO[] pct = pcc.getCrisisTypes().toArray(new CrisisTypeDTO[pcc.getCrisisTypes().size()]);
            if (pct.length >= 0) {
                plantitle = pct[0].getTitle();
            }

        } catch (JsonParseException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JsonMappingException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            ResourcePlanInfoQueries pinfoQuery = new ResourcePlanInfoQueries();
            pPersonnelPlanInfo = pinfoQuery.GetQueryLists(2, plantitle);
            //getpPersonnelPlanInfo().add(new ResourcePlanInfo("Police Officer",10,0));
            //getpPersonnelPlanInfo().add(new ResourcePlanInfo("Fire Fighter Special",5,0));
            //getpPersonnelPlanInfo().add(new ResourcePlanInfo("Fire Fighter Special",5,0));
        } catch (IOException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public LogisticsManager() {
        personnel = new ArrayList<Logistic>();
        selectedPersonnel = new ArrayList<Logistic>();
        pPersonnelPlanInfo = new ArrayList<ResourcePlanInfo>();
        PopulateLogistics();
        PopulateActionPlanInfo();



    }

    public List<Logistic> getPersonnel() {
        return personnel;
    }

    public List<Logistic> getSelectedPersonnel() {
        return selectedPersonnel;
    }

    private void UpdateSelectedInfo(String szPosition, int iadd) {
        for (int i = 0; i < pPersonnelPlanInfo.size(); i++) {
            ResourcePlanInfo pElement = pPersonnelPlanInfo.get(i);
            if (pElement.getPtype().equalsIgnoreCase(szPosition)) {
                if (iadd == 1) {
                    int iselected = pElement.getIselected();
                    int ineeded = pElement.getIneeded();
                    pElement.setIselected(iselected + 1);
                    pElement.setIneeded(ineeded - 1);
                }
                if (iadd == -1) {
                    int iselected = pElement.getIselected();
                    int ineeded = pElement.getIneeded();
                    pElement.setIselected(iselected - 1);
                    pElement.setIneeded(ineeded + 1);
                }
            }
        }
    }
    private int FindPersonPosition(String szTitle)
    {
        for(int i=0;i<selectedPersonnel.size();i++)
        {
            if (szTitle.equalsIgnoreCase(selectedPersonnel.get(i).getName()))
                return i;
        }
        return -1;
    }
    public void onPersonDrop(DragDropEvent event) {
        Logistic player = (Logistic) event.getData();
        UpdateSelectedInfo(player.getPosition(), 1);
        selectedPersonnel.add(player);
        
        personnel.remove(player);
        CrisisResourceManager pManager = new CrisisResourceManager();
        if (player.getPhoto().equalsIgnoreCase("reu")) {
            try {
                ReusableResourceDTO preturn=pManager.AssociateReusable(crisiscontextparam, String.valueOf(player.getNumber()), "1");
                int ipos=FindPersonPosition(player.getName());//selectedPersonnel.indexOf(player);
                
                player.setNumber2(preturn.getId().intValue());
                selectedPersonnel.set(ipos, player);
            } catch (JsonGenerationException ex) {
                Logger.getLogger(LogisticsManager.class.getName()).log(Level.SEVERE, null, ex);
            } catch (JsonMappingException ex) {
                Logger.getLogger(LogisticsManager.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(LogisticsManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else
        {
            try {
                ConsumableResourceDTO preturn1=pManager.AssociateConsumable(crisiscontextparam, String.valueOf(player.getNumber()), "1");
                int ipos=FindPersonPosition(player.getName());//selectedPersonnel.indexOf(player);
                player.setNumber2(preturn1.getId().intValue());
                selectedPersonnel.set(ipos, player);
            } catch (JsonGenerationException ex) {
                Logger.getLogger(LogisticsManager.class.getName()).log(Level.SEVERE, null, ex);
            } catch (JsonMappingException ex) {
                Logger.getLogger(LogisticsManager.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(LogisticsManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        clearlist();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(player.getName() + " added", "Position:" + event.getDropId()));
    }

    private void clearlist() {
        for (int i = 0; i < selectedPersonnel.size(); i++) {
            for (int d = 0; d < personnel.size(); d++) {
                if (selectedPersonnel.get(i).getName().equalsIgnoreCase(personnel.get(d).getName())) {
                    System.out.println("Remove : " + personnel.get(d).getName());
                    personnel.remove(d);
                }
            }
        }
    }

    public String removeperson() {
        int ifind = Integer.parseInt(logistictoremove);
        for (int i = 0; i < selectedPersonnel.size(); i++) {
            if (selectedPersonnel.get(i).getNumber() == ifind) {
                Logistic pelement = selectedPersonnel.get(i);
                UpdateSelectedInfo(pelement.getPosition(), -1);
                Logistic player= selectedPersonnel.get(i);
                selectedPersonnel.remove(i);
                CrisisResourceManager pManager = new CrisisResourceManager();
                if (player.getPhoto().equalsIgnoreCase("reu")) {
                    try {
                        pManager.deAssociateReusable(String.valueOf(player.getNumber2()), "1");
                    } catch (JsonGenerationException ex) {
                        Logger.getLogger(LogisticsManager.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (JsonMappingException ex) {
                        Logger.getLogger(LogisticsManager.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(LogisticsManager.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else
                {
                    try {
                        pManager.deAssociateConsumable(String.valueOf(player.getNumber2()), "1");
                    } catch (JsonGenerationException ex) {
                        Logger.getLogger(LogisticsManager.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (JsonMappingException ex) {
                        Logger.getLogger(LogisticsManager.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(LogisticsManager.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                personnel.add(pelement);
            }
        }
        return "CrisisLogistics?faces-redirect=true";
    }

    /**
     * @return the pPersonnelPlanInfo
     */
    /**
     * @return the pPersonnelPlanInfo
     */
    public List<ResourcePlanInfo> getpPersonnelPlanInfo() {
        return pPersonnelPlanInfo;
    }

    /**
     * @param pPersonnelPlanInfo the pPersonnelPlanInfo to set
     */
    public void setpPersonnelPlanInfo(List<ResourcePlanInfo> pPersonnelPlanInfo) {
        this.pPersonnelPlanInfo = pPersonnelPlanInfo;
    }

    /**
     * @return the logistictoremove
     */
    public String getLogistictoremove() {
        return logistictoremove;
    }

    /**
     * @param logistictoremove the logistictoremove to set
     */
    public void setLogistictoremove(String logistictoremove) {
        this.logistictoremove = logistictoremove;
    }

    /**
     * @return the crisiscontextparam
     */
    public String getCrisiscontextparam() {
        return crisiscontextparam;
    }

    /**
     * @param crisiscontextparam the crisiscontextparam to set
     */
    public void setCrisiscontextparam(String crisiscontextparam) {
        System.out.println("setter called : " + crisiscontextparam);
        this.crisiscontextparam = crisiscontextparam;
    }
}
