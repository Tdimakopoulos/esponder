/*
 * 
 */
package eu.esponder.dto.model.crisis.action;



// TODO: Auto-generated Javadoc
// 
/**
 * The Enum ActionOperationEnumDTO.
 */
public enum CrisisContextStatusEnumDTO  {

	/** The move. */
	STARTUP,
	
	/** The transport. */
	INPROGRESS,
	
	/** The fix. */
	FINISHED
}
