/*
 * 
 */
package eu.esponder.dto.model.crisis.resource;

//import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.view.VoIPURLDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class ActorDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "type", "title", "status", "personnel", "voIPURL"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ActorDTO extends ResourceDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 660813303586822307L;

	/**
	 * Instantiates a new actor dto.
	 */
	public ActorDTO() { }
	
	/** The voip url. */
	private VoIPURLDTO voIPURL;
	
	/** The personnel. */
	private PersonnelDTO personnel;

	

	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.crisis.resource.ResourceDTO#toString()
	 */
	@Override
	public String toString() {
		return "ActorDTO [type=" + type + ", status=" + status + ", id=" + id
				+ ", title=" + title + "]";
	}

	/**
	 * Gets the vo ipurl.
	 *
	 * @return the vo ipurl
	 */
	public VoIPURLDTO getVoIPURL() {
		return voIPURL;
	}

	/**
	 * Sets the vo ipurl.
	 *
	 * @param voIPURL the new vo ipurl
	 */
	public void setVoIPURL(VoIPURLDTO voIPURL) {
		this.voIPURL = voIPURL;
	}

	/**
	 * Gets the personnel.
	 *
	 * @return the personnel
	 */
	public PersonnelDTO getPersonnel() {
		return personnel;
	}

	/**
	 * Sets the personnel.
	 *
	 * @param personnel the new personnel
	 */
	public void setPersonnel(PersonnelDTO personnel) {
		this.personnel = personnel;
	}

	

}
