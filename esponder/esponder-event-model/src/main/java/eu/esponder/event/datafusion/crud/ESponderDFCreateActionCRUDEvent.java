/*
 * 
 */
package eu.esponder.event.datafusion.crud;

import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.event.model.CreateEvent;
import eu.esponder.event.model.ESponderEvent;



// TODO: Auto-generated Javadoc
/**
 * The Class CreateActionEvent.
 */
public class ESponderDFCreateActionCRUDEvent extends ESponderEvent<ActionDTO> implements CreateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2006035559724305579L;

	/**
	 * Instantiates a new creates the action event.
	 */
	public ESponderDFCreateActionCRUDEvent(){
		setJournalMessageInfo("Create Action Event");
	}
}
