package eu.esponder.event.mobile;

import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.event.model.ESponderEvent;

public class MobileUIAlarmIndicator extends ESponderEvent<ESponderUserDTO> {

	private static final long serialVersionUID = 3199488168824595130L;
	private String imei;
	private boolean alarmindicator;
	String szMessage;
	
	
	public String getSzMessage() {
		return szMessage;
	}

	public void setSzMessage(String szMessage) {
		this.szMessage = szMessage;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public boolean isAlarmindicator() {
		return alarmindicator;
	}

	public void setAlarmindicator(boolean alarmindicator) {
		this.alarmindicator = alarmindicator;
	}

}
