/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package esponder.biomed.lib;

import java.util.LinkedList;
import java.util.List;


/**
 *
 * @author afa
 */
public class BiomedAFrameParser{

    protected BiomedAFrame frame = null;
    protected BiomedACommand cmds[] = null;
    protected RegularDataBlock rdbs[] = null;

    public BiomedAFrameParser() {
    }
    
    public void setFrame(byte buffer[]){
        this.frame = new BiomedAFrame(buffer);
    }

    /**
     * Parse the frame and extract command responses and data payloads
     * The commands responoses and the payloads extracted are accessible then via the getters
     */
    public void process() {
        List<BiomedACommand> cmdList = new LinkedList<BiomedACommand>();
        List<RegularDataBlock> dataList = new LinkedList<RegularDataBlock>();
        Payload payload = null;
        BiomedAPacket packets[] = null;

        if (this.frame.isValid()) {
            this.frame.process();
            packets = this.frame.getPackets();
            if (packets != null) {
                for (int i = 0; i < packets.length; i++) {
                    payload = packets[i].getPayload();
                    if (payload instanceof BiomedACommand) {
                        cmdList.add((BiomedACommand) payload);
                    }
                    if (payload instanceof BiomedAData) {
                        ((BiomedAData) payload).parseHeader();
                        dataList.add(((BiomedAData) payload).getDataBlock());
                    }
                }
            }
        }
        else
        {
            //System.out.println("Invalid Frame");
        }
        if (cmdList != null) {
            this.cmds = new BiomedACommand[cmdList.size()];
            for (int i = 0; i < this.cmds.length; i++) {
                this.cmds[i] = cmdList.get(i);
            }
        }
        //this.cmds = (SewCommand[]) cmdList.toArray();
        if (dataList != null) {
            this.rdbs = new RegularDataBlock[dataList.size()];
            for (int i = 0; i < this.rdbs.length; i++) {
                this.rdbs[i] = dataList.get(i);
            }
        }
    }

    /**
     * @return the cmdscontained in the packets of the actual frame
     */
    public BiomedACommand[] getCmds() {
        return cmds;
    }

    /**
     * @return the rdbs contained in the packets of the actual frame
     */
    public RegularDataBlock[] getDataBlocks() {
        return rdbs;
    }
}
