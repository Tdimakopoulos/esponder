package eu.esponder.controller.crisis.terrain;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.TerrainAndVegetationDTO;

@Remote
public interface TerrainRemoteService {

	public TerrainAndVegetationDTO findTerrainAndVegetationDTOById(
			Long terrainAndVegetationID);

	public TerrainAndVegetationDTO findTerrainAndVegetationByTitleRemote(
			String tvTitle, Long userId);

	public TerrainAndVegetationDTO createTerrainAndVegetationRemote(
			TerrainAndVegetationDTO terrainAndVegetationDTO, Long userID);

	public void deleteTerrainAndVegetationRemote(Long tvID, Long userID);

	public TerrainAndVegetationDTO updateTerrainAndVegetationRemote(
			TerrainAndVegetationDTO terrainAndVegetationDTO, Long userID);
	
	
}
