package eu.esponder.security.keymanager;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

import javax.security.cert.Certificate;

public class KeyStoreManager {

	public void CreateKeyStore(String CerFile) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException
	{
		  
		KeyStore ks = KeyStore.getInstance("JKS");  
		  
		ks.load( null, null );  
		  
		FileInputStream fis = new FileInputStream( CerFile );  
		BufferedInputStream bis = new BufferedInputStream(fis);  
		  
		CertificateFactory cf = CertificateFactory.getInstance( "X.509" );  
		  
		java.security.cert.Certificate cert = null;  
		  
		while ( bis.available() > 0 )  
		{  
		 cert = cf.generateCertificate( bis );  
		 ks.setCertificateEntry( "SGCert", cert );  
		}  
		  
		ks.setCertificateEntry( "SGCert", cert );  
		  
		ks.store( new FileOutputStream( "EsponderKeyStore" ), "esponder12".toCharArray() );  
	}
}
