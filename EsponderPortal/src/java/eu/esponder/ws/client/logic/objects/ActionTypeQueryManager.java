package eu.esponder.ws.client.logic.objects;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.type.ActionTypeDTO;
import eu.esponder.ws.client.query.QueryManager;

public class ActionTypeQueryManager {

	List<ActionTypeDTO> pActionTypes = new ArrayList<ActionTypeDTO>();
	
	
	public void LoadAllCrisisTypes() throws JsonParseException, JsonMappingException, IOException {
		QueryManager pMan = new QueryManager();
		ResultListDTO pre = pMan.getAllTypes("1");
		
		for (int i = 0; i < pre.getResultList().size(); i++) {
			if (pre.getResultList().get(i) instanceof ActionTypeDTO) {
				ActionTypeDTO pitem=(ActionTypeDTO)pre.getResultList().get(i);
				pActionTypes.add(pitem);
			}
			
		}
	}


	public List<ActionTypeDTO> getpActionTypes() {
		return pActionTypes;
	}


	public void setpActionTypes(List<ActionTypeDTO> pActionTypes) {
		this.pActionTypes = pActionTypes;
	}
}
