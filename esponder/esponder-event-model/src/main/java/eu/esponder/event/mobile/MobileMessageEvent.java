package eu.esponder.event.mobile;

import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.event.model.ESponderEvent;

public class MobileMessageEvent extends ESponderEvent<ESponderUserDTO>{

	private static final long serialVersionUID = 5153096928395487133L;
		
	String mobileimei;
	String sourceEoc;
	String sourceMEoc;
	String sourceMobIMEI;
	String szMessage;
	
	public String getMobileimei() {
		return mobileimei;
	}

	public void setMobileimei(String mobileimei) {
		this.mobileimei = mobileimei;
	}

	public String getSourceEoc() {
		return sourceEoc;
	}

	public void setSourceEoc(String sourceEoc) {
		this.sourceEoc = sourceEoc;
	}

	public String getSourceMEoc() {
		return sourceMEoc;
	}

	public void setSourceMEoc(String sourceMEoc) {
		this.sourceMEoc = sourceMEoc;
	}

	public String getSourceMobIMEI() {
		return sourceMobIMEI;
	}

	public void setSourceMobIMEI(String sourceMobIMEI) {
		this.sourceMobIMEI = sourceMobIMEI;
	}

	public String getSzMessage() {
		return szMessage;
	}

	public void setSzMessage(String szMessage) {
		this.szMessage = szMessage;
	}

	
}
