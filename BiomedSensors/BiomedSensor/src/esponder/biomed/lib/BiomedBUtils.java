/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package esponder.biomed.lib;

/**
 *
 * @author afa
 */
public class BiomedBUtils {

//Sampling frequency
    public static final float ECG_SF = 321.25f;
    public static final float ECG_FIL_SF = 107.09f;
    public static final float IMP_SF = 32.13f;
    public static final float IMP_FIL_SF = 10.71f;
    public static final float VREF_MEAS_SF = 32.13f;
    public static final float ACC_SF = 100.0f;
    public static final float HR_SF = 0.2f;
    public static final float BR_SF = 0.2f;
    public static final float ACT_SF = 0.2f;
    public static final float CAD_SF = 0.2f;
    public static final float STEP_SF = 0.2f;
    public static final float SPEED_SF = 0.2f;
    public static final float DIST_SF = 0.2f;
    public static final float SKIN_DET_MEAS_SF = 32.13f;
    public static final float AMB_TEMP_SF = 1.0f;
    public static final float AMB_PRESS_SF = 0.2f;
    public static final float BODY_TEMP_SF = 0.2f;
    public static final float ALTITUDE_SF = 0.2f;
//Scaling factors
    public static final float ECG_FIL_FACTOR = (float) (2.4 * 1000 / (Math.pow(2.0, 12.0) * 214));
    public static final float HR_QI_FACTOR = (1.0f / 255.0f);
    public static final float IMP_FIL_FACTOR = 0.0115f;
    public static final float BR_QI_FACTOR = (1.0f / 255.0f);
    public static final float ACT_LVL_FACTOR = (1.0f / 1024.0f);
    public static final float ACT_QI_FACTOR = (1.0f / 255.0f);
    public static final float SPEED_FACTOR = (1.0f / 8.0f);
    public static final float SPEED_AVG_FACTOR = (1.0f / 8.0f);
    public static final float DIST_FACTOR = (1.0f);
    public static final float SBT_FACTOR = (1.0f / 100.0f);
    public static final float SBT_AVG_FACTOR = (1.0f / 100.0f);
    public static final float EXTT_FACTOR = (1.0f / 100.0f);
    public static final float EXTT_AVG_FACTOR = (1.0f / 100.0f);
    public static final float ALT_FACTOR = 1.0f;
    public static final float ALT_QI_FACTOR = (1.0f / 100.0f);
    public static final float ASCENT_FACTOR = 1.0f;
    public static final float DESCENT_FACTOR = 1.0f;
    public static final float ACC_FACTOR = (1.0f / 256.0f);
    public static BiomedBDictionary dictionary=new BiomedBDictionary();
    

    //name and scale factor
    public static String[] getParams(Signal signals[], int id) {
        String params[] = {"","1"};
        if(signals != null){
            for (int i = 0; i < signals.length; i++) {
                if (signals[i].getId() == id) {
                    params[0] = signals[i].getName();
                    params[1] = Float.toString(signals[i].getScaleFactor());
                    break;
                }
            }
        }
        return params;
    }
}
