package eu.esponder.event.mobile;

import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.event.model.ESponderEvent;

public class MobileFileResponseEvent extends ESponderEvent<ESponderUserDTO> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1464139627625801477L;
	
	String fileid;
	boolean received;
	private String imei;
	int ierrortype;
	
	public int getIerrortype() {
		return ierrortype;
	}
	public void setIerrortype(int ierrortype) {
		this.ierrortype = ierrortype;
	}
	public String getFileid() {
		return fileid;
	}
	public void setFileid(String fileid) {
		this.fileid = fileid;
	}
	public boolean isReceived() {
		return received;
	}
	public void setReceived(boolean received) {
		this.received = received;
	}
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	
	

}
