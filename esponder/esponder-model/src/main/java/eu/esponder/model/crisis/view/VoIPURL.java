/*
 * 
 */
package eu.esponder.model.crisis.view;

import javax.persistence.Embeddable;


// TODO: Auto-generated Javadoc
/**
 * The Class VoIPURL.
 */
@Embeddable
public class VoIPURL extends URL {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3193388189632590549L;

	/**
	 * Instantiates a new VoIP URL.
	 */
	public VoIPURL() {
		this.setProtocol("sip");
	}
}
