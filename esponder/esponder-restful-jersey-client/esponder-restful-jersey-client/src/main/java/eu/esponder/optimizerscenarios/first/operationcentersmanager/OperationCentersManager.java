package eu.esponder.optimizerscenarios.first.operationcentersmanager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredOperationsCentreDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.optimizerscenarios.first.objects.OperationCenter;
import eu.esponder.ws.client.logic.ResourcePlanInfoQueries;
import eu.esponder.ws.client.query.QueryManager;

public class OperationCentersManager {

	List<OperationCenter> poclist;

	public List<OperationCenter> GetOCList()
	{
		return poclist;
	}
	
	public OperationCentersManager()
	{
		poclist = new ArrayList<OperationCenter>();
		PopulateOCs();
	}
	
	private void PopulateOCs() {

		try {
			ResourcePlanInfoQueries pFind = new ResourcePlanInfoQueries();
			QueryManager pQuery = new QueryManager();
			ResultListDTO pResults = pQuery.getRegisterdOCAll("1");
			for (int i = 0; i < pResults.getResultList().size(); i++) {
				
				RegisteredOperationsCentreDTO pItem = (RegisteredOperationsCentreDTO) pResults.getResultList().get(i);
				
				OperationCenter pperson = new OperationCenter(pItem.getTitle(),
						pItem.getId().intValue(),
						pFind.GetresourceCategoryforRegOC(pItem.getOperationsCentreCategoryId()));
				
				if (pItem.getStatus() == ResourceStatusDTO.AVAILABLE)
					poclist.add(pperson);
				
			}
		} catch (JsonParseException ex) {

		} catch (JsonMappingException ex) {

		} catch (IOException ex) {

		}
	}
	
	public void PrintOC()
	{
		System.out.println(" ");
		System.out.println(" ");
		for (int i = 0; i < poclist.size(); i++) {
			System.out.println("id: " + poclist.get(i).getNumber());
			System.out.println("Name: " + poclist.get(i).getName());
			System.out.println("Position: " + poclist.get(i).getCategory());
			System.out.println(" ");
			System.out.println(" ");
		}
	}
}
