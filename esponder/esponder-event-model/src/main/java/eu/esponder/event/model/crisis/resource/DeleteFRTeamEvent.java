/*
 * 
 */
package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.FRTeamDTO;
import eu.esponder.event.model.DeleteEvent;



// TODO: Auto-generated Javadoc
/**
 * The Class CreateActorEvent.
 */
public class DeleteFRTeamEvent extends FRTeamEvent<FRTeamDTO> implements DeleteEvent {

		


	/**
	 * 
	 */
	private static final long serialVersionUID = -7056159958529021157L;

	/**
	 * Instantiates a new creates the actor event.
	 */
	public DeleteFRTeamEvent(){
		setJournalMessageInfo("DeleteFRTeamEvent");
	}
	
}
