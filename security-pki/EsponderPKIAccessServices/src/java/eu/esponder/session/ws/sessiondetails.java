/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.session.ws;

import java.util.List;

/**
 *
 * @author tdim
 */
public class sessiondetails {
    private String sessionID;
    private String role;
    private List<String> accfunc;
    private Long userID;
    

    /**
     * @return the sessionID
     */
    public String getSessionID() {
        return sessionID;
    }

    /**
     * @param sessionID the sessionID to set
     */
    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    /**
     * @return the role
     */
    public String getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * @return the accfunc
     */
    public List<String> getAccfunc() {
        return accfunc;
    }

    /**
     * @param accfunc the accfunc to set
     */
    public void setAccfunc(List<String> accfunc) {
        this.accfunc = accfunc;
    }

    /**
     * @return the userID
     */
    public Long getUserID() {
        return userID;
    }

    /**
     * @param userID the userID to set
     */
    public void setUserID(Long userID) {
        this.userID = userID;
    }

    
}
