/*
 * 
 */
package eu.esponder.event.model.login;

import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.event.model.crisis.resource.ActorEvent;
import eu.esponder.event.model.crisis.resource.EsponderUserEvent;

// TODO: Auto-generated Javadoc
/**
 * The Class LoginErrorEsponderUserEvent.
 */
public class LoginErrorEsponderUserEvent extends EsponderUserEvent<ESponderUserDTO> implements LoginError {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5408128404989456038L;
	
	private String loginErrorCode;
	private String loginErrorMsg;
	
String IMEI;
	
public String getIMEI() {
	return IMEI;
}

public void setIMEI(String iMEI) {
	IMEI = iMEI;
}
	
	public String getLoginErrorCode() {
		return loginErrorCode;
	}

	public void setLoginErrorCode(String loginErrorCode) {
		this.loginErrorCode = loginErrorCode;
	}

	public String getLoginErrorMsg() {
		return loginErrorMsg;
	}

	public void setLoginErrorMsg(String loginErrorMsg) {
		this.loginErrorMsg = loginErrorMsg;
	}

	/**
	 * Instantiates a new login error esponder user event.
	 */
	public LoginErrorEsponderUserEvent(){
		setJournalMessageInfo("LoginErrorEsponderUserEvent");
	}

}
