
package eu.esponder.datasync.server.web.rest.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Identity {

	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	
}
