/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author tdim
 */
@Entity
@Table(name="es_patientconnections")
public class patientconnections implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long idcontact;
    private Long idpersonnal;
    private Long idaddress;
    private Long idpatient;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof patientconnections)) {
            return false;
        }
        patientconnections other = (patientconnections) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eu.nephron.entity.patientconnections[ id=" + id + " ]";
    }

    /**
     * @return the idcontact
     */
    public Long getIdcontact() {
        return idcontact;
    }

    /**
     * @param idcontact the idcontact to set
     */
    public void setIdcontact(Long idcontact) {
        this.idcontact = idcontact;
    }

    /**
     * @return the idpersonnal
     */
    public Long getIdpersonnal() {
        return idpersonnal;
    }

    /**
     * @param idpersonnal the idpersonnal to set
     */
    public void setIdpersonnal(Long idpersonnal) {
        this.idpersonnal = idpersonnal;
    }

    /**
     * @return the idaddress
     */
    public Long getIdaddress() {
        return idaddress;
    }

    /**
     * @param idaddress the idaddress to set
     */
    public void setIdaddress(Long idaddress) {
        this.idaddress = idaddress;
    }

    /**
     * @return the idpatient
     */
    public Long getIdpatient() {
        return idpatient;
    }

    /**
     * @param idpatient the idpatient to set
     */
    public void setIdpatient(Long idpatient) {
        this.idpatient = idpatient;
    }
    
}
