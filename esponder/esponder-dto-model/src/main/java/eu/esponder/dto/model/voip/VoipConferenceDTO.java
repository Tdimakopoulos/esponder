package eu.esponder.dto.model.voip;



import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.ESponderEntityDTO;



@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id","connectionID","conferenceID","conferenceTitle","istatus"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class VoipConferenceDTO extends ESponderEntityDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5862759086706188354L;
	/** The id. */
	
	protected Long id;
	
	Long connectionID;
	
	Long conferenceID;
	
	String conferenceTitle;
	
	int istatus;
	
	
	public Long getConnectionID() {
		return connectionID;
	}

	public void setConnectionID(Long connectionID) {
		this.connectionID = connectionID;
	}

	public Long getConferenceID() {
		return conferenceID;
	}

	public void setConferenceID(Long conferenceID) {
		this.conferenceID = conferenceID;
	}

	public String getConferenceTitle() {
		return conferenceTitle;
	}

	public void setConferenceTitle(String conferenceTitle) {
		this.conferenceTitle = conferenceTitle;
	}

	public int getIstatus() {
		return istatus;
	}

	public void setIstatus(int istatus) {
		this.istatus = istatus;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}
}
