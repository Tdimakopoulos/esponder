/*
 * 
 */
package eu.esponder.rest.crisis.test;

import java.io.IOException;
import java.math.BigDecimal;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import eu.esponder.dto.model.security.KeyStorageDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.rest.ESponderResource;


// TODO: Auto-generated Javadoc
/**
 * The Class TestResource.
 */
@Path("/crisis/contextTest")
public class TestResource extends ESponderResource {

	

	/**
	 * Gets the rest test.
	 *
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the rest test
	 */
	@GET
	@Path("/test")
	@Produces({MediaType.APPLICATION_JSON})
	public PointDTO getRestTest(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		PointDTO point = new PointDTO();

		point.setAltitude(new BigDecimal(1));
		point.setLongitude(new BigDecimal(2));
		point.setLatitude(new BigDecimal(3));

		return point; 
	}


	/**
	 * Post rest test.
	 *
	 * @param point the point
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the point dto
	 */
	@POST
	@Path("/testPost")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public PointDTO postRestTest(
			@NotNull(message="point may not be null") PointDTO point, 
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		//		PointDTO anotherPoint = new PointDTO();
		System.out.println("Server: POST operation received");
		System.out.println(point.toString());
		point.setAltitude(new BigDecimal(55L));
		System.out.println("Server: POST operation returning");
		return point;
	}

}
