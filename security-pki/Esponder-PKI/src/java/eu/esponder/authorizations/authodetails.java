/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.authorizations;

/**
 *
 * @author tdim
 */
public class authodetails {
    private Long id;
    private String role;
    private String fnc;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the role
     */
    public String getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * @return the fnc
     */
    public String getFnc() {
        return fnc;
    }

    /**
     * @param fnc the fnc to set
     */
    public void setFnc(String fnc) {
        this.fnc = fnc;
    }
}
