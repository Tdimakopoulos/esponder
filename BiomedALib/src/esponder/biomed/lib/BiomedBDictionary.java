/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package esponder.biomed.lib;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author afa
 */
 class BiomedBDictionary extends Payload{
    public List <BiomedBDictionaryEntry> entryList;
    
    public BiomedBDictionary(){
        this.entryList = new LinkedList<BiomedBDictionaryEntry>();
    }
    
    public BiomedBDictionary(byte stream[], int channel, int payloadStartIndex, int sizeOfPayload){
        this.size = sizeOfPayload;
        this.buffer = new byte[this.size];
        System.arraycopy(stream, payloadStartIndex, this.buffer, 0, this.size);      
    }

    public BiomedBDictionaryEntry getEntry(int number){
        BiomedBDictionaryEntry entry = null;
        
        for(int i = 0; i < this.entryList.size(); i++){
            if(this.entryList.get(i).number == number){
                entry = this.entryList.get(i);
            }
        }
        return entry;
    }
}
