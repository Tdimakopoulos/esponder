/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.reusable;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredReusableResourceDTO;
import eu.esponder.dto.model.crisis.resource.ResourceStatusDTO;
import eu.esponder.dto.model.crisis.resource.category.ReusableResourceCategoryDTO;
import eu.esponder.dto.model.type.DisciplineTypeDTO;
import eu.esponder.dto.model.type.ESponderTypeDTO;
import eu.esponder.dto.model.type.OrganisationTypeDTO;
import eu.esponder.dto.model.type.ReusableResourceTypeDTO;
import eu.esponder.organization.OrganizationManager;
import eu.esponder.ws.client.create.CreateOperations;
import eu.esponder.ws.client.query.QueryManager;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class reusablemanager {

    private Map<String, String> pResourceType;
    private String pResourceTypeSelected;
    private ResultListDTO pRes = null;

    private String szTitle;
    private String Quantity;
    
    /**
     * regReusableResource1.setQuantity(new BigDecimal(20));
     * ReusableResourceTypeDTO reusType1 = (ReusableResourceTypeDTO)
     * this.typeService.findDTOByTitle("Medical Kit");
     * ReusableResourceCategoryDTO reusableCategory1 =
     * this.resourceCategoryService.findReusableCategoryDTOByType(reusType1);
     * regReusableResource1.setReusableResourceCategoryId(reusableCategory1.getId());
     * regReusableResource1.setStatus(ResourceStatusDTO.AVAILABLE);
     * regReusableResource1.setTitle("RegisteredReusable1");
     
     */
  

    public reusablemanager() {
        pResourceType = new HashMap<String, String>();

        try {
            QueryManager pQuery = new QueryManager();
            pRes = pQuery.getAllTypes("6");
            System.out.println(" Size " + pRes.getResultList().size());
            for (int i = 0; i < pRes.getResultList().size(); i++) {
                ESponderTypeDTO pEnt = (ESponderTypeDTO) pRes.getResultList().get(i);
                if (pEnt instanceof ReusableResourceTypeDTO) {

                    pResourceType.put(pEnt.getTitle(), pEnt.getId().toString());
                }
            }
        } catch (JsonParseException ex) {
            Logger.getLogger(OrganizationManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JsonMappingException ex) {
            Logger.getLogger(OrganizationManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(OrganizationManager.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    public String ReusableSave() throws IOException
    {
        QueryManager pq=new QueryManager();
        CreateOperations pco=new CreateOperations();
        
        RegisteredReusableResourceDTO registeredReusableResourceDTO= new RegisteredReusableResourceDTO();
        registeredReusableResourceDTO.setTitle(szTitle);
        registeredReusableResourceDTO.setStatus(ResourceStatusDTO.AVAILABLE);
        registeredReusableResourceDTO.setReusableResourceCategoryId(pq.getReusableResourceCategoryDTO(Long.valueOf(pResourceTypeSelected), "1").getId());
        registeredReusableResourceDTO.setQuantity(BigDecimal.valueOf(Long.valueOf(Quantity)));
        pco.createRegisterReusableResourceReal("1", registeredReusableResourceDTO);
        return null;
        
    }

    /**
     * @return the pResourceType
     */
    public Map<String, String> getpResourceType() {
        return pResourceType;
    }

    /**
     * @param pResourceType the pResourceType to set
     */
    public void setpResourceType(Map<String, String> pResourceType) {
        this.pResourceType = pResourceType;
    }

    /**
     * @return the pResourceTypeSelected
     */
    public String getpResourceTypeSelected() {
        return pResourceTypeSelected;
    }

    /**
     * @param pResourceTypeSelected the pResourceTypeSelected to set
     */
    public void setpResourceTypeSelected(String pResourceTypeSelected) {
        this.pResourceTypeSelected = pResourceTypeSelected;
    }

    /**
     * @return the pRes
     */
    public ResultListDTO getpRes() {
        return pRes;
    }

    /**
     * @param pRes the pRes to set
     */
    public void setpRes(ResultListDTO pRes) {
        this.pRes = pRes;
    }

    /**
     * @return the szTitle
     */
    public String getSzTitle() {
        return szTitle;
    }

    /**
     * @param szTitle the szTitle to set
     */
    public void setSzTitle(String szTitle) {
        this.szTitle = szTitle;
    }

    /**
     * @return the Quantity
     */
    public String getQuantity() {
        return Quantity;
    }

    /**
     * @param Quantity the Quantity to set
     */
    public void setQuantity(String Quantity) {
        this.Quantity = Quantity;
    }
}
