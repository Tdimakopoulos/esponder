/*
 * 
 */
package eu.esponder.rest.crisis;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredReusableResourceDTO;
import eu.esponder.dto.model.crisis.resource.ReusableResourceDTO;
import eu.esponder.rest.ESponderResource;

// TODO: Auto-generated Javadoc
/**
 * The Class CrisisViewLogistics.
 */
@Path("/crisis/view")
public class CrisisViewLogistics extends ESponderResource {

	

	/**
	 * Read consumable by id.
	 *
	 * @param consumableResourceId the consumable resource id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the consumable resource dto
	 */
	@GET
	@Path("/consumables/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public ConsumableResourceDTO readConsumableById(
			@QueryParam("consumableResourceId") @NotNull(message="consumableResourceId may not be null") Long consumableResourceId,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		ConsumableResourceDTO consumableResourceDTO = this.getLogisticsRemoteService().findConsumableResourceByIdRemote(consumableResourceId);
		return consumableResourceDTO;
	}

	/**
	 * Read all consumables.
	 *
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the result list dto
	 */
	@GET
	@Path("/consumables/findAll")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO readAllConsumables(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return new ResultListDTO(this.getLogisticsRemoteService().findAllConsumableResourcesRemote());

	}

	/**
	 * Read consumable by title.
	 *
	 * @param consumableResourceTitle the consumable resource title
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the consumable resource dto
	 */
	@GET
	@Path("/consumables/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public ConsumableResourceDTO readConsumableByTitle(
			@QueryParam("consumableResourceTitle") @NotNull(message="consumableResourceTitle may not be null") String consumableResourceTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		ConsumableResourceDTO consumableResourceDTO = this.getLogisticsRemoteService().findConsumableResourceByTitleRemote(consumableResourceTitle);
		return consumableResourceDTO;
	}

	/**
	 * Creates the consumable resource.
	 *
	 * @param consumableResourceDTO the consumable resource dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the consumable resource dto
	 */
	@POST
	@Path("/consumables/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ConsumableResourceDTO createConsumableResource(
			@NotNull(message="Consumable Resource object may not be null") ConsumableResourceDTO consumableResourceDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		consumableResourceDTO = this.getLogisticsRemoteService().createConsumableResourceRemote(consumableResourceDTO, userID);
		return consumableResourceDTO;
	}

	/**
	 * Update consumable resource.
	 *
	 * @param consumableResourceDTO the consumable resource dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the consumable resource dto
	 */
	@PUT
	@Path("/consumables/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ConsumableResourceDTO updateConsumableResource(
			@NotNull(message="Consumable Resource object may not be null") ConsumableResourceDTO consumableResourceDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		consumableResourceDTO = this.getLogisticsRemoteService().updateConsumableResourceRemote(consumableResourceDTO, userID);
		return consumableResourceDTO;
	}

	/**
	 * Delete consumable resource.
	 *
	 * @param consumableResourceID the consumable resource id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the long
	 */
	@DELETE
	@Path("/consumables/delete")
	public Long deleteConsumableResource(
			@QueryParam("consumableResourceID") @NotNull(message="Consumable Resource ID may not be null") Long consumableResourceID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		this.getLogisticsRemoteService().deleteConsumableResourceRemote(consumableResourceID, userID);
		return consumableResourceID;
	}

	/**
	 * Read registered consumable by id.
	 *
	 * @param registeredConsumableResourceId the registered consumable resource id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the registered consumable resource dto
	 */
	@GET
	@Path("/regConsumables/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredConsumableResourceDTO readRegisteredConsumableById(
			@QueryParam("registeredConsumableResourceId") @NotNull(message="registeredConsumableResourceId may not be null") Long registeredConsumableResourceId,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		RegisteredConsumableResourceDTO registeredConsumableResourceDTO = this.getLogisticsRemoteService().findRegisteredConsumableResourceByIdRemote(registeredConsumableResourceId);
		return registeredConsumableResourceDTO;
	}

	/**
	 * Read all registered consumables.
	 *
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the result list dto
	 */
	@GET
	@Path("/regConsumables/findAll")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO readAllRegisteredConsumables(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return new ResultListDTO(this.getLogisticsRemoteService().findAllRegisteredConsumableResourcesRemote());

	}

	/**
	 * Read registered consumable by title.
	 *
	 * @param registeredConsumableResourceTitle the registered consumable resource title
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the registered consumable resource dto
	 */
	@GET
	@Path("/regConsumables/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredConsumableResourceDTO readRegisteredConsumableByTitle(
			@QueryParam("registeredConsumableResourceTitle") @NotNull(message="registeredConsumableResourceTitle may not be null") String registeredConsumableResourceTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		RegisteredConsumableResourceDTO registeredConsumableResourceDTO = this.getLogisticsRemoteService().findRegisteredConsumableResourceByTitleRemote(registeredConsumableResourceTitle);
		return registeredConsumableResourceDTO;
	}

	/**
	 * Creates the registered consumable resource.
	 *
	 * @param registeredConsumableResourceDTO the registered consumable resource dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the registered consumable resource dto
	 */
	@POST
	@Path("/regConsumables/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredConsumableResourceDTO createRegisteredConsumableResource(
			@NotNull(message="Registered Consumable Resource object may not be null") RegisteredConsumableResourceDTO registeredConsumableResourceDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		registeredConsumableResourceDTO = this.getLogisticsRemoteService().createRegisteredConsumableResourceRemote(registeredConsumableResourceDTO, userID);
		return registeredConsumableResourceDTO;
	}

	/**
	 * Update registered consumable resource.
	 *
	 * @param registeredConsumableResourceDTO the registered consumable resource dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the registered consumable resource dto
	 */
	@PUT
	@Path("/regConsumables/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredConsumableResourceDTO updateRegisteredConsumableResource(
			@NotNull(message="Registered Consumable Resource object may not be null") RegisteredConsumableResourceDTO registeredConsumableResourceDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		registeredConsumableResourceDTO = this.getLogisticsRemoteService().updateRegisteredConsumableResourceRemote(registeredConsumableResourceDTO, userID);
		return registeredConsumableResourceDTO;
	}

	/**
	 * Delete registered consumable resource.
	 *
	 * @param registeredConsumableResourceID the registered consumable resource id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the long
	 */
	@DELETE
	@Path("/regConsumables/delete")
	public Long deleteRegisteredConsumableResource(
			@QueryParam("registeredConsumableResourceID") @NotNull(message="Registered Consumable Resource ID may not be null") Long registeredConsumableResourceID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		this.getLogisticsRemoteService().deleteRegisteredConsumableResourceRemote(registeredConsumableResourceID, userID);
		return registeredConsumableResourceID;
	}

	/**
	 * Read registered reusable by id.
	 *
	 * @param registeredReusableResourceId the registered reusable resource id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the registered reusable resource dto
	 */
	@GET
	@Path("/regReusables/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredReusableResourceDTO readRegisteredReusableById(
			@QueryParam("registeredReusableResourceId") @NotNull(message="registeredReusableResourceId may not be null") Long registeredReusableResourceId,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		RegisteredReusableResourceDTO registeredReusableResourceDTO = this.getLogisticsRemoteService().findRegisteredReusableResourceByIdRemote(registeredReusableResourceId);
		return registeredReusableResourceDTO;
	}

	/**
	 * Read all registered reusables.
	 *
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the result list dto
	 */
	@GET
	@Path("/regReusables/findAll")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO readAllRegisteredReusables(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return new ResultListDTO(this.getLogisticsRemoteService().findAllRegisteredReusableResourcesRemote());

	}

	/**
	 * Read registered reusable by title.
	 *
	 * @param registeredReusableResourceTitle the registered reusable resource title
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the registered reusable resource dto
	 */
	@GET
	@Path("/regReusables/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredReusableResourceDTO readRegisteredReusableByTitle(
			@QueryParam("registeredResourceTitle") @NotNull(message="registeredReusableResourceTitle may not be null") String registeredReusableResourceTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		RegisteredReusableResourceDTO registeredReusableResourceDTO = this.getLogisticsRemoteService().findRegisteredReusableResourceByTitleRemote(registeredReusableResourceTitle);
		return registeredReusableResourceDTO;
	}

	/**
	 * Creates the registered reusable resource.
	 *
	 * @param registeredReusableResourceDTO the registered reusable resource dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the registered reusable resource dto
	 */
	@POST
	@Path("/regReusables/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredReusableResourceDTO createRegisteredReusableResource(
			@NotNull(message="Registered Reusable Resource object may not be null") RegisteredReusableResourceDTO registeredReusableResourceDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		registeredReusableResourceDTO = this.getLogisticsRemoteService().createRegisteredReusableResourceRemote(registeredReusableResourceDTO, userID);
		return registeredReusableResourceDTO;
	}

	/**
	 * Update registered reusable resource.
	 *
	 * @param registeredReusableResourceDTO the registered reusable resource dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the registered reusable resource dto
	 */
	@PUT
	@Path("/regReusables/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public RegisteredReusableResourceDTO updateRegisteredReusableResource(
			@NotNull(message="Registered Reusable Resource object may not be null") RegisteredReusableResourceDTO registeredReusableResourceDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		registeredReusableResourceDTO = this.getLogisticsRemoteService().updateRegisteredReusableResourceRemote(registeredReusableResourceDTO, userID);
		return registeredReusableResourceDTO;
	}

	/**
	 * Delete registered reusable resource.
	 *
	 * @param registeredReusableResourceID the registered reusable resource id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the long
	 */
	@DELETE
	@Path("/regReusables/delete")
	public Long deleteRegisteredReusableResource(
			@QueryParam("registeredReusableResourceID") @NotNull(message="Registered Reusable Resource ID may not be null") Long registeredReusableResourceID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		this.getLogisticsRemoteService().deleteRegisteredReusableResourceRemote(registeredReusableResourceID, userID);
		return registeredReusableResourceID;
	}

	/**
	 * Read reusable by id.
	 *
	 * @param reusableResourceId the reusable resource id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the reusable resource dto
	 */
	@GET
	@Path("/reusables/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public ReusableResourceDTO readReusableById(
			@QueryParam("reusableResourceId") @NotNull(message="reusableResourceId may not be null") Long reusableResourceId,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		ReusableResourceDTO reusableResourceDTO = this.getLogisticsRemoteService().findReusableResourceByIdRemote(reusableResourceId);
		return reusableResourceDTO;
	}

	/**
	 * Read all reusables.
	 *
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the result list dto
	 */
	@GET
	@Path("/reusables/findAll")
	@Produces({MediaType.APPLICATION_JSON})
	public ResultListDTO readAllReusables(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return new ResultListDTO(this.getLogisticsRemoteService().findAllReusableResourcesRemote());

	}

	/**
	 * Read reusable by title.
	 *
	 * @param reusableResourceTitle the reusable resource title
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the reusable resource dto
	 */
	@GET
	@Path("/reusables/findByTitle")
	@Produces({MediaType.APPLICATION_JSON})
	public ReusableResourceDTO readReusableByTitle(
			@QueryParam("reusableResourceTitle") @NotNull(message="reusableResourceTitle may not be null") String reusableResourceTitle,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		ReusableResourceDTO reusableResourceDTO = this.getLogisticsRemoteService().findReusableResourceByTitleRemote(reusableResourceTitle);
		return reusableResourceDTO;
	}

	/**
	 * Creates the reusable resource.
	 *
	 * @param reusableResourceDTO the reusable resource dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the reusable resource dto
	 */
	@POST
	@Path("/reusables/create")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ReusableResourceDTO createReusableResource(
			@NotNull(message="Reusable Resource object may not be null") ReusableResourceDTO reusableResourceDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		reusableResourceDTO = this.getLogisticsRemoteService().createReusableResourceRemote(reusableResourceDTO, userID);
		return reusableResourceDTO;
	}

	/**
	 * Update reusable resource.
	 *
	 * @param reusableResourceDTO the reusable resource dto
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the reusable resource dto
	 */
	@PUT
	@Path("/reusables/update")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ReusableResourceDTO updateReusableResource(
			@NotNull(message="Reusable Resource object may not be null") ReusableResourceDTO reusableResourceDTO,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		reusableResourceDTO = this.getLogisticsRemoteService().updateReusableResourceRemote(reusableResourceDTO, userID);
		return reusableResourceDTO;
	}

	/**
	 * Delete reusable resource.
	 *
	 * @param reusableResourceID the reusable resource id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the long
	 */
	@DELETE
	@Path("/reusables/delete")
	public Long deleteReusableResource(
			@QueryParam("reusableResourceID") @NotNull(message="Reusable Resource ID may not be null") Long reusableResourceID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {
		
		this.getLogisticsRemoteService().deleteReusableResourceRemote(reusableResourceID, userID);
		return reusableResourceID;
	}

	/**
	 * Read reusable by title.
	 *
	 * @param equipmentID the equipment id
	 * @param userID the user id
	 * @param sessionID the session id
	 * @return the equipment dto
	 */
	@GET
	@Path("/equipments/findByID")
	@Produces({MediaType.APPLICATION_JSON})
	public EquipmentDTO readReusableByTitle(
			@QueryParam("equipmentID") @NotNull(message="userID may not be null") Long equipmentID,
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		EquipmentDTO pFind=this.getEquipmentRemoteService().findByIdRemote(equipmentID);

		return pFind;
	}

}
