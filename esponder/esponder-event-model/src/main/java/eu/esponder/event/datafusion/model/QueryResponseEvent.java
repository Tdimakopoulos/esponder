/*
 * 
 */
package eu.esponder.event.datafusion.model;

import eu.esponder.event.model.ESponderOperationEvent;


/**
 * The Interface CreateEvent.
 */
public abstract interface QueryResponseEvent extends ESponderOperationEvent {
	
}

