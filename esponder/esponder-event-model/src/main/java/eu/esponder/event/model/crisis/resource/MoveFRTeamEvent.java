/*
 * 
 */
package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.FRTeamDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.event.model.UpdateEvent;



// TODO: Auto-generated Javadoc
/**
 * The Class CreateActorEvent.
 */
public class MoveFRTeamEvent extends FRTeamEvent<FRTeamDTO> implements UpdateEvent {

		
	/**
	 * 
	 */
	private static final long serialVersionUID = 2037088567151808364L;
	
	PointDTO pFinalPosition;
	

	public PointDTO getpFinalPosition() {
		return pFinalPosition;
	}


	public void setpFinalPosition(PointDTO pFinalPosition) {
		this.pFinalPosition = pFinalPosition;
	}


	/**
	 * Instantiates a new creates the actor event.
	 */
	public MoveFRTeamEvent(){
		setJournalMessageInfo("UpdateFRTeamEvent");
	}
	
}
