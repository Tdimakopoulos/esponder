package eu.esponder.model.crisis;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import eu.esponder.model.ESponderEntity;
import eu.esponder.model.snapshot.location.Sphere;

@Entity
@Table(name="sub_crisis_context")
@NamedQueries({
	@NamedQuery(name="SubCrisis.findByTitle", query="select cc from SubCrisis cc where cc.title=:title"),
	@NamedQuery(name="SubCrisis.findAll", query="select cc from SubCrisis cc")
})
public class SubCrisis extends ESponderEntity<Long> {
	private static final long serialVersionUID = -5332261813135360352L;

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="SUB_CRISIS_CONTEXT_ID")
	private Long id;
	
	/** The incident title. Will be provided by the user on creation of the entity. Not null , Unique */
	@Column(name="TITLE", nullable=false, unique=true, length=255)
	private String title;
	
		
	/** The crisis location. This field provides . Type Sphere */
	@OneToOne
	@JoinColumn(name="LOCATION_ID")
	private Sphere crisisLocation;

	@Column(name="CRISISID", nullable=false, unique=false)
	private Long CrisisID;
    
	@Column(name="MEOCID", nullable=false, unique=false)
	private Long MeocID;
    
    
	public Long getCrisisID() {
		return CrisisID;
	}


	public void setCrisisID(Long crisisID) {
		CrisisID = crisisID;
	}


	public Long getMeocID() {
		return MeocID;
	}


	public void setMeocID(Long meocID) {
		MeocID = meocID;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public Sphere getCrisisLocation() {
		return crisisLocation;
	}


	public void setCrisisLocation(Sphere crisisLocation) {
		this.crisisLocation = crisisLocation;
	}
	
	
}
