/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package esponder.biomed.lib;

/**
 * The acquired signals are classed by their type.
 * This class represents the acquires regular signals. That means that the sampling frequency is constant until the user decide to change it.
 * This class contains many attributes in order to be compatible with the parsing of EDF files
 * @author afa
 */
public class Signal {

    /**
     * the name of the signal is represented as the string that can be unique to every signal
     */
    private String name;
    /**
     * Unique id of the signal.
     * This id is defined in a fixed table
     */
    private int id;
    /**
     * Physical tranducer used to measure this signal physically
     */
    private String transducer;
    /**
     * Unit of the measurment of the signals
     */
    private String unit;
    /**
     * The minimum possible of the signal
     */
    private float minphys;
    /**
     * The maximum possible of the signal
     */
    private float maxphys;
    /**
     * The minimum of the digital reprentation of the signal
     */
    private int mindig;
    /**
     * The maximum of the digital reprentation of the signal
     */
    private int maxdig;
    /**
     * The number of the samples of the signal contained in the object
     */
    private int numberOfSamples;
    /**
     * The filter used in the device
     */
    private String prefiltering;
    /**
     * The values of the samples
     */
    private float values[];
    /**
     * The current sampling frequency of the signal. It cannot change without the interaction of the user
     */
    private float samplingFrequency;
    /**
     * The variable telling the state of the signal whether is enabled in the process or not
     */
    private boolean enabled;
    
    /**
     * The scale factor to be applied on the acquired samples of this signal
     */
    private float scaleFactor;

    /**
     * Constructor
     */
    public Signal() {
        this.enabled = true;
    }
    /**
     * Gets the name of the signal
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**Sets the name of the signal
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the number of the samples acquired of the signal at this very moment
     * @return the numberOfSamples
     */
     int getNumberOfSamples() {
        return numberOfSamples;
    }

    /**
     * Sets the number of the samples acquired of that signal
     * @param numberOfSamples the numberOfSamples to set
     */
     void setNumberOfSamples(int numberOfSamples) {
        this.numberOfSamples = numberOfSamples;
    }

    /**
     * Gets the samples values
     * @return the values
     */
     float[] getValues() {
        return values;
    }

    /**
     * Assigns the given values array to the samples values array
     * @param values the values to set
     */
     void setValues(float[] values) {
        this.values = values;
    }

    /**
     * Gets the name of the transducer used to measure this signal
     * @return the transducer
     */
     String getTransducer() {
        return transducer;
    }

    /**
     * Sets the name of the transducer used to measure this signal
     * @param transducer the transducer to set
     */
     void setTransducer(String transducer) {
        this.transducer = transducer;
    }

    /**
     * Gets the units in which this signal is measured
     * @return the unit
     */
     String getUnit() {
        return unit;
    }

    /**
     * Sets the units in which this signal is measured
     * @param unit the unit to set
     */
     void setUnit(String unit) {
        this.unit = unit;
    }

    /**
     * Gets the minimum of the phisical value of the signal
     * @return the minphys
     */
     float getMinphys() {
        return minphys;
    }

    /**
     * Sets the minimum of the physical value of the signal
     * @param minphys the minphys to set
     */
     void setMinphys(float minphys) {
        this.minphys = minphys;
    }

    /**
     * Gets the maximum of the physical value of the signal
     * @return the maxphys
     */
     float getMaxphys() {
        return maxphys;
    }

    /**
     * Sets the minimum of the physical value of the signal
     * @param maxphys the maxphys to set
     */
     void setMaxphys(float maxphys) {
        this.maxphys = maxphys;
    }

    /**
     * Gets the minimum of the digital value of the signal obtained from the converter
     * @return the mindig
     */
     int getMindig() {
        return mindig;
    }

    /**
     * Sets the minimum of the digital value of the signal obtained from the converter
     * @param mindig the mindig to set
     */
     void setMindig(int mindig) {
        this.mindig = mindig;
    }

    /**
     * Gets the maximum of the digital value of the signal obtained from the converter
     * @return the maxdig
     */
     int getMaxdig() {
        return maxdig;
    }

    /**
     * Sets the maximum of the digital value of the signal obtained from the converter
     * @param maxdig the maxdig to set
     */
     void setMaxdig(int maxdig) {
        this.maxdig = maxdig;
    }

    /**
     * Gets the prefeltering information of the signal before digitization
     * @return the prefiltering
     */
     String getPrefiltering() {
        return prefiltering;
    }

    /**
     * Sets the prefeltering information of the signal before digitization
     * @param prefiltering the prefiltering to set
     */
     void setPrefiltering(String prefiltering) {
        this.prefiltering = prefiltering;
    }

    /**
     * Gets the sampling frequency of the signal
     * @return the samplingFrequency
     */
    public float getSamplingFrequency() {
        return samplingFrequency;
    }

    /**
     * Sets the sampling frequency of the signal
     * @param samplingFrequency the samplingFrequency to set
     */
    public void setSamplingFrequency(float samplingFrequency) {
        this.samplingFrequency = samplingFrequency;
    }

    /**
     * Checks whether the acquisition of the signal is enabled or not
     * @return the enabled
     */
     boolean isEnabled() {
        return enabled;
    }

    /**
     * Enable or disable the acquistion of the signal
     * @param enabled the enabled to set
     */
     void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * Get the unique id of the signal
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Set the unique id of the signal
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the scaleFactor
     */
    public float getScaleFactor() {
        return scaleFactor;
    }

    /**
     * @param scaleFactor the scaleFactor to set
     */
    public void setScaleFactor(float scaleFactor) {
        this.scaleFactor = scaleFactor;
    }
}
