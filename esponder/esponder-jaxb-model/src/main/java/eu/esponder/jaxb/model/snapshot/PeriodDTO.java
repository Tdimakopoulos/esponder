package eu.esponder.jaxb.model.snapshot;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

import org.codehaus.jackson.annotate.JsonPropertyOrder;

@XmlRootElement(name="period")
@XmlType(name="Period")
@JsonPropertyOrder({"dateFrom", "dateTo"})
public class PeriodDTO {
	
	private XMLGregorianCalendar dateFrom;
	
	private XMLGregorianCalendar dateTo;

	public XMLGregorianCalendar getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(XMLGregorianCalendar dateFrom) {
		this.dateFrom = dateFrom;
	}

	public XMLGregorianCalendar getDateTo() {
		return dateTo;
	}

	public void setDateTo(XMLGregorianCalendar dateTo) {
		this.dateTo = dateTo;
	}

	@Override
	public String toString() {
		return "Period [dateFrom=" + dateFrom + ", dateTo=" + dateTo + "]";
	}

}
