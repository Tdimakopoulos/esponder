/*
 * 
 */
package eu.esponder.event.model.crisis.resource;


import eu.esponder.dto.model.crisis.resource.FRTeamDTO;
import eu.esponder.event.model.ESponderEvent;

// TODO: Auto-generated Javadoc
/**
 * The Class ActorEvent.
 * 
 * @param <T>
 *            the generic type
 */
public abstract class FRTeamEvent<T extends FRTeamDTO> extends ESponderEvent<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6533084808084953724L;

	
	
}
