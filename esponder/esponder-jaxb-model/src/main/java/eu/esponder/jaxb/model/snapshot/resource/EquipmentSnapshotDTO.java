package eu.esponder.jaxb.model.snapshot.resource;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;

import eu.esponder.jaxb.model.snapshot.SnapshotDTO;
import eu.esponder.jaxb.model.snapshot.status.EquipmentSnapshotStatusDTO;

@XmlRootElement(name="equipmentSnapshot")
@XmlType(name="EquipmentSnapshot")
@JsonPropertyOrder({"status", "period"})
public class EquipmentSnapshotDTO extends SnapshotDTO {
	
	private EquipmentSnapshotStatusDTO status;

	public EquipmentSnapshotStatusDTO getStatus() {
		return status;
	}

	public void setStatus(EquipmentSnapshotStatusDTO status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "EquipmentSnapshotDTO [status=" + status + ", period=" + period + ", id=" + id + "]";
	}
	
}
