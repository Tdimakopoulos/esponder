

package eu.esponder.datasync.server.integrate;

import org.jumpmind.util.Context;

public interface IPublisher {
    
    public void publish(Context context, String text);
    
}