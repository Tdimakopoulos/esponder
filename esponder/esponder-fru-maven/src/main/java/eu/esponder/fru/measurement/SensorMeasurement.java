package eu.esponder.fru.measurement;

import java.util.Date;

public abstract class SensorMeasurement {
  
	private Date timestamp;
	
	private MeasurementProcessingType processingType;

	public Date getTimestamp(){
	 	return timestamp;
	}
	
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public MeasurementProcessingType getProcessingType() {
		return processingType;
	}

	public void setProcessingType(MeasurementProcessingType processingType) {
		this.processingType = processingType;
	}

	
	
}
