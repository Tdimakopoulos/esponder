/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.ws.client.logic;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredConsumableResourceDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredOperationsCentreDTO;
import eu.esponder.dto.model.crisis.resource.RegisteredReusableResourceDTO;
import eu.esponder.dto.model.crisis.resource.ReusableResourceDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.ws.client.urlmanager.UrlManager;
import java.io.IOException;
import java.math.BigDecimal;
import javax.ws.rs.core.MultivaluedMap;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author Tom
 */
public class CrisisResourceManager {
     ObjectMapper mapper = new ObjectMapper();
    UrlManager URL = new UrlManager();

    public ReusableResourceDTO AssociateReusable(String ccID, String regreu,
            String userID) throws JsonGenerationException,
            JsonMappingException, IOException {

        ReusableResourceDTO pReturn;
        // Variables
        String szReturn=null;
        Client client2 = Client.create();
        WebResource webResource2 = client2
                .resource(URL.getSzLogicassociatereusables());

        // Initialize Params
        MultivaluedMap queryParams2 = new MultivaluedMapImpl();
        queryParams2.add("userID", userID);
        queryParams2.add("regReusableID", regreu);
        queryParams2.add("crisisContextID", ccID);

        // Call webservice
        szReturn = webResource2.queryParams(queryParams2).type("application/json").get(String.class);

        // Convert to DTO
        pReturn = mapper.readValue(szReturn, ReusableResourceDTO.class);

        return pReturn;
    }
    
     public RegisteredReusableResourceDTO deAssociateReusable(String regreu,
            String userID) throws JsonGenerationException,
            JsonMappingException, IOException {

        RegisteredReusableResourceDTO pReturn;
        // Variables
        String szReturn=null;
        Client client2 = Client.create();
        WebResource webResource2 = client2
                .resource(URL.getSzLogicdeassociatereusables());

        // Initialize Params
        MultivaluedMap queryParams2 = new MultivaluedMapImpl();
        queryParams2.add("userID", userID);
        queryParams2.add("reusableID", regreu);
    

        // Call webservice
        szReturn = webResource2.queryParams(queryParams2).type("application/json").get(String.class);

        // Convert to DTO
        pReturn = mapper.readValue(szReturn, RegisteredReusableResourceDTO.class);

        return pReturn;
    }
     
     public RegisteredConsumableResourceDTO deAssociateConsumable(String regreu,
            String userID) throws JsonGenerationException,
            JsonMappingException, IOException {

        RegisteredConsumableResourceDTO pReturn;
        // Variables
        String szReturn=null;
        Client client2 = Client.create();
        WebResource webResource2 = client2
                .resource(URL.getSzLogicdeassociateconsumables());

        // Initialize Params
        MultivaluedMap queryParams2 = new MultivaluedMapImpl();
        queryParams2.add("userID", userID);
        queryParams2.add("consumableID", regreu);
    

        // Call webservice
        szReturn = webResource2.queryParams(queryParams2).type("application/json").get(String.class);

        // Convert to DTO
        pReturn = mapper.readValue(szReturn, RegisteredConsumableResourceDTO.class);

        return pReturn;
    }

     public RegisteredOperationsCentreDTO deAssociateoc(String regreu,
            String userID) throws JsonGenerationException,
            JsonMappingException, IOException {

        RegisteredOperationsCentreDTO pReturn;
        // Variables
        String szReturn=null;
        Client client2 = Client.create();
        WebResource webResource2 = client2
                .resource(URL.getSzLogicdeassociateoc());

        // Initialize Params
        MultivaluedMap queryParams2 = new MultivaluedMapImpl();
        queryParams2.add("userID", userID);
        queryParams2.add("ocID", regreu);
    

        // Call webservice
        szReturn = webResource2.queryParams(queryParams2).type("application/json").get(String.class);

        // Convert to DTO
        pReturn = mapper.readValue(szReturn, RegisteredOperationsCentreDTO.class);

        return pReturn;
    }
     
    public ConsumableResourceDTO AssociateConsumable(String ccID, String regreu,
            String userID) throws JsonGenerationException,
            JsonMappingException, IOException {

        ConsumableResourceDTO pReturn;
        // Variables
        String szReturn=null;
        Client client2 = Client.create();
        WebResource webResource2 = client2
                .resource(URL.getSzLogicassociateconsumables());

        // Initialize Params
        MultivaluedMap queryParams2 = new MultivaluedMapImpl();
        queryParams2.add("userID", userID);
        queryParams2.add("regConsumableID", regreu);
        queryParams2.add("crisisContextID", ccID);

        // Call webservice
        szReturn = webResource2.queryParams(queryParams2).type("application/json").get(String.class);

        // Convert to DTO
        pReturn = mapper.readValue(szReturn, ConsumableResourceDTO.class);

        return pReturn;
    }
    
	public OperationsCentreDTO AssociateOC(String ccID, String regreu,
            String userID) throws JsonGenerationException,
            JsonMappingException, IOException {

        OperationsCentreDTO pReturn;
        // Variables
        String szReturn=null;
        Client client2 = Client.create();
        WebResource webResource2 = client2
                .resource(URL.getSzLogicassociateoc());

        // Initialize Params
        MultivaluedMap queryParams2 = new MultivaluedMapImpl();
        queryParams2.add("userID", userID);
        queryParams2.add("regocID", regreu);
        queryParams2.add("ccID", ccID);

        // Call webservice
        szReturn = webResource2.queryParams(queryParams2).type("application/json").get(String.class);

        // Convert to DTO
        pReturn = mapper.readValue(szReturn, OperationsCentreDTO.class);

        return pReturn;
    }
    
    
        public ActorDTO AssociateActor(String personnelid,
            String userID) throws JsonGenerationException,
            JsonMappingException, IOException {

        ActorDTO pReturn;
        // Variables
        String szReturn=null;
        Client client2 = Client.create();
        WebResource webResource2 = client2
                .resource(URL.getSzccassociateactor());

        // Initialize Params
        MultivaluedMap queryParams2 = new MultivaluedMapImpl();
        queryParams2.add("userID", userID);
        queryParams2.add("personnelID", personnelid);
        

        // Call webservice
        szReturn = webResource2.queryParams(queryParams2).type("application/json").get(String.class);

        // Convert to DTO
        pReturn = mapper.readValue(szReturn, ActorDTO.class);

        return pReturn;
    }
    
    
        public ActorDTO deAssociateActor(String actorID, String personnelid,
            String userID) throws JsonGenerationException,
            JsonMappingException, IOException {

        ActorDTO pReturn;
        // Variables
        String szReturn=null;
        Client client2 = Client.create();
        WebResource webResource2 = client2
                .resource(URL.getSzccdeassociateactor());

        // Initialize Params
        MultivaluedMap queryParams2 = new MultivaluedMapImpl();
        queryParams2.add("userID", userID);
        queryParams2.add("personnelID", personnelid);
        queryParams2.add("actorID", actorID);

        // Call webservice
        szReturn = webResource2.queryParams(queryParams2).type("application/json").get(String.class);

        // Convert to DTO
        pReturn = mapper.readValue(szReturn, ActorDTO.class);

        return pReturn;
    }
}
