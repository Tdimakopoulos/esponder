/*
 * 
 */
package eu.esponder.dto.model.crisis.alternative;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;



// TODO: Auto-generated Javadoc
/**
 * The Class FirstResponderActorALTDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "title", "personnel", "voIPURL", "equipmentSet", "snapshots"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class FirstResponderActorALTDTO extends ActorALTDTO {

	
	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3103723279002433313L;

	/**
	 * Instantiates a new first responder actor altdto.
	 */
	public FirstResponderActorALTDTO() {}
	
	/** The equipment set. */
	private Set<Long> equipmentSet;
	
	/** The snapshots. */
	private ActorSnapshotDTO snapshot;

	/**
	 * Gets the equipment set.
	 *
	 * @return the equipment set
	 */
	public Set<Long> getEquipmentSet() {
		return equipmentSet;
	}

	/**
	 * Sets the equipment set.
	 *
	 * @param equipmentSet the new equipment set
	 */
	public void setEquipmentSet(Set<Long> equipmentSet) {
		this.equipmentSet = equipmentSet;
	}

	/**
	 * Gets the snapshots.
	 *
	 * @return the snapshots
	 */
	public ActorSnapshotDTO getSnapshot() {
		return snapshot;
	}

	/**
	 * Sets the snapshots.
	 *
	 * @param snapshots the new snapshots
	 */
	public void setSnapshot(ActorSnapshotDTO snapshots) {
		this.snapshot = snapshots;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.crisis.alternative.ActorALTDTO#toString()
	 */
	@Override
	public String toString() {
		return this.getClass().getName()+" [id=" + id + ", title=" + title + "]";
	}

}
