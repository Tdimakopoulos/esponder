package eu.esponder.jaxb.model.crisis.action;

import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.jaxb.model.ESponderEntityDTO;
import eu.esponder.jaxb.model.crisis.resource.ConsumableResourceDTO;
import eu.esponder.jaxb.model.crisis.resource.ReusableResourceDTO;
import eu.esponder.jaxb.model.snapshot.PeriodDTO;
import eu.esponder.jaxb.model.snapshot.location.LocationAreaDTO;


@XmlRootElement(name="actionPartObjective")
@XmlType(name="ActionPartObjective")
@JsonSerialize(include=Inclusion.NON_NULL)
public class ActionPartObjectiveDTO extends ESponderEntityDTO {

	private String title;
	
	private PeriodDTO period;
	
	private LocationAreaDTO locationArea;
	
	private Set<ConsumableResourceDTO> consumableResources;
	
	private Set<ReusableResourceDTO> reusuableResources;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public PeriodDTO getPeriod() {
		return period;
	}

	public void setPeriod(PeriodDTO period) {
		this.period = period;
	}

	public LocationAreaDTO getLocationArea() {
		return locationArea;
	}

	public void setLocationArea(LocationAreaDTO locationArea) {
		this.locationArea = locationArea;
	}

	public Set<ConsumableResourceDTO> getConsumableResources() {
		return consumableResources;
	}

	public void setConsumableResources(Set<ConsumableResourceDTO> consumableResources) {
		this.consumableResources = consumableResources;
	}

	public Set<ReusableResourceDTO> getReusuableResources() {
		return reusuableResources;
	}

	public void setReusuableResources(Set<ReusableResourceDTO> reusuableResources) {
		this.reusuableResources = reusuableResources;
	}
	
}
