/*
 * 
 */
package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.FRTeamDTO;
import eu.esponder.event.model.UpdateEvent;



// TODO: Auto-generated Javadoc
/**
 * The Class CreateActorEvent.
 */
public class UpdateFRTeamEvent extends FRTeamEvent<FRTeamDTO> implements UpdateEvent {

		
	/**
	 * 
	 */
	private static final long serialVersionUID = 2037088567151808364L;

	/**
	 * Instantiates a new creates the actor event.
	 */
	public UpdateFRTeamEvent(){
		setJournalMessageInfo("UpdateFRTeamEvent");
	}
	
}
