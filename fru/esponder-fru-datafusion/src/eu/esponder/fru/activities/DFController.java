package eu.esponder.fru.activities;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.fru.JSONParser;
import eu.esponder.fru.R;

public class DFController extends Activity {

//	private String filter = CreateSensorMeasurementEnvelopeEvent.class.getName();
//	private DataHandler receiver;
//
//	private String filterConfing = UpdateConfigurationEvent.class.getName();
//	private UpdateConfig receiverConfig;
	public static Map< Class<? extends SensorDTO> , Long > actorSensorMap;
	TextView status;
	Button startB,stopB;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.controller);
		status = (TextView) findViewById(R.id.ControllerStatus);
		startB = (Button) findViewById(R.id.Start);
		stopB = (Button) findViewById(R.id.Stop);
		
		JSONParser jpa = new JSONParser();
		ActorDTO actor = jpa.getActor();
	    actorSensorMap = new HashMap <Class<? extends SensorDTO>,Long>();
		Iterator iteratorEquipment = actor.getEquipmentSet().iterator();
		Iterator iteratorSensors = ((EquipmentDTO)iteratorEquipment.next()).getSensors().iterator();
	 	 while(iteratorSensors.hasNext()){
	 		SensorDTO sensor = ((SensorDTO)iteratorSensors.next());
	 	 	actorSensorMap.put( sensor.getClass(), sensor.getId());
		 }

	  	Set set = actorSensorMap.entrySet(); 
		 // Get an iterator 
		 Iterator i = set.iterator(); 
		 // Display elements 
		 while(i.hasNext()) { 
		 Map.Entry me = (Map.Entry)i.next(); 
		 System.out.print(me.getKey() + ": "); 
		 System.out.println(me.getValue()); 
		 }  
		 
		if(isMyServiceRunning()){
			status.setText("Controller Active");
			startB.setEnabled(false);
	 	}else{
	 		stopB.setEnabled(false);
	 	}   
	        
  	}

	// Click on the Measurements button of Main
	public void clickHandler(View view) {
		switch (view.getId()) {
		case R.id.Start: // Start Controller
			//  Log.d("!!START!!", "!!START!!");
			//  DataHandler.flag = true;
			//Start the Service
			Intent start = new Intent("eu.esponder.fru.activities.Controller");
			DFController.this.startService(start);
			status.setText("Controller Active");
			view.setEnabled(false);
			if(!stopB.isEnabled()){
				stopB.setEnabled(true);
			}
			
		/*	Thread s = new DataSender(this);
			s.start();*/
				
			break;
		case R.id.Stop: // Stop Controller
			//  Log.d("!!STOP!!", "!!STOP!!");
			//  DataHandler.flag = false;
			
			//Stop the Service
			Intent stop = new Intent("eu.esponder.fru.activities.Controller");
			DFController.this.stopService(stop);
			status.setText("Controller Not Active");
			view.setEnabled(false);
			if(!startB.isEnabled()){
				startB.setEnabled(true);
			}
			break;
		default:
			break;
		}
	}
 
	private boolean isMyServiceRunning() {
	    ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
	    for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
	        if ("eu.esponder.fru.activities.Controller".equals(service.service.getClassName())) {
	            return true;
	        }
	    }
	    return false;
	}
	
}