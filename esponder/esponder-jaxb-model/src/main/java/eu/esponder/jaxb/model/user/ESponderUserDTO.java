package eu.esponder.jaxb.model.user;

import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;

import eu.esponder.jaxb.model.ESponderEntityDTO;
import eu.esponder.jaxb.model.crisis.resource.OperationsCentreDTO;

@XmlRootElement(name="user")
@XmlType(name="ESponderUser")
@JsonPropertyOrder({"userName", "operationsCentres"})
public class ESponderUserDTO extends ESponderEntityDTO {
	
	private String userName;
	
	private Set<OperationsCentreDTO> operationsCentres;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Set<OperationsCentreDTO> getOperationsCentres() {
		return operationsCentres;
	}

	public void setOperationsCentres(Set<OperationsCentreDTO> operationsCentres) {
		this.operationsCentres = operationsCentres;
	}

	@Override
	public String toString() {
		return "ESponderUserDTO [userName=" + userName + ", id=" + id + "]";
	}

}
