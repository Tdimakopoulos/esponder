/*
 * 
 */
package eu.esponder.model.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;



// TODO: Auto-generated Javadoc
/**
 * The Class ReusableResourceType.
 */
@Entity
@DiscriminatorValue("VehicleType")
public class VehicleType extends LogisticsResourceType {

	private static final long serialVersionUID = 90384346461269796L;

	
	
	
}
