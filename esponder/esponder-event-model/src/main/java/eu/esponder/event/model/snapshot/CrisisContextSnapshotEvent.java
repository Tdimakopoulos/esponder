/*
 * 
 */
package eu.esponder.event.model.snapshot;

import eu.esponder.dto.model.snapshot.CrisisContextSnapshotDTO;


// TODO: Auto-generated Javadoc
/**
 * The Class CrisisContextSnapshotEvent.
 *
 * @param <T> the generic type
 */
public class CrisisContextSnapshotEvent<T extends CrisisContextSnapshotDTO> extends SnapshotEvent<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5971268269850771662L;

	/**
	 * Instantiates a new crisis context snapshot event.
	 */
	public CrisisContextSnapshotEvent(){
		setJournalMessageInfo("CrisisContextSnapshotEvent");
	}
}
