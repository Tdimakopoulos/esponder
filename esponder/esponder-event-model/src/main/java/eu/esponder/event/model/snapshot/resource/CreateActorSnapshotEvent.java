/*
 * 
 */
package eu.esponder.event.model.snapshot.resource;

import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.event.model.CreateEvent;



// TODO: Auto-generated Javadoc
/**
 * The Class CreateActorSnapshotEvent.
 */
public class CreateActorSnapshotEvent extends ActorSnapshotEvent<ActorSnapshotDTO> implements CreateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -7149492311767628355L;
	
	/**
	 * Instantiates a new creates the actor snapshot event.
	 */
	public CreateActorSnapshotEvent(){
		setJournalMessageInfo("CreateActorSnapshotEvent");
	}
}
