/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.role.bean;

import eu.esponder.entity.RoleDB;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author tdim
 */
@Local
public interface RoleDBFacadeLocal {

    void create(RoleDB roleDB);

    void edit(RoleDB roleDB);

    void remove(RoleDB roleDB);

    RoleDB find(Object id);

    List<RoleDB> findAll();

    List<RoleDB> findRange(int[] range);

    int count();
    
}
