/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.nephron.cache.alerts;

import java.io.Serializable;


/**
 *
 * @author tdim
 */

public class alertscached implements Serializable {
     private static final long serialVersionUID = 2194219048211L;

    private Long id;

    private Long iddoctor;
    
    private Long idpatient;
    
    private int itype;
    
    private Long cDate;
    
    
    private String alertmsg;
    
    private String alertmsg1;
    
    private String alertmsg2;
    
    private String alerttext;
    
    private String alerttext1;
    
    private String alerttext2;
    
    private Long lvalue;
    
    private String szvalue;
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

   


   

    /**
     * @return the iddoctor
     */
    public Long getIddoctor() {
        return iddoctor;
    }

    /**
     * @param iddoctor the iddoctor to set
     */
    public void setIddoctor(Long iddoctor) {
        this.iddoctor = iddoctor;
    }

    /**
     * @return the idpatient
     */
    public Long getIdpatient() {
        return idpatient;
    }

    /**
     * @param idpatient the idpatient to set
     */
    public void setIdpatient(Long idpatient) {
        this.idpatient = idpatient;
    }

    /**
     * @return the itype
     */
    public int getItype() {
        return itype;
    }

    /**
     * @param itype the itype to set
     */
    public void setItype(int itype) {
        this.itype = itype;
    }

    /**
     * @return the alertmsg
     */
    public String getAlertmsg() {
        return alertmsg;
    }

    /**
     * @param alertmsg the alertmsg to set
     */
    public void setAlertmsg(String alertmsg) {
        this.alertmsg = alertmsg;
    }

    /**
     * @return the alertmsg1
     */
    public String getAlertmsg1() {
        return alertmsg1;
    }

    /**
     * @param alertmsg1 the alertmsg1 to set
     */
    public void setAlertmsg1(String alertmsg1) {
        this.alertmsg1 = alertmsg1;
    }

    /**
     * @return the alertmsg2
     */
    public String getAlertmsg2() {
        return alertmsg2;
    }

    /**
     * @param alertmsg2 the alertmsg2 to set
     */
    public void setAlertmsg2(String alertmsg2) {
        this.alertmsg2 = alertmsg2;
    }

    /**
     * @return the alerttext
     */
    public String getAlerttext() {
        return alerttext;
    }

    /**
     * @param alerttext the alerttext to set
     */
    public void setAlerttext(String alerttext) {
        this.alerttext = alerttext;
    }

    /**
     * @return the alerttext1
     */
    public String getAlerttext1() {
        return alerttext1;
    }

    /**
     * @param alerttext1 the alerttext1 to set
     */
    public void setAlerttext1(String alerttext1) {
        this.alerttext1 = alerttext1;
    }

    /**
     * @return the alerttext2
     */
    public String getAlerttext2() {
        return alerttext2;
    }

    /**
     * @param alerttext2 the alerttext2 to set
     */
    public void setAlerttext2(String alerttext2) {
        this.alerttext2 = alerttext2;
    }

    /**
     * @return the lvalue
     */
    public Long getLvalue() {
        return lvalue;
    }

    /**
     * @param lvalue the lvalue to set
     */
    public void setLvalue(Long lvalue) {
        this.lvalue = lvalue;
    }

    /**
     * @return the szvalue
     */
    public String getSzvalue() {
        return szvalue;
    }

    /**
     * @param szvalue the szvalue to set
     */
    public void setSzvalue(String szvalue) {
        this.szvalue = szvalue;
    }

    /**
     * @return the cDate
     */
    public Long getcDate() {
        return cDate;
    }

    /**
     * @param cDate the cDate to set
     */
    public void setcDate(Long cDate) {
        this.cDate = cDate;
    }
    
}
