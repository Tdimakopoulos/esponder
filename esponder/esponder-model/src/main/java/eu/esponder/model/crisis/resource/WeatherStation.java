package eu.esponder.model.crisis.resource;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;

@Entity
@Table(name="weatherstation")
@NamedQueries({
		@NamedQuery(name = "WeatherStation.findByTitle", query = "select c from WeatherStation c where c.title=:title"),
		@NamedQuery(name = "WeatherStation.findAll", query = "select c from WeatherStation c") })
public class WeatherStation extends ESponderEntity<Long> {

	private static final long serialVersionUID = 7509967835019265806L;
	
	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(name="WEATHERSTATION_ID")
	protected Long id;
	
	/** The title. Not Null,Unique*/
	@Column(name="TITLE", nullable=false, unique=true, length=255)
	protected String title;

	/** The status. Not Null*/
	@Enumerated(EnumType.STRING)
	@Column(name="WEATHERSTATION_STATUS", nullable=false)
	protected ResourceStatus status;
	
	@OneToOne(mappedBy="weatherStation")
	private OCMeoc operationsCentre;
	

	protected String temperature;
	
	protected String evaporation;
	
	protected String barometricpressure;
	
	protected String windspeed;
	
	protected String winddirection;
	
	protected String windgust;
	
	protected String relativehumidity;
	
	protected String solarradiation;
	
	protected String anemometer;
	
	protected String rain_gauge;
	
	protected String disdrometer;
	
	protected String transmissometer;
	
	protected String ceilometer;
	

	public OCMeoc getOperationsCentre() {
		return operationsCentre;
	}

	public void setOperationsCentre(OCMeoc operationsCentre) {
		this.operationsCentre = operationsCentre;
	}
	

	public String getTemperature() {
		return temperature;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}

	public String getEvaporation() {
		return evaporation;
	}

	public void setEvaporation(String evaporation) {
		this.evaporation = evaporation;
	}

	public String getBarometricpressure() {
		return barometricpressure;
	}

	public void setBarometricpressure(String barometricpressure) {
		this.barometricpressure = barometricpressure;
	}

	public String getWindspeed() {
		return windspeed;
	}

	public void setWindspeed(String windspeed) {
		this.windspeed = windspeed;
	}

	public String getWinddirection() {
		return winddirection;
	}

	public void setWinddirection(String winddirection) {
		this.winddirection = winddirection;
	}

	public String getWindgust() {
		return windgust;
	}

	public void setWindgust(String windgust) {
		this.windgust = windgust;
	}

	public String getRelativehumidity() {
		return relativehumidity;
	}

	public void setRelativehumidity(String relativehumidity) {
		this.relativehumidity = relativehumidity;
	}

	public String getSolarradiation() {
		return solarradiation;
	}

	public void setSolarradiation(String solarradiation) {
		this.solarradiation = solarradiation;
	}

	public String getAnemometer() {
		return anemometer;
	}

	public void setAnemometer(String anemometer) {
		this.anemometer = anemometer;
	}

	public String getRain_gauge() {
		return rain_gauge;
	}

	public void setRain_gauge(String rain_gauge) {
		this.rain_gauge = rain_gauge;
	}

	public String getDisdrometer() {
		return disdrometer;
	}

	public void setDisdrometer(String disdrometer) {
		this.disdrometer = disdrometer;
	}

	public String getTransmissometer() {
		return transmissometer;
	}

	public void setTransmissometer(String transmissometer) {
		this.transmissometer = transmissometer;
	}

	public String getCeilometer() {
		return ceilometer;
	}

	public void setCeilometer(String ceilometer) {
		this.ceilometer = ceilometer;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public ResourceStatus getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(ResourceStatus status) {
		this.status = status;
	}

}
