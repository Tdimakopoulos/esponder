package eu.esponder.model.crisis;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;
import eu.esponder.model.type.AngleType;
import eu.esponder.model.type.BiogeoclimaticvariantsType;
import eu.esponder.model.type.BiogeoclimaticzoneType;
import eu.esponder.model.type.DeseaseType;
import eu.esponder.model.type.EnvirconditionsType;
import eu.esponder.model.type.InsectsType;
import eu.esponder.model.type.LandType;
import eu.esponder.model.type.PlantationType;
import eu.esponder.model.type.SoilType;
import eu.esponder.model.type.WildfireType;
import eu.esponder.model.type.WindType;

@Entity
@Table(name="terrain_and_vegetation")
@NamedQueries({
	@NamedQuery(name="TerrainAndVegetation.findByTitle", query="select cc from TerrainAndVegetation cc where cc.title=:title"),
	@NamedQuery(name="TerrainAndVegetation.findAll", query="select cc from TerrainAndVegetation cc")
})
public class TerrainAndVegetation  extends ESponderEntity<Long> {
	
	
	private static final long serialVersionUID = 5427119875171788110L;
	
	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="TERRAIN_VEGETATION_ID")
	private Long id;
	
	/** The incident title. Will be provided by the user on creation of the entity. Not null , Unique */
	@Column(name="TITLE", nullable=false, unique=true, length=255)
	private String title;
	
	private SoilType soil;
	
	private LandType land;
	
	private AngleType angle;
	
	private PlantationType plantation;
	
	private BiogeoclimaticzoneType biogeoclimaticzone;
	
	private BiogeoclimaticvariantsType biogeoclimaticvariants;
	
	private EnvirconditionsType environmentalconditions;
	
	private WildfireType wildfire;
	
	private WindType wind;
	
	private InsectsType insects;
	
	private DeseaseType disease;
	
	private boolean humanactivity;
	
	
	public SoilType getSoil() {
		return soil;
	}


	public void setSoil(SoilType soil) {
		this.soil = soil;
	}


	public LandType getLand() {
		return land;
	}


	public void setLand(LandType land) {
		this.land = land;
	}


	public AngleType getAngle() {
		return angle;
	}


	public void setAngle(AngleType angle) {
		this.angle = angle;
	}


	public PlantationType getPlantation() {
		return plantation;
	}


	public void setPlantation(PlantationType plantation) {
		this.plantation = plantation;
	}


	public BiogeoclimaticzoneType getBiogeoclimaticzone() {
		return biogeoclimaticzone;
	}


	public void setBiogeoclimaticzone(BiogeoclimaticzoneType biogeoclimaticzone) {
		this.biogeoclimaticzone = biogeoclimaticzone;
	}


	public BiogeoclimaticvariantsType getBiogeoclimaticvariants() {
		return biogeoclimaticvariants;
	}


	public void setBiogeoclimaticvariants(
			BiogeoclimaticvariantsType biogeoclimaticvariants) {
		this.biogeoclimaticvariants = biogeoclimaticvariants;
	}


	public EnvirconditionsType getEnvironmentalconditions() {
		return environmentalconditions;
	}


	public void setEnvironmentalconditions(
			EnvirconditionsType environmentalconditions) {
		this.environmentalconditions = environmentalconditions;
	}


	public WildfireType getWildfire() {
		return wildfire;
	}


	public void setWildfire(WildfireType wildfire) {
		this.wildfire = wildfire;
	}


	public WindType getWind() {
		return wind;
	}


	public void setWind(WindType wind) {
		this.wind = wind;
	}


	public InsectsType getInsects() {
		return insects;
	}


	public void setInsects(InsectsType insects) {
		this.insects = insects;
	}


	public DeseaseType getDisease() {
		return disease;
	}


	public void setDisease(DeseaseType disease) {
		this.disease = disease;
	}


	public boolean isHumanactivity() {
		return humanactivity;
	}


	public void setHumanactivity(boolean humanactivity) {
		this.humanactivity = humanactivity;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}

	
	
	
		

}
