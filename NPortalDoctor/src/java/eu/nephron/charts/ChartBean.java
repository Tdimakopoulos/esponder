/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.charts;

import eu.cache.database.CDWAKDMeasurments;
import eu.nephron.cache.measurments.mesdb;
import eu.nephron.labelmanager.labelManager;
import eu.nephron.mestypes.mestypes;
import eu.nephron.model.DSSResults;
import eu.nephron.model.Sensors;
import eu.nephron.secure.ws.WAKDMeasurmentsManager_Service;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.faces.bean.ManagedBean;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.xml.ws.WebServiceRef;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LineChartSeries;

/**
 *
 * @author tdim
 */
@ManagedBean
@ViewScoped
public class ChartBean implements Serializable {
    

    private boolean bchartTotalView1;
    private boolean bchartTotalView2;
    private boolean bcondactivity_Dialysate;
    private boolean bpressure_Bloodline;
    private boolean bpressure_Dialysate;
    private boolean btemp_IN;
    private boolean btemp_OUT;
    private boolean binlet_PH;
    private boolean binlet_Potasium;
    private boolean binlet_Calcium;
    private boolean binlet_Temp;
    private boolean binlet_Uria;
    private boolean boutlet_PH;
    private boolean boutlet_Potasium;
    private boolean boutlet_Calcium;
    private boolean boutlet_Temp;
    private boolean boutlet_Uria;
    private boolean bdiastolic;
    private boolean bheartrate;
    private boolean bsystolic;
    private boolean bbodyfat;
    private boolean bweight;
    private boolean cbcondactivity_Dialysate;
    private boolean cbpressure_Bloodline;
    private boolean cbpressure_Dialysate;
    private boolean cbtemp_IN;
    private boolean cbtemp_OUT;
    private boolean cbinlet_PH;
    private boolean cbinlet_Potasium;
    private boolean cbinlet_Calcium;
    private boolean cbinlet_Temp;
    private boolean cbinlet_Uria;
    private boolean cboutlet_PH;
    private boolean cboutlet_Potasium;
    private boolean cboutlet_Calcium;
    private boolean cboutlet_Temp;
    private boolean cboutlet_Uria;
    private boolean cbdiastolic;
    private boolean cbheartrate;
    private boolean cbsystolic;
    private boolean cbbodyfat;
    private boolean cbweight;
    private CartesianChartModel chartTotalView1;
    private CartesianChartModel chartTotalView2;
    private CartesianChartModel condactivity_Dialysate;
    private CartesianChartModel pressure_Bloodline;
    private CartesianChartModel pressure_Dialysate;
    private CartesianChartModel temp_IN;
    private CartesianChartModel temp_OUT;
    private CartesianChartModel inlet_PH;
    private CartesianChartModel inlet_Potasium;
    private CartesianChartModel inlet_Calcium;
    private CartesianChartModel inlet_Temp;
    private CartesianChartModel inlet_Uria;
    private CartesianChartModel outlet_PH;
    private CartesianChartModel outlet_Potasium;
    private CartesianChartModel outlet_Calcium;
    private CartesianChartModel outlet_Temp;
    private CartesianChartModel outlet_Uria;
    private CartesianChartModel diastolic;
    private CartesianChartModel heartrate;
    private CartesianChartModel systolic;
    private CartesianChartModel bodyfat;
    private CartesianChartModel weight;
    private CartesianChartModel model;
    private List<Sensors> sensors;
    private List<Sensors> sensorsOriginal;
    String previousid = "NA";
    private Date dateFromM = new Date();
    private Date dateToM = new Date();
    private String number;
    private List<String> selectedCharts=new ArrayList();
    private boolean value1;

    private final static int MAX_VALUE = 20;
private final static int NUMBER_OF_POINTS = 20;
private final static DateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");

private void createLinearModel22() {
        setModel(new CartesianChartModel());
    Calendar day = Calendar.getInstance();
    day.set(Calendar.HOUR_OF_DAY, 0);
    day.set(Calendar.MINUTE, 0);
    day.set(Calendar.SECOND, 0);
    day.set(Calendar.MILLISECOND, 0);
    
    LineChartSeries series = new LineChartSeries();
    series.setLabel("My series");
    for (int i = 0; i < NUMBER_OF_POINTS; i++) {
        Date dd = new Date();
        dd.setTime(day.getTime().getTime());
        series.set(dd.toString(), getRandomValue());
        day.add(Calendar.DAY_OF_MONTH, 1);
    }
        getModel().addSeries(series);
}

private int getRandomValue() {
    
    Random p= new Random();
            return p.nextInt(MAX_VALUE);
}

    public double ConvertValues(int itype,double dvalue)
    {
        double dnew;
        dnew=dvalue;
        if(itype==12)
        {
            dnew=dvalue/10;
        }
        if(itype==11)
        {
            dnew=dvalue/10;
        }
        if(itype==13)
        {
            dnew=dvalue/10;
        }
        if(itype==15)
        {
            dnew=dvalue/10;
        }
        if(itype==17)
        {
            dnew=dvalue/10;
        }
        if(itype==5)
        {
            dnew=dvalue*10;
        }
        if(itype==18)
        {
            dnew=dvalue/10;
        }
        
        return dnew;
    }
    
    public void ReloadDataFromDB()
    {
        java.util.List<eu.nephron.secure.ws.WakdMeasurments> pReturn = findAllWAKDMeasurmentsManager();
        sensors.clear();
        sensorsOriginal.clear();
        for (int i = 0; i < pReturn.size(); i++) {
            Sensors psensor = new Sensors();
            Date dd = new Date();
            dd.setTime(pReturn.get(i).getDate());
            psensor.setTime(dd);
            psensor.setType(pReturn.get(i).getType().toString());
    //        psensor.setValue(pReturn.get(i).getValue());
double dValue=ConvertValues(Integer.valueOf(pReturn.get(i).getType().toString()),pReturn.get(i).getValue());
            //psensor.setValue(pReturn.get(i).getValue());
            psensor.setValue(dValue);
            sensors.add(psensor);
            sensorsOriginal.add(psensor);
        }
    }
    public void UpdateRecords(ActionEvent actionEvent) {
        ReloadDataFromDB();
        sensors.clear();

        mestypes ptypes = new mestypes();
        for (int i = 0; i < sensorsOriginal.size(); i++) {
            String typecomp = String.valueOf(ptypes.GetType(number));
            String typecomp1 = sensorsOriginal.get(i).getType();
            Date date = sensorsOriginal.get(i).getTime();
            if (typecomp1.equalsIgnoreCase(typecomp)) {
                if (date.getTime() > dateFromM.getTime()) {
                    if (date.getTime() < dateToM.getTime()) {
                        sensors.add(sensorsOriginal.get(i));
                    }
                }
            } else {
            }
        }


    }

    public void UpdateRecords1(ActionEvent actionEvent) {

        System.out.println("***** Start update"+new Date());

        bcondactivity_Dialysate = false;
        bpressure_Bloodline = false;
        bpressure_Dialysate = false;
        btemp_IN = false;
        btemp_OUT = false;
        binlet_PH = false;
        binlet_Potasium = false;
        binlet_Calcium = false;
        binlet_Temp = false;
        binlet_Uria = false;
        boutlet_PH = false;
        boutlet_Potasium = false;
        boutlet_Calcium = false;
        boutlet_Temp = false;
        boutlet_Uria = false;
        bdiastolic = false;
        bheartrate = false;
        bsystolic = false;
        bbodyfat = false;
        bweight = false;


        cbcondactivity_Dialysate = false;
        cbpressure_Bloodline = false;
        cbpressure_Dialysate = false;
        cbtemp_IN = false;
        cbtemp_OUT = false;
        cbinlet_PH = false;
        cbinlet_Potasium = false;
        cbinlet_Calcium = false;
        cbinlet_Temp = false;
        cbinlet_Uria = false;
        cboutlet_PH = false;
        cboutlet_Potasium = false;
        cboutlet_Calcium = false;
        cboutlet_Temp = false;
        cboutlet_Uria = false;
        cbdiastolic = false;
        cbheartrate = false;
        cbsystolic = false;
        cbbodyfat = false;
        cbweight = false;

        for (int ic = 0; ic < selectedCharts.size(); ic++) {
            String v1 = selectedCharts.get(ic);
            if (v1.equalsIgnoreCase("Condactivity Dialysate")) {
                bcondactivity_Dialysate = true;

            } else {
                //  bcondactivity_Dialysate = false;
            }

            if (v1.equalsIgnoreCase("Pressure Bloodline")) {
                bpressure_Bloodline = true;

            } else {
                //bpressure_Bloodline = false;
            }

            if (v1.equalsIgnoreCase("Pressure Dialysate")) {
                bpressure_Dialysate = true;

            } else {
                //bpressure_Dialysate = false;
            }

            if (v1.equalsIgnoreCase("Temp IN")) {
                btemp_IN = true;

            } else {
                //btemp_IN = false;
            }

            if (v1.equalsIgnoreCase("Temp OUT")) {
                btemp_OUT = true;

            } else {
                //btemp_OUT = false;
            }

            if (v1.equalsIgnoreCase("Inlet PH")) {
                binlet_PH = true;

            } else {
                //binlet_PH = false;
            }

            if (v1.equalsIgnoreCase("Inlet Potasium")) {
                binlet_Potasium = true;

            } else {
                //binlet_Potasium = false;
            }

            if (v1.equalsIgnoreCase("Inlet Calcium")) {
                binlet_Calcium = true;

            } else {
                //binlet_Calcium = false;
            }

            if (v1.equalsIgnoreCase("Inlet Temp")) {
                binlet_Temp = true;

            } else {
                //binlet_Temp = false;
            }

            if (v1.equalsIgnoreCase("Inlet Uria")) {
                binlet_Uria = true;

            } else {
                //binlet_Uria = false;
            }

            if (v1.equalsIgnoreCase("Outlet PH")) {
                boutlet_PH = true;

            } else {
                //boutlet_PH = false;
            }

            if (v1.equalsIgnoreCase("Outlet Potasium")) {
                boutlet_Potasium = true;

            } else {
                //boutlet_Potasium = false;
            }

            if (v1.equalsIgnoreCase("Outlet Calcium")) {
                boutlet_Calcium = true;

            } else {
                //boutlet_Calcium = false;
            }

            if (v1.equalsIgnoreCase("Outlet Temp")) {
                boutlet_Temp = true;

            } else {
                //boutlet_Temp = false;
            }

            if (v1.equalsIgnoreCase("Outlet Uria")) {
                boutlet_Uria = true;

            } else {
                //boutlet_Uria = false;
            }

            if (v1.equalsIgnoreCase("Diastolic")) {
                bdiastolic = true;

            } else {
                // bdiastolic = false;
            }

            if (v1.equalsIgnoreCase("Heartrate")) {
                bheartrate = true;

            } else {
                // bheartrate = false;
            }

            if (v1.equalsIgnoreCase("Systolic")) {
                bsystolic = true;

            } else {
                //bsystolic = false;
            }

            if (v1.equalsIgnoreCase("Bodyfat")) {
                bbodyfat = true;

            } else {
                //bbodyfat = false;
            }

            if (v1.equalsIgnoreCase("Weight")) {
                bweight = true;

            } else {
                //bweight = false;
            }
        }


        createLinearModel(true);
         System.out.println("***** End update"+new Date());
    }

    public String loadmeasurments()
    {
        System.out.println("***** Start Load"+new Date());
        dateFromM=new Date();
        dateToM=new Date();
        selectedCharts=new ArrayList();
        
        sensors.clear();
        sensorsOriginal.clear();
        createLinearModel(false);
        createCategoryModel();
        System.out.println("***** End Load"+new Date());
        return "Charts?faces-redirect=true";
    }
    
    public ChartBean() {
        String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");//.put("PUserID", getUseridforedit());
        if (previousid.equalsIgnoreCase("NA")) {
            previousid = puserid;
        }
        sensors = new ArrayList<Sensors>();
        sensorsOriginal = new ArrayList<Sensors>();
        createLinearModel(false);
        createCategoryModel();
    }

    public void updatereadings() {
        System.err.println("Updating Readings Called");
        createLinearModel(true);
        createCategoryModel();
    }

    public int FindValueTypeMax(java.util.List<eu.nephron.secure.ws.WakdMeasurments> pReturn, int itype) {
        int iret = 0;
        for (int i = 0; i < pReturn.size(); i++) {
            if (pReturn.get(i).getType() == itype) {
                iret++;
            }
        }
        return iret;
    }

    private void createLinearModel(boolean checkdvalues) {

        if(checkdvalues==false)
        {
            dateToM= new Date();
            Date pdate = new Date();
        Calendar pcal = new GregorianCalendar();
        pcal.setTime(pdate);
        pcal.add(Calendar.DATE, -2);
        dateFromM.setTime(pcal.getTime().getTime());
        }
        
        if (!checkdvalues) {
            bcondactivity_Dialysate = false;
            bpressure_Bloodline = false;
            bpressure_Dialysate = false;
            btemp_IN = false;
            btemp_OUT = false;
            binlet_PH = false;
            binlet_Potasium = false;
            binlet_Calcium = false;
            binlet_Temp = false;
            binlet_Uria = false;
            boutlet_PH = false;
            boutlet_Potasium = false;
            boutlet_Calcium = false;
            boutlet_Temp = false;
            boutlet_Uria = false;
            bdiastolic = false;
            bheartrate = false;
            bsystolic = false;
            bbodyfat = false;
            bweight = false;
        }

      
        cbcondactivity_Dialysate = false;
        cbpressure_Bloodline = false;
        cbpressure_Dialysate = false;
        cbtemp_IN = false;
        cbtemp_OUT = false;
        cbinlet_PH = false;
        cbinlet_Potasium = false;
        cbinlet_Calcium = false;
        cbinlet_Temp = false;
        cbinlet_Uria = false;
        cboutlet_PH = false;
        cboutlet_Potasium = false;
        cboutlet_Calcium = false;
        cboutlet_Temp = false;
        cboutlet_Uria = false;
        cbdiastolic = false;
        cbheartrate = false;
        cbsystolic = false;
        cbbodyfat = false;
        cbweight = false;

        chartTotalView1 = new CartesianChartModel();
        chartTotalView2 = new CartesianChartModel();
        condactivity_Dialysate = new CartesianChartModel();
        pressure_Bloodline = new CartesianChartModel();
        pressure_Dialysate = new CartesianChartModel();
        temp_IN = new CartesianChartModel();
        temp_OUT = new CartesianChartModel();
        inlet_PH = new CartesianChartModel();
        inlet_Potasium = new CartesianChartModel();
        inlet_Calcium = new CartesianChartModel();
        inlet_Temp = new CartesianChartModel();
        inlet_Uria = new CartesianChartModel();
        outlet_PH = new CartesianChartModel();
        outlet_Potasium = new CartesianChartModel();
        outlet_Calcium = new CartesianChartModel();
        outlet_Temp = new CartesianChartModel();
        outlet_Uria = new CartesianChartModel();
        diastolic = new CartesianChartModel();
        heartrate = new CartesianChartModel();
        systolic = new CartesianChartModel();
        bodyfat = new CartesianChartModel();
        weight = new CartesianChartModel();



        ChartSeries pCondactivity_Dialysate = new ChartSeries();
        ChartSeries pPressure_Bloodline = new ChartSeries();
        ChartSeries pPressure_Dialysate = new ChartSeries();
        ChartSeries pTemp_IN = new ChartSeries();
        ChartSeries pTemp_OUT = new ChartSeries();
        ChartSeries pInlet_PH = new ChartSeries();
        ChartSeries pInlet_Potasium = new ChartSeries();
        ChartSeries pInlet_Calcium = new ChartSeries();
        ChartSeries pInlet_Temp = new ChartSeries();
        ChartSeries pInlet_Uria = new ChartSeries();
        ChartSeries pOutlet_PH = new ChartSeries();
        ChartSeries pOutlet_Potasium = new ChartSeries();
        ChartSeries pOutlet_Calcium = new ChartSeries();
        ChartSeries pOutlet_Temp = new ChartSeries();
        ChartSeries pOutlet_Uria = new ChartSeries();
        ChartSeries pDiastolic = new ChartSeries();
        ChartSeries pHeartrate = new ChartSeries();
        ChartSeries pSystolic = new ChartSeries();
        ChartSeries pBodyfat = new ChartSeries();
        ChartSeries pWeight = new ChartSeries();

        pCondactivity_Dialysate.setLabel("Conductivity Dialysate (mS/cm)");
        pPressure_Bloodline.setLabel("Pressure Bloodline (mm/Hg)");
        pPressure_Dialysate.setLabel("Pressure Dialysate (mm/Hg)");
        pTemp_IN.setLabel("Temperature Bloodline In (°C)");
        pTemp_OUT.setLabel("Temperature Bloodline Out (°C)");
        pInlet_PH.setLabel("Inlet pH");
        pInlet_Potasium.setLabel("Potassium Inlet (mmol/L)");
        pInlet_Calcium.setLabel("Calcium Inlet (mmol/L)");
        pInlet_Temp.setLabel("Temp Inlet (°C)");
        pInlet_Uria.setLabel("Oxidation State");
        pOutlet_PH.setLabel("Outlet pH");
        pOutlet_Potasium.setLabel("Potassium Outlet (mmol/L)");
        pOutlet_Calcium.setLabel("Calcium Outlet (mmol/L)");
        pOutlet_Temp.setLabel("Temp Outlet (°C)");
        pOutlet_Uria.setLabel("Uria Outlet  (mmol/L)");
        pDiastolic.setLabel("Blood pressure Diastolic (mm/Hg)");
        pHeartrate.setLabel("Heart Rate (bpm)");
        pSystolic.setLabel("Blood pressure Systolic (mm/Hg)");
        pBodyfat.setLabel("Bodyfat");
        pWeight.setLabel("Weight");

        LineChartSeries lpCondactivity_Dialysate = new LineChartSeries();
        LineChartSeries lpPressure_Bloodline = new LineChartSeries();
        LineChartSeries lpPressure_Dialysate = new LineChartSeries();
        LineChartSeries lpTemp_IN = new LineChartSeries();
        LineChartSeries lpTemp_OUT = new LineChartSeries();
        LineChartSeries lpInlet_PH = new LineChartSeries();
        LineChartSeries lpInlet_Potasium = new LineChartSeries();
        LineChartSeries lpInlet_Calcium = new LineChartSeries();
        LineChartSeries lpInlet_Temp = new LineChartSeries();
        LineChartSeries lpInlet_Uria = new LineChartSeries();
        LineChartSeries lpOutlet_PH = new LineChartSeries();
        LineChartSeries lpOutlet_Potasium = new LineChartSeries();
        LineChartSeries lpOutlet_Calcium = new LineChartSeries();
        LineChartSeries lpOutlet_Temp = new LineChartSeries();
        LineChartSeries lpOutlet_Uria = new LineChartSeries();
        LineChartSeries lpDiastolic = new LineChartSeries();
        LineChartSeries lpHeartrate = new LineChartSeries();
        LineChartSeries lpSystolic = new LineChartSeries();
        LineChartSeries lpBodyfat = new LineChartSeries();
        LineChartSeries lpWeight = new LineChartSeries();

         lpCondactivity_Dialysate.setLabel("Conductivity Dialysate (mS/cm)");
        lpPressure_Bloodline.setLabel("Pressure Bloodline (mm/Hg)");
        lpPressure_Dialysate.setLabel("Pressure Dialysate (mm/Hg)");
        lpTemp_IN.setLabel("Temperature Bloodline In (°C)");
        lpTemp_OUT.setLabel("Temperature Bloodline Out (°C)");
        lpInlet_PH.setLabel("Inlet pH");
        lpInlet_Potasium.setLabel("Potassium Inlet (mmol/L)");
        lpInlet_Calcium.setLabel("Calcium Inlet (mmol/L)");
        lpInlet_Temp.setLabel("Temp Inlet (°C)");
        lpInlet_Uria.setLabel("Oxidation State");
        lpOutlet_PH.setLabel("Outlet pH");
        lpOutlet_Potasium.setLabel("Potassium Outlet (mmol/L)");
        lpOutlet_Calcium.setLabel("Calcium Outlet (mmol/L)");
        lpOutlet_Temp.setLabel("Temp Outlet (°C)");
        lpOutlet_Uria.setLabel("Uria Outlet  (mmol/L)");
        lpDiastolic.setLabel("Blood pressure Diastolic (mm/Hg)");
        lpHeartrate.setLabel("Heart Rate (bpm)");
        lpSystolic.setLabel("Blood pressure Systolic (mm/Hg)");
        lpBodyfat.setLabel("Bodyfat");
        lpWeight.setLabel("Weight");

//        lpCondactivity_Dialysate.setLabel("Condactivity Dialysate");
//        lpPressure_Bloodline.setLabel("Pressure Bloodline");
//        lpPressure_Dialysate.setLabel("Pressure Dialysate");
//        lpTemp_IN.setLabel("Temp In");
//        lpTemp_OUT.setLabel("Temp out");
//        lpInlet_PH.setLabel("PH Inlet");
//        lpInlet_Potasium.setLabel("Potasium Inlet");
//        lpInlet_Calcium.setLabel("Calcium Inlet");
//        lpInlet_Temp.setLabel("Temp Inlet");
//        lpInlet_Uria.setLabel("Uria Inlet");
//        lpOutlet_PH.setLabel("PH Outlet");
//        lpOutlet_Potasium.setLabel("Potasium Outlet");
//        lpOutlet_Calcium.setLabel("Calcium Outlet");
//        lpOutlet_Temp.setLabel("Temp Outlet");
//        lpOutlet_Uria.setLabel("Uria Outlet");
//        lpDiastolic.setLabel("Diastolic");
//        lpHeartrate.setLabel("Heartrate");
//        lpSystolic.setLabel("Systolic");
//        lpBodyfat.setLabel("Bodyfat");
//        lpWeight.setLabel("Weight");

        
        java.util.List<eu.nephron.secure.ws.WakdMeasurments> pReturn = findAllWAKDMeasurmentsManager();
        sensors.clear();
        sensorsOriginal.clear();
        for (int i = 0; i < pReturn.size(); i++) {
            Sensors psensor = new Sensors();
            Date dd = new Date();
            dd.setTime(pReturn.get(i).getDate());
            psensor.setTime(dd);
            psensor.setType(pReturn.get(i).getType().toString());
      //      psensor.setValue(pReturn.get(i).getValue());
double dValue=ConvertValues(pReturn.get(i).getType().intValue(),pReturn.get(i).getValue());
            //psensor.setValue(pReturn.get(i).getValue());
            psensor.setValue(dValue);
            sensors.add(psensor);
            sensorsOriginal.add(psensor);
        }
        
//Collections.reverse(sensors);
//Collections.reverse(sensorsOriginal);
        labelManager p1 = new labelManager(FindValueTypeMax(pReturn, 1), 6);
        labelManager p2 = new labelManager(FindValueTypeMax(pReturn, 2), 6);
        labelManager p3 = new labelManager(FindValueTypeMax(pReturn, 3), 6);
        labelManager p4 = new labelManager(FindValueTypeMax(pReturn, 4), 6);
        labelManager p5 = new labelManager(FindValueTypeMax(pReturn, 5), 6);
        labelManager p6 = new labelManager(FindValueTypeMax(pReturn, 6), 6);
        labelManager p7 = new labelManager(FindValueTypeMax(pReturn, 7), 6);
        labelManager p8 = new labelManager(FindValueTypeMax(pReturn, 8), 6);
        labelManager p9 = new labelManager(FindValueTypeMax(pReturn, 9), 6);
        labelManager p10 = new labelManager(FindValueTypeMax(pReturn, 10), 6);
        labelManager p11 = new labelManager(FindValueTypeMax(pReturn, 11), 6);
        labelManager p12 = new labelManager(FindValueTypeMax(pReturn, 12), 6);
        labelManager p13 = new labelManager(FindValueTypeMax(pReturn, 13), 6);
        labelManager p14 = new labelManager(FindValueTypeMax(pReturn, 14), 6);
        labelManager p15 = new labelManager(FindValueTypeMax(pReturn, 15), 6);
        labelManager p16 = new labelManager(FindValueTypeMax(pReturn, 16), 6);
        labelManager p17 = new labelManager(FindValueTypeMax(pReturn, 17), 6);
        labelManager p18 = new labelManager(FindValueTypeMax(pReturn, 18), 6);

        labelManager p30 = new labelManager(FindValueTypeMax(pReturn, 30), 6);
        labelManager p31 = new labelManager(FindValueTypeMax(pReturn, 31), 6);
        labelManager p32 = new labelManager(FindValueTypeMax(pReturn, 32), 6);

        labelManager p40 = new labelManager(FindValueTypeMax(pReturn, 40), 6);
        labelManager p41 = new labelManager(FindValueTypeMax(pReturn, 41), 6);
        //Collections.reverse(sensors);
        Collections.reverse(sensorsOriginal);
        Date ppdate = new Date();
        boolean brun = true;
        for (int i = 0; i < pReturn.size(); i++) {
            ppdate.setTime(pReturn.get(i).getDate());
            //SimpleDateFormat sdfSource = new SimpleDateFormat(
              //      "dd-mm-yy hh:mm:ss");
            //String pdate = sdfSource.format(ppdate);//ppdate.toString();
            Long pdate=ppdate.getTime();
            
            if (checkdvalues) {
                if (ppdate.getTime() > dateFromM.getTime()) {
                    if (ppdate.getTime() < dateToM.getTime()) {
                        brun = true;
                        //  System.out.println("Date check add");
                    } else {
                        brun = false;
                        //    System.out.println("Date check no add");
                    }
                } else {
                    brun = false;
                    //  System.out.println("Date check no add");
                }

            } else {
                brun = true;
            }
            if (brun
                    == true) {
                if (pReturn.get(i).getType() == 1) {
                    cbinlet_PH = true;
                    //if (p1.isShow()) {
                        lpInlet_PH.set(pdate, pReturn.get(i).getValue());
                        pInlet_PH.set(pdate, pReturn.get(i).getValue());
                    //} else {
                      //  lpInlet_PH.set("", pReturn.get(i).getValue());
//                        pInlet_PH.set("", pReturn.get(i).getValue());
  //                  }
                }
                if (pReturn.get(i).getType() == 2) {
                    cbinlet_Potasium = true;
    //                if (p2.isShow()) {
                        lpInlet_Potasium.set(pdate, pReturn.get(i).getValue());
                        pInlet_Potasium.set(pdate, pReturn.get(i).getValue());
      //              } else {
        //                lpInlet_Potasium.set("", pReturn.get(i).getValue());
          //              pInlet_Potasium.set("", pReturn.get(i).getValue());
            //        }
                }
                if (pReturn.get(i).getType() == 3) {
                    cbinlet_Calcium = true;
              //      if (p3.isShow()) {
                        lpInlet_Calcium.set(pdate, pReturn.get(i).getValue());
                        pInlet_Calcium.set(pdate, pReturn.get(i).getValue());
                //    } else {
                  //      lpInlet_Calcium.set("", pReturn.get(i).getValue());
                    //    pInlet_Calcium.set("", pReturn.get(i).getValue());
//
  //                  }
                }
                if (pReturn.get(i).getType() == 4) {
                    cbinlet_Temp = true;
    //                if (p4.isShow()) {
                        lpInlet_Temp.set(pdate, pReturn.get(i).getValue());
                        pInlet_Temp.set(pdate, pReturn.get(i).getValue());
      //              } else {
        //                lpInlet_Temp.set("", pReturn.get(i).getValue());
          //              pInlet_Temp.set("", pReturn.get(i).getValue());

            //        }
                }
                if (pReturn.get(i).getType() == 5) {
                    cbinlet_Uria = true;
              //      if (p5.isShow()) {
                        lpInlet_Uria.set(pdate, pReturn.get(i).getValue());
                        pInlet_Uria.set(pdate, pReturn.get(i).getValue());
                //    } else {
                  //      lpInlet_Uria.set("", pReturn.get(i).getValue());
                    //    pInlet_Uria.set("", pReturn.get(i).getValue());

                    //}
                }
                if (pReturn.get(i).getType() == 6) {
                    cboutlet_PH = true;
                    //if (p6.isShow()) {

                        lpOutlet_PH.set(pdate, pReturn.get(i).getValue());
                        pOutlet_PH.set(pdate, pReturn.get(i).getValue());
                    //} else {
                      //  lpOutlet_PH.set("", pReturn.get(i).getValue());
                      //  pOutlet_PH.set("", pReturn.get(i).getValue());
                   // }
                }
                if (pReturn.get(i).getType() == 7) {
                    cboutlet_Potasium = true;
                    //if (p7.isShow()) {

                        lpOutlet_Potasium.set(pdate, pReturn.get(i).getValue());
                        pOutlet_Potasium.set(pdate, pReturn.get(i).getValue());
                   // } else {
                     //   lpOutlet_Potasium.set("", pReturn.get(i).getValue());
                       // pOutlet_Potasium.set("", pReturn.get(i).getValue());

                    //}
                }
                if (pReturn.get(i).getType() == 8) {
                    cboutlet_Calcium = true;
                    //if (p8.isShow()) {

                        lpOutlet_Calcium.set(pdate, pReturn.get(i).getValue());
                        pOutlet_Calcium.set(pdate, pReturn.get(i).getValue());
                   // } else {
                     //   lpOutlet_Calcium.set("", pReturn.get(i).getValue());
                      //  pOutlet_Calcium.set("", pReturn.get(i).getValue());

                    //}
                }
                if (pReturn.get(i).getType() == 9) {
                    cboutlet_Temp = true;
                    //if (p9.isShow()) {

                        lpOutlet_Temp.set(pdate, pReturn.get(i).getValue());
                        pOutlet_Temp.set(pdate, pReturn.get(i).getValue());
                    //} else {
                      //  lpOutlet_Temp.set("", pReturn.get(i).getValue());
//                        pOutlet_Temp.set("", pReturn.get(i).getValue());
//
  //                  }
                }
                if (pReturn.get(i).getType() == 10) {
                    cboutlet_Uria = true;
    //                if (p10.isShow()) {

                        lpOutlet_Uria.set(pdate, pReturn.get(i).getValue());
                        pOutlet_Uria.set(pdate, pReturn.get(i).getValue());
      //              } else {
        //                lpOutlet_Uria.set("", pReturn.get(i).getValue());
          //              pOutlet_Uria.set("", pReturn.get(i).getValue());

            //        }
                }
                if (pReturn.get(i).getType() == 11) {
                    cbcondactivity_Dialysate = true;
              //      if (p11.isShow()) {
//ConvertValues
  
                        lpCondactivity_Dialysate.set(pdate, pReturn.get(i).getValue()/10);
                        pCondactivity_Dialysate.set(pdate, pReturn.get(i).getValue()/10);
                //    } else {
                  //      lpCondactivity_Dialysate.set("", pReturn.get(i).getValue());
                    //    pCondactivity_Dialysate.set("", pReturn.get(i).getValue());

                   // }
                }
                if (pReturn.get(i).getType() == 12) {
                }
                if (pReturn.get(i).getType() == 13) {
                    cbpressure_Bloodline = true;
                   // if (p13.isShow()) {

                        lpPressure_Bloodline.set(pdate, pReturn.get(i).getValue());
                        pPressure_Bloodline.set(pdate, pReturn.get(i).getValue());
                    //} else {
//                        lpPressure_Bloodline.set("", pReturn.get(i).getValue());
                      //  pPressure_Bloodline.set("", pReturn.get(i).getValue());

                    //}
                }
                if (pReturn.get(i).getType() == 14) {
                }
                if (pReturn.get(i).getType() == 15) {
                    cbpressure_Dialysate = true;
  //                  if (p15.isShow()) {

                        lpPressure_Dialysate.set(pdate, pReturn.get(i).getValue());
                        pPressure_Dialysate.set(pdate, pReturn.get(i).getValue());
    //                } else {
      //                  lpPressure_Dialysate.set("", pReturn.get(i).getValue());
        //                pPressure_Dialysate.set("", pReturn.get(i).getValue());

          //          }
                }
                if (pReturn.get(i).getType() == 16) {
                }
                if (pReturn.get(i).getType() == 17) {
                    cbtemp_IN = true;
            //        if (p17.isShow()) {

                        lpTemp_IN.set(pdate, pReturn.get(i).getValue());
                        pTemp_IN.set(pdate, pReturn.get(i).getValue());
              //      } else {
                //        lpTemp_IN.set("", pReturn.get(i).getValue());
                  //      pTemp_IN.set("", pReturn.get(i).getValue());

                    //}
                }
                if (pReturn.get(i).getType() == 18) {
                    cbtemp_OUT = true;
 //                   /if (p18.isShow()) {
//
                        lpTemp_OUT.set(pdate, pReturn.get(i).getValue());
                        pTemp_OUT.set(pdate, pReturn.get(i).getValue());
 //                   } else {
   //                     lpTemp_OUT.set("", pReturn.get(i).getValue());
     //                   pTemp_OUT.set("", pReturn.get(i).getValue());

       //             }
                }
                if (pReturn.get(i).getType() == 30) {
                    cbdiastolic = true;
         //           if (p30.isShow()) {

                        lpDiastolic.set(pdate, pReturn.get(i).getValue());
                        pDiastolic.set(pdate, pReturn.get(i).getValue());
           //         } else {
             //           lpDiastolic.set("", pReturn.get(i).getValue());
               //         pDiastolic.set("", pReturn.get(i).getValue());

                 //   }
                }
                if (pReturn.get(i).getType() == 31) {
                    cbheartrate = true;
                   // if (p31.isShow()) {

                        lpHeartrate.set(pdate, pReturn.get(i).getValue());
                        pHeartrate.set(pdate, pReturn.get(i).getValue());
                   // } else {
                     //   lpHeartrate.set("", pReturn.get(i).getValue());
                    //    pHeartrate.set("", pReturn.get(i).getValue());

                   // }
                }
                if (pReturn.get(i).getType() == 32) {
                    cbsystolic = true;
                   // if (p32.isShow()) {

                        lpSystolic.set(pdate, pReturn.get(i).getValue());
                        pSystolic.set(pdate, pReturn.get(i).getValue());
                   // } else {
                     //   lpSystolic.set("", pReturn.get(i).getValue());
                     //   pSystolic.set("", pReturn.get(i).getValue());

                   // }
                }
                if (pReturn.get(i).getType() == 40) {
                    cbbodyfat = true;
                   // if (p40.isShow()) {

                        lpBodyfat.set(pdate, pReturn.get(i).getValue());
                        pBodyfat.set(pdate, pReturn.get(i).getValue());
                   // } else {
                     //   lpBodyfat.set("", pReturn.get(i).getValue());
                       // pBodyfat.set("", pReturn.get(i).getValue());

                   // }
                }
                if (pReturn.get(i).getType() == 41) {
                    cbweight = true;
                   // if (p41.isShow()) {

                        lpWeight.set(pdate, pReturn.get(i).getValue());
                        pWeight.set(pdate, pReturn.get(i).getValue());
                   // } else {
                     //   lpWeight.set("", pReturn.get(i).getValue());
                       // pWeight.set("", pReturn.get(i).getValue());

                   // }
                }
            }
        }
        if (cbcondactivity_Dialysate == false) {
            lpCondactivity_Dialysate.set(0, 0);
        }
        if (cbpressure_Bloodline == false) {
            lpPressure_Bloodline.set(0, 0);
        }
        if (cbpressure_Dialysate == false) {
            lpPressure_Dialysate.set(0, 0);
        }
        if (cbtemp_IN == false) {
            lpTemp_IN.set(0, 0);
        }
        if (cbtemp_OUT == false) {
            lpTemp_OUT.set(0, 0);
        }
        if (cbinlet_PH == false) {
            lpInlet_PH.set(0, 0);
        }
        if (cbinlet_Potasium == false) {
            lpInlet_Potasium.set(0, 0);
        }
        if (cbinlet_Calcium == false) {
            lpInlet_Calcium.set(0, 0);
        }
        if (cbinlet_Temp == false) {
            lpInlet_Temp.set(0, 0);
        }
        if (cbinlet_Uria == false) {
            lpInlet_Uria.set(0, 0);
        }
        if (cboutlet_PH == false) {
            lpOutlet_PH.set(0, 0);
        }
        if (cboutlet_Potasium == false) {
            lpOutlet_Potasium.set(0, 0);
        }
        if (cboutlet_Calcium == false) {
            lpOutlet_Calcium.set(0, 0);
        }
        if (cboutlet_Temp == false) {
            lpOutlet_Temp.set(0, 0);
        }
        if (cboutlet_Uria == false) {
            lpOutlet_Uria.set(0, 0);
        }
        if (cbdiastolic == false) {
            lpDiastolic.set(0, 0);
        }
        if (cbheartrate == false) {
            lpHeartrate.set(0, 0);
        }
        if (cbsystolic == false) {
            lpSystolic.set(0, 0);
        }
        if (cbbodyfat == false) {
            lpBodyfat.set(0, 0);
        }
        if (cbweight == false) {
            lpWeight.set(0, 0);
        }


        //add series into charts
        condactivity_Dialysate.addSeries(lpCondactivity_Dialysate);
        pressure_Bloodline.addSeries(lpPressure_Bloodline);
        pressure_Dialysate.addSeries(lpPressure_Dialysate);
        temp_IN.addSeries(lpTemp_IN);
        temp_OUT.addSeries(lpTemp_OUT);
        inlet_PH.addSeries(lpInlet_PH);
        inlet_Potasium.addSeries(lpInlet_Potasium);
        inlet_Calcium.addSeries(lpInlet_Calcium);
        inlet_Temp.addSeries(lpInlet_Temp);
        inlet_Uria.addSeries(lpInlet_Uria);
        outlet_PH.addSeries(lpOutlet_PH);
        outlet_Potasium.addSeries(lpOutlet_Potasium);
        outlet_Calcium.addSeries(lpOutlet_Calcium);
        outlet_Temp.addSeries(lpOutlet_Temp);
        outlet_Uria.addSeries(lpOutlet_Uria);
        diastolic.addSeries(lpDiastolic);
        heartrate.addSeries(lpHeartrate);
        systolic.addSeries(lpSystolic);
        bodyfat.addSeries(lpBodyfat);
        weight.addSeries(lpWeight);
        if (value1) {
            if (bcondactivity_Dialysate) {
            chartTotalView1.addSeries(lpCondactivity_Dialysate);
            }
            if (bpressure_Bloodline) {
            chartTotalView1.addSeries(lpPressure_Bloodline);
            }
            if (bpressure_Dialysate) {
                chartTotalView1.addSeries(lpPressure_Dialysate);
            }
            if (btemp_IN) {
                chartTotalView1.addSeries(lpTemp_IN);
            }
            if (btemp_OUT) {
                chartTotalView1.addSeries(lpTemp_OUT);
            }
            if (binlet_PH) {
                chartTotalView1.addSeries(lpInlet_PH);
            }
            if (binlet_Potasium) {
                chartTotalView1.addSeries(lpInlet_Potasium);
            }
            if (binlet_Calcium) {
                chartTotalView1.addSeries(lpInlet_Calcium);
            }
            if (binlet_Temp) {
                chartTotalView1.addSeries(lpInlet_Temp);
            }
            if (binlet_Uria) {
                chartTotalView1.addSeries(lpInlet_Uria);
            }
            if (boutlet_PH) {
                chartTotalView1.addSeries(lpOutlet_PH);
            }
            if (boutlet_Potasium) {
                chartTotalView1.addSeries(lpOutlet_Potasium);
            }
            if (boutlet_Calcium) {
                chartTotalView1.addSeries(lpOutlet_Calcium);
            }
            if (boutlet_Temp) {
                chartTotalView1.addSeries(lpOutlet_Temp);
            }
            if (boutlet_Uria) {
                chartTotalView1.addSeries(lpOutlet_Uria);
            }
            if (bdiastolic) {
                chartTotalView1.addSeries(lpDiastolic);
            }
            if (bheartrate) {
                chartTotalView1.addSeries(lpHeartrate);
            }
            if (bsystolic) {
                chartTotalView1.addSeries(lpSystolic);
            }
            if (bbodyfat) {
                chartTotalView1.addSeries(lpBodyfat);
            }
            if (bweight) {
                chartTotalView1.addSeries(lpWeight);
            }
        }
    
      if(value1)
        {
            bcondactivity_Dialysate = false;
            bpressure_Bloodline = false;
            bpressure_Dialysate = false;
            btemp_IN = false;
            btemp_OUT = false;
            binlet_PH = false;
            binlet_Potasium = false;
            binlet_Calcium = false;
            binlet_Temp = false;
            binlet_Uria = false;
            boutlet_PH = false;
            boutlet_Potasium = false;
            boutlet_Calcium = false;
            boutlet_Temp = false;
            boutlet_Uria = false;
            bdiastolic = false;
            bheartrate = false;
            bsystolic = false;
            bbodyfat = false;
            bweight = false;
        }
    
    }

    private void createCategoryModel() {
        //Not used yet
    }

    /**
     * @return the sensors
     */
    public List<Sensors> getSensors() {

        return sensors;
    }

    /**
     * @param sensors the sensors to set
     */
    public void setSensors(List<Sensors> sensors) {
        this.sensors = sensors;
    }

    

//    private java.util.List<eu.nephron.secure.ws.WakdMeasurments> findWAKDMeasurmentsManagerWithIMEI() {
//        String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");
//
//        String PhoneIMEI = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PHONEIMEI");
//        WAKDMeasurmentsManager_Service service = new WAKDMeasurmentsManager_Service();
//        eu.nephron.secure.ws.WAKDMeasurmentsManager port = service.getWAKDMeasurmentsManagerPort();
//        return port.findWAKDMeasurmentsManagerWithIMEI(PhoneIMEI);
//    }
    
    
    
    /**
     * @return the dateFromM
     */
    public Date getDateFromM() {
        return dateFromM;
    }

    /**
     * @param dateFromM the dateFromM to set
     */
    public void setDateFromM(Date dateFromM) {
        this.dateFromM = dateFromM;
    }

    /**
     * @return the dateToM
     */
    public Date getDateToM() {
        return dateToM;
    }

    /**
     * @param dateToM the dateToM to set
     */
    public void setDateToM(Date dateToM) {
        this.dateToM = dateToM;
    }

    /**
     * @return the number
     */
    public String getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * @return the selectedCharts
     */
    public List<String> getSelectedCharts() {
        return selectedCharts;
    }

    /**
     * @param selectedCharts the selectedCharts to set
     */
    public void setSelectedCharts(List<String> selectedCharts) {
        this.selectedCharts = selectedCharts;
    }

    /**
     * @return the chartTotalView1
     */
    public CartesianChartModel getChartTotalView1() {
        return chartTotalView1;
    }

    /**
     * @param chartTotalView1 the chartTotalView1 to set
     */
    public void setChartTotalView1(CartesianChartModel chartTotalView1) {
        this.chartTotalView1 = chartTotalView1;
    }

    /**
     * @return the chartTotalView2
     */
    public CartesianChartModel getChartTotalView2() {
        return chartTotalView2;
    }

    /**
     * @param chartTotalView2 the chartTotalView2 to set
     */
    public void setChartTotalView2(CartesianChartModel chartTotalView2) {
        this.chartTotalView2 = chartTotalView2;
    }

    /**
     * @return the condactivity_Dialysate
     */
    public CartesianChartModel getCondactivity_Dialysate() {
        return condactivity_Dialysate;
    }

    /**
     * @param condactivity_Dialysate the condactivity_Dialysate to set
     */
    public void setCondactivity_Dialysate(CartesianChartModel condactivity_Dialysate) {
        this.condactivity_Dialysate = condactivity_Dialysate;
    }

    /**
     * @return the pressure_Bloodline
     */
    public CartesianChartModel getPressure_Bloodline() {
        return pressure_Bloodline;
    }

    /**
     * @param pressure_Bloodline the pressure_Bloodline to set
     */
    public void setPressure_Bloodline(CartesianChartModel pressure_Bloodline) {
        this.pressure_Bloodline = pressure_Bloodline;
    }

    /**
     * @return the pressure_Dialysate
     */
    public CartesianChartModel getPressure_Dialysate() {
        return pressure_Dialysate;
    }

    /**
     * @param pressure_Dialysate the pressure_Dialysate to set
     */
    public void setPressure_Dialysate(CartesianChartModel pressure_Dialysate) {
        this.pressure_Dialysate = pressure_Dialysate;
    }

    /**
     * @return the temp_IN
     */
    public CartesianChartModel getTemp_IN() {
        return temp_IN;
    }

    /**
     * @param temp_IN the temp_IN to set
     */
    public void setTemp_IN(CartesianChartModel temp_IN) {
        this.temp_IN = temp_IN;
    }

    /**
     * @return the temp_OUT
     */
    public CartesianChartModel getTemp_OUT() {
        return temp_OUT;
    }

    /**
     * @param temp_OUT the temp_OUT to set
     */
    public void setTemp_OUT(CartesianChartModel temp_OUT) {
        this.temp_OUT = temp_OUT;
    }

    /**
     * @return the inlet_PH
     */
    public CartesianChartModel getInlet_PH() {
        return inlet_PH;
    }

    /**
     * @param inlet_PH the inlet_PH to set
     */
    public void setInlet_PH(CartesianChartModel inlet_PH) {
        this.inlet_PH = inlet_PH;
    }

    /**
     * @return the inlet_Potasium
     */
    public CartesianChartModel getInlet_Potasium() {
        return inlet_Potasium;
    }

    /**
     * @param inlet_Potasium the inlet_Potasium to set
     */
    public void setInlet_Potasium(CartesianChartModel inlet_Potasium) {
        this.inlet_Potasium = inlet_Potasium;
    }

    /**
     * @return the inlet_Calcium
     */
    public CartesianChartModel getInlet_Calcium() {
        return inlet_Calcium;
    }

    /**
     * @param inlet_Calcium the inlet_Calcium to set
     */
    public void setInlet_Calcium(CartesianChartModel inlet_Calcium) {
        this.inlet_Calcium = inlet_Calcium;
    }

    /**
     * @return the inlet_Temp
     */
    public CartesianChartModel getInlet_Temp() {
        return inlet_Temp;
    }

    /**
     * @param inlet_Temp the inlet_Temp to set
     */
    public void setInlet_Temp(CartesianChartModel inlet_Temp) {
        this.inlet_Temp = inlet_Temp;
    }

    /**
     * @return the inlet_Uria
     */
    public CartesianChartModel getInlet_Uria() {
        return inlet_Uria;
    }

    /**
     * @param inlet_Uria the inlet_Uria to set
     */
    public void setInlet_Uria(CartesianChartModel inlet_Uria) {
        this.inlet_Uria = inlet_Uria;
    }

    /**
     * @return the outlet_PH
     */
    public CartesianChartModel getOutlet_PH() {
        return outlet_PH;
    }

    /**
     * @param outlet_PH the outlet_PH to set
     */
    public void setOutlet_PH(CartesianChartModel outlet_PH) {
        this.outlet_PH = outlet_PH;
    }

    /**
     * @return the outlet_Potasium
     */
    public CartesianChartModel getOutlet_Potasium() {
        return outlet_Potasium;
    }

    /**
     * @param outlet_Potasium the outlet_Potasium to set
     */
    public void setOutlet_Potasium(CartesianChartModel outlet_Potasium) {
        this.outlet_Potasium = outlet_Potasium;
    }

    /**
     * @return the outlet_Calcium
     */
    public CartesianChartModel getOutlet_Calcium() {
        return outlet_Calcium;
    }

    /**
     * @param outlet_Calcium the outlet_Calcium to set
     */
    public void setOutlet_Calcium(CartesianChartModel outlet_Calcium) {
        this.outlet_Calcium = outlet_Calcium;
    }

    /**
     * @return the outlet_Temp
     */
    public CartesianChartModel getOutlet_Temp() {
        return outlet_Temp;
    }

    /**
     * @param outlet_Temp the outlet_Temp to set
     */
    public void setOutlet_Temp(CartesianChartModel outlet_Temp) {
        this.outlet_Temp = outlet_Temp;
    }

    /**
     * @return the outlet_Uria
     */
    public CartesianChartModel getOutlet_Uria() {
        return outlet_Uria;
    }

    /**
     * @param outlet_Uria the outlet_Uria to set
     */
    public void setOutlet_Uria(CartesianChartModel outlet_Uria) {
        this.outlet_Uria = outlet_Uria;
    }

    /**
     * @return the diastolic
     */
    public CartesianChartModel getDiastolic() {
        return diastolic;
    }

    /**
     * @param diastolic the diastolic to set
     */
    public void setDiastolic(CartesianChartModel diastolic) {
        this.diastolic = diastolic;
    }

    /**
     * @return the heartrate
     */
    public CartesianChartModel getHeartrate() {
        return heartrate;
    }

    /**
     * @param heartrate the heartrate to set
     */
    public void setHeartrate(CartesianChartModel heartrate) {
        this.heartrate = heartrate;
    }

    /**
     * @return the systolic
     */
    public CartesianChartModel getSystolic() {
        return systolic;
    }

    /**
     * @param systolic the systolic to set
     */
    public void setSystolic(CartesianChartModel systolic) {
        this.systolic = systolic;
    }

    /**
     * @return the bodyfat
     */
    public CartesianChartModel getBodyfat() {
        return bodyfat;
    }

    /**
     * @param bodyfat the bodyfat to set
     */
    public void setBodyfat(CartesianChartModel bodyfat) {
        this.bodyfat = bodyfat;
    }

    /**
     * @return the weight
     */
    public CartesianChartModel getWeight() {
        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(CartesianChartModel weight) {
        this.weight = weight;
    }

    /**
     * @return the bchartTotalView1
     */
    public boolean isBchartTotalView1() {
        return bchartTotalView1;
    }

    /**
     * @param bchartTotalView1 the bchartTotalView1 to set
     */
    public void setBchartTotalView1(boolean bchartTotalView1) {
        this.bchartTotalView1 = bchartTotalView1;
    }

    /**
     * @return the bchartTotalView2
     */
    public boolean isBchartTotalView2() {
        return bchartTotalView2;
    }

    /**
     * @param bchartTotalView2 the bchartTotalView2 to set
     */
    public void setBchartTotalView2(boolean bchartTotalView2) {
        this.bchartTotalView2 = bchartTotalView2;
    }

    /**
     * @return the bcondactivity_Dialysate
     */
    public boolean isBcondactivity_Dialysate() {
        return bcondactivity_Dialysate;
    }

    /**
     * @param bcondactivity_Dialysate the bcondactivity_Dialysate to set
     */
    public void setBcondactivity_Dialysate(boolean bcondactivity_Dialysate) {
        this.bcondactivity_Dialysate = bcondactivity_Dialysate;
    }

    /**
     * @return the bpressure_Bloodline
     */
    public boolean isBpressure_Bloodline() {
        return bpressure_Bloodline;
    }

    /**
     * @param bpressure_Bloodline the bpressure_Bloodline to set
     */
    public void setBpressure_Bloodline(boolean bpressure_Bloodline) {
        this.bpressure_Bloodline = bpressure_Bloodline;
    }

    /**
     * @return the bpressure_Dialysate
     */
    public boolean isBpressure_Dialysate() {
        return bpressure_Dialysate;
    }

    /**
     * @param bpressure_Dialysate the bpressure_Dialysate to set
     */
    public void setBpressure_Dialysate(boolean bpressure_Dialysate) {
        this.bpressure_Dialysate = bpressure_Dialysate;
    }

    /**
     * @return the btemp_IN
     */
    public boolean isBtemp_IN() {
        return btemp_IN;
    }

    /**
     * @param btemp_IN the btemp_IN to set
     */
    public void setBtemp_IN(boolean btemp_IN) {
        this.btemp_IN = btemp_IN;
    }

    /**
     * @return the btemp_OUT
     */
    public boolean isBtemp_OUT() {
        return btemp_OUT;
    }

    /**
     * @param btemp_OUT the btemp_OUT to set
     */
    public void setBtemp_OUT(boolean btemp_OUT) {
        this.btemp_OUT = btemp_OUT;
    }

    /**
     * @return the binlet_PH
     */
    public boolean isBinlet_PH() {
        return binlet_PH;
    }

    /**
     * @param binlet_PH the binlet_PH to set
     */
    public void setBinlet_PH(boolean binlet_PH) {
        this.binlet_PH = binlet_PH;
    }

    /**
     * @return the binlet_Potasium
     */
    public boolean isBinlet_Potasium() {
        return binlet_Potasium;
    }

    /**
     * @param binlet_Potasium the binlet_Potasium to set
     */
    public void setBinlet_Potasium(boolean binlet_Potasium) {
        this.binlet_Potasium = binlet_Potasium;
    }

    /**
     * @return the binlet_Calcium
     */
    public boolean isBinlet_Calcium() {
        return binlet_Calcium;
    }

    /**
     * @param binlet_Calcium the binlet_Calcium to set
     */
    public void setBinlet_Calcium(boolean binlet_Calcium) {
        this.binlet_Calcium = binlet_Calcium;
    }

    /**
     * @return the binlet_Temp
     */
    public boolean isBinlet_Temp() {
        return binlet_Temp;
    }

    /**
     * @param binlet_Temp the binlet_Temp to set
     */
    public void setBinlet_Temp(boolean binlet_Temp) {
        this.binlet_Temp = binlet_Temp;
    }

    /**
     * @return the binlet_Uria
     */
    public boolean isBinlet_Uria() {
        return binlet_Uria;
    }

    /**
     * @param binlet_Uria the binlet_Uria to set
     */
    public void setBinlet_Uria(boolean binlet_Uria) {
        this.binlet_Uria = binlet_Uria;
    }

    /**
     * @return the boutlet_PH
     */
    public boolean isBoutlet_PH() {
        return boutlet_PH;
    }

    /**
     * @param boutlet_PH the boutlet_PH to set
     */
    public void setBoutlet_PH(boolean boutlet_PH) {
        this.boutlet_PH = boutlet_PH;
    }

    /**
     * @return the boutlet_Potasium
     */
    public boolean isBoutlet_Potasium() {
        return boutlet_Potasium;
    }

    /**
     * @param boutlet_Potasium the boutlet_Potasium to set
     */
    public void setBoutlet_Potasium(boolean boutlet_Potasium) {
        this.boutlet_Potasium = boutlet_Potasium;
    }

    /**
     * @return the boutlet_Calcium
     */
    public boolean isBoutlet_Calcium() {
        return boutlet_Calcium;
    }

    /**
     * @param boutlet_Calcium the boutlet_Calcium to set
     */
    public void setBoutlet_Calcium(boolean boutlet_Calcium) {
        this.boutlet_Calcium = boutlet_Calcium;
    }

    /**
     * @return the boutlet_Temp
     */
    public boolean isBoutlet_Temp() {
        return boutlet_Temp;
    }

    /**
     * @param boutlet_Temp the boutlet_Temp to set
     */
    public void setBoutlet_Temp(boolean boutlet_Temp) {
        this.boutlet_Temp = boutlet_Temp;
    }

    /**
     * @return the boutlet_Uria
     */
    public boolean isBoutlet_Uria() {
        return boutlet_Uria;
    }

    /**
     * @param boutlet_Uria the boutlet_Uria to set
     */
    public void setBoutlet_Uria(boolean boutlet_Uria) {
        this.boutlet_Uria = boutlet_Uria;
    }

    /**
     * @return the bdiastolic
     */
    public boolean isBdiastolic() {
        return bdiastolic;
    }

    /**
     * @param bdiastolic the bdiastolic to set
     */
    public void setBdiastolic(boolean bdiastolic) {
        this.bdiastolic = bdiastolic;
    }

    /**
     * @return the bheartrate
     */
    public boolean isBheartrate() {
        return bheartrate;
    }

    /**
     * @param bheartrate the bheartrate to set
     */
    public void setBheartrate(boolean bheartrate) {
        this.bheartrate = bheartrate;
    }

    /**
     * @return the bsystolic
     */
    public boolean isBsystolic() {
        return bsystolic;
    }

    /**
     * @param bsystolic the bsystolic to set
     */
    public void setBsystolic(boolean bsystolic) {
        this.bsystolic = bsystolic;
    }

    /**
     * @return the bbodyfat
     */
    public boolean isBbodyfat() {
        return bbodyfat;
    }

    /**
     * @param bbodyfat the bbodyfat to set
     */
    public void setBbodyfat(boolean bbodyfat) {
        this.bbodyfat = bbodyfat;
    }

    /**
     * @return the bweight
     */
    public boolean isBweight() {
        return bweight;
    }

    /**
     * @param bweight the bweight to set
     */
    public void setBweight(boolean bweight) {
        this.bweight = bweight;
    }

    /**
     * @return the value1
     */
    public boolean isValue1() {
        return value1;
    }

    /**
     * @param value1 the value1 to set
     */
    public void setValue1(boolean value1) {
        this.value1 = value1;
    }

    /**
     * @return the model
     */
    public CartesianChartModel getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(CartesianChartModel model) {
        this.model = model;
    }

    

    
    private java.util.List<eu.nephron.secure.ws.WakdMeasurments> findAllWAKDMeasurmentsManager() {
        String PhoneIMEI = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PHONEIMEI");
        
        System.out.println("***** Phone IMEI"+PhoneIMEI);
     
//           WAKDMeasurmentsManager_Service service = new WAKDMeasurmentsManager_Service();
//     
//        eu.nephron.secure.ws.WAKDMeasurmentsManager port = service.getWAKDMeasurmentsManagerPort();
//        return port.findWAKDMeasurmentsManagerWithIMEIAndDateFromTo(PhoneIMEI, dateFromM.getTime(), dateToM.getTime());
        
                try {

            mesdb dd = new mesdb();
            java.util.List<eu.nephron.secure.ws.WakdMeasurments> pp = new ArrayList();
            dd.SetIMEI(PhoneIMEI);
            dd.LoadFromFile();
            ArrayList<CDWAKDMeasurments> pl = dd.getList();
            for (int i = 0; i < pl.size(); i++) {
                if (true) {
                    if (true) {
                        eu.nephron.secure.ws.WakdMeasurments pitem = new eu.nephron.secure.ws.WakdMeasurments();
                        pitem.setDate(pl.get(i).getDate());
                        pitem.setIMEI(pl.get(i).getIMEI());
                        pitem.setId(pl.get(i).getId());
                        pitem.setType(pl.get(i).getType());
                        pitem.setValue(pl.get(i).getValue());
                        pp.add(pitem);
                    }
                }
            }
            return pp;
        } catch (FileNotFoundException ex) {
            return null;
        } catch (IOException ex) {
            return null;
        } catch (ClassNotFoundException ex) {
            return null;
        }
        
     
    }
    
}
