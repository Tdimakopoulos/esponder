/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exus.esponder.biomedb.client;

import eu.exus.esponder.biomeda.client.*;
import esponder.biomed.lib.BiomedACommand;
import esponder.biomed.lib.BiomedAFrameParser;
import esponder.biomed.lib.BiomedBData;
import esponder.biomed.lib.BiomedBFrame;
//import esponder.biomed.lib.BiomedAPacket;
//import esponder.biomed.lib.BiomedBFrame;
import esponder.biomed.lib.BiomedBFrameParser;
import esponder.biomed.lib.BiomedBPacket;
import esponder.biomed.lib.Cobs;
import esponder.biomed.lib.Payload;
//import esponder.biomed.lib.Payload;
import esponder.biomed.lib.RegularDataBlock;
import exus.esponder.receive.processor.processor;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.microedition.io.StreamConnection;

/**
 *
 * @author tdim
 */
public class BiomedBReadThread extends Thread {

    StreamConnection scglobal;
    boolean brun = true;

    public void SetBTConnection(StreamConnection sc) {
        scglobal = sc;
    }

    public void StopReadThread() {
        brun = false;
    }

    byte[] combine(byte[] a, byte[] b) {
        if (a == null) {
            return b;
        } else {
            byte[] result = new byte[a.length + b.length];
            System.arraycopy(a, 0, result, 0, a.length);
            System.arraycopy(b, 0, result, a.length, b.length);
            return result;
        }
    }

    @Override
    public void run() {
        try {
            DataInputStream datain = null;
            BiomedBFrameParser parser = new BiomedBFrameParser();

            RegularDataBlock dataBlocks[] = null;

            datain = scglobal.openDataInputStream();
            int iloop = 0;
            int il = 0;
            int ill = 0;

            byte b1[] = new byte[1048];
            byte b3[] = null;
            byte b2[] = null;
            while (brun) {
                try {

                    il = datain.read(b1);

                    b3 = new byte[il];

                    System.arraycopy(b1, 0, b3, 0, il);
                    b2 = combine(b2, b3);

                    iloop++;
                    if (il == 1) {
                        iloop = 0;
                        try {
                            String str1 = new String(b2);
                            //System.out.println("Original" + str1);
                            byte incoming[] = Cobs.decode(b2, b2.length);
if(incoming!=null)
{
                            //System.out.println("real" + str2);

                            List<BiomedACommand> cmdList = new LinkedList<BiomedACommand>();
                            List<RegularDataBlock> dataList = new LinkedList<RegularDataBlock>();
                            Payload payload = null;
                            BiomedBPacket packets[] = null;

                            BiomedBFrame frame = new BiomedBFrame(incoming);
                            frame.process();
                            packets = frame.getPackets();
                            if (packets != null) {
                                for (int i = 0; i < packets.length; i++) {
                                    payload = packets[i].getPayload();

                                    if (payload instanceof BiomedACommand) {
                                        //cmdList.add((BiomedACommand) payload);
                                    }
                                    if (payload instanceof BiomedBData) {

                                        //System.out.println("Found Data!");
                                        payload = (BiomedBData) payload;


                                        //System.out.println("Parse Header!");

                                        ((BiomedBData) payload).parseHeader();

                                        //System.out.println("DataBlock!");
                                        System.out.println("Size : " + ((BiomedBData) payload).getDataBlock().getValues().length);

//                        System.out.println("Size DS: "+((BiomedBData) payload).getDataBlocks().length);
                                        //System.out.println("Size S: "+((BiomedBData) payload).getDataBlocks().length);

                                        System.out.println("Name xx : " + ((BiomedBData) payload).getDataBlock().getName());
                                        //System.out.println("Name : "+((BiomedBData) payload).getDataBlocks().getName());
                                        //for(int ixx=0;ixx<((BiomedBData) payload).getDataBlock().getValues().length;ixx++)
                                        //{
                                        //System.out.println(((BiomedBData) payload).getDataBlock().getValues()[ixx]);
                                        //}

                                        //RegularDataBlock rdbs[] = ((BiomedBData) payload).getDataBlocks();

//                        RegularDataBlock rdbs=((BiomedBData) payload).getDataBlock();
                                        //System.out.println("Name : "+rdbs.getName());
//                        System.out.println("DataBlocks!");
//                        RegularDataBlock rdbs[] = ((BiomedBData) payload).getDataBlock();
//                        
//                        System.out.println("DataBlocks size"+rdbs.length);
//                        if(rdbs != null){
//                            for(int rdbIdx = 0; rdbIdx < rdbs.length; rdbIdx++){
//                                dataList.add(rdbs[rdbIdx]);
//                            }
//                        }
                                    }
                                }
                            }
                            System.out.println("D List Size " + dataList.size());
                        }
                            
                            //parser.setFrame(incoming);
                            //parser.process();
                            //    cmds = parser.getCmds();
                            //dataBlocks = parser.getDataBlocks();
                            //System.out.println("Size : " + dataBlocks.length);

                        } catch (Exception e) {
                            //System.out.println("ex 1 "+e.getMessage());
                            e.printStackTrace();
                        }
                        b1 = new byte[1048];

                        b3 = new byte[1048];
                        b2 = new byte[1];
                    }
                } catch (IOException ex) {
                    //System.out.println("ex 2 "+ex.getMessage());
                    ex.printStackTrace();
                }

            }
            System.out.println("Closing down BiomedA");
            datain.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        System.out.println("Closing down");
    }
}
