package exodus.com.rest.client;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

/**
 *
 * @author tdim
 */
public class ListCClient {

    private WebResource webResource;// web resource variable
    private Client client;// client variable
    private static final String BASE_URI = "http://localhost:8080/NetworkInterf/resources";// the url where the web service is deployed

    public ListCClient() {
        com.sun.jersey.api.client.config.ClientConfig config = new com.sun.jersey.api.client.config.DefaultClientConfig();
        client = Client.create(config);// default config, no security
        webResource = client.resource(BASE_URI).path("listC");// base path of the web service
    }

    public void putCreateListC(Object requestEntity) throws UniformInterfaceException {
        //call the web service for help : http://jersey.java.net/nonav/apidocs/1.4/jersey/com/sun/jersey/api/client/WebResource.html             
        webResource.path("CreateListC").type(javax.ws.rs.core.MediaType.APPLICATION_XML).put(requestEntity);
    }

    public <T> T getQueryListCAll(Class<T> responseType) throws UniformInterfaceException {
        //call the web service for help : http://jersey.java.net/nonav/apidocs/1.4/jersey/com/sun/jersey/api/client/WebResource.html
        WebResource resource = webResource;
        resource = resource.path("QueryAllListC");
        return resource.accept(javax.ws.rs.core.MediaType.APPLICATION_XML).get(responseType);
    }

    public void close() {
        //close the connection and destroy the object variable
        client.destroy();
    }
}
