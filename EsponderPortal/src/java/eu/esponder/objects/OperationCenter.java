/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.objects;

import java.util.Date;

/**
 *
 * @author Tom
 */
public class OperationCenter {
    private String name;
        
        private int number;
 private int number2;
        private String photo;
        
        private String position;
        
        private String nationality;
        
        private String height;
        
        private String weight;
        private String age;
        
        private Date birth;
        
        private String organization;
        
        public OperationCenter() {
                
        }
        
        public OperationCenter(String name) {
                this.name = name;
        }

        public OperationCenter(String name, int number, String position) {
                this.name = name;
        this.number = number;
                
        this.position = position;
        }
        
    public OperationCenter(String name, int number, String photo, String position) {
                this.name = name;
        this.number = number;
                this.photo = photo;
        this.position = position;
        }
        
        public String getHeight() {
                return height;
        }

        public void setHeight(String height) {
                this.height = height;
        }

        public String getWeight() {
                return weight;
        }

        public void setWeight(String weight) {
                this.weight = weight;
        }

        public Date getBirth() {
                return birth;
        }

        public void setBirth(Date birth) {
                this.birth = birth;
        }
        
        public OperationCenter(String name, int number) {
                this.name = name;
                this.number = number;
        }

        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }

        public String getPhoto() {
                return photo;
        }

        public void setPhoto(String photo) {
                this.photo = photo;
        }
        
        public String getPosition() {
                return position;
        }

        public void setPosition(String position) {
                this.position = position;
        }

        public String getNationality() {
                return nationality;
        }

        public void setNationality(String nationality) {
                this.nationality = nationality;
        }
        

        public int getNumber() {
                return number;
        }

        public void setNumber(int number) {
                this.number = number;
        }

        @Override
        public boolean equals(Object obj) {
                if(obj == null) {
                return false;
            }
                if(!(obj instanceof person)) {
                return false;
            }
                
                return ((person)obj).getNumber() == this.number;
        }

        @Override
        public int hashCode() {
            int hash = 1;
            return hash * 31 + name.hashCode();
        }

        @Override
        public String toString() {
                return name;
        }  

    /**
     * @return the organization
     */
    public String getOrganization() {
        return organization;
    }

    /**
     * @param organization the organization to set
     */
    public void setOrganization(String organization) {
        this.organization = organization;
    }

    /**
     * @return the age
     */
    public String getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(String age) {
        this.age = age;
    } 

    /**
     * @return the number2
     */
    public int getNumber2() {
        return number2;
    }

    /**
     * @param number2 the number2 to set
     */
    public void setNumber2(int number2) {
        this.number2 = number2;
    }
}
