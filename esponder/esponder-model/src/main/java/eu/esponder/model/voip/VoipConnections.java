package eu.esponder.model.voip;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import eu.esponder.model.ESponderEntity;

@Entity
@Table(name="voipconnections")
@NamedQueries({
		@NamedQuery(name = "VoipConnections.findAll", query = "select u from VoipConnections u"),
		@NamedQuery(name = "VoipConnections.findByID", query = "select u from VoipConnections u where u.id=:id"),
		@NamedQuery(name = "VoipConnections.findByconnectionID", query = "select u from VoipConnections u where u.connectionID=:connectionID")
		})
public class VoipConnections extends ESponderEntity<Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6749439888200814818L;

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.TABLE)
	@Column(name="VOIPC_ID")
	protected Long id;
	

	Long connectionID;
	
	
	
	public Long getConnectionID() {
		return connectionID;
	}

	public void setConnectionID(Long connectionID) {
		this.connectionID = connectionID;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#getId()
	 */
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.model.Identifiable#setId(java.lang.Object)
	 */
	public void setId(Long id) {
		this.id = id;
	}

}
