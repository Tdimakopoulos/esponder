/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.resource.ActorService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.model.crisis.resource.ActorIC;
import eu.esponder.util.ejb.ServiceLocator;

// TODO: Auto-generated Javadoc
// 
/**
 * The Class ActionPartObjectiveConsumablesConverter.
 */
public class SetOfSubordinatesInActorCMConverter implements CustomConverter {

	/**
	 * Gets the mapping service.
	 *
	 * @return the mapping service
	 */
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/**
	 * Gets the logistics service.
	 *
	 * @return the logistics service
	 */
	protected ActorService getActorService() {
		try {
			return ServiceLocator.getResource("esponder/ActorBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {

		if (source instanceof Set && source != null) {

			Set<?> sourceSet = (Set<?>) source;
			Iterator<?> it = sourceSet.iterator();
			if(it.hasNext()){
				Object next = it.next();
				if(next.getClass() == ActorIC.class || next.getClass() == ActorIC.class) {

					@SuppressWarnings("unchecked")
					Set<Object> sourceSubordinatesSet = (Set<Object>) source;
					Set<Long> destSubordinatesDTOSet = new HashSet<Long>();
					for(Object subordinate : sourceSubordinatesSet) {
						destSubordinatesDTOSet.add(((ActorIC)subordinate).getId());
					}
					destination = destSubordinatesDTOSet;
				}
				else
					if(next.getClass() == Long.class) {

						@SuppressWarnings("unchecked")
						Set<Long> sourceSubordinatesDTOSet = (Set<Long>) source;
						Set<Object> destSubordinatesSet = new HashSet<Object>();
						for(Long subordinateID : sourceSubordinatesDTOSet) {
							Object destSubordinate = this.getActorService().findById(subordinateID);
							destSubordinatesSet.add(destSubordinate);
						}
						destination = destSubordinatesSet;
					}
					else
						destination = null; 
			}
		}
		else {
			destination = null;
		}
		return destination;
	}

}
