/*
 * 
 */
package eu.esponder.event.model.crisis.resource;

import eu.esponder.dto.model.crisis.resource.EquipmentDTO;
import eu.esponder.event.model.DeleteEvent;



// TODO: Auto-generated Javadoc
/**
 * The Class DeleteResourceEquipmentEvent.
 */
public class DeleteResourceEquipmentEvent extends ResourceEquipmentEvent<EquipmentDTO> implements DeleteEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8961408917425131483L;
	
	/**
	 * Instantiates a new delete resource equipment event.
	 */
	public DeleteResourceEquipmentEvent(){
		setJournalMessageInfo("DeleteResourceEquipmentEvent");
	}
}
