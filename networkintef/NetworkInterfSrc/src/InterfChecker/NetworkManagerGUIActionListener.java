
package InterfChecker;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JOptionPane;

public class NetworkManagerGUIActionListener implements ActionListener{

    NetworkManagerGUI fm = null;
    long result = 0;
    long val1 = 0;
    long val2 = 0;
    String val = "";
    String operator = "$";
    
    public NetworkManagerGUIActionListener(NetworkManagerGUI frame){
        fm = frame;
    }
    
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.fm.jmitem1){
            JOptionPane.showMessageDialog(null, "Linux Network Interface Monitor");
            return;
        }

        if(e.getSource() == this.fm.jmitem2){
            System.exit(1);
            return;
        }

        for(int i=0;i<this.fm.numbers.length;i++){
            if(e.getSource() == this.fm.numbers[i]){
                	if(i==0){
                    displayQueryResults("select a.interfId as ID, a.name as NAME, a.device as DEVICE, b.ipAddress as IP, b.macAddress as 'MAC ADDR',b.netAddress as NETADDRESS,b.broadcastAddress as BROADCAST,b.netMask as 'MASK',b.gateway, b.maxTXRate as 'MAX Rate',b.curTxRate,b.curPercLineRate,b.packetErrorRate,b.TxBytes,b.RxBytes,b.rxPackets,b.txPackets,b.rxErrors,b.txErrors from interf a join lista b on a.interfId = b.listAId order by a.device,a.interfId ;");
                }
                	else if(i==1){
                                displayQueryResults("select a.interfId as ID, a. name as NAME, a.device as DEVICE, b.macAddressAP as MAC,b.Essid as ESSID,b.Channel as CHANNEL,b.status,b.txPower,b.linkQuality,b.signalLevel,b.linkLevelNoise,b.discardedPackets from interf a join listb b on a.interfId = b.interfId order by a.device,a.interfId ;");
                	}
                	else if(i==2){
                		displayQueryResults("select * from listc;"); 
                	}
            }
        }
        
                
    }
    public void displayQueryResults(final String q) {
    	// It may take a while to get the results, so give the user some
    	// immediate feedback that their query was accepted.
    	this.fm.msgline.setText("Contacting database...");
    	
    	// In order to allow the feedback message to be displayed, we don't
    	// run the query directly, but instead place it on the event queue
    	// to be run after all pending events and redisplays are done.
    	EventQueue.invokeLater(new Runnable() {
    		public void run() {
    		    try {
    			// This is the crux of it all.  Use the factory object
    			// to obtain a TableModel object for the query results
    			// and display that model in the JTable component.
    		    	fm.table.setModel(fm.factory.getResultSetTableModel(q));
    			// We're done, so clear the feedback message
    		    	fm.msgline.setText(" ");  
    		    }
    		    catch (SQLException ex) {
    			// If something goes wrong, clear the message line
    			fm.msgline.setText(" ");
    			// Then display the error in a dialog box
    			JOptionPane.showMessageDialog(fm,
    			          new String[] {  // Display a 2-line message
    				      ex.getClass().getName() + ": ",
    				      ex.getMessage()
    				  });
    		    }
    		}
    	    });
        }   
}