/*
 * 
 */
package eu.esponder.model.crisis.view;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import eu.esponder.model.snapshot.ReferencePOISnapshot;


// TODO: Auto-generated Javadoc
/**
 * The Class ReferencePOI.
 */
@Entity
@DiscriminatorValue("REF")
@NamedQueries({
	@NamedQuery(name="ReferencePOI.findAll", query="select r from ReferencePOI r"),
	@NamedQuery(name="ReferencePOI.findByReferencePOIId", query="select s from ReferencePOI s where s.id=:referencePOIId"),
	@NamedQuery(name="ReferencePOI.findByTitle", query="select s from ReferencePOI s where s.title=:referencePOITitle")
})
public class ReferencePOI extends ResourcePOI {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7856655848723729892L;
	
	@OneToMany(mappedBy="referencePOI")
	private Set<ReferencePOISnapshot> snapshots;
	
	/** The average size. */
	@Column(name="AVERAGE_SIZE")
	private Float averageSize;

	/**
	 * Gets the average size.
	 *
	 * @return the average size
	 */
	public Float getAverageSize() {
		return averageSize;
	}

	/**
	 * Sets the average size.
	 *
	 * @param averageSize the new average size
	 */
	public void setAverageSize(Float averageSize) {
		this.averageSize = averageSize;
	}

	public Set<ReferencePOISnapshot> getSnapshots() {
		return snapshots;
	}

	public void setSnapshots(Set<ReferencePOISnapshot> snapshots) {
		this.snapshots = snapshots;
	}

	
}
