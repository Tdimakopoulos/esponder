/*
 * 
 */
package eu.esponder.dto.model.crisis.resource;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;


// TODO: Auto-generated Javadoc
/**
 * The Class ActorCMDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "type", "title", "status", "personnel", "voIPURL", "subordinates", "operationsCentre"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ActorCMDTO extends ActorDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 7267304623436492017L;
	
	/** The subordinates. */
	private Set<Long> subordinates;
	
	/** The operations centre. */
	private Long operationsCentre;

	/**
	 * Gets the subordinates.
	 *
	 * @return the subordinates
	 */
	public Set<Long> getSubordinates() {
		return subordinates;
	}

	/**
	 * Sets the subordinates.
	 *
	 * @param subordinates the new subordinates
	 */
	public void setSubordinates(Set<Long> subordinates) {
		this.subordinates = subordinates;
	}

	/**
	 * Gets the operations centre.
	 *
	 * @return the operations centre
	 */
	public Long getOperationsCentre() {
		return operationsCentre;
	}

	/**
	 * Sets the operations centre.
	 *
	 * @param operationsCentre the new operations centre
	 */
	public void setOperationsCentre(Long operationsCentre) {
		this.operationsCentre = operationsCentre;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.crisis.resource.ActorDTO#toString()
	 */
	@Override
	public String toString() {
		return "ActorCMDTO [type=" + type + ", status=" + status + ", id=" + id
				+ ", title=" + title + "]";
	}
}
