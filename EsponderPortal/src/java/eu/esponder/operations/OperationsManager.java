/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.operations;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.objects.OperationCenter;
import eu.esponder.objects.ResourcePlanInfo;
import eu.esponder.objects.person;
import eu.esponder.objects.teams;
import eu.esponder.personnel.PersonnelManager;
import eu.esponder.ws.client.query.QueryManager;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

/**
 *
 * @author Thomas
 */
@ManagedBean
@RequestScoped
public class OperationsManager {

    private List<person> personnel;
    private List<person> selectedStrategicPersonnel;
    private List<person> selectedTacticalPersonnel;
    private List<teams> selectedFRTeamPersonnel;
    private List<person> selectedFRTeamMembersPersonnel;
  private List<ResourcePlanInfo> pPersonnelPlanInfo;
  
  private List<String> selectedcrisis;  
  
    private Map<String,String> crisis; 
    
     private void PopulateCrisis()
    {
        crisis = new HashMap<String, String>();
        try {
            QueryManager pQuery = new QueryManager();
                          ResultListDTO pResults=pQuery.getCrisisContextAll("1");
                          for (int i=0;i<pResults.getResultList().size();i++)
                          {
                              CrisisContextDTO pItem = (CrisisContextDTO) pResults.getResultList().get(i);
                                
                              getCrisis().put(pItem.getTitle(), pItem.getId().toString());  
                          }
        } catch (JsonParseException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JsonMappingException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PersonnelManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     public String selectcrisisbtn()
    {
          personnel = new ArrayList<person>();
        selectedStrategicPersonnel = new ArrayList<person>();
        selectedTacticalPersonnel = new ArrayList<person>();
        selectedFRTeamPersonnel = new ArrayList<teams>();
        selectedFRTeamMembersPersonnel = new ArrayList<person>();
    pPersonnelPlanInfo = new ArrayList<ResourcePlanInfo>();
//        pPersonnelPlanInfo.add(new ResourcePlanInfo("Police Officer",10,0));
//        pPersonnelPlanInfo.add(new ResourcePlanInfo("Fire Fighter",20,0));
//        pPersonnelPlanInfo.add(new ResourcePlanInfo("Fire Fighter Special",5,0));
//        
//        personnel.add(new person("Messi", 10, "po.jpg", "Police Officer"));
//        personnel.add(new person("Villa", 7, "po.jpg", "Police Officer"));
//        personnel.add(new person("Pedro", 17, "po.jpg", "Police Officer"));
//        personnel.add(new person("Bojan", 9, "po.jpg", "Police Officer"));
//        personnel.add(new person("Xavi", 6, "po.jpg", "Police Officer"));
//        personnel.add(new person("Iniesta", 8, "ff.jpg", "Fire Fighter"));
//        personnel.add(new person("Mascherano", 16, "ff.jpg", "Fire Fighter"));
//        personnel.add(new person("Puyol", 5, "ff.jpg", "Fire Fighter"));
//        personnel.add(new person("Alves", 2, "ff.jpg", "Fire Fighter"));
//        personnel.add(new person("Valdes", 1, "ff.jpg", "Fire Fighter"));
//        personnel.add(new person("Abidal", 22, "ff.jpg", "Fire Fighter"));
//        personnel.add(new person("Adriano", 16, "ff.jpg", "Fire Fighter"));
//        personnel.add(new person("Pinto", 13, "ffs.jpg", "Fire Fighter Special"));
//        personnel.add(new person("Pique", 3, "ffs.jpg", "Fire Fighter Special"));
//        personnel.add(new person("Keita", 7, "ffs.jpg", "Fire Fighter Special"));
//        personnel.add(new person("Maxwell", 5, "ffs.jpg", "Fire Fighter Special"));
         return "Operations?faces-redirect=true";
    }
     
    public String saveStrategic()
    {
         selectedStrategicPersonnel.add(new person("Messi", 10, "po.jpg", "Police Officer"));
         selectedStrategicPersonnel.add(new person("Messi2", 12, "po.jpg", "Police Officer"));
         System.out.print("Add new actor"+selectedStrategicPersonnel.size());
        return null;
    }
    /**
     * Creates a new instance of OperationsManager
     */
    public OperationsManager() {
        personnel = new ArrayList<person>();
        selectedStrategicPersonnel = new ArrayList<person>();
        selectedTacticalPersonnel = new ArrayList<person>();
        selectedFRTeamPersonnel = new ArrayList<teams>();
        selectedFRTeamMembersPersonnel = new ArrayList<person>();
    pPersonnelPlanInfo = new ArrayList<ResourcePlanInfo>();
    PopulateCrisis();
//        pPersonnelPlanInfo.add(new ResourcePlanInfo("Police Officer",10,0));
//        pPersonnelPlanInfo.add(new ResourcePlanInfo("Fire Fighter",20,0));
//        pPersonnelPlanInfo.add(new ResourcePlanInfo("Fire Fighter Special",5,0));
//        
//        personnel.add(new person("Messi", 10, "po.jpg", "Police Officer"));
//        personnel.add(new person("Villa", 7, "po.jpg", "Police Officer"));
//        personnel.add(new person("Pedro", 17, "po.jpg", "Police Officer"));
//        personnel.add(new person("Bojan", 9, "po.jpg", "Police Officer"));
//        personnel.add(new person("Xavi", 6, "po.jpg", "Police Officer"));
//        personnel.add(new person("Iniesta", 8, "ff.jpg", "Fire Fighter"));
//        personnel.add(new person("Mascherano", 16, "ff.jpg", "Fire Fighter"));
//        personnel.add(new person("Puyol", 5, "ff.jpg", "Fire Fighter"));
//        personnel.add(new person("Alves", 2, "ff.jpg", "Fire Fighter"));
//        personnel.add(new person("Valdes", 1, "ff.jpg", "Fire Fighter"));
//        personnel.add(new person("Abidal", 22, "ff.jpg", "Fire Fighter"));
//        personnel.add(new person("Adriano", 16, "ff.jpg", "Fire Fighter"));
//        personnel.add(new person("Pinto", 13, "ffs.jpg", "Fire Fighter Special"));
//        personnel.add(new person("Pique", 3, "ffs.jpg", "Fire Fighter Special"));
//        personnel.add(new person("Keita", 7, "ffs.jpg", "Fire Fighter Special"));
//        personnel.add(new person("Maxwell", 5, "ffs.jpg", "Fire Fighter Special"));
    }

    /**
     * @return the personnel
     */
    public List<person> getPersonnel() {
        return personnel;
    }

    /**
     * @param personnel the personnel to set
     */
    public void setPersonnel(List<person> personnel) {
        this.personnel = personnel;
    }

    /**
     * @return the selectedStrategicPersonnel
     */
    public List<person> getSelectedStrategicPersonnel() {
        return selectedStrategicPersonnel;
    }

    /**
     * @param selectedStrategicPersonnel the selectedStrategicPersonnel to set
     */
    public void setSelectedStrategicPersonnel(List<person> selectedStrategicPersonnel) {
        this.selectedStrategicPersonnel = selectedStrategicPersonnel;
    }

    /**
     * @return the selectedTacticalPersonnel
     */
    public List<person> getSelectedTacticalPersonnel() {
        return selectedTacticalPersonnel;
    }

    /**
     * @param selectedTacticalPersonnel the selectedTacticalPersonnel to set
     */
    public void setSelectedTacticalPersonnel(List<person> selectedTacticalPersonnel) {
        this.selectedTacticalPersonnel = selectedTacticalPersonnel;
    }

    /**
     * @return the selectedFRTeamPersonnel
     */
    public List<teams> getSelectedFRTeamPersonnel() {
        return selectedFRTeamPersonnel;
    }

    /**
     * @param selectedFRTeamPersonnel the selectedFRTeamPersonnel to set
     */
    public void setSelectedFRTeamPersonnel(List<teams> selectedFRTeamPersonnel) {
        this.selectedFRTeamPersonnel = selectedFRTeamPersonnel;
    }

    /**
     * @return the selectedFRTeamMembersPersonnel
     */
    public List<person> getSelectedFRTeamMembersPersonnel() {
        return selectedFRTeamMembersPersonnel;
    }

    /**
     * @param selectedFRTeamMembersPersonnel the selectedFRTeamMembersPersonnel
     * to set
     */
    public void setSelectedFRTeamMembersPersonnel(List<person> selectedFRTeamMembersPersonnel) {
        this.selectedFRTeamMembersPersonnel = selectedFRTeamMembersPersonnel;
    }
    /**
     * @return the pPersonnelPlanInfo
     */
    public List<ResourcePlanInfo> getpPersonnelPlanInfo() {
        return pPersonnelPlanInfo;
    }

    /**
     * @param pPersonnelPlanInfo the pPersonnelPlanInfo to set
     */
    public void setpPersonnelPlanInfo(List<ResourcePlanInfo> pPersonnelPlanInfo) {
        this.pPersonnelPlanInfo = pPersonnelPlanInfo;
    }

    /**
     * @return the selectedcrisis
     */
    public List<String> getSelectedcrisis() {
        return selectedcrisis;
    }

    /**
     * @param selectedcrisis the selectedcrisis to set
     */
    public void setSelectedcrisis(List<String> selectedcrisis) {
        this.selectedcrisis = selectedcrisis;
    }

    /**
     * @return the crisis
     */
    public Map<String,String> getCrisis() {
        return crisis;
    }

    /**
     * @param crisis the crisis to set
     */
    public void setCrisis(Map<String,String> crisis) {
        this.crisis = crisis;
    }
}
