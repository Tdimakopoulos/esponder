package eu.esponder.fru.measurement;

import java.util.ArrayList;
import java.util.List;

import eu.esponder.fru.event.EventAttachment;
import eu.esponder.jaxb.model.snapshot.sensor.measurement.SensorMeasurementDTO;

public class MeasurementsEnvelope extends EventAttachment {

	List<SensorMeasurementDTO> envelope;

	public MeasurementsEnvelope() {
		envelope = new ArrayList<SensorMeasurementDTO>();
	}

	public List<SensorMeasurementDTO> getEnvelope() {
		return envelope;
	}

	public void setEnvelope(List<SensorMeasurementDTO> envelope) {
		this.envelope = envelope;
	}

	public void add(SensorMeasurementDTO sm) {
		envelope.add(sm);
	}

	public SensorMeasurementDTO get(int position) {
		return envelope.get(position);
	}

	public int size() {
		return envelope.size();
	}

	public void clear() {
		envelope.clear();
	}
}
