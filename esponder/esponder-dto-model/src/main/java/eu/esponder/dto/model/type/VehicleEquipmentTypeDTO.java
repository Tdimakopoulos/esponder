/*
 * 
 */
package eu.esponder.dto.model.type;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import eu.esponder.dto.model.crisis.resource.RegisteredReusableResourceDTO;
import eu.esponder.dto.model.crisis.resource.ReusableResourceDTO;



@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "parent", "children","vehiclequipments"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class VehicleEquipmentTypeDTO extends LogisticsResourceTypeDTO {

	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8840006333064697336L;
	private ReusableResourceDTO vehiclequipments;

		
	public ReusableResourceDTO getVehiclequipments() {
		return vehiclequipments;
	}

	public void setVehiclequipments(ReusableResourceDTO vehiclequipments) {
		this.vehiclequipments = vehiclequipments;
	}

		
	
}
