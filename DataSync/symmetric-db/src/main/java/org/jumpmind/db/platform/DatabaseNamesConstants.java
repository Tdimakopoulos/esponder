package org.jumpmind.db.platform;

final public class DatabaseNamesConstants {

    private DatabaseNamesConstants() {
    }
    
    public final static String H2 = "h2";
    public final static String HSQLDB = "hsqldb";
    public final static String HSQLDB2 = "hsqldb2";
    public final static String DERBY = "derby";
    public final static String GREENPLUM = "greenplum";
    public final static String INFORMIX = "informix";
    public final static String FIREBIRD = "firebird";
    public final static String SQLITE = "sqlite";
    public final static String INTERBASE = "interbase";    
    public final static String MSSQL = "mssql";    
    public final static String ORACLE = "oracle";
    public final static String MYSQL = "mysql";
    public final static String DB2 = "db2";
    public final static String POSTGRESQL = "postgres";
    public final static String SYBASE = "sybase";
    public final static String MARIADB = "mariadb";
    
}
