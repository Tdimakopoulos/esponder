/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package udpservermulti.exus;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import udp.server.event.publish.PublishEventMeasurment;

/**
 *
 * @author tdim
 */
public class ServerThread implements Runnable {

    private DatagramSocket socket = null;


    public ServerThread(DatagramSocket socket) {
        this.socket = socket;

    }

    public String removeFirstChar(String s) {
        return s.substring(1);
    }

    @Override
    public void run() {


        byte[] word = new byte[1000];
        byte[] definition = new byte[1000];

        try {
            // Get client request
            DatagramPacket request = new DatagramPacket(word, word.length);
            socket.receive(request);



            String sentence = new String(request.getData());

            

            String firstLetter = String.valueOf(sentence.charAt(0));
            if (firstLetter.equalsIgnoreCase("X")) {
                sentence = removeFirstChar(sentence);
                System.out.println("A or B Message: " + sentence);
                PublishEventMeasurment pM = new PublishEventMeasurment();
                pM.PublishM(sentence, "N/A");
            }
            if (firstLetter.equalsIgnoreCase("B")) {
                sentence = removeFirstChar(sentence);
                System.out.println("UI Message: " + sentence);

            }

        } catch (Exception e) {
            System.out.println("Error send and read loop : " + e.getMessage());
        }

        //System.out.println(Thread.currentThread().getName() + " just run.");
        UDPServerMultiEXUS.decNumThreads();
    }
}