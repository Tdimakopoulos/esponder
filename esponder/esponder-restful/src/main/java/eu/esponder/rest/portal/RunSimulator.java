/*
 * 
 */
package eu.esponder.rest.portal;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.codehaus.enunciate.XmlTransient;

import eu.esponder.rest.ESponderResource;

// TODO: Auto-generated Javadoc
/**
 * The Class RunSimulator.
 */
@Path("/portal/simulator")
@XmlTransient
public class RunSimulator extends ESponderResource {



	/**
	 * Run simulation.
	 *
	 * @param pkiKey the pki key
	 * @return the string
	 */
	@GET
	@Path("/run")
	@Produces({ MediaType.APPLICATION_JSON })
	public String RunSimulation(
			@QueryParam("startPointLat1") Double startPointLat1,
			@QueryParam("startPointLong1") Double startPointLong1,
			@QueryParam("destPointLat1") Double destPointLat1,
			@QueryParam("destPointLong1") Double destPointLong1,
			@QueryParam("startPointLat2") Double startingPointLat2,
			@QueryParam("startPointLong2") Double startPointLong2,
			@QueryParam("destPointLat2") Double destPointLat2,
			@QueryParam("destPointLong2") Double destPointLong2) {

		Boolean inputCoordsSelected = false;

		if(startPointLat1 != null &&
				startPointLong1 != null &&
				destPointLat1 != null &&
				destPointLong1 != null &&
				startingPointLat2 != null &&
				startPointLong2 != null &&
				destPointLat2 != null &&
				destPointLong2 != null) {

			inputCoordsSelected = true;
		}
		else
			inputCoordsSelected = false;
		
		
		if(inputCoordsSelected){
			
			System.out.println("\n\nParameters have been passed to the Simulator\n\n");
			MeasurementSimulator simulator = new MeasurementSimulator(startPointLat1, startPointLong1, destPointLat1, destPointLong1, startingPointLat2, startPointLong2, destPointLat2, destPointLong2);
			try {
				simulator.runSimulation();
			} catch (InterruptedException e) {
				return "Simulation with given parameters failed";
			}

			return "Simulation with given parameters succeeded";
		}
		else {
			System.out.println("\n\nSimulator will work with the default location values\n\n");
			MeasurementSimulator simulator = new MeasurementSimulator();
			try {
				simulator.runSimulation();
			} catch (InterruptedException e) {
				return "Simulation with default parameters failed";
			}

			return "Simulation with default parameters succeeded";
		}
	}

}