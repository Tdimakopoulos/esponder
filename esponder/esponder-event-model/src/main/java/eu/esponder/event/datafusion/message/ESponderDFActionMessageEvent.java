package eu.esponder.event.datafusion.message;

import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.event.model.ESponderEvent;



/**
 * The Class ActionEvent.
 *
 * @param <T> the generic type
 */
public abstract class ESponderDFActionMessageEvent<T extends ActionDTO> extends ESponderEvent<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -345759696434734591L;
	
}

