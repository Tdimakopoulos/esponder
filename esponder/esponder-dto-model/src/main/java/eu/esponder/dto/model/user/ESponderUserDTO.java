/*
 * 
 */
package eu.esponder.dto.model.user;



import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import eu.esponder.dto.model.crisis.resource.OperationsCentreDTO;
import eu.esponder.dto.model.crisis.resource.ResourceDTO;
import eu.esponder.dto.model.crisis.view.VoIPURLDTO;

// TODO: Auto-generated Javadoc
/**
 * The Class ESponderUserDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"pkikey","username","password","securityRole","actorID","eventadminID", "operationsCentre","fullName","actorRole"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ESponderUserDTO extends ResourceDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6015093808353080103L;
	
	/** The operations centre. */
	private OperationsCentreDTO operationsCentre;
	
	/** The actor id. */
	private Long actorID;
	
	// Media variables
	private String sIPusername;
	private String sIPpassword;
	private String sIPaddressMEOC;
	private String sIPaddressEOC;
	private String sIPaddressFRC;
	
	private String sIPaddress;
	private String sIPaddresssrv;
	
	//webcam ip
	private String webcamip;
	
	//rasberry ip
	private String rasberryip;
	
	String IMEI;
	
	String pkikey;
	String username;
	String password;
	String securityRole;
	
	/** The voip url. */
	private VoIPURLDTO voIPURL;
	
	private int actorRole;

	private String fullName;
	
	public int getActorRole() {
		return actorRole;
	}

	public void setActorRole(int actorRole) {
		this.actorRole = actorRole;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getPkikey() {
		return pkikey;
	}

	public void setPkikey(String pkikey) {
		this.pkikey = pkikey;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSecurityRole() {
		return securityRole;
	}

	public void setSecurityRole(String securityRole) {
		this.securityRole = securityRole;
	}


	
	
	public VoIPURLDTO getVoIPURL() {
		return voIPURL;
	}

	public void setVoIPURL(VoIPURLDTO voIPURL) {
		this.voIPURL = voIPURL;
	}

	public String getIMEI() {
		return IMEI;
	}

	public void setIMEI(String iMEI) {
		IMEI = iMEI;
	}

	public String getsIPaddresssrv() {
		return sIPaddresssrv;
	}

	public void setsIPaddresssrv(String sIPaddresssrv) {
		this.sIPaddresssrv = sIPaddresssrv;
	}

	public String getsIPusername() {
		return sIPusername;
	}

	public void setsIPusername(String sIPusername) {
		this.sIPusername = sIPusername;
	}

	public String getsIPpassword() {
		return sIPpassword;
	}

	public void setsIPpassword(String sIPpassword) {
		this.sIPpassword = sIPpassword;
	}

	public String getsIPaddressMEOC() {
		return sIPaddressMEOC;
	}

	public void setsIPaddressMEOC(String sIPaddressMEOC) {
		this.sIPaddressMEOC = sIPaddressMEOC;
	}

	public String getsIPaddressEOC() {
		return sIPaddressEOC;
	}

	public void setsIPaddressEOC(String sIPaddressEOC) {
		this.sIPaddressEOC = sIPaddressEOC;
	}

	public String getsIPaddressFRC() {
		return sIPaddressFRC;
	}

	public void setsIPaddressFRC(String sIPaddressFRC) {
		this.sIPaddressFRC = sIPaddressFRC;
	}

	public String getsIPaddress() {
		return sIPaddress;
	}

	public void setsIPaddress(String sIPaddress) {
		this.sIPaddress = sIPaddress;
	}

	public String getWebcamip() {
		return webcamip;
	}

	public void setWebcamip(String webcamip) {
		this.webcamip = webcamip;
	}

	public String getRasberryip() {
		return rasberryip;
	}

	public void setRasberryip(String rasberryip) {
		this.rasberryip = rasberryip;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ESponderUserDTO [userName=" + actorID + ", id=" + id + "]";
	}

	/**
	 * Gets the actor id.
	 *
	 * @return the actor id
	 */
	public Long getActorID() {
		return actorID;
	}

	/**
	 * Sets the actor id.
	 *
	 * @param actorID the new actor id
	 */
	public void setActorID(Long actorID) {
		this.actorID = actorID;
	}



	/**
	 * Gets the operations centre.
	 *
	 * @return the operations centre
	 */
	public OperationsCentreDTO getOperationsCentre() {
		return operationsCentre;
	}

	/**
	 * Sets the operations centre.
	 *
	 * @param operationsCentre the new operations centre
	 */
	public void setOperationsCentre(OperationsCentreDTO operationsCentre) {
		this.operationsCentre = operationsCentre;
	}

}
