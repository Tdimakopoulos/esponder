/*
 * 
 */
package eu.esponder.event.model.crisis;

import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.event.model.UpdateEvent;



// TODO: Auto-generated Javadoc
/**
 * The Class UpdateCrisisContextEvent.
 */
public class UpdateCrisisContextEvent extends CrisisContextEvent<CrisisContextDTO> implements UpdateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6676309937264842792L;

	/**
	 * Instantiates a new update crisis context event.
	 */
	public UpdateCrisisContextEvent(){
		setJournalMessageInfo("Update Crisis Context Event");
	}
	
}
