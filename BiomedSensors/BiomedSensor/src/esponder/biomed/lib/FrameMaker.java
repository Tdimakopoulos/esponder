/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package esponder.biomed.lib;

/**
 *
 * @author afa
 */
 public class FrameMaker {

    /**
     * fill a command frame respecting the communication protocol
     * @param cmd the command to be sent
     * @param frameNumber the  number identifying the frame to be sent
     * @return a byte stream containing the serialized frame
     */
    public  byte[] getFrame(final BiomedACommand cmd, int frameNumber) {
        int length = -1;
        byte buffer[] = null;
        BiomedACommand scmd = null;
        
        if(cmd instanceof BiomedACommand){
            scmd = (BiomedACommand)cmd;
            length = scmd.getIndex();
            buffer = new byte[length + 8];
            buffer[0] = (byte) (frameNumber >> 8);  //frame number MSB
            buffer[1] = (byte) frameNumber;         //frame number LSB
            buffer[2] = 0x00;                       //frame flags
            buffer[3] = 0x00;                       //packet flags (no time stamp, no interval, 8bits samples, signed)
            buffer[4] = (byte) length;              //payload size
            buffer[5] = 0;                          //frame is command    
            System.arraycopy(scmd.getBuffer(), 0, buffer, 6, length);
            short crc16 = (short) CRC.crc16(buffer, length + 6);
            buffer[length-1 + 7] = (byte) (crc16 >> 8);          //crc msb
            buffer[length-1 + 8] = (byte) (crc16 & 0x00FF);  //crc lsb
        }
        return Cobs.encode(buffer);
    }
}
