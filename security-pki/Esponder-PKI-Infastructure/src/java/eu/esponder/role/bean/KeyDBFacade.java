/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.role.bean;

import eu.esponder.entity.KeyDB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author tdim
 */
@Stateless
public class KeyDBFacade extends AbstractFacade<KeyDB> {
    @PersistenceContext(unitName = "Esponder-PKI-InfastructurePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public KeyDBFacade() {
        super(KeyDB.class);
    }
    
}
