package eu.esponder.jaxb.model.crisis.view;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jackson.annotate.JsonPropertyOrder;

import eu.esponder.jaxb.model.ESponderEntityDTO;
import eu.esponder.jaxb.model.snapshot.location.PointDTO;

@XmlRootElement(name="mapPoint")
@XmlType(name="MapPoint")
@JsonPropertyOrder({"title", "point"})
public class MapPointDTO extends ESponderEntityDTO {
	
	private String title;

	private PointDTO point;
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public PointDTO getPoint() {
		return point;
	}

	public void setPoint(PointDTO point) {
		this.point = point;
	}

}
