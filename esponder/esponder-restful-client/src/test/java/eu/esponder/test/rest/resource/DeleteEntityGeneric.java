package eu.esponder.test.rest.resource;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.Test;

import eu.esponder.rest.client.ResteasyClient;

public class DeleteEntityGeneric {
	private String GENERIC_DELETE_ENTITY_URI = "http://localhost:8080/esponder-restful/crisis/generic";
	
	
	@Test
	public void DeleteEntity () throws ClassNotFoundException, Exception {

		System.out.println("***************************************************");
		System.out.println("Delete Method for Generic Entity beginning");
		System.out.println("***************************************************");
		String serviceName = GENERIC_DELETE_ENTITY_URI + "/delete";

		ResteasyClient deleteClient = new ResteasyClient(serviceName, "application/xml");
		System.out.println("Client for deleteGenericEntity created successfully...");
		Map<String, String> params =  getDeleteEntityServiceParameters();
		deleteClient.delete(params);
		
		
	}


	private Map<String, String> getDeleteEntityServiceParameters() {
		Map<String, String> params = new HashMap<String, String>();
		params.put("entityClass", "eu.esponder.model.crisis.resource.Actor" );
		params.put("entityID", "14" );
		params.put("userID", "1" );
		return params;
	}

}
