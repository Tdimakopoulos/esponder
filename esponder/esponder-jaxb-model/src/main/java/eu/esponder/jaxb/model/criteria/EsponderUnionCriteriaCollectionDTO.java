package eu.esponder.jaxb.model.criteria;

import java.util.Collection;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="EsponderUnionCriteriaCollectionDTO")
public class EsponderUnionCriteriaCollectionDTO extends EsponderCriteriaCollectionDTO {

	private static final long serialVersionUID = -4963444702454465879L;
	
	/*public Set<EsponderQueryRestrictionDTO> restrictions = new HashSet<EsponderQueryRestrictionDTO>();


	public Set<EsponderQueryRestrictionDTO> getRestrictions() {
		return (Set<EsponderQueryRestrictionDTO>) restrictions;
	}

	public void setRestrictions(Set<EsponderQueryRestrictionDTO> restrictions) {
		this.restrictions = restrictions;
	}
*/
	public EsponderUnionCriteriaCollectionDTO() { }
	
	public EsponderUnionCriteriaCollectionDTO(
			Collection<EsponderQueryRestrictionDTO> restrictions) {
		super(restrictions);
	}

	
}
