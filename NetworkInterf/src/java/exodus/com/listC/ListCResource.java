/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exodus.com.listC;

import exodus.com.dbcontrol.DBControl;
import exodus.com.objects.RestListC;
import java.util.List;
import javax.ejb.Stateless;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;

/**
 * REST Web Service
 *
 * @author tom
 */
@Stateless
@Path("/listC")
public class ListCResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of ListCResource
     */
    public ListCResource() {
    }

    /**
     * Retrieves representation of an instance of exodus.com.listC.ListCResource
     * @return an instance of exodus.com.objects.ListC
     */
    @GET
    @Path("/QueryAllListC")
    @Produces("application/xml")
    public List<RestListC> getQueryListCAll() {
        DBControl pdb=new DBControl();
        return pdb.SelectFromListC("");
    }

    /**
     * PUT method for updating or creating an instance of ListCResource
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Path("/CreateListC")
    @Consumes("application/xml")
    public void putCreateListC(RestListC content) {
        DBControl pdb=new DBControl();
        pdb.createListC(content);
    }
}
