package eu.esponder.rest.voip;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author MEOC-WS1
 */
public class voipDB {

	ArrayList<voipset> pdb = new ArrayList<voipset>();

	int ipos;

	String filename;

	public void SaveOnFile() throws FileNotFoundException, IOException {
		// OsgiSettings pset=new OsgiSettings();

		FileOutputStream fos = new FileOutputStream(filename);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(pdb);
		oos.close();
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public void LoadFronFile() throws FileNotFoundException, IOException,
			ClassNotFoundException {
		// OsgiSettings pset=new OsgiSettings();
		pdb.clear();
		FileInputStream fis = new FileInputStream(filename);
		ObjectInputStream ois = new ObjectInputStream(fis);
		pdb = (ArrayList<voipset>) (List<voipset>) ois.readObject();
		ois.close();
		ipos = pdb.size();

	}

	public voipset getItem(int indexpos) {
		return pdb.get(indexpos);
	}

	public int getpos() {
		return ipos;
	}

	public void Add(voipset pitem) {
		ipos = ipos + 1;
		pdb.add(pitem);
	}

	
	public boolean Edit(voipset pitem) {
		for (int i = 0; i < pdb.size(); i++) {
			if (pdb.get(i).getFrUsername()
					.equalsIgnoreCase(pitem.getFrUsername())) {
				pdb.set(i, pitem);
				return true;
			}
		}
		return false;
	}

	public boolean Delete(voipset pitem) {
		for (int i = 0; i < pdb.size(); i++) {
			if (pdb.get(i).getFrUsername()
					.equalsIgnoreCase(pitem.getFrUsername())) {
				pdb.remove(i);
				return true;
			}
		}
		return false;
	}
}
