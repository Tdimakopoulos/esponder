/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exus.esponder.biomedb.client;

import eu.exus.esponder.biomeda.client.BiomedACommandSender;
import eu.exus.esponder.biomeda.client.BiomedAReadThread;
import exus.esponder.settings.settingsmanager;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;


public class BiomedBClient {
    
    StreamConnection sc = null;
    
    
    public void OpenConnection() throws IOException {
        settingsmanager pp = new settingsmanager();
        pp.ReadValues();
        System.out.println("Open Bluetooth Connection with the BiomedB... Please wait ....");
        sc = (StreamConnection) Connector.open("btspp://000BCE05D649:1;authenticate=false;encrypt=false;master=false");
        System.out.println("Connection and streaming is open for BiomedB ....");
    }
    
    public void CloseConnection() throws IOException {
        System.out.println("Closing connection with BiomedB");
        sc.close();
        System.out.println("Connection with biomedB closed");
    }
    
    

    
}
