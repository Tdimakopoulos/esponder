/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package esponder.pki.test;

import eu.esponder.session.ws.Sessiondetails;
import eu.esponder.session.ws.Sessiontable;

/**
 *
 * @author tdim
 */
public class EsponderPKITest {

    static String pkiKey="30820122300d06092a864886f70d01010105000382010f003082010a028201010081a98f02c9111e0b4d689cc60e078c3a86f48ff9d808e913a54a5bfec75ad6f69dab82e6f9991e724aded846708ef9b1a39a633ffd2719cd0546ecba9e6886b55be6992ab2df7ffc2c2260e7826f3be41264157b2fd1c134d71cd38bda162ea03425d49b501a83bbf3df8d5b2d393dabb9364e9c8aaa432c22ffcff91442f8b1b12148a5e6fbcbde2857927a80afaa52862c0ac47b470f686f18a6b8b280d36886fb603de443a708d2e85201e3a084d883c95eccc1dfb0b374488f99578d0934ead8179ed3dc467015c732b590f806e5a0e05494ccf0cca30246d07d1c01083aae3c8f51a641ca33937b54caf74c3677b5a5e9176008cbd195fc1bc29bd78c4f0203010001";
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        System.out.println("------------------------------------------------------------------------------------------------------------");
        System.out.println("********************************************** NO SESSION **************************************************");
        System.out.println("------------------------------------------------------------------------------------------------------------");
        System.out.println("Direct Access using PKIKey and check for role and authorization. NO SESSION");
        System.out.println("Access Role");
        System.out.println("Role for PKI Key : "+getRoleForKey(pkiKey));
        System.out.println("Access Authorization List");
        java.util.List<java.lang.String> pAuthorized=getAuthorizationListForKey(pkiKey);
        System.out.println("Authorization List");
        for(int i=0;i<pAuthorized.size();i++)
        {
            System.out.println("Function Authorized : "+pAuthorized.get(i)+" "+(i+1)+" From "+pAuthorized.size());
        }
        System.out.println("------------------------------------------------------------------------------------------------------------");
        
        
        
        System.out.println("------------------------------------------------------------------------------------------------------------");
        System.out.println("*********************************************** SESSION ****************************************************");
        System.out.println("------------------------------------------------------------------------------------------------------------");
        System.out.println("Passing the key returing a structure with the sessionid and role and other information");
        Sessiondetails psession=sessionLogin(pkiKey,1);
        String SessionID=psession.getSessionID();
        System.out.println(" Session ID :"+psession.getSessionID());
        System.out.println(" Login Role : "+psession.getRole());
        for(int i=0;i<psession.getAccfunc().size();i++)
        {
            System.out.println("Function Authorized : "+psession.getAccfunc().get(i)+" "+(i+1)+" From "+psession.getAccfunc().size());
        }
        
        System.out.println("Before calling any function in your GUI application, you need to update the session. This will keep the session alive");
        System.out.println("Session Update : "+sessionUpdate(SessionID));
        
        System.out.println("And finally logout");
        System.out.println("Session Logout : "+sessionLogout(SessionID));
        System.out.println("------------------------------------------------------------------------------------------------------------");
        
    }

    private static String getRoleForKey(java.lang.String pkiKey) {
        eu.esponder.pki.access.EsponderAccessService_Service service = new eu.esponder.pki.access.EsponderAccessService_Service();
        eu.esponder.pki.access.EsponderAccessService port = service.getEsponderAccessServicePort();
        return port.getRoleForKey(pkiKey);
    }

    private static java.util.List<java.lang.String> getAuthorizationListForKey(java.lang.String pkiKey) {
        eu.esponder.pki.access.EsponderAccessService_Service service = new eu.esponder.pki.access.EsponderAccessService_Service();
        eu.esponder.pki.access.EsponderAccessService port = service.getEsponderAccessServicePort();
        return port.getAuthorizationListForKey(pkiKey);
    }

    private static Sessiontable sessionInfo(java.lang.String arg0) {
        eu.esponder.session.ws.SessionManager_Service service = new eu.esponder.session.ws.SessionManager_Service();
        eu.esponder.session.ws.SessionManager port = service.getSessionManagerPort();
        return port.sessionInfo(arg0);
    }

    private static Sessiondetails sessionLogin(java.lang.String arg0, int arg1) {
        eu.esponder.session.ws.SessionManager_Service service = new eu.esponder.session.ws.SessionManager_Service();
        eu.esponder.session.ws.SessionManager port = service.getSessionManagerPort();
        return port.sessionLogin(arg0, arg1);
    }

    private static boolean sessionLogout(java.lang.String arg0) {
        eu.esponder.session.ws.SessionManager_Service service = new eu.esponder.session.ws.SessionManager_Service();
        eu.esponder.session.ws.SessionManager port = service.getSessionManagerPort();
        return port.sessionLogout(arg0);
    }

    private static boolean sessionUpdate(java.lang.String arg0) {
        eu.esponder.session.ws.SessionManager_Service service = new eu.esponder.session.ws.SessionManager_Service();
        eu.esponder.session.ws.SessionManager port = service.getSessionManagerPort();
        return port.sessionUpdate(arg0);
    }
}
