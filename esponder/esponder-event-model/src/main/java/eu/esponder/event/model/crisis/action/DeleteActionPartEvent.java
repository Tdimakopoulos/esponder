/*
 * 
 */
package eu.esponder.event.model.crisis.action;

import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.event.model.DeleteEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class DeleteActionPartEvent.
 */
public class DeleteActionPartEvent extends ActionPartEvent<ActionPartDTO> implements DeleteEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -225309812850357319L;
	
	/**
	 * Instantiates a new delete action part event.
	 */
	public DeleteActionPartEvent(){
		setJournalMessageInfo("DeleteActionPartEvent");
	}
}
