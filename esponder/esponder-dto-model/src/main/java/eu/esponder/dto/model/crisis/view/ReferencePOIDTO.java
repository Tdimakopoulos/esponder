/*
 * 
 */
package eu.esponder.dto.model.crisis.view;

import java.util.Set;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;


// TODO: Auto-generated Javadocs
/**
 * The Class ReferencePOIDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"id", "title", "averageSize", "snapshots"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ReferencePOIDTO extends ResourcePOIDTO {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 698766046297897727L;
	
	private Set<Long> snapshots;
	
	/** The average size. */
	private Float averageSize;

	/**
	 * Gets the average size.
	 *
	 * @return the average size
	 */
	public Float getAverageSize() {
		return averageSize;
	}

	/**
	 * Sets the average size.
	 *
	 * @param averageSize the new average size
	 */
	public void setAverageSize(Float averageSize) {
		this.averageSize = averageSize;
	}

	public Set<Long> getSnapshots() {
		return snapshots;
	}

	public void setSnapshots(Set<Long> snapshots) {
		this.snapshots = snapshots;
	}

}
