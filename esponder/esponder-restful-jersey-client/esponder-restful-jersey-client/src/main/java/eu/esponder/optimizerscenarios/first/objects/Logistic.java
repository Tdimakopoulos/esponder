package eu.esponder.optimizerscenarios.first.objects;

import java.util.Date;

/**
 *
 * @author Tom
 */
public class Logistic {
    private String name;
        
        private int number;
            
        private String type;
        private String category;
        
        
        public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getNumber() {
			return number;
		}

		public void setNumber(int number) {
			this.number = number;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getCategory() {
			return category;
		}

		public void setCategory(String category) {
			this.category = category;
		}

		public Logistic() {
                
        }
        
        public Logistic(String name) {
                this.name = name;
        }

    public Logistic(String name, int number, String photo, String position) {
                this.name = name;
        this.number = number;
                this.type = photo;
        this.category = position;
        }
    
    
}
