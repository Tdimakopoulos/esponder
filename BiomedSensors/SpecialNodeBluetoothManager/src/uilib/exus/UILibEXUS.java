/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uilib.exus;

import exus.esponder.settings.settingsmanager;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;

/**
 *
 * @author tdim
 */
public class UILibEXUS {

    StreamConnection sc = null;
    UICommands pp = new UICommands();
    UIReadThread rt = new UIReadThread();

    public UICommands GetCommandModule()
    {
        return pp;
    }
    
    public void OpenConnection() throws IOException {
        settingsmanager pps = new settingsmanager();
        pps.ReadValues();
        System.out.println("Open Bluetooth Connection with the UI... Please wait ....");
        sc = (StreamConnection) Connector.open(pps.getbUI());//"btspp://000BCE0AE779:1;authenticate=false;encrypt=false;master=false");
        pp.OpenStream(sc);//pps.getbUI());//(
        System.out.println("Connection and streaming is open for UI ....");
    }

    public void CloseConnection() throws IOException {
        System.out.println("Closing connection with UI");
        pp.CloseStream();
        sc.close();
        System.out.println("Connection with UI closed");
    }

    public void StartReadUIThread() {

        rt.SetBTConnection(sc);
        rt.start();
    }

    public void StopReadUIThread() {
        rt.StopReadThread();
    }

    
}
