
package eu.esponder.datasync.server.web.rest.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Node {

    private String name;
    private String externalId;
	private boolean registrationServer;
    private String syncUrl;
    private int batchToSendCount;    
    private int batchInErrorCount;
    private Date lastHeartbeat;
	private boolean registered;
    private boolean initialLoaded;
    private boolean reverseInitialLoaded;
    private String createdAtNodeId;
    
    public String getCreatedAtNodeId() {
		return createdAtNodeId;
	}

	public void setCreatedAtNodeId(String createdAtNodeId) {
		this.createdAtNodeId = createdAtNodeId;
	}

	public boolean isReverseInitialLoaded() {
		return reverseInitialLoaded;
	}

	public void setReverseInitialLoaded(boolean reverseInitialLoaded) {
		this.reverseInitialLoaded = reverseInitialLoaded;
	}

	public int getBatchToSendCount() {
		return batchToSendCount;
	}

	public void setBatchToSendCount(int batchToSendCount) {
		this.batchToSendCount = batchToSendCount;
	}

	public int getBatchInErrorCount() {
		return batchInErrorCount;
	}

	public void setBatchInErrorCount(int batchInErrorCount) {
		this.batchInErrorCount = batchInErrorCount;
	}

	public boolean isRegistered() {
		return registered;
	}

	public void setRegistered(boolean registered) {
		this.registered = registered;
	}

	public boolean isInitialLoaded() {
		return initialLoaded;
	}

	public void setInitialLoaded(boolean initialLoaded) {
		this.initialLoaded = initialLoaded;
	}

	public String getSyncUrl() {
		return syncUrl;
	}

	public void setSyncUrl(String syncUrl) {
		this.syncUrl = syncUrl;
	}

	public boolean isRegistrationServer() {
		return registrationServer;
	}

	public void setRegistrationServer(boolean registrationServer) {
		this.registrationServer = registrationServer;
	}

	public Node(String name) {
        setName(name);
    }

    public Node() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getLastHeartbeat() {
		return lastHeartbeat;
	}

	public void setLastHeartbeat(Date lastHeartbeat) {
		this.lastHeartbeat = lastHeartbeat;
	}
	
    public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}	
}
