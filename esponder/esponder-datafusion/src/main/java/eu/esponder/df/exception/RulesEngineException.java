/*
 * 
 */
package eu.esponder.df.exception;


// TODO: Auto-generated Javadoc
/**
 * The Class RulesEngineException.
 */
public class RulesEngineException extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new rules engine exception.
	 *
	 * @param message the message
	 */
	public RulesEngineException(String message) {
        super(message);
    }

    /**
     * Instantiates a new rules engine exception.
     *
     * @param message the message
     * @param cause the cause
     */
    public RulesEngineException(String message, Throwable cause) {
        super(message, cause);
    }

}
