package eu.esponder.controller.security.session;

import java.util.List;

import javax.ejb.Local;

import eu.esponder.session.ESponderSession;


@Local
public interface SecuritySessionManagerService extends SecuritySessionManagerRemoteService {

	ESponderSession createsession(ESponderSession user, Long userID);

	ESponderSession updatesession(ESponderSession user, Long userID);

	Long deletesession(long deletedUserId, Long userID);

	List<ESponderSession> findAllSessions();

}
