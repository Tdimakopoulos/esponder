/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exodus.com.rest.client;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

/**
 * Jersey REST client generated for REST resource:InterfResource [/interf]<br>
 *  USAGE:
 * <pre>
 *        InterfClient client = new InterfClient();
 *        Object response = client.XXX(...);
 *        // do whatever with response
 *        client.close();
 * </pre>
 *
 * @author user
 */
public class InterfClient {
    private WebResource webResource;
    private Client client;
    private static final String BASE_URI = "http://localhost:8080/NetworkInterf/resources";

    public InterfClient() {
        com.sun.jersey.api.client.config.ClientConfig config = new com.sun.jersey.api.client.config.DefaultClientConfig();
        client = Client.create(config);
        webResource = client.resource(BASE_URI).path("interf");
    }

    public void putCreateInterf(Object requestEntity) throws UniformInterfaceException {
        webResource.path("CreateInterf").type(javax.ws.rs.core.MediaType.APPLICATION_XML).put(requestEntity);
    }

    public <T> T getQueryInterfAll(Class<T> responseType) throws UniformInterfaceException {
        WebResource resource = webResource;
        resource = resource.path("QueryAllInterf");
        return resource.accept(javax.ws.rs.core.MediaType.APPLICATION_XML).get(responseType);
    }

    public void close() {
        client.destroy();
    }
    
}
