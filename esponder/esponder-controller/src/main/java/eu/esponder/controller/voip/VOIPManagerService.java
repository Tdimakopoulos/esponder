package eu.esponder.controller.voip;

import javax.ejb.Local;



import eu.esponder.dto.model.voip.ConferenceParticipantsDTO;
import eu.esponder.dto.model.voip.VoipConferenceDTO;
import eu.esponder.model.voip.ConferenceParticipants;
import eu.esponder.model.voip.VoipConference;
import eu.esponder.session.ESponderSession;


@Local
public interface VOIPManagerService extends VOIPManagerRemoteService {

	
	VoipConference AddConference(
			VoipConference voipConference,
			ConferenceParticipants conferenceParticipants);
	
	VoipConference UpdateConference(
			VoipConferenceDTO voipConference,
			ConferenceParticipantsDTO conferenceParticipants);
	
	VoipConference FindByConfEnID(Long ID);
	VoipConference FindByConID(Long ID);
	VoipConference FindByConfID(Long ID);
	VoipConference FindByConfTitle(Long ID);
	
	VoipConference CreateConference(VoipConferenceDTO pCreate);
	
	VoipConference UpdateConference(VoipConferenceDTO pUpdate);
	
	ConferenceParticipants FindByConfPEnID(Long ID);
	ConferenceParticipants FindByConPID(Long ID);
	ConferenceParticipants FindByConfPID(Long ID);
	ConferenceParticipants FindByConfPTitle(Long ID);
	
	ConferenceParticipants CreateConferenceParticipants(ConferenceParticipantsDTO pCreate);
	
	ConferenceParticipantsDTO UpdateConferenceParticipants(ConferenceParticipantsDTO pUpdate);

}
