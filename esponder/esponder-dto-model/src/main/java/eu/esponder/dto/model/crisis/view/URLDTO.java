/*
 * 
 */
package eu.esponder.dto.model.crisis.view;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonTypeInfo;


// TODO: Auto-generated Javadoc
/**
 * The Class URLDTO.
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class URLDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5414252531599578444L;

	/** The Constant separator. */
	private static final String separator = "/";
	
	/** The Constant protocolSeparator. */
	private static final String protocolSeparator = "://";
	
	/** The protocol. */
	private String protocol;
	
	/** The host name. */
	private String hostName;
	
	/** The path. */
	private String path;
	
	private String sIURLPusername;
	private String sIURLPpassword;

	private String sIURLPaddresssrv;
	private String sIURLPaddresscli;
	
	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	
	
	

	public String getsIURLPusername() {
		return sIURLPusername;
	}

	public void setsIURLPusername(String sIURLPusername) {
		this.sIURLPusername = sIURLPusername;
	}

	public String getsIURLPpassword() {
		return sIURLPpassword;
	}

	public void setsIURLPpassword(String sIURLPpassword) {
		this.sIURLPpassword = sIURLPpassword;
	}

	public String getsIURLPaddresssrv() {
		return sIURLPaddresssrv;
	}

	public void setsIURLPaddresssrv(String sIURLPaddresssrv) {
		this.sIURLPaddresssrv = sIURLPaddresssrv;
	}

	public String getsIURLPaddresscli() {
		return sIURLPaddresscli;
	}

	public void setsIURLPaddresscli(String sIURLPaddresscli) {
		this.sIURLPaddresscli = sIURLPaddresscli;
	}

	public static String getSeparator() {
		return separator;
	}

	public static String getProtocolseparator() {
		return protocolSeparator;
	}

	/**
	 * Gets the url.
	 *
	 * @return the url
	 */
	@JsonIgnore
	public String getURL() {
		return protocol + protocolSeparator + hostName + separator + path; 
	}
	
	/**
	 * Gets the protocol.
	 *
	 * @return the protocol
	 */
	public String getProtocol() {
		return protocol;
	}

	/**
	 * Sets the protocol.
	 *
	 * @param protocol the new protocol
	 */
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	/**
	 * Gets the host.
	 *
	 * @return the host
	 */
	public String getHost() {
		return hostName;
	}

	/**
	 * Sets the host.
	 *
	 * @param host the new host
	 */
	public void setHost(String host) {
		this.hostName = host;
	}

	/**
	 * Gets the path.
	 *
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * Sets the path.
	 *
	 * @param path the new path
	 */
	public void setPath(String path) {
		this.path = path;
	}

}
