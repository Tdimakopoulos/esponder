package eu.esponder.test.rest.client.proxy;

import org.testng.annotations.Test;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.rest.client.proxy.ResteasyProxyClient;

public class ResteasyProxyClientTest {
	
	@Test
	public void invokeServiceWithProxyTest() {
		ResteasyProxyClient client = new ResteasyProxyClient();
		ResultListDTO result = client.invokeService(new Long(1));
		System.out.println(result.toString());
	}

}
