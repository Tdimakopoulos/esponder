/*
 * 
 */
package eu.esponder.rest.portal;

import java.math.BigDecimal;
import java.util.Date;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.codehaus.enunciate.XmlTransient;

import com.prosyst.mprm.backend.event.EventListenerException;

import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.FRTeamDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.event.mobile.MobileFileNotifierEvent;
import eu.esponder.event.model.crisis.resource.MoveFRTeamEvent;
import eu.esponder.eventadmin.ESponderEventPublisher;
import eu.esponder.rest.ESponderResource;

// TODO: Auto-generated Javadoc
/**
 * The Class RunSimulator.
 */
@Path("/portal/files")
@XmlTransient
public class FileNotifierGenerator extends ESponderResource {

	@GET
	@Path("/sendactionevent")
	@Produces({ MediaType.APPLICATION_JSON })
	public String SendEventForAction(@QueryParam("title") String title)
	{
		ESponderEventPublisher<MoveFRTeamEvent> publisher = new ESponderEventPublisher<MoveFRTeamEvent>(
				MoveFRTeamEvent.class);

		MoveFRTeamEvent result = new MoveFRTeamEvent();
		//result.setEventAttachment(actionSnapshot);
		result.setJournalMessage("Test Event");
		result.setEventSeverity(SeverityLevelDTO.SERIOUS);
		result.setEventTimestamp(new Date());
		result.setEventSeverity(SeverityLevelDTO.UNDEFINED);
		//result.setEventSource(subactorDTO);
		result.setEventAttachment(new FRTeamDTO());
		PointDTO pFinalPosition= new PointDTO();
		pFinalPosition.setAltitude(new BigDecimal(10));
		pFinalPosition.setLatitude(new BigDecimal(37.952647));
		pFinalPosition.setLongitude(new BigDecimal(23.735902));
		result.setpFinalPosition(pFinalPosition);
		try {

			publisher.publishEvent(result);
			publisher.CloseConnection();
		} catch (EventListenerException e) {

			
			return "ERROR";
		}
		return "OK";
	}
	@GET
	@Path("/sendfile")
	@Produces({ MediaType.APPLICATION_JSON })
	public String SendFile(
			@QueryParam("imei") String imei,
			@QueryParam("host") String host,
			@QueryParam("port") String port,
			@QueryParam("dir") String dir,
			@QueryParam("file") String file,
			@QueryParam("description") String description) {
		
			ESponderEventPublisher<MobileFileNotifierEvent> publisher = 
					new ESponderEventPublisher<MobileFileNotifierEvent>(MobileFileNotifierEvent.class);
			
			MobileFileNotifierEvent event = new MobileFileNotifierEvent();
			event.setDescription(description);
			event.setFileURL("http://"+host+":"+port+"/"+dir+"/"+file);
			event.setImei(imei);
			event.setEventSeverity(SeverityLevelDTO.SERIOUS);
			event.setEventTimestamp(new Date());
			event.setJournalMessage("File notification has been generated for FRC with imei "+imei);
			event.setJournalMessageInfo("File notification has been generated for FRC with imei "+imei);
			
			try {
				publisher.publishEvent(event);
			} catch (Exception e) {
				return e.getMessage();
			}
			publisher.CloseConnection();
			return "Success";
	}
	

	/**
	 * Run simulation.
	 *
	 * @param pkiKey the pki key
	 * @return the string
	 */
	@GET
	@Path("/run")
	@Produces({ MediaType.APPLICATION_JSON })
	public String RunFileNotifierGenerator(
			@QueryParam("file") String file,
			@QueryParam("description") String description) {
		
			ESponderEventPublisher<MobileFileNotifierEvent> publisher = 
					new ESponderEventPublisher<MobileFileNotifierEvent>(MobileFileNotifierEvent.class);
			
			MobileFileNotifierEvent event = new MobileFileNotifierEvent();
			event.setDescription(description);
			event.setFileURL("http://192.168.2.249/mob/"+file);
			event.setImei("353581051146316");
			event.setEventSeverity(SeverityLevelDTO.SERIOUS);
			event.setEventTimestamp(new Date());
			event.setJournalMessage("File notification has been generated for FRC");
			event.setJournalMessageInfo("File notification has been generated for FRC");
			
			try {
				publisher.publishEvent(event);
			} catch (Exception e) {
				return e.getMessage();
			}
			publisher.CloseConnection();
			return "Success";
	}

		

}