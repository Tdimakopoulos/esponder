/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.admin.pki;

import eu.esponder.keymanagerws.KeyDB;
import eu.esponder.keymanagerws.KeyDBManagerCRUD_Service;
import eu.esponder.usermanager.ws.EventAdminUsermanager_Service;
import eu.esponder.webservices.EsKeyManager;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipOutputStream;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class pkimanager {
    
    
    

    OutputStream out = null;
    String filename = "pki.key";
    OutputStream outs = null;
    String filenames = "secret.key";
    private List<pkidetails> pList = new ArrayList<pkidetails>();
    private pkidetails selectedDoctor;
    private String szKey1;
    private String username;
    private String organization;
    private String role;
    private String password;
    
    public String adduser() {
        username = "";
        organization = "";
        role = "0";
        password=" ";
        return "Add_User?faces-redirect=true";
    }

    public String saveadduser() {
        generateUserKeyPair(username, organization, role, new Long(-1));
        createUserInEventAdmin(username, password, role);
        refresh();
        return "DashBoard?faces-redirect=true";
    }

    public String canceladduser() {

        return "DashBoard?faces-redirect=true";
    }

    public String deletedoctor()
    {
        Revoke(selectedDoctor.getId());
        refresh();
        return "DashBoard?faces-redirect=true";
        
    }
            
    //public String GenerateUserKeyPair(String Username, String Organization, String Role, Long iuserid) {
    //private String generateUserKeyPair(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.Long arg3) {
    /**
     * Creates a new instance of pkimanager
     */
    public String download() throws IOException {

        System.out.println("Looking for key for " + selectedDoctor.getUsername() + " -- " + selectedDoctor.getRole() + " -- " + selectedDoctor.getType());
        String szKey = getUserPublicKeyString(selectedDoctor.getUsername(), selectedDoctor.getType(), selectedDoctor.getRole());
        System.out.println("Download" + szKey);
        szKey1 = szKey;
        return "umngr_1?faces-redirect=true";
    }

    public String downloadback() throws IOException {



        return "DashBoard?faces-redirect=true";
    }

    public pkimanager() {

        java.util.List<eu.esponder.webservices.KeyDB> pFind = queryAllUsers();
        for (int i = 0; i < pFind.size(); i++) {
            pkidetails item = new pkidetails();
            item.setId(pFind.get(i).getId());
            item.setRole(pFind.get(i).getRole());
            item.setType(pFind.get(i).getOrganization());
            item.setUsername(pFind.get(i).getUsername());
            if (pFind.get(i).getOrganization().equalsIgnoreCase("SmartPhone")) {
            } else {
                pList.add(item);
            }
        }
    }

    public void refresh() {
        pList.clear();
        java.util.List<eu.esponder.webservices.KeyDB> pFind = queryAllUsers();
        for (int i = 0; i < pFind.size(); i++) {
            pkidetails item = new pkidetails();
            item.setId(pFind.get(i).getId());
            item.setRole(pFind.get(i).getRole());
            item.setType(pFind.get(i).getOrganization());
            item.setUsername(pFind.get(i).getUsername());
            if (pFind.get(i).getOrganization().equalsIgnoreCase("SmartPhone")) {
            } else {
                pList.add(item);
            }
        }
    }

    /**
     * @return the pList
     */
    public List<pkidetails> getpList() {
        refresh();
        return pList;
    }

    /**
     * @param pList the pList to set
     */
    public void setpList(List<pkidetails> pList) {
        this.pList = pList;
    }

    /**
     * @return the selectedDoctor
     */
    public pkidetails getSelectedDoctor() {
        return selectedDoctor;
    }

    /**
     * @param selectedDoctor the selectedDoctor to set
     */
    public void setSelectedDoctor(pkidetails selectedDoctor) {
        this.selectedDoctor = selectedDoctor;
    }

    /**
     * @return the szKey1
     */
    public String getSzKey1() {
        //szKey1=getUserPublicKeyString(selectedDoctor.getUsername(), selectedDoctor.getRole(), selectedDoctor.getType());
        //System.out.println("getter "+szKey1);
        return szKey1;
    }

    /**
     * @param szKey1 the szKey1 to set
     */
    public void setSzKey1(String szKey1) {
        this.szKey1 = szKey1;
    }

    private java.util.List<eu.esponder.webservices.KeyDB> queryAllUsers() {
        EsKeyManager service = new EsKeyManager();
        eu.esponder.webservices.KeyManager port = service.getKeyManagerPort();
        return port.queryAllUsers();
    }

    private String getUserPublicKeyString(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2) {
        EsKeyManager service = new EsKeyManager();
        eu.esponder.webservices.KeyManager port = service.getKeyManagerPort();
        return port.getUserPublicKeyString(arg0, arg1, arg2);
    }

    private byte[] getUserSecretKey(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2) {
        EsKeyManager service = new EsKeyManager();
        eu.esponder.webservices.KeyManager port = service.getKeyManagerPort();
        return port.getUserSecretKey(arg0, arg1, arg2);
    }

    private byte[] getUserPublicKey(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2) {
        EsKeyManager service = new EsKeyManager();
        eu.esponder.webservices.KeyManager port = service.getKeyManagerPort();
        return port.getUserPublicKey(arg0, arg1, arg2);
    }

    private String generateUserKeyPair(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2, java.lang.Long arg3) {
        EsKeyManager service = new EsKeyManager();
        eu.esponder.webservices.KeyManager port = service.getKeyManagerPort();
        return port.generateUserKeyPair(arg0, arg1, arg2, arg3);
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the organization
     */
    public String getOrganization() {
        return organization;
    }

    /**
     * @param organization the organization to set
     */
    public void setOrganization(String organization) {
        this.organization = organization;
    }

    /**
     * @return the role
     */
    public String getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(String role) {
        this.role = role;
    }

    private void Revoke(Long ID)
    {
        remove(find(ID));
    }
    private KeyDB find(java.lang.Object id) {
        KeyDBManagerCRUD_Service service= new KeyDBManagerCRUD_Service();
        eu.esponder.keymanagerws.KeyDBManagerCRUD port = service.getKeyDBManagerCRUDPort();
        return port.find(id);
    }

    private void remove(eu.esponder.keymanagerws.KeyDB keyDB) {
        KeyDBManagerCRUD_Service service= new KeyDBManagerCRUD_Service();
        eu.esponder.keymanagerws.KeyDBManagerCRUD port = service.getKeyDBManagerCRUDPort();
        port.remove(keyDB);
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    private String createUserInEventAdmin(java.lang.String name, java.lang.String pass, java.lang.String group) {
        EventAdminUsermanager_Service service=new EventAdminUsermanager_Service();
        eu.esponder.usermanager.ws.EventAdminUsermanager port = service.getEventAdminUsermanagerPort();
        return port.createUserInEventAdmin(name, pass, group);
    }
}
