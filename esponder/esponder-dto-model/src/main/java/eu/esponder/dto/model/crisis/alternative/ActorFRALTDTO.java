/*
 * 
 */
package eu.esponder.dto.model.crisis.alternative;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;


// TODO: Auto-generated Javadoc
/**
 * The Class ActorFRALTDTO.
 */
@JsonSerialize(include = Inclusion.NON_NULL)
@JsonPropertyOrder({ "resourceId",  "title",  "personnel",
		"voIPURL", "equipmentSet", "snapshots", "frchief", "team" })
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class ActorFRALTDTO extends FirstResponderActorALTDTO {

	

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3898480228648605614L;

	/** The frchief. */
//	private ActorFRCALTDTO frchief;
	private Long frchief;
	
	
	/**
	 * The team.
	 *
	 * @return the frchief
	 */
//	private FRTeamALTDTO team;
	
	/**
	 * Gets the team.
	 *
	 * @return the team
	 */
//	public FRTeamALTDTO getTeam() {
//		return team;
//	}

	/**
	 * Sets the team.
	 *
	 * @param team the new team
	 */
//	public void setTeam(FRTeamALTDTO team) {
//		this.team = team;
//	}

	/**
	 * Gets the frchief.
	 *
	 * @return the frchief
	 */
	public long getFrchief() {
		return frchief;
	}

	/**
	 * Sets the frchief.
	 *
	 * @param frchief the new frchief
	 */
	public void setFrchief(Long frchief) {
		this.frchief = frchief;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.crisis.alternative.FirstResponderActorALTDTO#toString()
	 */
	@Override
	public String toString() {
		return this.getClass().getName()+" [id=" + id + ", title=" + title + "]";
	}
}
