/*
 * 
 */
package eu.esponder.event.model.crisis;




import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.event.model.CreateEvent;



// TODO: Auto-generated Javadoc
/**
 * The Class CreateCrisisContextEvent.
 */
public class CreateCrisisContextEvent extends CrisisContextEvent<CrisisContextDTO> implements CreateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5728175601548401117L;

	
	
	/**
	 * Instantiates a new creates the crisis context event.
	 */
	public CreateCrisisContextEvent(){
		setJournalMessageInfo("Create Crisis Context Event");
	}
		
}
