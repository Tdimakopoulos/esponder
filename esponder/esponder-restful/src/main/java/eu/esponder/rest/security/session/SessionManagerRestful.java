package eu.esponder.rest.security.session;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.rest.ESponderResource;

@Path("/security")
public class SessionManagerRestful extends ESponderResource {

	@GET
	@Path("/ValidateSessionID")
	@Produces({MediaType.APPLICATION_JSON})
	public ESponderUserDTO ValidateSessionID(
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		return null;
	}
	
	@GET
	@Path("/ValidateSessionIDAndUserID")
	@Produces({MediaType.APPLICATION_JSON})
	public ESponderUserDTO ValidateSessionIDAndUserID(
			@QueryParam("userID") @NotNull(message="userID may not be null") Long userID,
			@QueryParam("sessionID") @NotNull(message="sessionID may not be null") String sessionID) {

		
		return null;
	}
	
	@GET
	@Path("/ValidateUsernameAndPassword")
	@Produces({MediaType.APPLICATION_JSON})
	public ESponderUserDTO ValidateUsernameAndPassword(
			@QueryParam("userName") @NotNull(message="userID may not be null") String userName,
			@QueryParam("passWord") @NotNull(message="sessionID may not be null") String passWord) {

		ESponderUserDTO puser=getActorRemoteService().findUserByUsernameRemote(userName);
		if(puser.getPassword().equalsIgnoreCase(passWord))
			return puser;
		else
			return null;
	}
	
	@GET
	@Path("/ValidateUsernameAndPasswordVOIP")
	@Produces({MediaType.APPLICATION_JSON})
	public ESponderUserDTO ValidateUsernameAndPasswordVOIP(
			@QueryParam("userName") @NotNull(message="userID may not be null") String userName,
			@QueryParam("passWord") @NotNull(message="sessionID may not be null") String passWord) {

		ESponderUserDTO puser=getActorRemoteService().findUserByUsernameSipRemote(userName);
		if(puser.getsIPpassword().equalsIgnoreCase(passWord))
			return puser;
		else
			return null;
		
		
	}
	
	@GET
	@Path("/ValidateUsernameAndPasswordVOIPString")
	public String ValidateUsernameAndPasswordVOIPStr(
			@QueryParam("userName") @NotNull(message="userID may not be null") String userName,
			@QueryParam("passWord") @NotNull(message="sessionID may not be null") String passWord) {

		ESponderUserDTO puser=getActorRemoteService().findUserByUsernameSipRemote(userName);
		if(puser.getsIPpassword().equalsIgnoreCase(passWord))
			return "1";
		else
			return "0";
		
		
	}
}
