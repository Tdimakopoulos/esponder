package eu.esponder.jaxb.model.crisis.resource;

import javax.xml.bind.annotation.XmlSeeAlso;

@XmlSeeAlso({
	ConsumableResourceDTO.class,
	ReusableResourceDTO.class
})
public abstract class LogisticsResourceDTO extends ResourceDTO {

}
