package eu.esponder.ws.client.logic.objects;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.xml.sax.SAXException;

import eu.esponder.CoordinatesForAddress.Coordinates;
import eu.esponder.CoordinatesForAddress.addressToCordinates;
import eu.esponder.dto.model.crisis.CrisisContextAlertEnumDTO;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.action.ActionOperationEnumDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.resource.ActorDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.ws.client.create.CreateOperations;

public class CrisisManager {

	
    private String status;
    private String addinfo;
    private String id;
    private String titlec;
    private String location = "Leoforos Mesogion 17, Athens 115 27, Greece";
    private Date date1;
    private Date date2;
    private Date date3;
    private List<String> naturalDisaster;
    private List<String> otherDisaster;
    private String title;
    private double lat;
    private double lng;
    public CrisisContextAlert alertlevel;
    public enum CrisisContextAlert{
        Level1,
        Level2,
        Level3,
        Level4,
        Level5
    }
   
    
	public String createCrisis() {
        try {
            System.out.println("\n\n******* Create New crisis *******");
            CreateOperations pCreate = new CreateOperations();
            addressToCordinates pGPS = new addressToCordinates();
            Coordinates pCordinates=pGPS.FindAddressCoordinates(this.location);
            PointDTO pPoint=new PointDTO();
            pPoint.setAltitude(BigDecimal.ZERO);
            pPoint.setLatitude(new BigDecimal(Float.toString(pCordinates.getLatitude())));
            pPoint.setLongitude(new BigDecimal(Float.toString(pCordinates.getLongitude())));
            
            SphereDTO pSphere=pCreate.CreateSphereNoRadius("1",this.titlec+" - Location" , pPoint);
            CrisisContextDTO pCrisisContext = new CrisisContextDTO();
            pCrisisContext.setCrisisLocation(pSphere);
            pCrisisContext.setTitle(titlec);
            pCrisisContext.setAdditionalInfo(addinfo);
            pCrisisContext.setStartDate(new java.sql.Date(this.date1.getTime()));
            pCrisisContext.setEndDate(new java.sql.Date(this.date2.getTime()));
            
            if (CrisisContextAlertEnumDTO.Level1.toString()==alertlevel.toString())
                pCrisisContext.setCrisisContextAlert(CrisisContextAlertEnumDTO.Level1);
            if (CrisisContextAlertEnumDTO.Level2.toString()==alertlevel.toString())
                pCrisisContext.setCrisisContextAlert(CrisisContextAlertEnumDTO.Level2);
            if (CrisisContextAlertEnumDTO.Level3.toString()==alertlevel.toString())
                pCrisisContext.setCrisisContextAlert(CrisisContextAlertEnumDTO.Level3);
            if (CrisisContextAlertEnumDTO.Level4.toString()==alertlevel.toString())
                pCrisisContext.setCrisisContextAlert(CrisisContextAlertEnumDTO.Level4);
            if (CrisisContextAlertEnumDTO.Level5.toString()==alertlevel.toString())
                pCrisisContext.setCrisisContextAlert(CrisisContextAlertEnumDTO.Level5);

            CrisisContextDTO persistedcc=pCreate.createCrisisContext("1", pCrisisContext);
            
            CreateActionPart(CreateAction(persistedcc), persistedcc);
            return "EsponderDashBoard?faces-redirect=true";
        } catch (XPathExpressionException ex) {
            
            return null;
        } catch (ParserConfigurationException ex) {
            
            return null;
        } catch (SAXException ex) {
            
            return null;
        } catch (JsonGenerationException ex) {
            
            return null;
        } catch (JsonMappingException ex) {
            
            return null;
        } catch (IOException ex) {
            
            return null;
        }

    }
    
  

	private ActionDTO CreateAction(CrisisContextDTO persistedcc) throws JsonGenerationException, JsonMappingException, IOException
    {
		ActionTypeQueryManager pActionManager = new ActionTypeQueryManager();
		CreateOperations pCreate = new CreateOperations();
		
		pActionManager.LoadAllCrisisTypes();
		
        ActionDTO actionDTO= new ActionDTO();
        actionDTO.setCrisisContext(persistedcc);
        actionDTO.setTitle(persistedcc.getTitle());
        actionDTO.setActionOperation(ActionOperationEnumDTO.FIX);
		actionDTO.setType(pActionManager.getpActionTypes().get(0).getTitle());
		actionDTO.setSeverityLevel(SeverityLevelDTO.MEDIUM);
		
		return pCreate.createAction("1", actionDTO);
    }
    
    private ActionPartDTO CreateActionPart(ActionDTO action,CrisisContextDTO cc) throws JsonGenerationException, JsonMappingException, IOException
    {
        CreateOperations pCreate = new CreateOperations();
        ActionPartDTO actionpartDTO= new ActionPartDTO();
        actionpartDTO.setActionId(action.getId());
        actionpartDTO.setTitle(cc.getTitle());
        actionpartDTO.setActionId(action.getId());
        actionpartDTO.setActionOperation(ActionOperationEnumDTO.FIX);
        actionpartDTO.setSeverityLevel(SeverityLevelDTO.MEDIUM);
		
		//ActorDTO actorDTO = actorService.findByTitleRemote("FR #1.1");
//		actionPart1.setActor(actorDTO);
		
		
		
        return pCreate.createActionPart("1", actionpartDTO);
        
    }
    
    
    public String getStatus() {
  		return status;
  	}

  	public void setStatus(String status) {
  		this.status = status;
  	}

  	public String getAddinfo() {
  		return addinfo;
  	}

  	public void setAddinfo(String addinfo) {
  		this.addinfo = addinfo;
  	}

  	public String getId() {
  		return id;
  	}

  	public void setId(String id) {
  		this.id = id;
  	}

  	public String getTitlec() {
  		return titlec;
  	}

  	public void setTitlec(String titlec) {
  		this.titlec = titlec;
  	}

  	public String getLocation() {
  		return location;
  	}

  	public void setLocation(String location) {
  		this.location = location;
  	}

  	public Date getDate1() {
  		return date1;
  	}

  	public void setDate1(Date date1) {
  		this.date1 = date1;
  	}

  	public Date getDate2() {
  		return date2;
  	}

  	public void setDate2(Date date2) {
  		this.date2 = date2;
  	}

  	public Date getDate3() {
  		return date3;
  	}

  	public void setDate3(Date date3) {
  		this.date3 = date3;
  	}

  	public List<String> getNaturalDisaster() {
  		return naturalDisaster;
  	}

  	public void setNaturalDisaster(List<String> naturalDisaster) {
  		this.naturalDisaster = naturalDisaster;
  	}

  	public List<String> getOtherDisaster() {
  		return otherDisaster;
  	}

  	public void setOtherDisaster(List<String> otherDisaster) {
  		this.otherDisaster = otherDisaster;
  	}

  	public String getTitle() {
  		return title;
  	}

  	public void setTitle(String title) {
  		this.title = title;
  	}

  	public double getLat() {
  		return lat;
  	}

  	public void setLat(double lat) {
  		this.lat = lat;
  	}

  	public double getLng() {
  		return lng;
  	}

  	public void setLng(double lng) {
  		this.lng = lng;
  	}

  	public CrisisContextAlert getAlertlevel() {
  		return alertlevel;
  	}

  	public void setAlertlevel(CrisisContextAlert alertlevel) {
  		this.alertlevel = alertlevel;
  	}
}
