/*
 * 
 */
package eu.esponder.event.model.crisis.action;

import java.util.Set;

import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.dto.model.crisis.action.ActionObjectiveDTO;
import eu.esponder.dto.model.crisis.action.ActionPartDTO;
import eu.esponder.dto.model.crisis.action.ActionPartObjectiveDTO;
import eu.esponder.event.model.CreateEvent;



// TODO: Auto-generated Javadoc
/**
 * The Class CreateActionEvent.
 */
public class NewActionEvent extends ActionEvent<ActionDTO> implements CreateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2006035559724305579L;
	
	
	private ResultListDTO actionObjectives;
	
	private ResultListDTO actionParts;
	
	private ResultListDTO actionPartObjectives;
	
	String imei;
	

	public String getImei() {
		return imei;
	}


	public void setImei(String imei) {
		this.imei = imei;
	}


	public ResultListDTO getActionObjectives() {
		return actionObjectives;
	}


	public void setActionObjectives(ResultListDTO actionObjectives) {
		this.actionObjectives = actionObjectives;
	}


	public ResultListDTO getActionParts() {
		return actionParts;
	}


	public void setActionParts(ResultListDTO actionParts) {
		this.actionParts = actionParts;
	}


	public ResultListDTO getActionPartObjectives() {
		return actionPartObjectives;
	}


	public void setActionPartObjectives(ResultListDTO actionPartObjectives) {
		this.actionPartObjectives = actionPartObjectives;
	}
	
	/**
	 * Instantiates a new creates the action event.
	 */
	public NewActionEvent(){
		setJournalMessageInfo("Create Action Event");
	}
	
}
