/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.ws.client.crisisparams;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import eu.esponder.dto.model.ResultListDTO;
import eu.esponder.dto.model.crisis.CrisisContextParameterDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.ws.client.urlmanager.UrlManager;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MultivaluedMap;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author tdim
 * @GET
 * @Path("/crisisContextParameter/findAllByCrisis")
 * @Produces({MediaType.APPLICATION_JSON}) public ResultListDTO
 * findAllCrisisContextsParametersByCrisis(
 * @QueryParam("crisisContextID")
 * @NotNull(message="crisisContextID may not be null") Long crisisContextID,
 * @QueryParam("userID")
 * @NotNull(message="userID may not be null") Long userID,
 * @QueryParam("sessionID")
 * @NotNull(message="sessionID may not be null") String sessionID) {
 *
 * @POST
 * @Path("/crisisContextParameter/create")
 * @Consumes({MediaType.APPLICATION_JSON})
 * @Produces({MediaType.APPLICATION_JSON}) public CrisisContextParameterDTO
 * createCrisisContextParameters(
 * @QueryParam("paramName")
 * @NotNull(message="pkiKey may not be null") String paramName,
 * @QueryParam("paramValue")
 * @NotNull(message="pkiKey may not be null") String paramValue,
 * @QueryParam("crisisContextID")
 * @NotNull(message="pkiKey may not be null") Long crisisContextID,
 * @QueryParam("userID")
 * @NotNull(message="userID may not be null") Long userID,
 * @QueryParam("sessionID")
 * @NotNull(message="sessionID may not be null") String sessionID) {
 *
 * @PUT
 * @Path("/crisisContextParameter/update")
 * @Consumes({MediaType.APPLICATION_JSON})
 * @Produces({MediaType.APPLICATION_JSON}) public CrisisContextParameterDTO
 * updateCrisisContextParameter(
 * @QueryParam("paramID")
 * @NotNull(message="pkiKey may not be null") Long paramID,
 * @QueryParam("paramValue")
 * @NotNull(message="pkiKey may not be null") String paramValue,
 * @QueryParam("userID")
 * @NotNull(message="userID may not be null") Long userID,
 * @QueryParam("sessionID")
 * @NotNull(message="sessionID may not be null") String sessionID) {
 *
 * @DELETE
 * @Path("/crisisContextParameter/delete") public Long
 * deleteCrisisContextParameter(
 * @QueryParam("crisisContextParameterID")
 * @NotNull(message="crisisContextParameterID may not be null") Long
 * crisisContextParameterID,
 * @QueryParam("userID")
 * @NotNull(message="userID may not be null") Long userID,
 * @QueryParam("sessionID")
 * @NotNull(message="sessionID may not be null") String sessionID) {
 *
 * crisisContextParameterID =
 * this.getCrisisRemoteService().deleteCrisisContextParameterRemote(crisisContextParameterID,
 * userID); return crisisContextParameterID; } CrisisContextParameterDTO
 */
public class crisisparamswsmanager {

    ObjectMapper mapper = new ObjectMapper();
    UrlManager URL = new UrlManager();

    public String deletepcrisis(String userID,String sessionid, String ccpid)
            throws JsonParseException, JsonMappingException, IOException {

        // Variables

        Client client = Client.create();
        WebResource webResource = client.resource(URL.getCparamsdelete());

        // Initialize Parameters
        MultivaluedMap queryParams = new MultivaluedMapImpl();
        
        queryParams.add("crisisContextParameterID", ccpid);
        queryParams.add("userID", "6");
        queryParams.add("sessionID", "31391e2d-d369-441f-9ada-e133374128ee");

        // Call Webservice
        String szReturn = webResource.queryParams(queryParams)
                .delete(String.class);



        return szReturn;
    }

    public ResultListDTO FindAllForCrisis(String crisisContextID, String userID, String szsessionid) {
        ResultListDTO pResults = null;
        Client client = Client.create();
        WebResource webResource = client
                .resource(URL.getCparamsfind());

        // Initialize Parameters
        MultivaluedMap queryParams = new MultivaluedMapImpl();
        queryParams.add("crisisContextID", crisisContextID);
        queryParams.add("userID", "6");
        queryParams.add("sessionID", "31391e2d-d369-441f-9ada-e133374128ee");
        String szReturn = webResource.queryParams(queryParams)
                .get(String.class);
        try {
            // Convert to DTO
            pResults = mapper.readValue(szReturn, ResultListDTO.class);
            return pResults;
        } catch (IOException ex) {
            Logger.getLogger(crisisparamswsmanager.class.getName()).log(Level.SEVERE, null, ex);
            return pResults;
        }
    }

    public CrisisContextParameterDTO CreateParam(String crisisContextID, String szname, String szvalue, String userID, String szsessionid) throws Exception {

        CrisisContextParameterDTO parameter1 = new CrisisContextParameterDTO();
        parameter1.setParamName("Fire");
        parameter1.setParamValue("No");
        String parameterJSON = mapper.writeValueAsString(parameter1);

        MultivaluedMap queryParams2 = new MultivaluedMapImpl();
        queryParams2.add("paramName", szname);
        queryParams2.add("paramValue", szvalue);
        queryParams2.add("crisisContextID", crisisContextID);
        queryParams2.add("userID", "6");
        queryParams2.add("sessionID", "31391e2d-d369-441f-9ada-e133374128ee");

        String szReturn = null;
        Client client2 = Client.create();
        WebResource webResource2 = client2
                .resource(URL.getCparamscreate());


        // Call webservice
        szReturn = webResource2.queryParams(queryParams2)
                .type("application/json").post(String.class, parameterJSON);
        System.out.println(szReturn);
        return null;
    }
}
