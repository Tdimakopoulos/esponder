

package eu.esponder.datasync.server.map;

import org.jumpmind.util.Context;

public interface IValueFilter {

    public String filter (String tableName, String columnName, String originalValue, Context context);
    
}