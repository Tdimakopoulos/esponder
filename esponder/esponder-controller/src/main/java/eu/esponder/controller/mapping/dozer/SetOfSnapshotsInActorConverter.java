/*
 * 
 */
package eu.esponder.controller.mapping.dozer;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.naming.NamingException;

import org.dozer.CustomConverter;

import eu.esponder.controller.crisis.action.ActionService;
import eu.esponder.controller.crisis.resource.ActorService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.exception.EsponderCheckedException;
import eu.esponder.model.snapshot.resource.ActorSnapshot;
import eu.esponder.util.ejb.ServiceLocator;
// TODO: Auto-generated Javadoc
//import eu.esponder.model.crisis.action.Action;

// 
/**
 * The Class ActionPartObjectiveConsumablesConverter.
 */
public class SetOfSnapshotsInActorConverter implements CustomConverter {

	/**
	 * Gets the mapping service.
	 *
	 * @return the mapping service
	 */
	protected ESponderMappingService getMappingService() {
		try {
			return ServiceLocator.getResource("esponder/ESponderMappingBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/**
	 * Gets the logistics service.
	 *
	 * @return the logistics service
	 */
	protected ActionService getActionService() {
		try {
			return ServiceLocator.getResource("esponder/ActionBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	protected ActorService getActorService() {
		try {
			return ServiceLocator.getResource("esponder/ActorBean/local");
		} catch (NamingException e) {
			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object convert( Object destination, Object source,
			Class<?> destinationClass, Class<?> sourceClass) {



		if (source instanceof Set && source != null) {

			Set<?> sourceSet = (Set<?>) source;
			Iterator<?> it = sourceSet.iterator();
			if(it.hasNext()){
				Object next = it.next();
				if(next.getClass() == ActorSnapshot.class) {
					Long actorID = ((ActorSnapshot)next).getActor().getId();
					Set<ActorSnapshotDTO> destActorSnapshotDTOSet = new HashSet<ActorSnapshotDTO>();
					ActorSnapshotDTO latestSnapshotDTO = getActorService().findLatestActorSnapshotRemote(actorID);
					if(latestSnapshotDTO !=null)
						destActorSnapshotDTOSet.add(latestSnapshotDTO);
					destination = destActorSnapshotDTOSet;
				}
				else if(next.getClass() == ActorSnapshotDTO.class) {
					Long actorID = ((ActorSnapshotDTO)next).getActor();
					Set<ActorSnapshot> destActorSnapshotSet = new HashSet<ActorSnapshot>();
					List<ActorSnapshot> snapshots = getActorService().findAllActorSnapshots(actorID);
					if(snapshots !=null)
						destActorSnapshotSet.addAll(snapshots);
					destination = destActorSnapshotSet;
				}
				else
					destination = null;
			}
		}
		return destination;
	}

}
