package eu.esponder.df.ruleengine.controller.bean;

import eu.esponder.df.eventhandler.bean.MobileServicesEventHandlerBean;

public class DatafusionEventController {

	private String[] szMobileCommunication=
	{
	"ESponderDFQueryResponseEvent",
	"ESponderDFQueryRequestEvent",
	"ESponderDFCreateActionCRUDEvent",
	"ESponderDFCreateActionPartCRUDEvent",
	"ESponderDFUpdateActionCRUDEvent",
	"ESponderDFUpdateActionPartCRUDEvent",
	"ESponderDFConfirmActionEvent",
	"ESponderDFConfirmActionPartEvent",
	"ESponderDFCreateActionEvent",
	"ESponderDFCreateActionPartEvent"};
	
	public String[] getSzMobileCommunication() {
		return szMobileCommunication;
	}

	public void setSzMobileCommunication(String[] szMobileCommunication) {
		this.szMobileCommunication = szMobileCommunication;
	}

	public boolean IsMobileEvent(String szName)
	{
		for(int i=0;i<szMobileCommunication.length;i++)
		{
			if (szName.equalsIgnoreCase(szMobileCommunication[i]))
			{
				return true;
			}
		}
		return false;
	}
	
	public String GetNameClassMobileEvent(String szName)
	{
		for(int i=0;i<szMobileCommunication.length;i++)
		{
			if (szName.equalsIgnoreCase(szMobileCommunication[i]))
			{
				return szMobileCommunication[i];
			}
		}
		return "-1";
	}
}
