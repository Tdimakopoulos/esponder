/*
 * 
 */
package eu.esponder.dto.model.security;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;



// TODO: Auto-generated Javadoc
/**
 * Entity class KeyDB.
 *
 * Passing the security/session details into the webservices
 */

@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"userID", "sessionID","role"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class KeyStorageDTO implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6365218952367066386L;

	/** The user id. */
	private Long userID;
	
	/** The session id. */
	private String sessionID;

	/** The role. */
	private String role;

	/**
	 * Instantiates a new key storage dto.
	 */
	public KeyStorageDTO()
	{
		
	}
	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public Long getUserID() {
		return userID;
	}

	/**
	 * Sets the user id.
	 *
	 * @param userID the new user id
	 */
	public void setUserID(Long userID) {
		this.userID = userID;
	}

	/**
	 * Gets the session id.
	 *
	 * @return the session id
	 */
	public String getSessionID() {
		return sessionID;
	}

	/**
	 * Sets the session id.
	 *
	 * @param sessionID the new session id
	 */
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	/**
	 * Gets the role.
	 *
	 * @return the role
	 */
	public String getRole() {
		return role;
	}

	/**
	 * Sets the role.
	 *
	 * @param role the new role
	 */
	public void setRole(String role) {
		this.role = role;
	}

	
}
