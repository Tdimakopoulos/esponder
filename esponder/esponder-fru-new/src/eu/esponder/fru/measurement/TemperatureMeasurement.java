package eu.esponder.fru.measurement;

import java.math.BigDecimal;


public class TemperatureMeasurement extends DoubleValueSensorMeasurement {

	public TemperatureMeasurement(BigDecimal value) {
		super(value);
	}
	
	public TemperatureMeasurement(int value) {
		super(value);
	}

}
