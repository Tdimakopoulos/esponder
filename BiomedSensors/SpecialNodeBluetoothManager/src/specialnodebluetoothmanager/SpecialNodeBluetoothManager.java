/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package specialnodebluetoothmanager;

import LPSLibExus.LPSLibEXUS;
import eu.exus.esponder.biomeda.client.BiomedAHandler;
import java.io.IOException;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.exus.server.udp.udpserver;
import uilib.exus.UILibEXUS;

/**
 *
 * @author tdim
 */
public class SpecialNodeBluetoothManager {

    public boolean WaitForStartUp() {
        udpserver ps = new udpserver();
        try {
            ps.WaitForMessage("ON", 1099);
            return true;
        } catch (SocketException ex) {
            System.out.println("-- Error : " + ex.getMessage());
            return false;
        } catch (IOException ex) {
            System.out.println("-- Error : " + ex.getMessage());
            return false;
        }
    }
    
    public boolean WaitForStartUpDummy() {
        
            return true;
        
    }

    public boolean ManageBiomedAConnection(BiomedAHandler pBiomedA) throws InterruptedException {
        boolean brun = true;
        int irun = 0;
        while (brun) {
            try {
                irun = irun + 1;
                pBiomedA.OpenConnection();
                brun = false;
                return true;
            } catch (Exception e) {
                Thread.sleep(1000 * 2);
            }
            if (irun == 20) {
                return false;
            }
        }
        return false;
    }

    public boolean StartBiomedASensor(BiomedAHandler pBiomedA) throws InterruptedException {

        try {
            pBiomedA.StopStreaming();
        } catch (Exception eb) {
            System.out.println("Warning on Stop Streamming on BiomedA !");
        }

        //wait for command execution
        Thread.sleep(500);

        try {
            pBiomedA.StartStreaming();
        } catch (IOException ex) {
            System.out.println("Error on Start Streamming on BiomedA !");
            return false;
        }

        pBiomedA.StartReadingThread();
        return true;
    }

    public boolean ManageUIConnection(UILibEXUS pUI) throws InterruptedException {
        boolean brun = true;
        int irun = 0;
        while (brun) {
            try {
                irun = irun + 1;
                pUI.OpenConnection();
                brun = false;
                return true;
            } catch (Exception e) {
                Thread.sleep(1000 * 2);
            }
            if (irun == 20) {
                return false;
            }
        }
        return false;
    }

    public boolean ManageLPSConnection(LPSLibEXUS pLPS) throws InterruptedException {
        boolean brun = true;
        int irun = 0;
        while (brun) {
            try {
                irun = irun + 1;
                pLPS.OpenConnection();
                brun = false;
                return true;
            } catch (Exception e) {
                Thread.sleep(1000 * 2);
            }
            if (irun == 20) {
                return false;
            }
        }
        return false;
    }

    public static void main(String[] args) throws InterruptedException {

        SpecialNodeBluetoothManager pnbm = new SpecialNodeBluetoothManager();
        BiomedAHandler pBiomedA = new BiomedAHandler();
        UILibEXUS pUI = new UILibEXUS();
        LPSLibEXUS pLPS = new LPSLibEXUS();
        boolean brunmain = true;
        int iloops = 0;
        if (pnbm.WaitForStartUp()) {
            if (pnbm.ManageBiomedAConnection(pBiomedA)) {
                if (pnbm.StartBiomedASensor(pBiomedA)) {
                    if (pnbm.ManageUIConnection(pUI)) {
                        pUI.StartReadUIThread();
                        if (pnbm.ManageLPSConnection(pLPS)) {
                            pLPS.StartReadLPSThread();
                            while (brunmain) {
                                Thread.sleep(1000 * 30);
                                if (iloops == 12) {
                                    try {
                                        pBiomedA.StopStreaming();
                                    } catch (IOException ex) {
                                        System.out.println("Error on BiomedA Stop Streaming Refresh ! ");
                                        brunmain = false;
                                    }
                                    Thread.sleep(1000);
                                    try {
                                        pBiomedA.StartStreaming();
                                    } catch (IOException ex) {
                                        System.out.println("Error on BiomedA Start Streaming Refresh ! ");
                                        brunmain = false;
                                    }
                                    try {
                                        pLPS.CloseConnection();
                                    } catch (IOException ex) {
                                        System.out.println("Error on LPS Close Connection Refresh ! ");
                                        brunmain = false;
                                    }
                                    if (pnbm.ManageLPSConnection(pLPS)) {
                                    } else {
                                        System.out.println("Error on LPS Open Connection Refresh ! ");
                                        brunmain = false;
                                    }
                                }
                                iloops = iloops + 1;
                                //check for exit
                            }
                        } else {
                            System.out.println("Error on LPS Connection ! ");
                        }
                    } else {
                        System.out.println("Error on UI Connection ! ");
                    }
                } else {
                    System.out.println("Error on BiomedA Start Streaming ! ");
                }
            } else {
                System.out.println("Error on BiomedA Connection ! ");
            }
        } else {
            System.out.println("Error on Startup Network Signal ! ");
        }
    }
}
