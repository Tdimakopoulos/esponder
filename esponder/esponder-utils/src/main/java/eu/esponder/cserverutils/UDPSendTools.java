package eu.esponder.cserverutils;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class UDPSendTools {

	public void SendValue(String host,int port,String msg) throws UnknownHostException, SocketException, IOException {
	       byte[] message;
	       message = msg.getBytes();
	      
	      // Get the internet address of the specified host
	      InetAddress address = InetAddress.getByName(host);

	      // Initialize a datagram packet with data and address
	      DatagramPacket packet = new DatagramPacket(message, message.length, 
	                                                 address, port);

	      // Create a datagram socket, send the packet through it, close it.
	      DatagramSocket dsocket = new DatagramSocket();
	      dsocket.send(packet);
	      dsocket.close();
	    }
	
}
