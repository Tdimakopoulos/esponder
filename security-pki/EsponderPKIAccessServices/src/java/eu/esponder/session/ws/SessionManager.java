/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.session.ws;

import eu.esponder.role.ws.EsRoleService;
import eu.esponder.session.bean.sessiontableFacade;
import eu.esponder.session.db.sessiontable;
import eu.esponder.webservices.EsKeyManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import javax.ejb.EJB;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.WebServiceRef;
import org.security.utils.Util;

/**
 *
 * @author tdim
 */
@WebService(serviceName = "SessionManager")
public class SessionManager {
    

    @EJB
    private sessiontableFacade ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "SessionLogin")
    public sessiondetails SessionLogin(String pkikey, int itype) {
        String sessionID = UUID.randomUUID().toString();
        Long userID=new Long(0);
        java.util.List<eu.esponder.webservices.KeyDB> pFind=queryAllUsers();
        for(int i=0;i<pFind.size();i++)
        {
            Util pu = new Util();
            String s1 = pu.byteArray2Hex(pFind.get(i).getPublicKey());
            if(s1.equalsIgnoreCase(pkikey))
            {
               userID=pFind.get(i).getPuserID();
            }
        }
        
        Date paccess = new Date();


        sessiontable entity = new sessiontable();
        entity.setItype(itype);
        entity.setPkikey("No Key Needed");
        entity.setLastaccess(paccess.getTime());
        entity.setSessionID(sessionID);
        ejbRef.create(entity);

        
        sessiondetails pRet = new sessiondetails();
        pRet.setRole(getUserRole(pkikey));
        pRet.setAccfunc(QueryRoles(getUserRole(pkikey)));
        pRet.setSessionID(sessionID);
        pRet.setUserID(userID);
        return pRet;
    }

    @WebMethod(operationName = "SessionUpdate")
    public boolean SessionUpdate(String sessionID) {
        List<sessiontable> pFind = ejbRef.findAll();
        Long id = null;
        Date paccess = new Date();
        for (int i = 0; i < pFind.size(); i++) {
            if (pFind.get(i).getSessionID().equalsIgnoreCase(sessionID)) {
                id = pFind.get(i).getId();
            }
        }
        if (id == null) {
            return false;
        } else {
            sessiontable pfind = ejbRef.find(id);
            pfind.setLastaccess(paccess.getTime());
            ejbRef.edit(pfind);
            return true;
        }
    }

    @WebMethod(operationName = "SessionInfo")
    public sessiontable SessionInfo(String sessionID) {
        List<sessiontable> pFind = ejbRef.findAll();
        Long id = null;
        Date paccess = new Date();
        for (int i = 0; i < pFind.size(); i++) {
            if (pFind.get(i).getSessionID().equalsIgnoreCase(sessionID)) {
                id = pFind.get(i).getId();
            }
        }
        if (id == null) {
            return null;
        } else {
            sessiontable pfind = ejbRef.find(id);
            return pfind;
        }
    }

    @WebMethod(operationName = "SessionLogout")
    public boolean SessionLogout(String sessionID) {

        List<sessiontable> pFind = ejbRef.findAll();
        Long id = null;
        for (int i = 0; i < pFind.size(); i++) {
            if (pFind.get(i).getSessionID().equalsIgnoreCase(sessionID)) {
                id = pFind.get(i).getId();
            }
        }
        if (id == null) {
            return false;
        } else {
            ejbRef.remove(ejbRef.find(id));
            return true;
        }

    }

    private List<String> QueryRoles(String Role) {
        List<String> pRet = new ArrayList();
        List<eu.esponder.role.ws.RoleDB> pFind = esFindAll();
        for (int i = 0; i < pFind.size(); i++) {
            if (pFind.get(i).getRole().equalsIgnoreCase(Role)) {
                pRet.add(pFind.get(i).getAccessfunction());
            }
        }
        return pRet;
    }

    private String getUserRole(java.lang.String key) {
        EsKeyManager service = new EsKeyManager();
        eu.esponder.webservices.KeyManager port = service.getKeyManagerPort();
        return port.getUserRole(key);
    }

    private java.util.List<eu.esponder.role.ws.RoleDB> esFindAll() {
        EsRoleService service = new EsRoleService();
        eu.esponder.role.ws.RoleService port = service.getRoleServicePort();
        return port.esFindAll();
    }

    private java.util.List<eu.esponder.webservices.KeyDB> queryAllUsers() {
        EsKeyManager service=new EsKeyManager();
        eu.esponder.webservices.KeyManager port = service.getKeyManagerPort();
        return port.queryAllUsers();
    }
}
