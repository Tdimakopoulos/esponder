/*
 * 
 */
package eu.esponder.dto.model.crisis.resource;

import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;


// TODO: Auto-generated Javadoc
/**
 * The Class ActorFRDTO.
 */
@JsonSerialize(include=Inclusion.NON_NULL)
@JsonPropertyOrder({"resourceId", "type", "title", "status", "personnel", "voIPURL", "equipmentSet", "snapshots", "frchief", "team"})
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ActorFRDTO extends FirstResponderActorDTO {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3907149300826934492L;

	/** The frchief. */
	private Long FRChief;

	/**
	 * Gets the frchief.
	 *
	 * @return the frchief
	 */
	public Long getFRChief() {
		return FRChief;
	}

	/**
	 * Sets the frchief.
	 *
	 * @param FRChief the new fR chief
	 */
	public void setFRChief(Long FRChief) {
		this.FRChief = FRChief;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.dto.model.crisis.resource.FirstResponderActorDTO#toString()
	 */
	@Override
	public String toString() {
		return "ActorFRDTO [type=" + type + ", status=" + status + ", id=" + id
				+ ", title=" + title + "]";
	}
}
