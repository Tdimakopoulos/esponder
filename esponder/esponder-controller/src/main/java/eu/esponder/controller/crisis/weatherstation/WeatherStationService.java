/*
 * 
 */
package eu.esponder.controller.crisis.weatherstation;

import javax.ejb.Local;

import eu.esponder.model.crisis.resource.WeatherStation;

@Local
public interface WeatherStationService {

	public WeatherStation findWeatherStationById(Long weatherStationID);

	public WeatherStation findWeatherStationByTitle(String wsTitle, Long userId);

	public WeatherStation createWeatherStation(WeatherStation weatherStation,
			Long userID);

	public void deleteWeatherStation(Long weatherStationID, Long userID);

	public WeatherStation updateWeatherStation(WeatherStation weatherStation,
			Long userID);

}
