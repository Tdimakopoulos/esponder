package org.exus.df.statistics.model;

public class DaySeries {

	Long dStart;
	Long dEnd;
	public Long getdStart() {
		return dStart;
	}
	public void setdStart(Long dStart) {
		this.dStart = dStart;
	}
	public Long getdEnd() {
		return dEnd;
	}
	public void setdEnd(Long dEnd) {
		this.dEnd = dEnd;
	}

}
