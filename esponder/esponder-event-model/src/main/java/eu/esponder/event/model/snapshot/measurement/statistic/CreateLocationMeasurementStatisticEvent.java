/*
 * 
 */
package eu.esponder.event.model.snapshot.measurement.statistic;

import java.util.List;

import eu.esponder.dto.model.snapshot.sensor.measurement.LocationSensorMeasurementDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.SensorMeasurementStatisticEnvelopeDTO;
import eu.esponder.event.model.CreateEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class CreateLocationMeasurementStatisticEvent.
 */
public class CreateLocationMeasurementStatisticEvent extends SensorMeasurementStatisticEvent<SensorMeasurementStatisticEnvelopeDTO> implements CreateEvent { 
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2994208980374328457L;

	/**
	 * Instantiates a new creates the location measurement statistic event.
	 */
	public CreateLocationMeasurementStatisticEvent(){
		setJournalMessageInfo("Received Location Measurement Statistics Envelope");
	}
	
	/**
	 * Gets the journal message info detailed.
	 *
	 * @return the journal message info detailed
	 */
	public String getJournalMessageInfoDetailed() {
		List<SensorMeasurementStatisticDTO> locationMeasurements = this.getEventAttachment().getMeasurementStatistics();
		
		String journalMessageInfo = "Received Location Measurement Statistics Envelope including: \n";
		for (SensorMeasurementStatisticDTO sensorMeasurement : locationMeasurements) {
			LocationSensorMeasurementDTO locMeasurement = (LocationSensorMeasurementDTO) sensorMeasurement.getStatistic();
			journalMessageInfo += locMeasurement.toString();
		}
		return journalMessageInfo;
	}
}
