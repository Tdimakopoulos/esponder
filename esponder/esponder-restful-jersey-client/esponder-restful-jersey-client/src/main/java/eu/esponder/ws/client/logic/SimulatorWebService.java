/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.ws.client.logic;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import eu.esponder.dto.model.crisis.resource.ReusableResourceDTO;
import eu.esponder.ws.client.urlmanager.UrlManager;
import java.io.IOException;
import javax.ws.rs.core.MultivaluedMap;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author tdim
 */
public class SimulatorWebService {
     ObjectMapper mapper = new ObjectMapper();
    UrlManager URL = new UrlManager();

    public void AssociateReusable(String ccID, String regreu,
            String userID) throws JsonGenerationException,
            JsonMappingException, IOException {

        // Variables
        String szReturn=null;
        Client client2 = Client.create();
        WebResource webResource2 = client2
                .resource(URL.getSzrunsimulator());

        // Initialize Params
        MultivaluedMap queryParams2 = new MultivaluedMapImpl();
        queryParams2.add("userID", userID);
        
        // Call webservice
        szReturn = webResource2.queryParams(queryParams2).type("application/json").get(String.class);
        
    }
}
