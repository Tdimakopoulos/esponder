/*
 * 
 */
package eu.esponder.event.model.crisis;

import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.event.model.DeleteEvent;



// TODO: Auto-generated Javadoc
/**
 * The Class DeleteCrisisContextEvent.
 */
public class DeleteCrisisContextEvent extends CrisisContextEvent<CrisisContextDTO> implements DeleteEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1139683152265684159L;
		
	
	/**
	 * Instantiates a new delete crisis context event.
	 */
	public DeleteCrisisContextEvent(){
		setJournalMessageInfo("Delete Crisis Context Event");
	}
}
