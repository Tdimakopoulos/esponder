package org.exus.df.statistics;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.exus.df.statistics.model.DaySeries;
import org.exus.df.statistics.model.DayTimeSeries;

public class TimeSeriesManager {

	Date dstart;
	Date dend;
	List<DayTimeSeries> pTime = new ArrayList<DayTimeSeries>();
	List<DaySeries> pDates = new ArrayList<DaySeries>();

	List<Date> dates = new ArrayList<Date>();

	public void GenerateTimeSeries() throws ParseException {
		Date startDate = dstart;
		Date endDate = dend;
		long interval = 1000 * 60 * 60;// 24*1000 * 60 * 60; // 1 hour in millis
		long endTime = endDate.getTime(); // create your endtime here, possibly
		long curTime = startDate.getTime();

		dates.clear();
		pTime.clear();

		while (curTime <= endTime) {
			dates.add(new Date(curTime));
			curTime += interval;
		}

		for (int i = 0; i < dates.size(); i++) {
			if (i < dates.size() - 1) {
				DayTimeSeries pSeries = new DayTimeSeries();
				pSeries.setdTimeStart(dates.get(i).getTime());
				pSeries.setdTimeEnd(dates.get(i + 1).getTime());
				pTime.add(pSeries);
			}
		}

	}

	public void GenerateDaySeries() throws ParseException {
		Date startDate = dstart;
		Date endDate = dend;
		long interval = 24 * 1000 * 60 * 60; // 1 Day in millis
		long endTime = endDate.getTime(); // create your endtime here, possibly
		long curTime = startDate.getTime();

		dates.clear();
		pDates.clear();

		while (curTime <= endTime) {
			dates.add(new Date(curTime));
			curTime += interval;
		}

		for (int i = 0; i < dates.size(); i++) {
			if (i < dates.size() - 1) {
				DaySeries pSeries = new DaySeries();
				pSeries.setdStart(dates.get(i).getTime());
				pSeries.setdEnd(dates.get(i + 1).getTime());
				pDates.add(pSeries);
			}
		}

	}

	public Date getDstart() {
		return dstart;
	}

	public void setDstart(Date dstart) {
		this.dstart = dstart;
	}

	public Date getDend() {
		return dend;
	}

	public void setDend(Date dend) {
		this.dend = dend;
	}

	public List<DayTimeSeries> getpTime() {
		return pTime;
	}

	public void setpTime(List<DayTimeSeries> pTime) {
		this.pTime = pTime;
	}

	public List<DaySeries> getpDates() {
		return pDates;
	}

	public void setpDates(List<DaySeries> pDates) {
		this.pDates = pDates;
	}

	/**
	 * Testing the generation of time and day series from given two dates
	 * 
	 * @param args
	 * @throws ParseException
	 */
	public static void main(String[] args) throws ParseException {
		TimeSeriesManager ptsm = new TimeSeriesManager();
		Calendar calendar = new GregorianCalendar();
		calendar.add(Calendar.DATE, 4);
		Date pend = new Date();
		pend.setTime(calendar.getTimeInMillis());
		ptsm.setDend(pend);
		ptsm.setDstart(new Date());
		ptsm.GenerateTimeSeries();
		ptsm.GenerateDaySeries();
	}

}
