/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uilib.exus;

import exus.esponder.receive.processor.processor;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.microedition.io.StreamConnection;

/**
 *
 * @author tdim
 */
public class UIReadThread extends Thread {

    StreamConnection scglobal;
    boolean brun = true;

    public void SetBTConnection(StreamConnection sc) {
        scglobal = sc;
    }

    public void StopReadThread() {
        brun = false;
    }

    byte[] combine(byte[] a, byte[] b) {
        if (a == null) {
            return b;
        } else {
            byte[] result = new byte[a.length + b.length];
            System.arraycopy(a, 0, result, 0, a.length);
            System.arraycopy(b, 0, result, a.length, b.length);
            return result;
        }
    }

    @Override
    public void run() {
        processor pProcessor=new processor();
        String frame = null;
        String results[] = null;
        byte buffer[] = new byte[128];
        int length = 0;
        byte b = -1;
        boolean receiving = false;
        int tries = 0;
        try {
            DataInputStream datain = null;

            datain = scglobal.openDataInputStream();


            byte b1[] = new byte[1048];
            byte b3[] = null;
            byte b2[] = null;
            int il;
            String s1="";
            while (brun) {
                try {

                    il = datain.read(b1);
                    b3 = new byte[il];

                    System.arraycopy(b1, 0, b3, 0, il);
                    frame = new String(b3);
                    s1=s1+frame;
                    pProcessor.ProcessUICommand(frame);
//                    if(frame.contains(">"))
//                    {
//                    pProcessor.ProcessLPSCommand(s1);
//                    s1="";
//                    }                    
//System.out.println(" String -->"+frame);
                } catch (IOException ex) {
                   
                }
               
            }
            System.out.println("Closing down UI");
            datain.close();

        } catch (IOException ex) {
            Logger.getLogger(UIReadThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
