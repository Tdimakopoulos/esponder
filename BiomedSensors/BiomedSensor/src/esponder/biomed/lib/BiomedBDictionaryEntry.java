/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package esponder.biomed.lib;


/**
 *
 * @author afa
 */
 class BiomedBDictionaryEntry extends Payload {

    public int number;
    public BiomedBDictionaryBlock blocks[];
    
    public BiomedBDictionaryEntry(int size, byte buffer[]){
        super(size, buffer);
    }

    public BiomedBDictionaryEntry() {
    }

    public void parse(int startIdx) {

        this.blocks = new BiomedBDictionaryBlock[size / 5];//5 byte per block
        for (int i = 0; i < this.blocks.length; i++) {
            this.blocks[i] = new BiomedBDictionaryBlock();
            this.blocks[i].channel = (buffer[startIdx++]&0xFF);
            int flags = buffer[startIdx++];
            this.blocks[i].sampleSize = ((flags >> 5)&0xFF);
            this.blocks[i].timeStampIncrementation = BiomedBDictionaryBlock.FROM_PREVIOUS_SAMPLE;
            if (((flags >> 4) & 0x01) == 0x01) {
                this.blocks[i].timeStampIncrementation = BiomedBDictionaryBlock.FROM_PACKET_HEADER;
            }
            this.blocks[i].intervalMultiplier = (int) Math.pow(2, (flags >> 2) & 0x03);
            this.blocks[i].timeStampMultiplier = (int) Math.pow(2, flags & 0x03);
            int sampleInfo = buffer[startIdx++];
            this.blocks[i].signed = true;
            if ((sampleInfo >> 7) == 0x01) {
                this.blocks[i].signed = false;
            }
            this.blocks[i].sampleCount = (sampleInfo & 0x7F);
            this.blocks[i].interval = buffer[startIdx++];
            this.blocks[i].timeStampOffset = buffer[startIdx++];
        }
    }
}