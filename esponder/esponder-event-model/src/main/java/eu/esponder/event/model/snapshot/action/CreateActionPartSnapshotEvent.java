/*
 * 
 */
package eu.esponder.event.model.snapshot.action;

import eu.esponder.dto.model.snapshot.action.ActionPartSnapshotDTO;
import eu.esponder.event.model.CreateEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class CreateActionPartSnapshotEvent.
 */
public class CreateActionPartSnapshotEvent extends ActionPartSnapshotEvent<ActionPartSnapshotDTO> implements CreateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8444980801237832581L;

	/**
	 * Instantiates a new creates the action part snapshot event.
	 */
	public CreateActionPartSnapshotEvent(){
		setJournalMessageInfo("CreateActionPartSnapshotEvent");
	}
}
