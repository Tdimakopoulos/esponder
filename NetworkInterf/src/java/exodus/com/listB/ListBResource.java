/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exodus.com.listB;

import exodus.com.dbcontrol.DBControl;
import exodus.com.objects.RestListB;
import java.util.List;
import javax.ejb.Stateless;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;

/**
 * REST Web Service
 *
 * @author tom
 */
@Stateless
@Path("/listB")
public class ListBResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of ListBResource
     */
    public ListBResource() {
    }

    /**
     * Retrieves representation of an instance of exodus.com.listB.ListBResource
     *
     * @return an instance of exodus.com.objects.ListB
     */
    @GET
    @Path("/QueryAllListB")
    @Produces("application/xml")
    public List<RestListB> getQueryListBAll() {
        DBControl pdb=new DBControl();
        return pdb.SelectFromListB("");
    }

    /**
     * PUT method for updating or creating an instance of ListBResource
     *
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Path("/CreateListB")
    @Consumes("application/xml")
    public void putCreateListB(RestListB content) {
        DBControl pdb=new DBControl();
        pdb.createListB(content);
    }
}
