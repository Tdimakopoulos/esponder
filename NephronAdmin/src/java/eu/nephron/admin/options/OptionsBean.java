/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.admin.options;

import eu.nephron.opt.ws.NephronOptionWS_Service;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import javax.xml.ws.WebServiceRef;
import org.primefaces.model.DualListModel;

/**
 *
 * @author tdim
 */
@ManagedBean
@ViewScoped
public class OptionsBean {
    static {
        setManufacturers(new String[41]);
        getManufacturers()[0] = "BP-sys lower than normal range";
        getManufacturers()[1] = "BP-sys higher than normal range";
        getManufacturers()[2] = "ALARM: BP-sys high";
        getManufacturers()[3] = "BP-dia lower than normal range";
        getManufacturers()[4] = "BP-dia higher than normal range";
        getManufacturers()[5] = "ALARM: BP-dia high";
        getManufacturers()[6] = "Measured weight lower than threshold";
        getManufacturers()[7] = "Measured weight higher than threshold";
        getManufacturers()[8] = "Weight trend exceeds threshold";
        getManufacturers()[9] = "Detected blood pump deviation";
        getManufacturers()[10] = "Detected dialysate pump deviation";
        getManufacturers()[11] = "ALARM: pressure (high) excess, all pumps stopped";
        getManufacturers()[12] = "ALARM: pressure (low) excess, all pumps stopped";
        getManufacturers()[13] = "Temperature (high) excess at BTSo, all pumps stopped";
        getManufacturers()[14] = "Temperature (low) excess at BTSo, all pumps stopped";
        getManufacturers()[15] = "Temperature (high) excess at BTSi, all pumps stopped";
        getManufacturers()[16] = "Temperature (low) excess at BTSi, all pumps stopped";
        getManufacturers()[17] = "A sorbent disfunction was detected, patient was instructed to replace the cartridge";
        getManufacturers()[18] = "pH low";
        getManufacturers()[19] = "pH high";
        getManufacturers()[20] = "pH out of normal range";
        getManufacturers()[21] = "pH decrease out of normal range";
        getManufacturers()[22] = "pH increase out of normal range";
        getManufacturers()[23] = "Calcium out of normal range";
        getManufacturers()[24] = "Fast change in Calcium was detected in trend analysis";
        getManufacturers()[25] = "Patient was asked to replace sorbent cartridge";
        getManufacturers()[26] = "K+ out of normal range";
        getManufacturers()[27] = "Potassium lower than normal range";
        getManufacturers()[28] = "Patient was asked to replace sorbent cartridge";
        getManufacturers()[29] = "Potassium higher than normal range";
        getManufacturers()[30] = "Potassium trend deviates from threshold";
        getManufacturers()[31] = "A increasing fast change in K+ trend was detected  in trend analysis.";
        getManufacturers()[32] = "Patient was asked to replace sorbent cartridge due to low potassium removal";
        getManufacturers()[33] = "pH dialysate out of range";
        getManufacturers()[34] = "Urea removal low";
        getManufacturers()[35] = "Urea out of normal range (high)";
        getManufacturers()[36] = "Urea increase out of normal range (high)";
        getManufacturers()[37] = "Detected air bubble in bloodline, all pumps stopped";
        getManufacturers()[38] = "Detected leakage in bloodline, all pumps stopped";
        getManufacturers()[39] = "Detected air bubble in dialysate line, dialysate circuit stopped";
        getManufacturers()[40] = "Detected fluid leakage in dialysate circuit, dialysate circuit stopped ";
    }
    /**
     * @return the manufacturers
     */
    public static String[] getManufacturers() {
        return manufacturers;
    }

    /**
     * @param aManufacturers the manufacturers to set
     */
    public static void setManufacturers(String[] aManufacturers) {
        manufacturers = aManufacturers;
    }
    
    private static String[] manufacturers;
    private NephronOptionWS_Service service=new NephronOptionWS_Service();
    private List<String> pValuesDB=new ArrayList();
    private List<String> pValuesAll=new ArrayList();
    private DualListModel<String> optionsalerts;
    /**
     * Creates a new instance of OptionsBean
     */
    public OptionsBean() {
        LoadFromDB();
        pValuesAll=createListAlerts(manufacturers);
        RemoveValuesOnDB();
        optionsalerts = new DualListModel<String>(pValuesAll, pValuesDB);
    }

    public String savealerts() {
        
        java.util.List<eu.nephron.opt.ws.Optionsall> popt=nephronOptionWSfindAll();
        
        for(int i=0;i<popt.size();i++)
        {
            if(popt.get(i).getOpttype().equalsIgnoreCase("alerts"))
            {
                nephronOptionWSremove(popt.get(i));
            }
        }
        pValuesDB=optionsalerts.getTarget();
        System.out.println("Size : "+pValuesDB.size());
        for(int x=0;x<pValuesDB.size();x++)
        {
            eu.nephron.opt.ws.Optionsall pitem=new eu.nephron.opt.ws.Optionsall();
            pitem.setOpttype("alerts");
            pitem.setOptvalue(pValuesDB.get(x));
            nephronOptionWScreate(pitem);
        }
        
        return "DashBoard?faces-redirect=true";
    }
    
    private void RemoveValuesOnDB()
    {
        for(int i=0;i<pValuesDB.size();i++)
        {
            for(int ia=0;ia<pValuesAll.size();ia++)
            {
                if(pValuesAll.get(ia).equalsIgnoreCase(pValuesDB.get(i)))
                {
                    pValuesAll.remove(ia);
                }
            }
        }
    }
    private List<String> createListAlerts(String[] data) {
        List<String> pList=new ArrayList();
        
        for (int i = 0; i < data.length; i++) {
          pList.add(data[i]);
        }

        return pList;
    }

    private void LoadFromDB()
    {
        java.util.List<eu.nephron.opt.ws.Optionsall> popt=nephronOptionWSfindAll();
        pValuesDB.clear();
        for(int i=0;i<popt.size();i++)
        {
            if(popt.get(i).getOpttype().equalsIgnoreCase("alerts"))
            {
                pValuesDB.add(popt.get(i).getOptvalue());
            }
        }
    }
    
    private java.util.List<eu.nephron.opt.ws.Optionsall> nephronOptionWSfindAll() {
        eu.nephron.opt.ws.NephronOptionWS port = service.getNephronOptionWSPort();
        return port.nephronOptionWSfindAll();
    }

    /**
     * @return the pValuesDB
     */
    public List<String> getpValuesDB() {
        return pValuesDB;
    }

    /**
     * @param pValuesDB the pValuesDB to set
     */
    public void setpValuesDB(List<String> pValuesDB) {
        this.pValuesDB = pValuesDB;
    }

    /**
     * @return the pValuesAll
     */
    public List<String> getpValuesAll() {
        return pValuesAll;
    }

    /**
     * @param pValuesAll the pValuesAll to set
     */
    public void setpValuesAll(List<String> pValuesAll) {
        this.pValuesAll = pValuesAll;
    }

    /**
     * @return the optionsalerts
     */
    public DualListModel<String> getOptionsalerts() {
        return optionsalerts;
    }

    /**
     * @param optionsalerts the optionsalerts to set
     */
    public void setOptionsalerts(DualListModel<String> optionsalerts) {
        this.optionsalerts = optionsalerts;
    }

    private void nephronOptionWSremove(eu.nephron.opt.ws.Optionsall optionsall) {
        eu.nephron.opt.ws.NephronOptionWS port = service.getNephronOptionWSPort();
        port.nephronOptionWSremove(optionsall);
    }

    private void nephronOptionWScreate(eu.nephron.opt.ws.Optionsall optionsall) {
        eu.nephron.opt.ws.NephronOptionWS port = service.getNephronOptionWSPort();
        port.nephronOptionWScreate(optionsall);
    }
}
