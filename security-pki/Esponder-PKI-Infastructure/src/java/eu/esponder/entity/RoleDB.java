/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author tdim
 */
@Entity
@Table(name="es_RoleDB")
public class RoleDB implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String role;
    private String accessfunction;
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RoleDB)) {
            return false;
        }
        RoleDB other = (RoleDB) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "eu.nephron.entity.RoleDB[ id=" + id + " ]";
    }

    /**
     * @return the role
     */
    public String getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * @return the accessfunction
     */
    public String getAccessfunction() {
        return accessfunction;
    }

    /**
     * @param accessfunction the accessfunction to set
     */
    public void setAccessfunction(String accessfunction) {
        this.accessfunction = accessfunction;
    }

    
}
