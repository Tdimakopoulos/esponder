/*
 * 
 */
package eu.esponder.event.model.snapshot.action;

import eu.esponder.dto.model.snapshot.action.ActionPartSnapshotDTO;
import eu.esponder.event.model.UpdateEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class UpdateActionPartSnapshotEvent.
 */
public class UpdateActionPartSnapshotEvent extends ActionPartSnapshotEvent<ActionPartSnapshotDTO> implements UpdateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6948219527460346540L;
	
	/**
	 * Instantiates a new update action part snapshot event.
	 */
	public UpdateActionPartSnapshotEvent(){
		setJournalMessageInfo("UpdateActionPartSnapshotEvent");
	}
}
