/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package esponder.biomed.lib;

/**
 * This class represents the data blocks that are based on a regular sampling frequency of the acquired signal.<br>
 * The reconstruction of a portion of the signal can be done using the start time and the sampling frequency which are members of this class<br>
 * Time stamp of a sample n is : start time + n/sampling frequency
 * @author afa
 */
public class RegularDataBlock extends DataBlock{
    /**
     * start time in ms
     */
    private long startTimeInMillis;
    /**
     * sampling frequency in Hz
     */
    private float samplingFrequency;

    /**
     * Constructor
     * @param name the name of the signal
     * @param id the id of the signal
     */
    public RegularDataBlock(String name, int id){
        super(name, id);
    }
    
    /**
     * Constructor
     * @param name the name of the signal 
     * @param id the id of the signal
     * @param numberOfSamples  the number of the samples contained in this data block
     */
    public RegularDataBlock(String name, int id, int numberOfSamples){
        super(name, id);
        this.values = new float[numberOfSamples];
    }
    

    /**
     * Constructor
     * @param name the name of the signal
     * @param id the id of the signal
     * @param startTimeInMillis the start time of the samples in the this data block
     * @param samplingFrequency the sampling frequency of the signal
     * @param values the values of the samples
     */
    public RegularDataBlock(String name, int id, long startTimeInMillis, float samplingFrequency, float values[]){
        super(name, id, values);
        this.startTimeInMillis = startTimeInMillis;
        this.samplingFrequency = samplingFrequency;
    }
    
    /**
     * Gets the start time of data contained in data block
     * @return the start time in millliseconds of the data block
     */
    public long getStartTimeInMillis() {
        return startTimeInMillis;
    }

    /**
     * Sets the start time of data contained in data block
     * @param startTimeInMillis the start time in ms to set
     */
    public void setStartTimeInMillis(long startTimeInMillis) {
        this.startTimeInMillis = startTimeInMillis;
    }

    /**
     * Gets the sampling frequency of the signal acquisition
     * @return the sampling frequency in Hz
     */
    public float getSamplingFrequency() {
        return samplingFrequency;
    }

    /**
     * Sets the sampling frequency of the signal acquired
     * @param samplingFrequency the sampling frequency in Hz to set
     */
    public void setSamplingFrequency(float samplingFrequency) {
        this.samplingFrequency = samplingFrequency;
    }
}
