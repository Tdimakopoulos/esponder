/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package esponder.biomed.lib;

/**
 *
 * @author afa
 */
public class BiomedACommand extends Payload {

    /**
     * The buffer of the outgoing command
     */
    /**
     * The index of the current byte in the buffer
     * It can be used as the size for the outgoing command
     */
    private int index;

    /**
     * Constructor
     * This constructor is used to fill the outgoing command
     * @param cmdClass
     * @param opflag
     * @param opcode 
     */
    public BiomedACommand(BiomedACommandsDefinition.Classes cmdClass, BiomedACommandsDefinition.OpcodeFlag opflag, short opcode) {
        this.buffer = new byte[64];
        this.buffer[1] = (byte) (cmdClass.code | opflag.flag);
        this.buffer[2] = (byte) opcode;
        this.index = 3;
    }

    /**
     * Constructor
     * This constructor is used to parse the incoming command
     * @param stream the incoming stream
     * @param offset the offset of the stream, it is equivalent to the payload index
     * @param length the length of the payload
     */
    public BiomedACommand(byte stream[], int offset, int length) {
        this.size = length;
        this.buffer = new byte[length];
        System.arraycopy(stream, offset, this.buffer, 0, length);
        this.index = 0;
    }

    /**
     * Append the parameter in the command and update the internal buffer index
     * @param tag the tag of the parameter
     * @param value the value of the parameter
     */
    public void appendParameter(short tag, boolean exist, int value) {
        this.buffer[this.index++] = (byte) (tag & 0xff);
        if (exist == true) {
            if (value < 256) {
                this.buffer[this.index++] = 1;
                this.buffer[this.index++] = (byte) (value & 0xff);
            } else if (value < 65536) {
                this.buffer[this.index++] = 2;
                this.buffer[this.index++] = (byte) (value >> 8);
                this.buffer[this.index++] = (byte) (value & 0xff);
            } else {
                this.buffer[this.index++] = 4;
                this.buffer[this.index++] = (byte) (value >> (3 * 8));
                this.buffer[this.index++] = (byte) (value >> (2 * 8));
                this.buffer[this.index++] = (byte) (value >> (1 * 8));
                this.buffer[this.index++] = (byte) (value & 0xff);
            }
        } else {
            this.buffer[this.index++] = 0;
            return;
        }
    }

    public void insertSequenceNumber(int sequence) {
        this.buffer[0] = (byte) sequence;
    }

    /**
     * Get the sequence of the incoming command
     * @return the sequence number of the command
     */
    public int getSequenceNumber() {
        return (int) this.buffer[0];
    }

    /**
     * Get the type of the incoming command
     * @return the type of the command
     */
    public BiomedACommandsDefinition.OpcodeFlag getType() {
        short commandType = (short) (getBuffer()[this.index++] & (1 << 6 | 1 << 7));
        BiomedACommandsDefinition.OpcodeFlag opFlag = BiomedACommandsDefinition.OpcodeFlag.get(commandType);
        return opFlag;
    }

    /**
     * Get the class of the incoming command
     * @return the
     */
    public BiomedACommandsDefinition.Classes getCommandClass() {
        short commandClass = (short) (getBuffer()[this.index++] & (1 << 5 | 1 << 4 | 1 << 3 | 1 << 2 | 1 << 1 | 1 << 0));
        BiomedACommandsDefinition.Classes classes = BiomedACommandsDefinition.Classes.get(commandClass);
        return classes;
    }

    /**
     * Get the opcode of the incoming command
     * @return the opcode of the incoming command
     */
    public short getCommandOpcode() {
        return (short) (getBuffer()[0 + 2] & 0xff);
    }

    /**
     * Get the following parameter in the command
     * @return the following parameter
     */
    public byte[] getParameter() {
        byte param[] = new byte[this.getIndex() + 1];

        System.arraycopy(this.getBuffer(), this.getIndex(), param, 0, param.length);
        return param;
    }

    /**
     * @return the index
     */
    public int getIndex() {
        return index;
    }

    /**
     * Put the index at the first parameter
     */
    public void beginParameterSearch() {
        this.index = 3;//sequence,class+opcode, operation opcode
    }

    /**
     * Get the value of the parameter specified by the given code
     * @param parameterCode the code of the parameter
     * @return the value of the parameter
     */
    public String getStringParameterValue(short parameterCode) {

        byte buffer[] = null;
        while (this.index < this.size) {
            if ((this.buffer[this.index++] & 0xff) == parameterCode) {//parameter code consumed
                int parameterSize = this.buffer[this.index++];
                buffer = new byte[parameterSize];
                for (int i = 0; i < parameterSize; i++) {
                    buffer[i] = this.buffer[this.index++];
                }
                return new String(buffer);
            } else {
                this.index += 1 + this.buffer[this.index];//size filed and size
            }
        }
        return null;
    }

    /**
     * Get the value of the parameter specified by the given code
     * @param parameterCode the code of the parameter
     * @return the value of the parameter
     */
    public long getParameterValue(short parameterCode) {

        while (this.index < this.size) {
            if ((this.buffer[this.index++] & 0xff) == parameterCode) {//parameter code consumed
                long value = 0;
                int parameterSize = this.buffer[this.index++];
                for (int i = 0; i < parameterSize; i++) {
                    value = value * 256 + (buffer[this.index++]&0xFF);
                }
                return value;
            } else {
                this.index += 1 + this.buffer[this.index];//size filed and size
            }
        }
        return -1;
    }

    public boolean isResponseOf(BiomedACommand request) {
        return this.getSequenceNumber() == request.getSequenceNumber();
    }
    
    /**
     * Wait on the response of the command
     * @param waitTime maximum time for the response to come
     * @throws CommunicationException if the maximum wait time is exceeded
     */
    public void waitOnResponse(int waitTime) throws CommunicationException {
        CommunicationException commEx = null;
        long start = System.currentTimeMillis();
        
        try {
            synchronized (this) {
                wait(waitTime);
            }
            long end = System.currentTimeMillis();
            int duration = (int) (end - start);
            if (duration >= waitTime) {  
                commEx = new CommunicationException();
                commEx.setMessage("receiving time out");
            }
        } catch (InterruptedException ex) {
            commEx = new CommunicationException();
            commEx.setMessage("receiving time out");
            throw commEx;
        }
        if ((this.buffer[1] & 0xC0) == 0xC0) {//error
            switch (buffer[2]) {
                case 1:
                    commEx = new CommunicationException();
                    commEx.setMessage("The command is not implemented");
                    break;
                case 2:
                    commEx = new CommunicationException();
                    commEx.setMessage("An expected parameter is missing or out of range");
                    break;
                case 3:
                    commEx = new CommunicationException();
                    commEx.setMessage("The command execution failed");
                    break;
                case 4:
                    commEx = new CommunicationException();
                    commEx.setMessage("Memory allocation for the command faild");
                    break;
                case 5:
                    commEx = new CommunicationException();
                    commEx.setMessage("Unexpected hardware reply");
                    break;
            }
        }
        if (commEx != null) {
            throw commEx;
        }
    }
}
