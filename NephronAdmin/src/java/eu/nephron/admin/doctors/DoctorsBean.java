/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.admin.doctors;

import eu.nephron.credentials.ws.CredentialsWS_Service;
import eu.nephron.credentials.ws.Credentialsdb;
import eu.nephron.model.Doctors;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.hl7.cache.ws.Cachedoctors_Service;
import org.hl7.cache.ws.Hl7Doctors;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class DoctorsBean implements Serializable{

    private static final long serialVersionUID = 7526471155622776147L;
    
    private Cachedoctors_Service service = new Cachedoctors_Service();
    private String code;
    private String name;
    private String administrativeArea;
    private String city;
    private String street;
    private String streetNo;
    private String floor;
    private String postCode;
    private String title;
    private String firstName;
    private String middleName;
    private String lastName;
    private String fatherName;
    private int maritalStatus;
    private int educationalLevel;
    private String phone;
    private String companyPhone;
    private String mobile;
    private String companyMobile;
    private String email;
    private String companyEmail;
    private List<Doctors> pLists = new ArrayList<Doctors>();
    private String doctoridforedit;
    private Doctors selectedDoctor;
    private String username;
    private String password;
    private Long pdid;
    private Long icreid;

    public String deletedoctor() {
            String dd=FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("doctorID").toString();
                
                System.out.println("Calling listener : "+dd);
                
        long ifind = Long.valueOf(dd);//Long.parseLong(getDoctoridforedit());
        //long ifind = Long.parseLong(getDoctoridforedit());
        System.out.println("removing "+ifind);
        removedoctors(finddoctors(ifind));
        refresh();
        return "DoctorPage?faces-redirect=true";
    }

    public String editcredoctor() {

                    String dd=FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("doctorID").toString();
                
                System.out.println("Calling listener : "+dd);
                
        long ifind = Long.valueOf(dd);//Long.parseLong(getDoctoridforedit());
        //long ifind = Long.parseLong(getDoctoridforedit());
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("PDoctorID", String.valueOf(ifind));
        System.out.println("***** Doctor ID For Edit :  " + ifind);
        
        for(int i=0;i<pLists.size();i++)
        {
            if(pLists.get(i).getId().toString().equalsIgnoreCase(String.valueOf(ifind)))
            {
                selectedDoctor=pLists.get(i);
            }
        }
        
        setPdid(selectedDoctor.getId());
        setUsername("");
        setPassword("");
        setIcreid(new Long(-1));
        java.util.List<eu.nephron.credentials.ws.Credentialsdb> pfind = findAll();
        for (int i = 0; i < pfind.size(); i++) {
            if (pfind.get(i).getDoctorid().toString().equalsIgnoreCase(String.valueOf(ifind))) {
                setUsername(pfind.get(i).getUsername());
                setPassword(pfind.get(i).getPassword());
                setPdid(pfind.get(i).getDoctorid());
                setIcreid(pfind.get(i).getId());
            }
        }
        System.out.println("CreID : "+icreid);
        return "Credentials?faces-redirect=true";
    }

    private Credentialsdb find(java.lang.Object id) {
        CredentialsWS_Service service = new CredentialsWS_Service();
        eu.nephron.credentials.ws.CredentialsWS port = service.getCredentialsWSPort();
        return port.find(id);
    }
    
    private java.util.List<eu.nephron.credentials.ws.Credentialsdb> findAll() {
        CredentialsWS_Service service = new CredentialsWS_Service();
        eu.nephron.credentials.ws.CredentialsWS port = service.getCredentialsWSPort();
        return port.findAll();
    }

    private void remove(eu.nephron.credentials.ws.Credentialsdb credentialsdb) {
        CredentialsWS_Service service = new CredentialsWS_Service();
        eu.nephron.credentials.ws.CredentialsWS port = service.getCredentialsWSPort();
        port.remove(credentialsdb);
    }

    private void create(eu.nephron.credentials.ws.Credentialsdb credentialsdb) {
        CredentialsWS_Service service = new CredentialsWS_Service();
        eu.nephron.credentials.ws.CredentialsWS port = service.getCredentialsWSPort();
        port.create(credentialsdb);
    }
    
    public String editdoctor() {
        
                String dd=FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("doctorID").toString();
                
                System.out.println("Calling listener : "+dd);
                
        long ifind = Long.valueOf(dd);//Long.parseLong(getDoctoridforedit());
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("PDoctorID", String.valueOf(ifind));
        System.out.println("***** Doctor ID For Edit :  " + ifind);
        //selectedDoctor=this.pLists.get(maritalStatus)
        for(int i=0;i<pLists.size();i++)
        {
            if(pLists.get(i).getId().toString().equalsIgnoreCase(String.valueOf(ifind)))
            {
                selectedDoctor=pLists.get(i);
            }
        }
        //from list to values
        code = selectedDoctor.getCode();
        name = selectedDoctor.getName();
        administrativeArea = selectedDoctor.getAdministrativeArea();
        city = selectedDoctor.getCity();
        street = selectedDoctor.getStreet();
        streetNo = selectedDoctor.getStreetNo();
        floor = selectedDoctor.getFloor();
        postCode = selectedDoctor.getPostCode();
        title = selectedDoctor.getTitle();
        firstName = selectedDoctor.getFirstName();
        middleName = selectedDoctor.getMiddleName();
        lastName = selectedDoctor.getLastName();
        fatherName = selectedDoctor.getFatherName();
        maritalStatus = selectedDoctor.getMaritalStatus();
        educationalLevel = selectedDoctor.getEducationalLevel();
        phone = selectedDoctor.getPhone();
        companyPhone = selectedDoctor.getCompanyPhone();
        mobile = selectedDoctor.getMobile();
        companyMobile = selectedDoctor.getCompanyMobile();
        email = selectedDoctor.getEmail();
        companyEmail = selectedDoctor.getCompanyEmail();

        return "Edit_Doctor?faces-redirect=true";
    }

    public String adddoctor() {
        code = "";
        name = "";
        administrativeArea = "";
        city = "";
        street = "";
        streetNo = "";
        floor = "";
        postCode = "";
        title = "";
        firstName = "";
        middleName = "";
        lastName = "";
        fatherName = "";
        maritalStatus = 0;
        educationalLevel = 0;
        phone = "";
        companyPhone = "";
        mobile = "";
        companyMobile = "";
        email = "";
        companyEmail = "";
        return "Add_Doctor?faces-redirect=true";
    }

    public String savecreadddoctor() {

    if(icreid==-1)
    {
        eu.nephron.credentials.ws.Credentialsdb  credentialsdb= new Credentialsdb();
        credentialsdb.setDoctorid(pdid);
        credentialsdb.setUsername(username);
        credentialsdb.setPassword(password);
    create(credentialsdb);
    }else
    {
        remove(find(icreid));
        eu.nephron.credentials.ws.Credentialsdb  credentialsdb= new Credentialsdb();
        credentialsdb.setDoctorid(pdid);
        credentialsdb.setUsername(username);
        credentialsdb.setPassword(password);
    create(credentialsdb);
        
    }
        return "DoctorPage?faces-redirect=true";
    }

    public String saveadddoctor() {

        //save add
        org.hl7.cache.ws.Hl7Doctors item = new org.hl7.cache.ws.Hl7Doctors();
            
            item.setFatherName(fatherName);
            item.setFirstName(firstName);
            item.setLastName(lastName);
            item.setAdministrativeArea(administrativeArea);
            
            item.setPostCode(postCode);
            item.setTitle(title);
            item.setPhone(phone);
            item.setMobile(mobile);
            item.setEmail(email);
            item.setFloor(floor);
            item.setCompanyEmail(companyEmail);
            item.setCompanyMobile(companyMobile);
            item.setCompanyPhone(companyPhone);
            item.setStreet(street);
            item.setStreetNo(streetNo);
            item.setCode(code);
            item.setCity(city);
    createdoctors(item);    
        refresh();
        return "DoctorPage?faces-redirect=true";
    }

    public String canceldoctor() {

        code = "";
        name = "";
        administrativeArea = "";
        city = "";
        street = "";
        streetNo = "";
        floor = "";
        postCode = "";
        title = "";
        firstName = "";
        middleName = "";
        lastName = "";
        fatherName = "";
        maritalStatus = 0;
        educationalLevel = 0;
        phone = "";
        companyPhone = "";
        mobile = "";
        companyMobile = "";
        email = "";
        companyEmail = "";
        return "DoctorPage?faces-redirect=true";
    }

    public String saveeditdoctor() {

        long ifind = Long.parseLong(getDoctoridforedit());
        //copy from text to concept and edit
        org.hl7.cache.ws.Hl7Doctors item = new org.hl7.cache.ws.Hl7Doctors();
            item.setId(ifind);
            item.setFatherName(fatherName);
            item.setFirstName(firstName);
            item.setLastName(lastName);
            item.setAdministrativeArea(administrativeArea);
            //item.setEducationalLevel(pList.get(i).getEducationalLevel());
            //item.setMaritalStatus(pList.get(i).getMaritalStatus());
            item.setPostCode(postCode);
            item.setTitle(title);
            item.setPhone(phone);
            item.setMobile(mobile);
            item.setEmail(email);
            item.setFloor(floor);
            item.setCompanyEmail(companyEmail);
            item.setCompanyMobile(companyMobile);
            item.setCompanyPhone(companyPhone);
            item.setStreet(street);
            item.setStreetNo(streetNo);
            item.setCode(code);
            item.setCity(city);
            editdoctors(item);
        refresh();
        return "DoctorPage?faces-redirect=true";
    }

    public String editpdoctor() {
        long ifind = Long.parseLong(getDoctoridforedit());
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("PDoctorID", getDoctoridforedit());
        System.out.println("***** Doctor ID For pEdit :  " + getDoctoridforedit());
        return "Edit_Doctor?faces-redirect=true";
    }

    public void refresh() {
        pLists.clear();
        java.util.List<org.hl7.cache.ws.Hl7Doctors> pList = findAlldoctors();
        for (int i = 0; i < pList.size(); i++) {
            Doctors item = new Doctors();
            item.setId(pList.get(i).getId());
            item.setFatherName(pList.get(i).getFatherName());
            item.setFirstName(pList.get(i).getFirstName());
            item.setLastName(pList.get(i).getLastName());
            item.setAdministrativeArea(pList.get(i).getAdministrativeArea());
            item.setEducationalLevel(pList.get(i).getEducationalLevel());
            item.setMaritalStatus(pList.get(i).getMaritalStatus());
            item.setPostCode(pList.get(i).getPostCode());
            item.setTitle(pList.get(i).getTitle());
            item.setPhone(pList.get(i).getPhone());
            item.setMobile(pList.get(i).getMobile());
            item.setEmail(pList.get(i).getEmail());
            item.setFloor(pList.get(i).getFloor());
            item.setCompanyEmail(pList.get(i).getCompanyEmail());
            item.setCompanyMobile(pList.get(i).getCompanyMobile());
            item.setCompanyPhone(pList.get(i).getCompanyPhone());
            item.setStreet(pList.get(i).getStreet());
            item.setStreetNo(pList.get(i).getStreetNo());
            item.setCode(pList.get(i).getCode());
            item.setCity(pList.get(i).getCity());
            pLists.add(item);
        }

    }

    /**
     * Creates a new instance of DoctorsBean
     */
    public DoctorsBean() {

        java.util.List<org.hl7.cache.ws.Hl7Doctors> pList = findAlldoctors();
        for (int i = 0; i < pList.size(); i++) {
            Doctors item = new Doctors();
            item.setId(pList.get(i).getId());
            item.setFatherName(pList.get(i).getFatherName());
            item.setFirstName(pList.get(i).getFirstName());
            item.setLastName(pList.get(i).getLastName());
            item.setAdministrativeArea(pList.get(i).getAdministrativeArea());
            item.setEducationalLevel(pList.get(i).getEducationalLevel());
            item.setMaritalStatus(pList.get(i).getMaritalStatus());
            item.setPostCode(pList.get(i).getPostCode());
            item.setTitle(pList.get(i).getTitle());
            item.setPhone(pList.get(i).getPhone());
            item.setMobile(pList.get(i).getMobile());
            item.setEmail(pList.get(i).getEmail());
            item.setFloor(pList.get(i).getFloor());
            item.setCompanyEmail(pList.get(i).getCompanyEmail());
            item.setCompanyMobile(pList.get(i).getCompanyMobile());
            item.setCompanyPhone(pList.get(i).getCompanyPhone());
            item.setStreet(pList.get(i).getStreet());
            item.setStreetNo(pList.get(i).getStreetNo());
            item.setCode(pList.get(i).getCode());
            item.setCity(pList.get(i).getCity());
            pLists.add(item);
        }
    }

////////////////////////////////////////////////// Get Set////////////////////////////////////////////////////////////////////
    /**
     * @return the doctoridforedit
     */
    public String getDoctoridforedit() {
        return doctoridforedit;
    }

    /**
     * @param doctoridforedit the doctoridforedit to set
     */
    public void setDoctoridforedit(String doctoridforedit) {
        this.doctoridforedit = doctoridforedit;
    }

    /**
     * @return the selectedDoctor
     */
    public Doctors getSelectedDoctor() {
        return selectedDoctor;
    }

    /**
     * @param selectedDoctor the selectedDoctor to set
     */
    public void setSelectedDoctor(Doctors selectedDoctor) {
        this.selectedDoctor = selectedDoctor;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the administrativeArea
     */
    public String getAdministrativeArea() {
        return administrativeArea;
    }

    /**
     * @param administrativeArea the administrativeArea to set
     */
    public void setAdministrativeArea(String administrativeArea) {
        this.administrativeArea = administrativeArea;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return the streetNo
     */
    public String getStreetNo() {
        return streetNo;
    }

    /**
     * @param streetNo the streetNo to set
     */
    public void setStreetNo(String streetNo) {
        this.streetNo = streetNo;
    }

    /**
     * @return the floor
     */
    public String getFloor() {
        return floor;
    }

    /**
     * @param floor the floor to set
     */
    public void setFloor(String floor) {
        this.floor = floor;
    }

    /**
     * @return the postCode
     */
    public String getPostCode() {
        return postCode;
    }

    /**
     * @param postCode the postCode to set
     */
    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the middleName
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * @param middleName the middleName to set
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the fatherName
     */
    public String getFatherName() {
        return fatherName;
    }

    /**
     * @param fatherName the fatherName to set
     */
    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    /**
     * @return the maritalStatus
     */
    public int getMaritalStatus() {
        return maritalStatus;
    }

    /**
     * @param maritalStatus the maritalStatus to set
     */
    public void setMaritalStatus(int maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    /**
     * @return the educationalLevel
     */
    public int getEducationalLevel() {
        return educationalLevel;
    }

    /**
     * @param educationalLevel the educationalLevel to set
     */
    public void setEducationalLevel(int educationalLevel) {
        this.educationalLevel = educationalLevel;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the companyPhone
     */
    public String getCompanyPhone() {
        return companyPhone;
    }

    /**
     * @param companyPhone the companyPhone to set
     */
    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    /**
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * @param mobile the mobile to set
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * @return the companyMobile
     */
    public String getCompanyMobile() {
        return companyMobile;
    }

    /**
     * @param companyMobile the companyMobile to set
     */
    public void setCompanyMobile(String companyMobile) {
        this.companyMobile = companyMobile;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the companyEmail
     */
    public String getCompanyEmail() {
        return companyEmail;
    }

    /**
     * @param companyEmail the companyEmail to set
     */
    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the pdid
     */
    public Long getPdid() {
        return pdid;
    }

    /**
     * @param pdid the pdid to set
     */
    public void setPdid(Long pdid) {
        this.pdid = pdid;
    }

    /**
     * @return the icreid
     */
    public Long getIcreid() {
        return icreid;
    }

    /**
     * @param icreid the icreid to set
     */
    public void setIcreid(Long icreid) {
        this.icreid = icreid;
    }

    private java.util.List<org.hl7.cache.ws.Hl7Doctors> findAlldoctors() {
        org.hl7.cache.ws.Cachedoctors port = service.getCachedoctorsPort();
        return port.findAlldoctors();
    }

    /**
     * @return the pLists
     */
    public List<Doctors> getpLists() {
        return pLists;
    }

    /**
     * @param pLists the pLists to set
     */
    public void setpLists(List<Doctors> pLists) {
        this.pLists = pLists;
    }

    private void editdoctors(org.hl7.cache.ws.Hl7Doctors hl7Doctors) {
        org.hl7.cache.ws.Cachedoctors port = service.getCachedoctorsPort();
        port.editdoctors(hl7Doctors);
    }

    private void removedoctors(org.hl7.cache.ws.Hl7Doctors hl7Doctors) {
        org.hl7.cache.ws.Cachedoctors port = service.getCachedoctorsPort();
        port.removedoctors(hl7Doctors);
    }

    private Hl7Doctors finddoctors(java.lang.Object id) {
        org.hl7.cache.ws.Cachedoctors port = service.getCachedoctorsPort();
        return port.finddoctors(id);
    }

    private void createdoctors(org.hl7.cache.ws.Hl7Doctors hl7Doctors) {
        org.hl7.cache.ws.Cachedoctors port = service.getCachedoctorsPort();
        port.createdoctors(hl7Doctors);
    }
}
