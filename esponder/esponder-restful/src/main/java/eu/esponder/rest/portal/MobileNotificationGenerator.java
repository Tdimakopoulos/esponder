/*
 * 
 */
package eu.esponder.rest.portal;

import java.util.Date;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.codehaus.enunciate.XmlTransient;

import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.event.mobile.MobileMessageEvent;
import eu.esponder.eventadmin.ESponderEventPublisher;
import eu.esponder.rest.ESponderResource;



@Path("/portal/notifications")
@XmlTransient
public class MobileNotificationGenerator extends ESponderResource {


	@GET
	@Path("/sendmessage")
	@Produces({ MediaType.APPLICATION_JSON })
	public String SendMessage(
			@QueryParam("imei") String imei,@QueryParam("origin") String origin,@QueryParam("origintype") String origintype, 
			@QueryParam("message") String message) {
		
		if(origin!=null && message!=null) {
			ESponderEventPublisher<MobileMessageEvent> publisher = 
					new ESponderEventPublisher<MobileMessageEvent>(MobileMessageEvent.class);
			
			MobileMessageEvent event  = new MobileMessageEvent();
			event.setMobileimei(imei);
			
			if(origintype.equalsIgnoreCase("meoc"))
				event.setSourceMEoc(origin);
			else
				event.setSourceEoc(origin);
			
			event.setEventSeverity(SeverityLevelDTO.MEDIUM);
			event.setEventTimestamp(new Date());
			event.setJournalMessage("Notification generated from "+origin);
			event.setJournalMessageInfo("Notification generated from "+origin);
			event.setSzMessage(message);
			event.setEventAttachment(new ESponderUserDTO());
			
			try {
				publisher.publishEvent(event);
			} catch (Exception e) {
				return e.getMessage();
			}
			publisher.CloseConnection();
			return "Success";
		}
		else
			return "Something went wrong";
	}
	

	/**
	 * Run simulation.
	 *
	 * @param pkiKey the pki key
	 * @return the string
	 */
	@GET
	@Path("/run")
	@Produces({ MediaType.APPLICATION_JSON })
	public String RunNotificationGenerator(
			@QueryParam("origin") String origin, @QueryParam("message") String message) {
		
		if(origin!=null && message!=null) {
			ESponderEventPublisher<MobileMessageEvent> publisher = 
					new ESponderEventPublisher<MobileMessageEvent>(MobileMessageEvent.class);
			
			MobileMessageEvent event  = new MobileMessageEvent();
			event.setMobileimei("353581051146316");
			event.setSourceMEoc("MEOC Athens");
			event.setEventSeverity(SeverityLevelDTO.MEDIUM);
			event.setEventTimestamp(new Date());
			event.setJournalMessage("Notification generated from "+origin);
			event.setJournalMessageInfo("Notification generated from "+origin);
			event.setSzMessage("Your team needs to retreat from the crisis area...");
			event.setEventAttachment(new ESponderUserDTO());
			
			try {
				publisher.publishEvent(event);
			} catch (Exception e) {
				return e.getMessage();
			}
			publisher.CloseConnection();
			return "Success";
		}
		else
			return "Something went wrong";
	}

		

}