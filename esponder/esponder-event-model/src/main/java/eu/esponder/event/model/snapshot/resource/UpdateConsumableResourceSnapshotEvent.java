/*
 * 
 */
package eu.esponder.event.model.snapshot.resource;

import eu.esponder.dto.model.snapshot.SnapshotDTO;
import eu.esponder.event.model.UpdateEvent;



// TODO: Auto-generated Javadoc
/**
 * The Class UpdateConsumableResourceSnapshotEvent.
 */
public class UpdateConsumableResourceSnapshotEvent extends ConsumableResourceSnapshotEvent<SnapshotDTO> implements UpdateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3471927216194630496L;
	
	/**
	 * Instantiates a new update consumable resource snapshot event.
	 */
	public UpdateConsumableResourceSnapshotEvent(){
		setJournalMessageInfo("UpdateConsumableResourceSnapshotEvent");
	}
}
