package InterfChecker.data;

//Attributes for ListA
public class Interf {
	protected int interfId;
	protected String name;
	protected String device;
	
	public Interf() {

	}


	public int getInterfId() {
		return interfId;
	}


	public void setInterfId(int interfId) {
		this.interfId = interfId;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}
		
		
	
	
	
	
}
