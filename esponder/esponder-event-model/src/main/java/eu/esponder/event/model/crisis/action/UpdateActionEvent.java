/*
 * 
 */
package eu.esponder.event.model.crisis.action;

import eu.esponder.dto.model.crisis.action.ActionDTO;
import eu.esponder.event.model.UpdateEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class UpdateActionEvent.
 */
public class UpdateActionEvent extends ActionEvent<ActionDTO> implements UpdateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1950437603072013048L;
	
	/**
	 * Instantiates a new update action event.
	 */
	public UpdateActionEvent(){
		setJournalMessageInfo("Update Action Event");
	}	
}
