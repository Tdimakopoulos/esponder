/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.mestypes;

/**
 *
 * @author tdim
 */
public class mestypes {

    public Long GetType(String typename)
    {
        if(typename==null)
            return null;
        if(typename.equalsIgnoreCase("Condactivity Dialysate"))
        return new Long(11);
        
        if(typename.equalsIgnoreCase("Diastolic"))
        return new Long(30);
        
        if(typename.equalsIgnoreCase("Heartrate"))
        return new Long(31);
        
        if(typename.equalsIgnoreCase("Systolic"))
        return new Long(32);
        
        if (typename.equalsIgnoreCase("Bodyfat")) {
            return new Long(40);
        }
        
        if(typename.equalsIgnoreCase("Weight"))
        return new Long(41);
        
        if(typename.equalsIgnoreCase("Pressure Bloodline"))
        return new Long(13);
        
        if(typename.equalsIgnoreCase("Pressure Dialysate"))
        return new Long(15);
        
        
        if(typename.equalsIgnoreCase("Temp IN"))
        return new Long(17);
        
        if(typename.equalsIgnoreCase("Temp OUT"))
        return new Long(18);
        
        if(typename.equalsIgnoreCase("Inlet PH"))
        return new Long(1);
        
        if(typename.equalsIgnoreCase("Inlet Potasium"))
        return new Long(2);
        
        if(typename.equalsIgnoreCase("Inlet Calcium"))
        return new Long(3);
        
        if(typename.equalsIgnoreCase("Inlet Temp"))
        return new Long(4);
        
        if(typename.equalsIgnoreCase("Inlet Uria"))
        return new Long(5);
        
        if(typename.equalsIgnoreCase("Outlet PH"))
        return new Long(6);
        
        if(typename.equalsIgnoreCase("Outlet Potasium"))
        return new Long(7);
        
        if(typename.equalsIgnoreCase("Outlet Calcium"))
        return new Long(8);
        
        if(typename.equalsIgnoreCase("Outlet Temp"))
        return new Long(9);
        
        if(typename.equalsIgnoreCase("Outlet Uria"))
        return new Long(10);
        if(typename.equalsIgnoreCase("Condactivity Temperature Dialysate"))
        return new Long(12);
        
        return new Long(-1);
    }
    
    public String GetName(Long iType) {
        return GetName(iType.intValue());
    }
    
    public String GetName(int iType) {
        if (iType == 11) {
            return "Conductivity Dialysate";
        }
        if (iType == 12) {//PT
            return "Conductivity Temperature Dialysate";
        }
        if (iType == 13) {
            return "Pressure Bloodline";
        }
        if (iType == 14) {//not use
            return "Pressure BCO";
        }
        if (iType == 15) {
            return "Pressure Dialysate";
        }
        if (iType == 16) {//not use
            return "Pressure FCO";
        }
        if (iType == 17) {
            return "Temp IN";
        }
        if (iType == 18) {
            return "Temp OUT";
        }
        if (iType == 1) {
            return "Inlet PH";
        }
        if (iType == 2) {
            return "Inlet Potasium";
        }
        if (iType == 3) {
            return "Inlet Calcium";
        }
        if (iType == 4) {
            return "Inlet Temp";
        }
        if (iType == 5) {
            return "Inlet Uria";
        }
        if (iType == 6) {
            return "Outlet PH";
        }
        if (iType == 7) {
            return "Outlet Potasium";
        }
        if (iType == 8) {
            return "Outlet Calcium";
        }
        if (iType == 9) {
            return "Outlet Temp";
        }
        if (iType == 10) {
            return "Outlet Uria";
        }
        
        
        if (iType == 30) {
            return "Diastolic";
        }
        
        if (iType == 31) {
            return "Heartrate";
        }
        
        if (iType == 32) {
            return "Systolic";
        }
        
        if (iType == 40) {
            return "Bodyfat";
        }
        
        if (iType == 41) {
            return "Weight";
        }
        return "NA";
    }
}
