package eu.esponder.osgi.service.event;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import eu.esponder.df.ruleengine.controller.bean.DatafusionControllerBean;
import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.event.model.ESponderEvent;

public class DFThread implements Runnable {

	ESponderEvent<? extends ESponderEntityDTO> event=null;
	
	public void SetEvent(ESponderEvent<? extends ESponderEntityDTO> event2)
	{
		event=event2;
	}
	
	public void run() {
        try {
        	//System.out.println("------------------------------------------------------------------");
        	System.out.println("DF thread Started with Event : "+event.getClass().getCanonicalName()+" Hash Code : "+this.hashCode());
        //	System.out.println("------------------------------------------------------------------");
        	DatafusionControllerBean pEventManager = new DatafusionControllerBean();
    		
        	pEventManager.EsponderEventReceivedHandler(event);
    		//System.out.println("------------------------------------------------------------------");
        	System.out.println("DF thread Finished with No Errors : "+event.getClass().getCanonicalName()+" Hash Code : "+this.hashCode());
        	//System.out.println("------------------------------------------------------------------");
        } catch (Exception ex) {
        	
        	System.out.println("DF thread Finished with Exception : "+event.getClass().getCanonicalName()+" Exception : "+ex.getMessage()+" Hash Code : "+this.hashCode());
        	//System.out.println("------------------------------------------------------------------");
        }
    }
}


//public class HelloRunnable implements Runnable {
//
//    public void run() {
//        try {
//            System.out.println("Hello from a thread!");
//            Thread.sleep(10000);
//        } catch (InterruptedException ex) {
//            Logger.getLogger(HelloRunnable.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//    public static void main(String args[]) {
//        (new Thread(new HelloRunnable())).start();
//        System.out.println("Exit");
//    }
//
//}