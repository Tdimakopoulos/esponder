/*
 * 
 */
package eu.esponder.event.model.snapshot.resource;

import eu.esponder.dto.model.snapshot.resource.EquipmentSnapshotDTO;
import eu.esponder.event.model.CreateEvent;



// TODO: Auto-generated Javadoc
/**
 * The Class CreateEquipmentSnapshotEvent.
 */
public class CreateEquipmentSnapshotEvent extends EquipmentSnapshotEvent<EquipmentSnapshotDTO> implements CreateEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6363754485823085786L;
	
	/**
	 * Instantiates a new creates the equipment snapshot event.
	 */
	public CreateEquipmentSnapshotEvent(){
		setJournalMessageInfo("CreateEquipmentSnapshotEvent");
	}
}
