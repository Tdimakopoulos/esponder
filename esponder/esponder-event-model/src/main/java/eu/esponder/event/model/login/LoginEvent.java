/*
 * 
 */
package eu.esponder.event.model.login;

import eu.esponder.event.model.ESponderOperationEvent;

/**
 * The Interface LoginEvent.
 */
public abstract interface LoginEvent extends ESponderOperationEvent{

}
