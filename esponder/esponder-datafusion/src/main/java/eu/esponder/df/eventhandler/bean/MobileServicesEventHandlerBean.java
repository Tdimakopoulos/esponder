/*
 * 
 */
package eu.esponder.df.eventhandler.bean;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.naming.NamingException;

import com.prosyst.mprm.backend.event.EventListenerException;

import eu.esponder.df.datafusion.YMAP2DView;
import eu.esponder.df.eventhandler.MobileServicesEventHandlerRemoteService;
import eu.esponder.df.eventhandler.MobileServicesEventHandlerService;
import eu.esponder.df.optimizer.ManageTeams;
import eu.esponder.df.ruleengine.utilities.RuleUtilities;
import eu.esponder.df.ruleengine.utilities.SensorTypeHelper;
import eu.esponder.df.rules.profile.ProfileManager;
import eu.esponder.dto.model.QueryParamsDTO;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.action.SeverityLevelDTO;
import eu.esponder.dto.model.crisis.alternative.OCEocALTDTO;
import eu.esponder.dto.model.crisis.resource.sensor.ActivitySensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.BodyTemperatureSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.BreathRateSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.CarbonDioxideSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.CarbonMonoxideSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.EnvironmentalSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.HeartBeatRateSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.HydrogenSulfideSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.LPSSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.LocationSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.MethaneSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.OxygenSensorDTO;
import eu.esponder.dto.model.crisis.resource.sensor.SensorDTO;
import eu.esponder.dto.model.snapshot.PeriodDTO;
import eu.esponder.dto.model.snapshot.location.PointDTO;
import eu.esponder.dto.model.snapshot.location.SphereDTO;
import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.dto.model.snapshot.resource.SensorSnapshotDTO;
import eu.esponder.dto.model.snapshot.sensor.measurement.statistic.MeasurementStatisticTypeEnumDTO;
import eu.esponder.dto.model.snapshot.status.ActorSnapshotStatusDTO;
import eu.esponder.dto.model.snapshot.status.SensorSnapshotStatusDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;
import eu.esponder.event.datafusion.query.ESponderDFQueryRequestEvent;
import eu.esponder.event.datafusion.query.ESponderDFQueryResponseEvent;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.event.model.snapshot.resource.CreateActorSnapshotEvent;
import eu.esponder.eventadmin.ESponderEventPublisher;
import eu.esponder.servicemanager.ESponderResource;

/**
 * The Class ActionEventHandlerBean.
 */
@Stateless
public class MobileServicesEventHandlerBean extends dfEventHandlerBean
		implements MobileServicesEventHandlerService,
		MobileServicesEventHandlerRemoteService {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * eu.esponder.df.eventhandler.ActionEventHandlerService#ProcessEvent(eu
	 * .esponder.event.model.ESponderEvent)
	 */

	public void ProcessEvent(ESponderEvent<?> pEvent) {

		String pname = pEvent.getClass().getSimpleName();
		// System.out
		// .println("-------------------------------MOBILE SERVICES CONTROLLER -----------------------------------------");
		// System.out
		// .println("Mobile communication event received. Class name : "
		// + pname);

		if (pname.equalsIgnoreCase("ESponderDFQueryRequestEvent")) {
			// System.out
			// .println("Check passed start process " );
			System.out
					.println("Mobile Request using ESponderDFQueryRequestEvent");
			// ActionDTO pobject= (ActionDTO)pEvent.getEventAttachment();
			ESponderDFQueryRequestEvent pEventDF = (ESponderDFQueryRequestEvent) pEvent;
			Long RequestID = pEventDF.getRequestID();
			String IMEI = pEventDF.getIMEI();
			Long QueryID = pEventDF.getQueryID();
			QueryParamsDTO params = pEventDF.getParams();
			params.getQueryParamList();
			// System.out.println("Mobile Event received RID : " + RequestID
			// + " QID :" + QueryID);
			// System.out.println("Message : " + pEvent.getJournalMessage());
			// System.out
			// .println("---------------------------------------------------------------------------------------------");

			if (QueryID == 698) {
				try {

					HashMap<String, Object> eventParams = params
							.getQueryParamList();
					if (eventParams.containsKey("sensorMeasurements")) {
						String measurements = (String) eventParams
								.get("sensorMeasurements");
						// String measurements = "11234,1,3a";
						if (measurements != null) {

							List<String> frSensorMeasurements = Arrays
									.asList(measurements.split("a"));

							for (int i = 0; i < frSensorMeasurements.size(); i++) {
								List<String> frSensorMeasurement = Arrays
										.asList(frSensorMeasurements.get(i)
												.split(","));

								// for (int ix = 0; ix < frSensorMeasurement
								// .size(); ix++) {
								// System.out.println(frSensorMeasurement
								// .get(ix));

								// }
								Long frID = Long.valueOf(frSensorMeasurement
										.get(0));
								List<SensorDTO> sensorList = findSensorsforFR(frID);
								int sensorType = Integer
										.valueOf(frSensorMeasurement.get(1));
								Long sensorID = findSensorByTypeFromList(
										sensorType, sensorList);

								System.out.println("Save Measurment : FRID : "+frID+" Sensor : "+sensorID+" Value : "+frSensorMeasurement.get(2));
								
								SensorSnapshotDTO snapshot = new SensorSnapshotDTO();
								snapshot.setPeriod(new PeriodDTO(Long
										.valueOf((new Date()).getTime()), Long
										.valueOf((new Date()).getTime())));
								snapshot.setSensor(sensorID);
								snapshot.setStatisticType(MeasurementStatisticTypeEnumDTO.MEAN);
								snapshot.setValue(frSensorMeasurement.get(2));
								snapshot.setStatus(SensorSnapshotStatusDTO.WORKING);
								sensorService.createSensorSnapshotRemote(
										snapshot, new Long(0));

							}
						}
					}
				} catch (Exception e) {
System.out.println("Exeption on save measurment : "+e.getMessage());
				}

			}

			// start query 1
			if (QueryID == 33) {

				String totalMembers = "6";
				String title = "FirsttestforOPT";
				String titleper = "PersonnelTest";
				String titleic = "Incident Commander No1";
				String titlemeoc = "MEOC Schiphol I";

				HashMap<String, Object> eventParams = params
						.getQueryParamList();
				if (eventParams.containsKey("totalMembers")) {
					totalMembers = (String) eventParams.get("totalMembers");
				}

				if (eventParams.containsKey("title")) {
					title = (String) eventParams.get("title");
				}

				if (eventParams.containsKey("titleper")) {
					titleper = (String) eventParams.get("titleper");
				}

				if (eventParams.containsKey("titleic")) {
					titleic = (String) eventParams.get("titleic");
				}

				if (eventParams.containsKey("titlemeoc")) {
					titlemeoc = (String) eventParams.get("titlemeoc");
				}

				ManageTeams pt;
				try {
					pt = new ManageTeams();
					pt.CreateTeam(Integer.valueOf(totalMembers), title,
							titleic, titleper, "host", "path", "protocol",
							titlemeoc);
				} catch (NamingException e) {

					e.printStackTrace();
				}

			}
			if (QueryID == 21) {
				System.out.println("Map Location Request DB");
				// System.out
				// .println("Start 1");
				String userid = null;
				Iterator entries = params.getQueryParamList().entrySet()
						.iterator();
				while (entries.hasNext()) {
					Map.Entry entry = (Map.Entry) entries.next();
					String key = (String) entry.getKey();
					String value = (String) entry.getValue();
					if (key.equalsIgnoreCase("userid")) {
						// System.out
						// .println(" Query ID = 1 Find Userid Value Values = Key = "
						// + key + ", Value = " + value);
						userid = value;
					}
					// System.out.println(" Query ID = 1 Values = Key = " + key
					// + ", Value = " + value);
				}
				// System.out.println("Start 1-1");
				OCEocALTDTO pRes = null;
				try {
					YMAP2DView pDataFusion = new YMAP2DView();
					pRes = pDataFusion.find2DDetailsForOC(Long.valueOf(userid));
				} catch (Exception ex) {
					System.out.println("Error EV-> " + ex.getMessage());
				}

				// System.out.println("Start 1-2");
				ESponderUserDTO actionSnapshot = new ESponderUserDTO();
				actionSnapshot.setId(new Long(0));
				// crisis context dto as attachement
				ESponderEventPublisher<ESponderDFQueryResponseEvent> publisher = new ESponderEventPublisher<ESponderDFQueryResponseEvent>(
						ESponderDFQueryResponseEvent.class);
				ESponderDFQueryResponseEvent pEventSend = new ESponderDFQueryResponseEvent();
				pEventSend.setMapPositions(pRes);
				pEventSend.setRequestID(RequestID);
				pEventSend.setQueryID(QueryID);
				pEventSend.setIMEI(IMEI);
				pEventSend.setpResponse(null);
				ESponderResource pServices = null;
				try {
					pServices = new ESponderResource();
				} catch (NamingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				CrisisContextDTO pFind = pServices.getCrisisRemoteService()
						.findCrisisContextDTOById(
								new Long(pRes.getEocCrisisContext()));
				pEventSend.setEventAttachment(pFind);
				pEventSend.setJournalMessage("Test Event");
				pEventSend.setEventSeverity(SeverityLevelDTO.SERIOUS);
				pEventSend.setEventTimestamp(new Date());
				pEventSend.setEventSeverity(SeverityLevelDTO.UNDEFINED);
				pEventSend.setEventSource(actionSnapshot);
				// System.out.println("Start 1-3");
				// ObjectMapper mapper = new ObjectMapper();
				// try {
				// String sphereJSON = mapper.writeValueAsString(pEventSend);
				// //System.out.println("Sending request reply for positions ok ! Json : "+sphereJSON);
				// } catch (JsonGenerationException e1) {
				// // TODO Auto-generated catch block
				// e1.printStackTrace();
				// } catch (JsonMappingException e1) {
				// // TODO Auto-generated catch block
				// e1.printStackTrace();
				// } catch (IOException e1) {
				// // TODO Auto-generated catch block
				// e1.printStackTrace();
				// }
				try {
					// System.out.println("Start 1-4");
					publisher.publishEvent(pEventSend);
					// System.out.println("Start 1-5");
					Thread.currentThread().sleep(100);
				} catch (EventListenerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}// query 1 end

			// start query 3
			if (QueryID == 3) {

				System.out.println("\nNew Sensor Measurements from Team\n");

				try {
					HashMap<String, Object> eventParams = params
							.getQueryParamList();
					if (eventParams.containsKey("sensorMeasurements")) {
						String measurements = (String) eventParams
								.get("sensorMeasurements");
						if (measurements != null) {
							List<String> frSensorRecords = Arrays
									.asList(measurements.split("b"));

							for (String frSensorRecord : frSensorRecords) {
								List<String> frSensorMeasurements = Arrays
										.asList(frSensorRecord.split("a"));
								if (frSensorMeasurements.size() > 0) {

									Long frID = Long
											.valueOf(frSensorMeasurements
													.get(0));
									List<SensorDTO> sensorList = findSensorsforFR(frID);

									if (sensorList != null) {
										for (String frSensorMeasurement : frSensorMeasurements) {
											if (frSensorMeasurement
													.contains(",")) {
												// this contains the measurement
												String[] singleMeasurement = frSensorMeasurement
														.split(",");
												Integer sensorType = Integer
														.valueOf(singleMeasurement[0]);
												if (sensorType != null) {
													Long sensorID = findSensorByTypeFromList(
															sensorType,
															sensorList);
													if (sensorID != null) {

														SensorSnapshotDTO snapshot = new SensorSnapshotDTO();
														snapshot.setPeriod(new PeriodDTO(
																Long.valueOf(singleMeasurement[4]),
																Long.valueOf(singleMeasurement[5])));
														snapshot.setSensor(sensorID);
														snapshot.setStatisticType(MeasurementStatisticTypeEnumDTO.MEAN);
														snapshot.setValue(singleMeasurement[1]);
														snapshot.setStatus(SensorSnapshotStatusDTO.WORKING);
														sensorService
																.createSensorSnapshotRemote(
																		snapshot,
																		new Long(
																				0));

														SensorSnapshotDTO snapshotmin = new SensorSnapshotDTO();
														snapshotmin
																.setPeriod(new PeriodDTO(
																		Long.valueOf(singleMeasurement[4]),
																		Long.valueOf(singleMeasurement[5])));
														snapshotmin
																.setSensor(sensorID);
														snapshotmin
																.setStatisticType(MeasurementStatisticTypeEnumDTO.MINIMUM);
														snapshotmin
																.setValue(singleMeasurement[2]);
														snapshotmin
																.setStatus(SensorSnapshotStatusDTO.WORKING);
														sensorService
																.createSensorSnapshotRemote(
																		snapshotmin,
																		new Long(
																				0));

														SensorSnapshotDTO snapshotmax = new SensorSnapshotDTO();
														snapshotmax
																.setPeriod(new PeriodDTO(
																		Long.valueOf(singleMeasurement[4]),
																		Long.valueOf(singleMeasurement[5])));
														snapshotmax
																.setSensor(sensorID);
														snapshotmax
																.setStatisticType(MeasurementStatisticTypeEnumDTO.MAXIMUM);
														snapshotmax
																.setValue(singleMeasurement[3]);
														snapshotmax
																.setStatus(SensorSnapshotStatusDTO.WORKING);
														sensorService
																.createSensorSnapshotRemote(
																		snapshotmax,
																		new Long(
																				0));

														try {
															ProfileManager pProfileManager = new ProfileManager();
															// DF
															// -----------------------------------------------
															// Set rule profile
															// for sensor
															// measurement
															SetRuleEngineType(
																	pProfileManager
																			.GetProfileNameForSensor()
																			.getSzProfileType(),
																	pProfileManager
																			.GetProfileNameForSensor()
																			.getSzProfileName());
															SetDecisionTable(
																	pProfileManager
																			.GetProfileNameForSensor()
																			.getSzDecisionTable(),
																	pProfileManager
																			.GetProfileNameForSensor()
																			.isHasDecisionTable());
															// Load Rules
															LoadKnowledge();
															// Get current
															// snapshot
															// DF
															// ------------------------------------------------------------------

															RuleUtilities pUtils = new RuleUtilities();
															SensorTypeHelper pSensorType = new SensorTypeHelper();
															pSensorType
																	.SetType(pUtils
																			.CheckSensorType(snapshot
																					.getSensor()));
															if (snapshot
																	.getStatus() == SensorSnapshotStatusDTO.WORKING)
																pSensorType
																		.SetWorking(1);
															else
																pSensorType
																		.SetWorking(0);
															pSensorType
																	.SetSensorID(snapshot
																			.getSensor());

															// Add current
															// snapshot
															AddDTOObjects(pSensorType);
															AddDTOObjects(snapshot);
															AddDTOObjects(snapshotmin);
															AddDTOObjects(snapshotmax);
															// Run Rules, this
															// function
															// automatically
															// load the utility
															// classes
															ProcessRules();
															// Empty the session
															ReinitializeEngine();
														} catch (Exception ex) {

														}
													}
												}
											}
										}
									} else
										System.out
												.println("No sensors have been found for the selected fr");
								}
								System.out
										.println("Measurements for actor finished");

							}
						}
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
				}

			}
			// start query 2
			if (QueryID == 2) {
				System.out.println("New Actor Location Request");
				String userid = null;
				String datefrom = null;
				String dateto = null;
				String szalt = null;
				String szlog = null;
				String szlat = null;
				Iterator entries = params.getQueryParamList().entrySet()
						.iterator();
				while (entries.hasNext()) {
					Map.Entry entry = (Map.Entry) entries.next();
					String key = (String) entry.getKey();
					String value = (String) entry.getValue();
					if (key.equalsIgnoreCase("alt")) {

						szalt = value;
						// System.out
						// .println("Alt "+value);
					}

					if (key.equalsIgnoreCase("long")) {

						szlog = value;
						// System.out
						// .println("Long "+value);
					}

					if (key.equalsIgnoreCase("lat")) {

						szlat = value;
						// System.out
						// .println("Lat "+value);
					}
					if (key.equalsIgnoreCase("userid")) {

						userid = value;
						// System.out
						// .println("UserID "+value);
					}

					if (key.equalsIgnoreCase("datefrom")) {

						datefrom = value;
						// System.out
						// .println("Date From "+value);
					}

					if (key.equalsIgnoreCase("dateto")) {

						dateto = value;
						// System.out
						// .println("Date To "+value);
					}

				}

				// SensorMeasurementEnvelopeDTO envelope =
				// (SensorMeasurementEnvelopeDTO) pEvent.getEventAttachment();
				Long frID = Long.valueOf(userid);
				Long sensorID = sensorService
						.findLocationSensorIDByFRRemote(frID);
				if (sensorID != null) {
					// for(SensorMeasurementDTO sm : envelope.getMeasurements())
					// {

					PointDTO point = new PointDTO();// ((LocationSensorMeasurementDTO)sm).getPoint();
					point.setAltitude(new BigDecimal(szalt));
					point.setLatitude(new BigDecimal(szlat));
					point.setLongitude(new BigDecimal(szlog));
					SphereDTO sphere = new SphereDTO(new PointDTO(
							point.getLatitude(), point.getLongitude(),
							point.getAltitude()), new BigDecimal(0),
							"FRLocation-"
									+ String.valueOf(new Date().getTime()));
					SphereDTO persistedSphere = null;
					try {
						persistedSphere = (SphereDTO) genericService
								.createEntityRemote(sphere, Long.valueOf(1));
						System.out.println("Sphere has been created");
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
					if (persistedSphere != null) {
						ActorSnapshotDTO aSnapshot = new ActorSnapshotDTO();
						aSnapshot.setActor(frID);
						aSnapshot.setLocationArea(persistedSphere);
						aSnapshot.setPeriod(new PeriodDTO(Long
								.valueOf(datefrom), Long.valueOf(dateto)));
						aSnapshot.setStatus(ActorSnapshotStatusDTO.ACTIVE);
						ActorSnapshotDTO snapshotPersisted = actorService
								.createActorSnapshotRemote(aSnapshot,
										Long.valueOf(1));
						if (snapshotPersisted.getId() != null) {

							try {

								ESponderEventPublisher<CreateActorSnapshotEvent> publisherqid2 = new ESponderEventPublisher<CreateActorSnapshotEvent>(
										CreateActorSnapshotEvent.class, true);
								CreateActorSnapshotEvent event = new CreateActorSnapshotEvent();
								event.setEventAttachment(snapshotPersisted);
								event.setEventSeverity(SeverityLevelDTO.UNDEFINED);
								event.setEventTimestamp(new Date());
								event.setJournalMessage("Create actor snapshot for actor with id:"
										+ snapshotPersisted.getActor());
								event.setJournalMessageInfo("Create actor snapshot");
								publisherqid2.publishEvent(event);
								publisherqid2.CloseConnection();
								System.out.println("Event Published");
								Thread.currentThread().sleep(100);
							} catch (EventListenerException e) {

							} catch (InterruptedException e) {

							}

						}
						// System.out
						// .println("Actor Snapshot has been created");
						else
							System.out
									.println("Actor Snapshot has NOT been created");
					}

					// }

				}
				// else
				// return null;

				// old
				// YMAP2DView pDataFusion = new YMAP2DView();
				// OCEocALTDTO pRes = pDataFusion.find2DDetailsForOC(Long
				// .valueOf(userid));
				// ESponderUserDTO actionSnapshot = new ESponderUserDTO();
				// actionSnapshot.setId(new Long(0));
				// // crisis context dto as attachement
				// ESponderEventPublisher<ESponderDFQueryResponseEvent>
				// publisher = new
				// ESponderEventPublisher<ESponderDFQueryResponseEvent>(
				// ESponderDFQueryResponseEvent.class);
				// ESponderDFQueryResponseEvent pEventSend = new
				// ESponderDFQueryResponseEvent();
				// // pEventSend.setMapPositions(pRes);
				// pEventSend.setRequestID(RequestID);
				// pEventSend.setQueryID(QueryID);
				// pEventSend.setIMEI(IMEI);
				// pEventSend.setpResponse(null);
				// ESponderResource pServices = new ESponderResource();
				// // CrisisContextDTO
				// //
				// pFind=pServices.getCrisisRemoteService().findCrisisContextDTOById(new
				// // Long(pRes.getEocCrisisContext()));
				// // pEventSend.setEventAttachment(pFind);
				// pEventSend.setJournalMessage("Test Event");
				// pEventSend.setEventSeverity(SeverityLevelDTO.SERIOUS);
				// pEventSend.setEventTimestamp(new Date());
				// pEventSend.setEventSeverity(SeverityLevelDTO.UNDEFINED);
				// pEventSend.setEventSource(actionSnapshot);
				// ObjectMapper mapper = new ObjectMapper();
				// try {
				// String sphereJSON = mapper.writeValueAsString(pEventSend);
				// //
				// System.out.println("Sending request reply for positions ok ! Json : "+sphereJSON);
				// } catch (JsonGenerationException e1) {
				// // TODO Auto-generated catch block
				// e1.printStackTrace();
				// } catch (JsonMappingException e1) {
				// // TODO Auto-generated catch block
				// e1.printStackTrace();
				// } catch (IOException e1) {
				// // TODO Auto-generated catch block
				// e1.printStackTrace();
				// }
				// try {
				// publisher.publishEvent(pEventSend);
				// Thread.currentThread().sleep(20000);
				// } catch (EventListenerException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// } catch (InterruptedException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }

			}// query 2 end

		}
	}

	private CreateActorSnapshotEvent createActorSnapshotEventMaker(
			ActorSnapshotDTO snapshot) {
		CreateActorSnapshotEvent event = new CreateActorSnapshotEvent();
		event.setEventAttachment(snapshot);
		event.setEventSeverity(SeverityLevelDTO.UNDEFINED);
		event.setEventTimestamp(new Date());
		event.setJournalMessage("Create actor snapshot for actor with id:"
				+ snapshot.getActor());
		event.setJournalMessageInfo("Create actor snapshot");
		return event;
	}

	private List<SensorDTO> findSensorsforFR(Long frID) {

		List<SensorDTO> listOfSensors = sensorService
				.findAllSensorsByActorRemote(frID);

		return listOfSensors;
	}

	private Long findSensorByTypeFromList(int sensorType,
			List<SensorDTO> sensorList) {

		String className = null;

		switch (sensorType) {
		case 0: // ActivitySensor
			className = ActivitySensorDTO.class.getSimpleName();
			break;
		case 1: // BodyTemperatureSensor
			className = BodyTemperatureSensorDTO.class.getSimpleName();
			break;
		case 2: // BreathRateSensor
			className = BreathRateSensorDTO.class.getSimpleName();
			break;
		case 3: // HeartBeatRateSensor
			className = HeartBeatRateSensorDTO.class.getSimpleName();
			break;
		case 4: // EnvTemperatureSensor
			className = EnvironmentalSensorDTO.class.getSimpleName();
			break;
		case 5: // MethaneSensor
			className = MethaneSensorDTO.class.getSimpleName();
			break;
		case 6: // CarbonMonoxideSensor
			className = CarbonMonoxideSensorDTO.class.getSimpleName();
			break;
		case 7: // CarbonDioxideSensor
			className = CarbonDioxideSensorDTO.class.getSimpleName();
			break;
		case 8: // OxygenSensor
			className = OxygenSensorDTO.class.getSimpleName();
			break;
		case 9: // HydrogenSulfideSensor
			className = HydrogenSulfideSensorDTO.class.getSimpleName();
			break;
		case 10: // LPSSensor
			className = LPSSensorDTO.class.getSimpleName();
			break;
		case 11: // LocationSensor
			className = LocationSensorDTO.class.getSimpleName();
			break;
		default:
			System.out
					.println("Unknown type of sensor contained in a measurement returned from a mobile device");
			break;
		}

		if (className != null) {
			for (SensorDTO sensor : sensorList) {
				if (className.equalsIgnoreCase(sensor.getClass()
						.getSimpleName()))
					return sensor.getId();
			}
		}
		return null;
	}

}
