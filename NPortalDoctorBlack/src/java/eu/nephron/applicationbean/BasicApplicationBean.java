/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.applicationbean;

import eu.nephron.cache.alert.db.aledb;
import eu.nephron.cache.alerts.alertscached;
import eu.nephron.model.AlertsModel;
import eu.nephron.model.DSSResults;
import eu.nephron.model.Patient;
import eu.nephron.model.Sensors;
import eu.nephron.opt.ws.NephronOptionWS_Service;
import eu.nephron.secure.ws.WAKDAlertsManager_Service;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import javax.xml.ws.WebServiceRef;
import org.hl7.cache.ws.Cachealerts_Service;
import org.hl7.cache.ws.Cacheconnection_Service;
import org.hl7.cache.ws.Cachedoctors_Service;
import org.hl7.cache.ws.Cachepatients_Service;
import org.hl7.cache.ws.Hl7Doctors;
import org.primefaces.event.SelectEvent;
import org.primefaces.extensions.component.timeline.Timeline;

import org.primefaces.extensions.model.timeline.DefaultTimeLine;
import org.primefaces.extensions.model.timeline.DefaultTimelineEvent;
import org.primefaces.extensions.model.timeline.TimelineEvent;

import org.primefaces.model.ScheduleEvent;

/**
 *
 * @author tdim
 */
@ManagedBean
@ViewScoped
public class BasicApplicationBean {
    
    private NephronOptionWS_Service service_5=new NephronOptionWS_Service();
private List<String> pValuesDB=new ArrayList();
    static {
        setManufacturers(new String[41]);
        getManufacturers()[0] = "BP-sys lower than normal range";
        getManufacturers()[1] = "BP-sys higher than normal range";
        getManufacturers()[2] = "ALARM: BP-sys high";
        getManufacturers()[3] = "BP-dia lower than normal range";
        getManufacturers()[4] = "BP-dia higher than normal range";
        getManufacturers()[5] = "ALARM: BP-dia high";
        getManufacturers()[6] = "Measured weight lower than threshold";
        getManufacturers()[7] = "Measured weight higher than threshold";
        getManufacturers()[8] = "Weight trend exceeds threshold";
        getManufacturers()[9] = "Detected blood pump deviation";
        getManufacturers()[10] = "Detected dialysate pump deviation";
        getManufacturers()[11] = "ALARM: pressure (high) excess, all pumps stopped";
        getManufacturers()[12] = "ALARM: pressure (low) excess, all pumps stopped";
        getManufacturers()[13] = "Temperature (high) excess at BTSo, all pumps stopped";
        getManufacturers()[14] = "Temperature (low) excess at BTSo, all pumps stopped";
        getManufacturers()[15] = "Temperature (high) excess at BTSi, all pumps stopped";
        getManufacturers()[16] = "Temperature (low) excess at BTSi, all pumps stopped";
        getManufacturers()[17] = "A sorbent disfunction was detected, patient was instructed to replace the cartridge";
        getManufacturers()[18] = "pH low";
        getManufacturers()[19] = "pH high";
        getManufacturers()[20] = "pH out of normal range";
        getManufacturers()[21] = "pH decrease out of normal range";
        getManufacturers()[22] = "pH increase out of normal range";
        getManufacturers()[23] = "Calcium out of normal range";
        getManufacturers()[24] = "Fast change in Calcium was detected in trend analysis";
        getManufacturers()[25] = "Patient was asked to replace sorbent cartridge";
        getManufacturers()[26] = "K+ out of normal range";
        getManufacturers()[27] = "Potassium lower than normal range";
        getManufacturers()[28] = "Patient was asked to replace sorbent cartridge";
        getManufacturers()[29] = "Potassium higher than normal range";
        getManufacturers()[30] = "Potassium trend deviates from threshold";
        getManufacturers()[31] = "A increasing fast change in K+ trend was detected  in trend analysis.";
        getManufacturers()[32] = "Patient was asked to replace sorbent cartridge due to low potassium removal";
        getManufacturers()[33] = "pH dialysate out of range";
        getManufacturers()[34] = "Urea removal low";
        getManufacturers()[35] = "Urea out of normal range (high)";
        getManufacturers()[36] = "Urea increase out of normal range (high)";
        getManufacturers()[37] = "Detected air bubble in bloodline, all pumps stopped";
        getManufacturers()[38] = "Detected leakage in bloodline, all pumps stopped";
        getManufacturers()[39] = "Detected air bubble in dialysate line, dialysate circuit stopped";
        getManufacturers()[40] = "Detected fluid leakage in dialysate circuit, dialysate circuit stopped ";
    }
    private Cachealerts_Service service_4 = new Cachealerts_Service();
    private Cacheconnection_Service service_3 = new Cacheconnection_Service();
    private WAKDAlertsManager_Service service_2 = new WAKDAlertsManager_Service();
    private Cachedoctors_Service service_1 = new Cachedoctors_Service();
    private Cachepatients_Service service = new Cachepatients_Service();
    //java.util.List<eu.nephron.secure.ws.WakdAlerts> pAlerts = new ArrayList();
    java.util.List<org.hl7.cache.ws.Hl7Patients> ppat = new ArrayList();
    Hl7Doctors pd = null;
    private Long DoctorIDFromWeb = new Long(3);
    private String PKIKey = "WS";
    java.util.List<org.hl7.cache.ws.Hl7Alerts> paler = new ArrayList();
    //Bean Variables for Data Tables
    private List<Patient> filteredPatient;
    private List<Patient> Patients;
    long ifind;
    private Patient selectedPatient;
    private Patient[] selectedPatients;
    private String useridforedit;
    private List<DSSResults> sumups;
    private List<DSSResults> sumupsc;
    private List<DSSResults> filteredsumups;
    // bean variables for personal details view of patient
    private String fathername;
    private String firstname;
    private String lastname;
    private SelectItem[] alertOptions;
    private static String[] manufacturers;
    private String doctorname;
    java.util.List<org.hl7.cache.ws.Hl7Cacheconnectivity> pcon = new ArrayList();
    private Date dateFromM = new Date();
    private Date dateToM = new Date();
    private List<DSSResults> sumups2;
    //Time LINE
    private List<org.primefaces.extensions.model.timeline.Timeline> timelines;
    private TimelineEvent selectedEvent;
    private String eventStyle = "box";
    private String axisPosition = "bottom";
    private boolean showNavigation = true;
    String previousid = "NA";
    private List<DSSResults> sumupsoriginal;
    private int ilevel;
    private String fathernamep;
    private String firstnamep;
    private String lastnamep;
    private int imstatus;
    private String middlename;
    private String title;
    private String companyemail;
    private String companymobile;
    private String companyphone;
    private String email;
    private String mobile;
    private String phone;
    private String administrativearea;
    private String city;
    private String code;
    private String floor;
    private String postcode;
    private String street;
    private String streetno;
    private String patienteducationlevel;
    private String patientmaritalstatus;
    private boolean boneview=false;
    private boolean bmview=true;
    
    
    //TIME LINE
    /**
     * Creates a new instance of BasicApplicationBean
     */
    public BasicApplicationBean() {
        String duserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("DUserID");
        DoctorIDFromWeb = Long.parseLong(duserid);
      //  pAlerts = returnAllAlertsForLast7Days();
        pcon = findAllconnection();
        pd = finddoctors(DoctorIDFromWeb);
        doctorname = pd.getFirstName() + " " + pd.getLastName();
        sumups = new ArrayList<DSSResults>();
        sumupsc = new ArrayList<DSSResults>();
        sumups2 = new ArrayList<DSSResults>();
        sumupsoriginal = new ArrayList<DSSResults>();
        Patients = new ArrayList<Patient>();
        if (duserid == null) {
        } else {
            DoctorIDFromWeb = Long.parseLong(duserid);
            String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");
        }
        ppat = findPatientsByDoctor(DoctorIDFromWeb);
        for (int i = 0; i < ppat.size(); i++) {
            Patient pp = new Patient();
            pp.setFirstName(ppat.get(i).getFirstName());
            pp.setLastName(ppat.get(i).getLastName());
            pp.setFatherName(ppat.get(i).getFatherName());
            pp.setId(ppat.get(i).getId());
            Patients.add(pp);
        }

        List<AlertsModel> palerts = new ArrayList<AlertsModel>();
        sumups.clear();
        sumupsc.clear();

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -2);
        Date n7 = new Date();
        n7 = cal.getTime();


        paler = findAlertsByDoctorAndDateFrom(DoctorIDFromWeb, n7.getTime());

        for (int i = 0; i < paler.size(); i++) {
            DSSResults pNew = new DSSResults();
            pNew.setId(paler.get(i).getId());
            pNew.setRuleresults(FindPatientNameForIMEI(paler.get(i).getSzvalue()));
            pNew.setRuleresults1(paler.get(i).getAlertmsg());
            pNew.setRuleresults3(paler.get(i).getSzvalue());
            Date ndate = new Date();
            ndate.setTime(paler.get(i).getCDate());
            pNew.setRuleresults2(ndate.toString());
            pNew.setRulename(paler.get(i).getCDate().toString());
            pNew.setStylest("font-size: 11px!important;color:green!important;font-family: Helvetica, Verdana, sans-serif;\n" +
"                    font-style: oblique;\n" +
"                    text-shadow: none;");
            
            sumups.add(pNew);
        }
        sumups=reverseList(sumups);
        alertOptions = createFilterOptions(manufacturers);
        LoadFromDB();
        for(int ix=0;ix<sumups.size();ix++)
        {
            for(int ixx=0;ixx<pValuesDB.size();ixx++)
            {
                if(sumups.get(ix).getRuleresults1().equalsIgnoreCase(pValuesDB.get(ixx)))
                {
                    DSSResults pitem = sumups.get(ix);
                    pitem.setStylest("font-size: 11px!important;color:red!important;font-family: Helvetica, Verdana, sans-serif;\n" +
"                    font-style: oblique;\n" +
"                    text-shadow: none;");
                    sumups.set(ix, pitem);
                    sumupsc.add(pitem);
                }
            }
        }
        
    }

    public void increment() {
        System.out.println("Refresh Doctors !! ");
        String duserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("DUserID");
        DoctorIDFromWeb = Long.parseLong(duserid);
      
        pcon = findAllconnection();
        pd = finddoctors(DoctorIDFromWeb);
        doctorname = pd.getFirstName() + " " + pd.getLastName();
      
        Patients = new ArrayList<Patient>();
        if (duserid == null) {
        } else {
            //DoctorIDFromWeb = Long.parseLong(duserid);
            //String puserid = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("PUserID");
        }
        ppat = findPatientsByDoctor(DoctorIDFromWeb);
        for (int i = 0; i < ppat.size(); i++) {
            Patient pp = new Patient();
            pp.setFirstName(ppat.get(i).getFirstName());
            pp.setLastName(ppat.get(i).getLastName());
            pp.setFatherName(ppat.get(i).getFatherName());
            pp.setId(ppat.get(i).getId());
            Patients.add(pp);
        }
    }
    
    
    public String loadmeasurments() {

        return "Measurments_1?faces-redirect=true";
    }

    public String LoadDash() {

        return "DashBoard?faces-redirect=true";
    }

    public String LoadPatients() {

        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("PUserID", String.valueOf(ifind));

        System.out.println("Patient ID to EDIT : " + String.valueOf(ifind));

        //initialize_TimeLine();

        //LoadPatient();

        return "DashBoard_Patient?faces-redirect=true";
    }

    public String LoadPatientDetails() {

        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("PUserID", String.valueOf(ifind));

        System.out.println("Patient ID to EDIT : " + String.valueOf(ifind));

//        initialize_TimeLine();

        //      LoadPatient();

        return "PatientDetails?faces-redirect=true";
    }

    //tomd
    public String LoadAlertsPage() {

        dateFromM = new Date();
        dateToM = new Date();

        sumups2.clear();
        for (int i = 0; i < sumupsoriginal.size(); i++) {
            Date date = new Date();
            Long dtime = Long.parseLong(sumupsoriginal.get(i).getRulename());//.getRulername());
            date.setTime(dtime);
            if (date.getTime() > dateFromM.getTime()) {
                if (date.getTime() < dateToM.getTime()) {
                    sumups2.add(sumupsoriginal.get(i));
                }
            }
        }



        return "Alerts?faces-redirect=true";
    }

    public void UpdateRecords(ActionEvent actionEvent) {
        sumups2.clear();
        for (int i = 0; i < sumupsoriginal.size(); i++) {
            Date date = new Date();
            Long dtime = Long.parseLong(sumupsoriginal.get(i).getRulename());//.getRulername());
            date.setTime(dtime);
            if (date.getTime() > dateFromM.getTime()) {
                if (date.getTime() < dateToM.getTime()) {
                    sumups2.add(sumupsoriginal.get(i));
                }
            }
        }
    }

    private SelectItem[] createFilterOptions(String[] data) {
        
        Arrays.sort(data);
        
        SelectItem[] options = new SelectItem[data.length + 1];

        options[0] = new SelectItem("", "Select");
        for (int i = 0; i < data.length; i++) {
            options[i + 1] = new SelectItem(data[i], data[i]);
        }

        return options;
    }

    public void edituser1() {
        String dd=FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("userID").toString();
        System.out.println("Calling listener : "+dd);
        
        System.out.println("Calling listener");
        ifind = Long.valueOf(dd);//selectedPatient.getId();
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("PUserID", String.valueOf(ifind));
        System.out.println("Calling listener : " + ifind);
        System.out.println("Calling Action");
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("PUserID", String.valueOf(ifind));

        System.out.println("Calling Action : " + ifind);
        //ifind = selectedPatient.getId();

        for (int i = 0; i < pcon.size(); i++) {
            if (pcon.get(i).getIpatientID().toString().equalsIgnoreCase(String.valueOf(ifind))) {
                System.out.println("Set IMEI : " + pcon.get(i).getMobileimei());
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("PHONEIMEI", pcon.get(i).getMobileimei());
            }
        }
        System.out.println("Patient ID to EDIT : " + String.valueOf(ifind));
    }

    public String edituser() {


        return "DashBoard_Patient?faces-redirect=true";
    }

    //
    private String FindPatientName() {
        return null;
    }

    private String FindPatientNameForIMEI(String IMEI) {

        String patientname = "";
        for (int i = 0; i < pcon.size(); i++) {
            if (pcon.get(i).getMobileimei().equalsIgnoreCase(IMEI)) {

                for (int id = 0; id < Patients.size(); id++) {
                    if (Patients.get(id).getId().toString().equalsIgnoreCase(pcon.get(i).getIpatientID().toString())) {
                        patientname = Patients.get(id).getFirstName() + " " + Patients.get(id).getLastName();
                        return patientname;
                    }
                }
            }
        }
        return patientname;
    }

    private boolean IsIMEIOnMyPatients(String IMEI) {
        for (int i = 0; i < pcon.size(); i++) {

            if (pcon.get(i).getMobileimei().equalsIgnoreCase(IMEI)) {
                return true;
            }
        }
        return false;
    }

    private Hl7Doctors finddoctors(java.lang.Object id) {
        org.hl7.cache.ws.Cachedoctors port = service_1.getCachedoctorsPort();
        return port.finddoctors(id);
    }

    private java.util.List<eu.nephron.secure.ws.WakdAlerts> returnAllAlertsForLast7Days() {
        eu.nephron.secure.ws.WAKDAlertsManager port = service_2.getWAKDAlertsManagerPort();
        return port.returnAllAlertsForLast7Days();
    }

    /**
     * @return the manufacturers
     */
    public static String[] getManufacturers() {
        return manufacturers;
    }

    /**
     * @param aManufacturers the manufacturers to set
     */
    public static void setManufacturers(String[] aManufacturers) {
        manufacturers = aManufacturers;
    }

    public void LoadPatient() {
        System.out.println("Load Patient");
        for (int is = 0; is < ppat.size(); is++) {
            if (ppat.get(is).getId().toString().equalsIgnoreCase(String.valueOf(ifind))) {
                System.out.println("Patient Found !!");
                fathername = ppat.get(is).getFatherName();
                firstname = ppat.get(is).getFirstName();
                lastname = ppat.get(is).getLastName();
                middlename = ppat.get(is).getMiddleName();
                title = ppat.get(is).getTitle();
                companyemail = ppat.get(is).getCompanyEmail();
                companymobile = ppat.get(is).getCompanyMobile();
                companyphone = ppat.get(is).getCompanyPhone();
                email = ppat.get(is).getEmail();
                mobile = ppat.get(is).getMobile();
                phone = ppat.get(is).getPhone();
                administrativearea = ppat.get(is).getAdministrativeArea();
                city = ppat.get(is).getCity();
                code = ppat.get(is).getCode();
                floor = ppat.get(is).getFloor();
                postcode = ppat.get(is).getPostCode();
                street = ppat.get(is).getStreet();
                streetno = ppat.get(is).getStreetNo();
            }
        }
    }
    //TIME LINE

    public void initialize_TimeLine() {
        timelines = new ArrayList<org.primefaces.extensions.model.timeline.Timeline>();
        Calendar cal = Calendar.getInstance();
        Date ddate = new Date();
        cal.setTime(ddate);
        org.primefaces.extensions.model.timeline.Timeline timeline = new DefaultTimeLine("customEventTimeline", "Nephron Timeline");
        String IMEI = "";

        sumups2.clear();
        //sumupsoriginal.clear();
        for (int dd = 0; dd < sumupsoriginal.size(); dd++) {
            if (sumupsoriginal.get(dd).getRuleresults3().equalsIgnoreCase(IMEI)) {
                sumups2.add(sumupsoriginal.get(dd));
                //      sumupsoriginal.add(sumups.get(dd));
            }
        }



        for (int i = 0; i < paler.size(); i++) {
            if (paler.get(i).getIdpatient().toString().equalsIgnoreCase(String.valueOf(ifind))) {
                String title = paler.get(i).getAlertmsg();
                IMEI = paler.get(i).getSzvalue();
                Date ddated = new Date();
                ddated.setTime(paler.get(i).getCDate());
                TimelineEvent timelineEvent = new DefaultTimelineEvent(title, ddated);
                timelineEvent.setStyleClass("alarm");
                timeline.addEvent(timelineEvent);
            }
        }


        timelines.add(timeline);
    }

    public void onEventSelect(SelectEvent event) {
        selectedEvent = (TimelineEvent) event.getObject();
    }

    public List<org.primefaces.extensions.model.timeline.Timeline> getTimelines() {

        return timelines;
    }

    public TimelineEvent getSelectedEvent() {
        return selectedEvent;
    }

    /**
     * @return the eventStyle
     */
    public String getEventStyle() {
        return eventStyle;
    }

    /**
     * @param eventStyle the eventStyle to set
     */
    public void setEventStyle(String eventStyle) {
        this.eventStyle = eventStyle;
    }

    /**
     * @return the axisPosition
     */
    public String getAxisPosition() {
        return axisPosition;
    }

    /**
     * @param axisPosition the axisPosition to set
     */
    public void setAxisPosition(String axisPosition) {
        this.axisPosition = axisPosition;
    }

    /**
     * @return the showNavigation
     */
    public boolean isShowNavigation() {
        return showNavigation;
    }

    /**
     * @param showNavigation the showNavigation to set
     */
    public void setShowNavigation(boolean showNavigation) {
        this.showNavigation = showNavigation;
    }

    //TIME LINE
    /**
     * @return the ilevel
     */
    public int getIlevel() {
        return ilevel;
    }

    /**
     * @param ilevel the ilevel to set
     */
    public void setIlevel(int ilevel) {
        this.ilevel = ilevel;
    }

    /**
     * @return the fathernamep
     */
    public String getFathernamep() {
        return fathernamep;
    }

    /**
     * @param fathernamep the fathernamep to set
     */
    public void setFathernamep(String fathernamep) {
        this.fathernamep = fathernamep;
    }

    /**
     * @return the firstnamep
     */
    public String getFirstnamep() {
        return firstnamep;
    }

    /**
     * @param firstnamep the firstnamep to set
     */
    public void setFirstnamep(String firstnamep) {
        this.firstnamep = firstnamep;
    }

    /**
     * @return the lastnamep
     */
    public String getLastnamep() {
        return lastnamep;
    }

    /**
     * @param lastnamep the lastnamep to set
     */
    public void setLastnamep(String lastnamep) {
        this.lastnamep = lastnamep;
    }

    /**
     * @return the imstatus
     */
    public int getImstatus() {
        return imstatus;
    }

    /**
     * @param imstatus the imstatus to set
     */
    public void setImstatus(int imstatus) {
        this.imstatus = imstatus;
    }

    /**
     * @return the middlename
     */
    public String getMiddlename() {
        return middlename;
    }

    /**
     * @param middlename the middlename to set
     */
    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the companyemail
     */
    public String getCompanyemail() {
        return companyemail;
    }

    /**
     * @param companyemail the companyemail to set
     */
    public void setCompanyemail(String companyemail) {
        this.companyemail = companyemail;
    }

    /**
     * @return the companymobile
     */
    public String getCompanymobile() {
        return companymobile;
    }

    /**
     * @param companymobile the companymobile to set
     */
    public void setCompanymobile(String companymobile) {
        this.companymobile = companymobile;
    }

    /**
     * @return the companyphone
     */
    public String getCompanyphone() {
        return companyphone;
    }

    /**
     * @param companyphone the companyphone to set
     */
    public void setCompanyphone(String companyphone) {
        this.companyphone = companyphone;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * @param mobile the mobile to set
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the administrativearea
     */
    public String getAdministrativearea() {
        return administrativearea;
    }

    /**
     * @param administrativearea the administrativearea to set
     */
    public void setAdministrativearea(String administrativearea) {
        this.administrativearea = administrativearea;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the floor
     */
    public String getFloor() {
        return floor;
    }

    /**
     * @param floor the floor to set
     */
    public void setFloor(String floor) {
        this.floor = floor;
    }

    /**
     * @return the postcode
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * @param postcode the postcode to set
     */
    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return the streetno
     */
    public String getStreetno() {
        return streetno;
    }

    /**
     * @param streetno the streetno to set
     */
    public void setStreetno(String streetno) {
        this.streetno = streetno;
    }

    /**
     * @return the patienteducationlevel
     */
    public String getPatienteducationlevel() {
        return patienteducationlevel;
    }

    /**
     * @param patienteducationlevel the patienteducationlevel to set
     */
    public void setPatienteducationlevel(String patienteducationlevel) {
        this.patienteducationlevel = patienteducationlevel;
    }

    /**
     * @return the patientmaritalstatus
     */
    public String getPatientmaritalstatus() {
        return patientmaritalstatus;
    }

    /**
     * @param patientmaritalstatus the patientmaritalstatus to set
     */
    public void setPatientmaritalstatus(String patientmaritalstatus) {
        this.patientmaritalstatus = patientmaritalstatus;
    }

    /**
     * @return the dateFromM
     */
    public Date getDateFromM() {
        return dateFromM;
    }

    /**
     * @param dateFromM the dateFromM to set
     */
    public void setDateFromM(Date dateFromM) {
        this.dateFromM = dateFromM;
    }

    /**
     * @return the dateToM
     */
    public Date getDateToM() {
        return dateToM;
    }

    /**
     * @param dateToM the dateToM to set
     */
    public void setDateToM(Date dateToM) {
        this.dateToM = dateToM;
    }

    /**
     * @return the sumups2
     */
    public List<DSSResults> getSumups2() {
        return sumups2;
    }

    /**
     * @param sumups2 the sumups2 to set
     */
    public void setSumups2(List<DSSResults> sumups2) {
        this.sumups2 = sumups2;
    }

    //////////////////////////////////////
    private java.util.List<org.hl7.cache.ws.Hl7Patients> findPatientsByDoctor(java.lang.Long doctorID) {
        org.hl7.cache.ws.Cachepatients port = service.getCachepatientsPort();
        return port.findPatientsByDoctor(doctorID);
    }

    private java.util.List<org.hl7.cache.ws.Hl7Patients> findAllPatient() {
        org.hl7.cache.ws.Cachepatients port = service.getCachepatientsPort();
        return port.findAllPatient();
    }

    private java.util.List<org.hl7.cache.ws.Hl7Cacheconnectivity> findAllconnection() {
        org.hl7.cache.ws.Cacheconnection port = service_3.getCacheconnectionPort();
        return port.findAllconnection();
    }

    private java.util.List<org.hl7.cache.ws.Hl7Alerts> findAlertsByDoctorAndDateFrom(java.lang.Long doctorID, java.lang.Long dateFrom) {

        java.util.List<org.hl7.cache.ws.Hl7Alerts> pList = new ArrayList();

        java.util.List<org.hl7.cache.ws.Hl7Cacheconnectivity> dd = findAllconnection();
        sumups2.clear();
        sumupsoriginal.clear();

        for (int i = 0; i < ppat.size(); i++) {
            Long pid = ppat.get(i).getId();
            for (int d = 0; d < dd.size(); d++) {
                if (dd.get(d).getIpatientID().toString().equalsIgnoreCase(String.valueOf(pid))) {
                    try {
                        aledb pdb = new aledb();
                        pdb.SetIMEI(dd.get(d).getMobileimei());
                        pdb.LoadFromFile();
                        ArrayList<alertscached> plist = pdb.getList();

                        for (int ix = 0; ix < plist.size(); ix++) {

                            org.hl7.cache.ws.Hl7Alerts aa = new org.hl7.cache.ws.Hl7Alerts();
                            aa.setIddoctor(plist.get(ix).getIddoctor());
                            aa.setIdpatient(plist.get(ix).getIdpatient());
                            aa.setSzvalue(plist.get(ix).getSzvalue());
                            aa.setCDate(plist.get(ix).getcDate());
                            aa.setAlertmsg(plist.get(ix).getAlertmsg());
                            aa.setAlertmsg1(plist.get(ix).getAlertmsg1());
                            if (plist.get(ix).getcDate() >= dateFrom) {
                                pList.add(aa);
                            }

                            DSSResults pNew = new DSSResults();
                            pNew.setId(aa.getId());
                            pNew.setRuleresults(FindPatientNameForIMEI(aa.getSzvalue()));
                            pNew.setRuleresults1(aa.getAlertmsg());
                            pNew.setRuleresults3(aa.getSzvalue());
                            Date ndate = new Date();
                            ndate.setTime(aa.getCDate());
                            pNew.setRuleresults2(ndate.toString());
                            pNew.setRulename(aa.getCDate().toString());
                            sumups2.add(pNew);
                            sumupsoriginal.add(pNew);
                        }
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(BasicApplicationBean.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(BasicApplicationBean.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(BasicApplicationBean.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }

        return pList;
//        org.hl7.cache.ws.Cachealerts port = service_4.getCachealertsPort();
//        return port.findAlertsByDoctorAndDateFrom(doctorID, dateFrom);
        //findAllconnection

    }
    ///////////Get/Set

    private List reverseList(List myList) {
    List invertedList = new ArrayList();
    for (int i = myList.size() - 1; i >= 0; i--) {
        invertedList.add(myList.get(i));
    }
    return invertedList;
}
    
    
    /**
     * @return the DoctorIDFromWeb
     */
    public Long getDoctorIDFromWeb() {
        return DoctorIDFromWeb;
    }

    /**
     * @param DoctorIDFromWeb the DoctorIDFromWeb to set
     */
    public void setDoctorIDFromWeb(Long DoctorIDFromWeb) {
        this.DoctorIDFromWeb = DoctorIDFromWeb;
    }

    /**
     * @return the PKIKey
     */
    public String getPKIKey() {
        return PKIKey;
    }

    /**
     * @param PKIKey the PKIKey to set
     */
    public void setPKIKey(String PKIKey) {
        this.PKIKey = PKIKey;
    }

    /**
     * @return the filteredPatient
     */
    public List<Patient> getFilteredPatient() {
        return filteredPatient;
    }

    /**
     * @param filteredPatient the filteredPatient to set
     */
    public void setFilteredPatient(List<Patient> filteredPatient) {
        this.filteredPatient = filteredPatient;
    }

    /**
     * @return the Patients
     */
    public List<Patient> getPatients() {
        return Patients;
    }

    /**
     * @param Patients the Patients to set
     */
    public void setPatients(List<Patient> Patients) {
        this.Patients = Patients;
    }

    /**
     * @return the selectedPatient
     */
    public Patient getSelectedPatient() {
        return selectedPatient;
    }

    /**
     * @param selectedPatient the selectedPatient to set
     */
    public void setSelectedPatient(Patient selectedPatient) {
        this.selectedPatient = selectedPatient;
    }

    /**
     * @return the selectedPatients
     */
    public Patient[] getSelectedPatients() {
        return selectedPatients;
    }

    /**
     * @param selectedPatients the selectedPatients to set
     */
    public void setSelectedPatients(Patient[] selectedPatients) {
        this.selectedPatients = selectedPatients;
    }

    /**
     * @return the useridforedit
     */
    public String getUseridforedit() {
        return useridforedit;
    }

    /**
     * @param useridforedit the useridforedit to set
     */
    public void setUseridforedit(String useridforedit) {
        this.useridforedit = useridforedit;
    }

    /**
     * @return the sumups
     */
    public List<DSSResults> getSumups() {
        return sumups;
    }

    /**
     * @param sumups the sumups to set
     */
    public void setSumups(List<DSSResults> sumups) {
        this.sumups = sumups;
    }

    /**
     * @return the filteredsumups
     */
    public List<DSSResults> getFilteredsumups() {
        return filteredsumups;
    }

    /**
     * @param filteredsumups the filteredsumups to set
     */
    public void setFilteredsumups(List<DSSResults> filteredsumups) {
        this.filteredsumups = filteredsumups;
    }

    /**
     * @return the fathername
     */
    public String getFathername() {
        return fathername;
    }

    /**
     * @param fathername the fathername to set
     */
    public void setFathername(String fathername) {
        this.fathername = fathername;
    }

    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * @return the alertOptions
     */
    public SelectItem[] getAlertOptions() {
        
        return alertOptions;
    }

    /**
     * @param alertOptions the alertOptions to set
     */
    public void setAlertOptions(SelectItem[] alertOptions) {
        this.alertOptions = alertOptions;
    }

    /**
     * @return the doctorname
     */
    public String getDoctorname() {
        return doctorname;
    }

    /**
     * @param doctorname the doctorname to set
     */
    public void setDoctorname(String doctorname) {
        this.doctorname = doctorname;
    }
    
    ///////////
    private void LoadFromDB()
    {
        java.util.List<eu.nephron.opt.ws.Optionsall> popt=nephronOptionWSfindAll();
        pValuesDB.clear();
        for(int i=0;i<popt.size();i++)
        {
            if(popt.get(i).getOpttype().equalsIgnoreCase("alerts"))
            {
                pValuesDB.add(popt.get(i).getOptvalue());
            }
        }
    }

    private java.util.List<eu.nephron.opt.ws.Optionsall> nephronOptionWSfindAll() {
        eu.nephron.opt.ws.NephronOptionWS port = service_5.getNephronOptionWSPort();
        return port.nephronOptionWSfindAll();
    }

    /**
     * @return the sumupsc
     */
    public List<DSSResults> getSumupsc() {
        return sumupsc;
    }

    /**
     * @param sumupsc the sumupsc to set
     */
    public void setSumupsc(List<DSSResults> sumupsc) {
        this.sumupsc = sumupsc;
    }

    /**
     * @return the boneview
     */
    public boolean isBoneview() {
        return boneview;
    }

    /**
     * @param boneview the boneview to set
     */
    public void setBoneview(boolean boneview) {
        this.boneview = boneview;
    }

    /**
     * @return the bmview
     */
    public boolean isBmview() {
        return bmview;
    }

    /**
     * @param bmview the bmview to set
     */
    public void setBmview(boolean bmview) {
        this.bmview = bmview;
    }
    
}
