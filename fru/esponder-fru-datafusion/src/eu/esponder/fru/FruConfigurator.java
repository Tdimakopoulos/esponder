package eu.esponder.fru;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class FruConfigurator {
	
	private static Properties defaultProps;
	
	public FruConfigurator() {
		FruConfigurator.defaultProps = new Properties();
	}

	public void init() throws FileNotFoundException, IOException {
		// create and load default properties
		FileInputStream in = new FileInputStream(FruConfigProperties.FRU_PROPERTIES_CONFIGURATION_FULLNAME);
		defaultProps.load(in);
		in.close();
		
	}
	
	public String getProperty(String key) {
		return defaultProps.getProperty(key);
	}
	
}
