/*
 * 
 */
package eu.esponder.controller.crisis.weatherstation.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import eu.esponder.controller.ActionAuditInterceptor;
import eu.esponder.controller.crisis.weatherstation.WeatherStationRemoteService;
import eu.esponder.controller.crisis.weatherstation.WeatherStationService;
import eu.esponder.controller.mapping.ESponderMappingService;
import eu.esponder.controller.persistence.CrudService;
import eu.esponder.dto.model.crisis.CrisisContextDTO;
import eu.esponder.dto.model.crisis.CrisisContextParameterDTO;
import eu.esponder.dto.model.crisis.SubCrisisDTO;
import eu.esponder.dto.model.crisis.resource.WeatherStationDTO;
import eu.esponder.dto.model.crisis.resource.plan.CrisisResourcePlanDTO;
import eu.esponder.dto.model.crisis.resource.plan.PlannableResourcePowerDTO;
import eu.esponder.dto.model.snapshot.CrisisContextSnapshotDTO;
import eu.esponder.model.crisis.CrisisContext;
import eu.esponder.model.crisis.CrisisContextParameter;
import eu.esponder.model.crisis.SubCrisis;
import eu.esponder.model.crisis.resource.WeatherStation;
import eu.esponder.model.crisis.resource.plan.CrisisResourcePlan;
import eu.esponder.model.crisis.resource.plan.PlannableResourcePower;
import eu.esponder.model.snapshot.CrisisContextSnapshot;

// TODO: Auto-generated Javadoc
// 
// 
/**
 * The Class CrisisBean.
 */
@Stateless
public class WeatherStationBean implements WeatherStationService, WeatherStationRemoteService{

	/** The crisis crud service. */
	@EJB
	private CrudService<WeatherStation> wsCrudService;

	/** The mapping service. */
	@EJB
	ESponderMappingService mappingService;


	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#findCrisisContextDTOById(java.lang.Long)
	 */
	@Override
	public WeatherStationDTO findWeatherStationDTOById(Long weatherStationID) {
		WeatherStation weatherStation = findWeatherStationById(weatherStationID);
		return (WeatherStationDTO) mappingService.mapESponderEntity(weatherStation, WeatherStationDTO.class);
	}	

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#findCrisisContextById(java.lang.Long)
	 */
	@Override
	public WeatherStation findWeatherStationById(Long weatherStationID) {
		return (WeatherStation) wsCrudService.find(WeatherStation.class, weatherStationID);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#findCrisisResourcePlanByTitleRemote(java.lang.String, java.lang.Long)
	 */
	@Override
	public WeatherStationDTO findWeatherStationByTitleRemote(String wsTitle, Long userId) {
		WeatherStation weatherStation = findWeatherStationByTitle(wsTitle, userId);
		return (WeatherStationDTO) mappingService.mapESponderEntity(weatherStation, WeatherStationDTO.class);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#findCrisisResourcePlanByTitle(java.lang.String, java.lang.Long)
	 */
	@Override
	public WeatherStation findWeatherStationByTitle(String wsTitle, Long userId) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("title", wsTitle);
		return (WeatherStation) wsCrudService.findSingleWithNamedQuery("WeatherStation.findByTitle", params);
	}
	
	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#createCrisisContextRemote(eu.esponder.dto.model.crisis.CrisisContextDTO, java.lang.Long)
	 */
	@Override
	public WeatherStationDTO createWeatherStationRemote(WeatherStationDTO weatherStationDTO, Long userID) {
		WeatherStation weatherStation = (WeatherStation) mappingService.mapESponderEntityDTO(weatherStationDTO, WeatherStation.class);
		weatherStation = createWeatherStation(weatherStation, userID);
		WeatherStationDTO weatherStationPersisted = (WeatherStationDTO) mappingService.mapESponderEntity(weatherStation, WeatherStationDTO.class);
		return weatherStationPersisted;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#createCrisisContext(eu.esponder.model.crisis.CrisisContext, java.lang.Long)
	 */
	@Override
	@Interceptors(ActionAuditInterceptor.class)
	public WeatherStation createWeatherStation(WeatherStation weatherStation, Long userID) {
		return (WeatherStation) wsCrudService.create(weatherStation);
	}

	//-------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#deletePlannableResourcePowerRemote(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteWeatherStationRemote(Long weatherStationID, Long userID) {
		this.deleteWeatherStation(weatherStationID, userID);
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#deletePlannableResourcePower(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void deleteWeatherStation(Long weatherStationID, Long userID) {
		wsCrudService.delete(WeatherStation.class, weatherStationID);
	}

	// -------------------------------------------------------------------------

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisRemoteService#updateCrisisContextRemote(eu.esponder.dto.model.crisis.CrisisContextDTO, java.lang.Long)
	 */
	@Override
	public WeatherStationDTO updateWeatherStationRemote( WeatherStationDTO weatherStationDTO, Long userID) {
		WeatherStation weatherStation = (WeatherStation) mappingService.mapESponderEntityDTO(weatherStationDTO, WeatherStation.class);
		weatherStation = updateWeatherStation(weatherStation, userID);
		weatherStationDTO = (WeatherStationDTO) mappingService.mapESponderEntity(weatherStation, WeatherStationDTO.class);
		return weatherStationDTO;
	}

	/* (non-Javadoc)
	 * @see eu.esponder.controller.crisis.CrisisService#updateCrisisContext(eu.esponder.model.crisis.CrisisContext, java.lang.Long)
	 */
	@Override
	public WeatherStation updateWeatherStation(WeatherStation weatherStation, Long userID) {
		WeatherStation weatherStationPersisted = findWeatherStationById(weatherStation.getId());
		mappingService.mapEntityToEntity(weatherStation, weatherStationPersisted);
		weatherStationPersisted = wsCrudService.update(weatherStationPersisted);
		return weatherStationPersisted;
	}


}
