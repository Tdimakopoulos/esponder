/*
 * 
 */
package eu.esponder.osgi.service.event;

import javax.ejb.Local;



/**
 * The Interface EventServiceManager.
 */
@Local
public interface EventServiceManager extends EventServiceManagerRemote{

	public void StartServer();
	public void StopServer();
	
}
