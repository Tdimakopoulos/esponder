/*
 * 
 */
package eu.esponder.dto.model.snapshot.status;


// TODO: Auto-generated Javadoc
/**
 * The Enum OperationsCentreSnapshotStatusDTO.
 */
public enum OperationsCentreSnapshotStatusDTO {
	
	/** The stationary. */
	STATIONARY,
	
	/** The moving. */
	MOVING
}
