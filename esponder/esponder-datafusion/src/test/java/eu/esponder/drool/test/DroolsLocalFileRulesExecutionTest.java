package eu.esponder.drool.test;

/**
 * @author tdim
 *
 */
import javax.naming.NamingException;

import org.testng.annotations.Test;

//import eu.esponder.controller.configuration.ESponderConfigurationRemoteService;
import eu.esponder.df.ruleengine.core.RuleEngineLocalRules;
//import eu.esponder.dto.model.config.ESponderConfigParameterDTO;
//import eu.esponder.test.ResourceLocator;
/*
 * The test rule Sensor-test.drl needs to have the following contents
 * 
 * rule "Rule 01"   
 * when
 *	eval( 1==1 )
 * then
 *	System.out.println( "Rule 01 Works" );
 * end
 * 
 * Make sure the path is the same as define in the DroolSettings.java or in the Database
 */
public class DroolsLocalFileRulesExecutionTest {

	@Test
	public void ExecuteLocalFileTest() throws Exception
	{
		System.out.println("==============================================");
		System.out.println("      Local Rule Execution Test               ");
		
		RuleEngineLocalRules dEngine= new RuleEngineLocalRules();
		
		//full path of rule. 
		dEngine.InferenceEngineAddKnowledge(new String[]{"c:\\tmprepo\\Sensor-test.drl"});
		dEngine.InferenceEngineAddObjects(new Object[0]);
		dEngine.InferenceEngineRunAssets();
		
		System.out.println("==============================================");
	}
	
	@Test
	public void Connect() throws NamingException, ClassNotFoundException{
		System.out.println("Getting Options from DB");
		//eu.esponder.controller.configuration.ESponderConfigurationService dService;
	//	ESponderConfigurationRemoteService cfgService = ServiceLocator.getResource("esponder/ESponderConfigurationBean/remote");
		//ESponderConfigurationRemoteService cfgService = ResourceLocator.lookup("esponder/ESponderConfigurationBean/remote");
		
		
//		SensorRemoteService sensorService = ServiceLocator.getResource("esponder/SensorRemoteService/remote");
		//Get DroolsTempRep
		//ESponderConfigParameterDTO dTempRep=cfgService.findESponderConfigByNameRemote("DroolTempRep");
	//	String szTmpRepositoryDirectoryPath = dTempRep.getParameterValue();
	}
}
