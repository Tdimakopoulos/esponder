/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exus.esponder.receive.processor;

import esponder.biomed.lib.RegularDataBlock;
import eu.exus.esponder.udp.client.udpclient;
import exus.esponder.filedatabase.handlers.PersistSensorsReadings;
import exus.esponder.settings.settingsmanager;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tdim
 */
public class processor {

    byte[] binput = null;
    String szinput = "";

public void ProcessLPSCommand(String frame) throws UnknownHostException, IOException {


        settingsmanager pp = new settingsmanager();
        pp.ReadValues();
        String[] lines = frame.split(",");
        System.out.println(lines[3].subSequence(1, lines[3].length()));//43.89 - lat
        System.out.println(lines[4]);//5.83 - long
        String[] alt = lines[5].split("]");
        System.out.println(alt[0].substring(0, alt[0].length() - 1));//350.7 - alt
        udpclient pcl = new udpclient();
        System.out.println(pp.getAip() + Integer.valueOf(pp.getAport()) + pp.getSheader()+"," + alt[0].substring(0, alt[0].length() - 1) + "," + lines[4] + "," + lines[3].subSequence(1, lines[3].length()));
        pcl.SendToServerLPS(pp.getLPSIP(), Integer.valueOf(pp.getLPSPort()), pp.getSheader()+"," + alt[0].substring(0, alt[0].length() - 1) + "," + lines[4] + "," + lines[3].subSequence(1, lines[3].length()));

    }
    
    public void ProcessUICommand(String frame) {
        settingsmanager pp = new settingsmanager();
        pp.ReadValues();
        try {
            //PersistSensorsReadings ppo = new PersistSensorsReadings();
            //ppo.SaveUI(frame);
            udpclient pcl = new udpclient();
            pcl.SendToServer(pp.getUiip(), Integer.valueOf(pp.getUiport()), pp.getSheader(), "UI ", frame);
            if (pp.getMobUIip().equalsIgnoreCase("0")) {
            } else {
                udpclient pclm = new udpclient();
                pclm.SendToServer(pp.getMobUIip(), Integer.valueOf(pp.getMobUIport()), pp.getSheader(), "UI ", frame);
            }
            System.out.println("** UI Receive : " + frame);
        } catch (FileNotFoundException ex) {
            System.out.println("File Save Error" + ex.getMessage());
        } catch (IOException ex) {
            System.out.println("File Save Error" + ex.getMessage());
        }
    }

    public void ProcessBiomedACommand(RegularDataBlock dataBlocks[]) {
        PersistSensorsReadings ppo = new PersistSensorsReadings();
        settingsmanager pp = new settingsmanager();
        pp.ReadValues();
        for (int idb = 0; idb < dataBlocks.length; idb++) {
            int iflv = dataBlocks[idb].getValues().length;
            for (int iif = 0; iif < iflv; iif++) {
                if (dataBlocks[idb].getName().equalsIgnoreCase("ecg")) {
                    //try {
                    // ppo.SaveECG(dataBlocks[idb].getValues()[iif]);
                    //     udpclient pcl=new udpclient();
                    //       pcl.SendToServer(pp.getAip(), Integer.valueOf(pp.getAport()), pp.getSheader(), "ECG ", dataBlocks[idb].getValues()[iif]);
                    // } catch (FileNotFoundException ex) {
                    //   System.out.println("File Save Error"+ex.getMessage());
                    // } catch (IOException ex) {
                    //   System.out.println("File Save Error"+ex.getMessage());
                    // } 
                    //catch (ClassNotFoundException ex) {
                    // System.out.println("File Save Error"+ex.getMessage());
                    //}
                } else {
                    try {
                        udpclient pcl = new udpclient();
                        pcl.SendToServerA(pp.getAip(), Integer.valueOf(pp.getAport()), pp.getSheader(), dataBlocks[idb].getName(), dataBlocks[idb].getValues()[iif]);
                        if (pp.getMobABip().equalsIgnoreCase("0")) {
                        } else {
                            udpclient pclM = new udpclient();
                            pclM.SendToServerAMob(pp.getMobABip(), Integer.valueOf(pp.getMobABport()), pp.getSheader(), dataBlocks[idb].getName(), dataBlocks[idb].getValues()[iif]);
                        }
                    } catch (FileNotFoundException ex) {
                        System.out.println("File Save Error" + ex.getMessage());
                    } catch (IOException ex) {
                        System.out.println("File Save Error" + ex.getMessage());
                    }
                }
            }
        }
    }
}
