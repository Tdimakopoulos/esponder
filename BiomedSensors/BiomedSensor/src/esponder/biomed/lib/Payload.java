/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package esponder.biomed.lib;

/**
 *
 * @author afa
 */
public class Payload {
    protected int size = 0;
    protected byte buffer[] = null;
    
    public Payload(){
        
    }
    
    public Payload(int size, byte buffer[]){
        this.size = size;
        this.buffer = buffer;
    }

    /**
     * @return the size
     */
    public int getSize() {
        return size;
    }

    /**
     * @param size the size to set
     */
    public void setSize(int size) {
        this.size = size;
    }

    /**
     * @return the buffer
     */
    public byte[] getBuffer() {
        return buffer;
    }

    /**
     * @param buffer the buffer to set
     */
    public void setBuffer(byte[] buffer, int offset, int length) {
        this.buffer = new byte[length];
        System.arraycopy(buffer, offset, this.buffer, 0, length);
    }
}
