/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.nephron.admin.pki;

/**
 *
 * @author tdim
 */
public class pkidetails {
    private Long id;
    private String role;
    private String type;
    private String username;
    private String pkikey;
    private String ppkeykey;
    private String psecret;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the role
     */
    public String getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the pkikey
     */
    public String getPkikey() {
        return pkikey;
    }

    /**
     * @param pkikey the pkikey to set
     */
    public void setPkikey(String pkikey) {
        this.pkikey = pkikey;
    }

    /**
     * @return the ppkeykey
     */
    public String getPpkeykey() {
        return ppkeykey;
    }

    /**
     * @param ppkeykey the ppkeykey to set
     */
    public void setPpkeykey(String ppkeykey) {
        this.ppkeykey = ppkeykey;
    }

    /**
     * @return the psecret
     */
    public String getPsecret() {
        return psecret;
    }

    /**
     * @param psecret the psecret to set
     */
    public void setPsecret(String psecret) {
        this.psecret = psecret;
    }
}
