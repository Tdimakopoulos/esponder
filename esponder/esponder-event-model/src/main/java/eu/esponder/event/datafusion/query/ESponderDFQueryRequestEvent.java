/*
 * 
 */
package eu.esponder.event.datafusion.query;

import eu.esponder.dto.model.QueryParamsDTO;
import eu.esponder.dto.model.user.ESponderUserDTO;

import eu.esponder.event.datafusion.model.QueryRequestEvent;
import eu.esponder.event.model.ESponderEvent;



// TODO: Auto-generated Javadoc
/**
 * The Class CreateActionEvent.
 */
public class ESponderDFQueryRequestEvent extends ESponderEvent<ESponderUserDTO> implements QueryRequestEvent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2006035559724305579L;

	private Long QueryID;
	private Long RequestID;
	private QueryParamsDTO params;
	
String IMEI;
	
	public String getIMEI() {
		return IMEI;
	}

	public void setIMEI(String iMEI) {
		IMEI = iMEI;
	}
	
	public Long getQueryID() {
		return QueryID;
	}

	public void setQueryID(Long queryID) {
		QueryID = queryID;
	}

	public Long getRequestID() {
		return RequestID;
	}

	public void setRequestID(Long requestID) {
		RequestID = requestID;
	}

	public QueryParamsDTO getParams() {
		return params;
	}

	public void setParams(QueryParamsDTO params) {
		this.params = params;
	}

	/**
	 * Instantiates a new creates the action event.
	 */
	public ESponderDFQueryRequestEvent(){
		setJournalMessageInfo("ESponder DF Query Request Event");
		setJournalMessage("ESponder DF Query Request Event");
	}
}
