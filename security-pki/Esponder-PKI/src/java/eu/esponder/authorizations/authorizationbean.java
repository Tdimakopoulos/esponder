/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.authorizations;

import eu.esponder.authorizations.authodetails;
import eu.esponder.role.ws.EsRoleService;
import eu.esponder.role.ws.RoleDB;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class authorizationbean {
    
    private String szRole;
    private String szFnc;
    private List<authodetails> pList = new ArrayList<authodetails>();
    private authodetails selectedDoctor;
    private String role;
    private String fnc;
    /**
     * Creates a new instance of authorizationbean
     */
    public authorizationbean() {
        refresh();
    }
    
    public void refresh() {
        pList.clear();
        java.util.List<eu.esponder.role.ws.RoleDB> pfind = esFindAll();        
        for (int i = 0; i < pfind.size(); i++) {
            authodetails item = new authodetails();
            item.setFnc(pfind.get(i).getAccessfunction());
            item.setRole(pfind.get(i).getRole());
            item.setId(pfind.get(i).getId());
            pList.add(item);
        }
    }
    
     public String adduser() {
        
        setFnc("");
        setRole("");
        return "Add_fnc?faces-redirect=true";
    }

    public String saveadduser() {
        //generateUserKeyPair(username, organization, role, new Long(-1));
        esCreateValues(getRole(), getFnc());
        refresh();
        return "Authorization?faces-redirect=true";
    }
    
 public String canceladduser() {

        return "Authorization?faces-redirect=true";
    }

    public String deletedoctor()
    {
        Revoke(selectedDoctor.getId());
        refresh();
        return "Authorization?faces-redirect=true";
        
    }
    private void Revoke(Long ID)
    {
        esRemove(esFind(ID));
    }
    /**
     * @return the szRole
     */
    public String getSzRole() {
        return szRole;
    }

    /**
     * @param szRole the szRole to set
     */
    public void setSzRole(String szRole) {
        this.szRole = szRole;
    }

    /**
     * @return the szFnc
     */
    public String getSzFnc() {
        return szFnc;
    }

    /**
     * @param szFnc the szFnc to set
     */
    public void setSzFnc(String szFnc) {
        this.szFnc = szFnc;
    }

    /**
     * @return the pList
     */
    public List<authodetails> getpList() {
        return pList;
    }

    /**
     * @param pList the pList to set
     */
    public void setpList(List<authodetails> pList) {
        this.pList = pList;
    }

    /**
     * @return the selectedDoctor
     */
    public authodetails getSelectedDoctor() {
        return selectedDoctor;
    }

    /**
     * @param selectedDoctor the selectedDoctor to set
     */
    public void setSelectedDoctor(authodetails selectedDoctor) {
        this.selectedDoctor = selectedDoctor;
    }
    
    private void esCreateValues(java.lang.String arg0, java.lang.String arg1) {
        EsRoleService service = new EsRoleService();
        eu.esponder.role.ws.RoleService port = service.getRoleServicePort();
        port.esCreateValues(arg0, arg1);
    }
    
    private RoleDB esFind(java.lang.Object id) {
        EsRoleService service = new EsRoleService();
        eu.esponder.role.ws.RoleService port = service.getRoleServicePort();
        return port.esFind(id);
    }
    
    private java.util.List<eu.esponder.role.ws.RoleDB> esFindAll() {
        EsRoleService service = new EsRoleService();
        eu.esponder.role.ws.RoleService port = service.getRoleServicePort();
        return port.esFindAll();
    }
    
    private void esRemove(eu.esponder.role.ws.RoleDB roleDB) {
        EsRoleService service = new EsRoleService();
        eu.esponder.role.ws.RoleService port = service.getRoleServicePort();
        port.esRemove(roleDB);
    }

    /**
     * @return the role
     */
    public String getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * @return the fnc
     */
    public String getFnc() {
        return fnc;
    }

    /**
     * @param fnc the fnc to set
     */
    public void setFnc(String fnc) {
        this.fnc = fnc;
    }
}
