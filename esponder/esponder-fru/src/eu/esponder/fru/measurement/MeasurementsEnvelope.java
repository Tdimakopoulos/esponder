package eu.esponder.fru.measurement;

import java.util.ArrayList;
import java.util.List;

import eu.esponder.fru.event.EventAttachment;

public class MeasurementsEnvelope extends EventAttachment {

	List<SensorMeasurement> envelope;

	public MeasurementsEnvelope() {
		envelope = new ArrayList<SensorMeasurement>();
	}

	public List<SensorMeasurement> getEnvelope() {
		return envelope;
	}

	public void setEnvelope(List<SensorMeasurement> envelope) {
		this.envelope = envelope;
	}

	public void add(SensorMeasurement sm) {
		envelope.add(sm);
	}

	public SensorMeasurement get(int position) {
		return envelope.get(position);
	}

	public int size() {
		return envelope.size();
	}

	public void clear() {
		envelope.clear();
	}
}
