/*
 * 
 */
package eu.esponder.dto.model.crisis.action;



// TODO: Auto-generated Javadoc
// 
/**
 * The Enum ActionOperationEnumDTO.
 */
public enum ActionStageEnumDTO {

	Initial,
	
	Approved,
	
	NotApproved,
	
	Done,
	
	PartiallyDone,
	
	NotExecuted,
		
	InProgress
}
