package org.exus.df.statistics;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.exus.df.statistics.model.DaySeries;
import org.exus.df.statistics.model.DayTimeSeries;
import org.exus.df.statistics.model.InputSeries;
import org.exus.df.statistics.model.OutputStatistics;

public class TrendAnalysis {
	List<InputSeries> pInput=new ArrayList<InputSeries>();
	List<OutputStatistics> pOutput=new ArrayList<OutputStatistics>();
	
	
	
	public void GenerateHourStatistics() throws ParseException
	{
		Double davgsum = null;
		Double maxValue =null;
		Double minValue =null;
		int inumber=0;
		TimeSeriesManager ptsm = new TimeSeriesManager();
		pOutput.clear();
		ptsm.setDstart(pInput.get(0).getpDate());
		ptsm.setDend(pInput.get(pInput.size()-1).getpDate());
		ptsm.GenerateTimeSeries();
		List<DayTimeSeries> pTimeList=ptsm.getpTime();
		
		for(int i=0;i<pTimeList.size();i++)
		{
			Long lstart=pTimeList.get(i).getdTimeStart();
			Long lend=pTimeList.get(i).getdTimeEnd();
			OutputStatistics pAdd=new OutputStatistics();
			for(int ip=0;ip<pInput.size();ip++)
			{
				if((pInput.get(ip).getlDate()<lend)&&(pInput.get(ip).getlDate()>lstart))
				{
					davgsum=(davgsum+pInput.get(ip).getdValue());
					inumber++;
					
					if(minValue!=null)
					{
						if(pInput.get(ip).getdValue()<minValue)
							minValue=pInput.get(ip).getdValue();
					}else
					{
						minValue=pInput.get(ip).getdValue();
					}
					if(maxValue!=null)
					{
						if(pInput.get(ip).getdValue()>maxValue)
							maxValue=pInput.get(ip).getdValue();
					}else
					{
						maxValue=pInput.get(ip).getdValue();
					}
				}
			}
			Double davg=davgsum/inumber;
			pAdd.setAvgValue(davg);
			pAdd.setMaxValue(maxValue);
			pAdd.setMinValue(minValue);
			pAdd.setdStart(lstart);
			pAdd.setdEnd(lend);
			pAdd.setpStart(new Date(lstart));
			pAdd.setpEnd(new Date(lend));
			pOutput.add(pAdd);
		}
		
	}
	
	
	public void GenerateDayStatistics() throws ParseException
	{
		Double davgsum = null;
		Double maxValue =null;
		Double minValue =null;
		int inumber=0;
		TimeSeriesManager ptsm = new TimeSeriesManager();
		pOutput.clear();
		ptsm.setDstart(pInput.get(0).getpDate());
		ptsm.setDend(pInput.get(pInput.size()-1).getpDate());
		ptsm.GenerateDaySeries();
		List<DaySeries> pTimeList=ptsm.getpDates();
		
		for(int i=0;i<pTimeList.size();i++)
		{
			Long lstart=pTimeList.get(i).getdStart();
			Long lend=pTimeList.get(i).getdEnd();
			OutputStatistics pAdd=new OutputStatistics();
			for(int ip=0;ip<pInput.size();ip++)
			{
				if((pInput.get(ip).getlDate()<lend)&&(pInput.get(ip).getlDate()>lstart))
				{
					davgsum=(davgsum+pInput.get(ip).getdValue());
					inumber++;
					
					if(minValue!=null)
					{
						if(pInput.get(ip).getdValue()<minValue)
							minValue=pInput.get(ip).getdValue();
					}else
					{
						minValue=pInput.get(ip).getdValue();
					}
					if(maxValue!=null)
					{
						if(pInput.get(ip).getdValue()>maxValue)
							maxValue=pInput.get(ip).getdValue();
					}else
					{
						maxValue=pInput.get(ip).getdValue();
					}
				}
			}
			Double davg=davgsum/inumber;
			pAdd.setAvgValue(davg);
			pAdd.setMaxValue(maxValue);
			pAdd.setMinValue(minValue);
			pAdd.setdStart(lstart);
			pAdd.setdEnd(lend);
			pAdd.setpStart(new Date(lstart));
			pAdd.setpEnd(new Date(lend));
			pOutput.add(pAdd);
		}
	}
	
	public List<InputSeries> getpInput() {
		return pInput;
	}




	public void setpInput(List<InputSeries> pInput) {
		this.pInput = pInput;
	}




	public List<OutputStatistics> getpOutput() {
		return pOutput;
	}




	public void setpOutput(List<OutputStatistics> pOutput) {
		this.pOutput = pOutput;
	}




	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
