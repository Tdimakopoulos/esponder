/*
 * 
 */
package eu.esponder.event.model.snapshot.action;

import eu.esponder.dto.model.snapshot.action.ActionPartSnapshotDTO;
import eu.esponder.event.model.snapshot.SnapshotEvent;


// TODO: Auto-generated Javadoc
/**
 * The Class ActionPartSnapshotEvent.
 *
 * @param <T> the generic type
 */
public abstract class ActionPartSnapshotEvent<T extends ActionPartSnapshotDTO> extends SnapshotEvent<T> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -1431871320640276284L;
	
}
