package eu.esponder.datafusion.hserver;

import java.util.Set;

import eu.esponder.controller.crisis.resource.ActorRemoteService;
import eu.esponder.dto.model.crisis.alternative.ActorFRALTDTO;
import eu.esponder.dto.model.crisis.alternative.FRTeamALTDTO;
import eu.esponder.dto.model.crisis.alternative.OCEocALTDTO;
import eu.esponder.dto.model.crisis.alternative.OCMeocALTDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRCDTO;
import eu.esponder.dto.model.crisis.resource.ActorFRDTO;
import eu.esponder.dto.model.snapshot.resource.ActorSnapshotDTO;
import eu.esponder.dto.model.snapshot.resource.OperationsCentreSnapshotDTO;
import eu.esponder.test.ResourceLocator;

public class HierarchySnapshotUpdater {


	private ActorRemoteService actorService = ResourceLocator
			.lookup("esponder/ActorBean/remote");


	public void identifyAndUpdateSnapshot(OCEocALTDTO eoc, Object origSnapshot) {

		boolean frc = true;

		if(ActorSnapshotDTO.class.isInstance(origSnapshot)) {
			Long objectID = ((ActorSnapshotDTO)origSnapshot).getActor();
			String tempClass = actorService.findActorClassByIdRemote(objectID); //returns class simple name

			if(tempClass != null) {
				if(tempClass.equalsIgnoreCase(ActorFRCDTO.class.getSimpleName()))
					frc = true;
				else if(tempClass.equalsIgnoreCase(ActorFRDTO.class.getSimpleName()))
					frc = false;
				
				if(frc)
					System.out.println("looking to update a chief");
				else
					System.out.println("looking to update a fr");

				for(int i=0;i<eoc.getSubordinateMEOCs().size();i++)
				{
					OCMeocALTDTO meoc = (OCMeocALTDTO) (eoc.getSubordinateMEOCs().toArray())[i];
					boolean found = false;
					for(int j=0; j<meoc.getIncidentCommander().getFrTeams().size(); j++) {
						FRTeamALTDTO team = (FRTeamALTDTO) (meoc.getIncidentCommander().getFrTeams().toArray())[j];
						if(frc) {
							if(team.getFrchief().getId().toString().equalsIgnoreCase(objectID.toString())) {
								found = true;
								team.getFrchief().setSnapshot((ActorSnapshotDTO) origSnapshot);
								System.out.println("Found chief in ram object, updating snapshot");
							}
						}
						else {
							for(int k=0; k<team.getFrchief().getSubordinates().size(); k++) {
								ActorFRALTDTO fr = (ActorFRALTDTO) (team.getFrchief().getSubordinates().toArray())[k];
								if(fr.getId().toString().equalsIgnoreCase(objectID.toString())) {
									found =true;
									fr.setSnapshot((ActorSnapshotDTO) origSnapshot);
									System.out.println("Found fr in ram object, updating snapshot");
								}
							}
						}
						if(found)
							break;
					}
				}
			}
			else
				System.out.println("**** The class for the actor has not been found, cannot update hierarchy");
		}
		else if(OperationsCentreSnapshotDTO.class.isInstance(origSnapshot)) {
			Long objectID = ((OperationsCentreSnapshotDTO)origSnapshot).getOperationsCentre();
			Set<OCMeocALTDTO> meocs = eoc.getSubordinateMEOCs();
			for(OCMeocALTDTO meoc : meocs) {
				if(meoc.getId() == objectID) {
					meoc.setSnapshot((OperationsCentreSnapshotDTO)origSnapshot);
					break;
				}
			}


		}
		else
			System.out.println("**** Unknown snapshot received for process ****");

	}

}
