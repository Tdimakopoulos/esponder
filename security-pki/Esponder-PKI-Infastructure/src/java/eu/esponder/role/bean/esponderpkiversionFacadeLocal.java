/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.role.bean;

import eu.esponder.version.esponderpkiversion;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author tdim
 */
@Local
public interface esponderpkiversionFacadeLocal {

    void create(esponderpkiversion esponderpkiversion);

    void edit(esponderpkiversion esponderpkiversion);

    void remove(esponderpkiversion esponderpkiversion);

    esponderpkiversion find(Object id);

    List<esponderpkiversion> findAll();

    List<esponderpkiversion> findRange(int[] range);

    int count();
    
}
