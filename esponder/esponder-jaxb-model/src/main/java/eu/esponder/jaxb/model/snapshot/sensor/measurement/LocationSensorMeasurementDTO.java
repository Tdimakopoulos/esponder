package eu.esponder.jaxb.model.snapshot.sensor.measurement;

import eu.esponder.jaxb.model.snapshot.location.PointDTO;

public class LocationSensorMeasurementDTO extends SensorMeasurementDTO {

	private PointDTO point;

	public PointDTO getPoint() {
		return point;
	}

	public void setPoint(PointDTO point) {
		this.point = point;
	}

}
