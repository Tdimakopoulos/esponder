/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package esponder.biomed.lib;

/**
 * Class with some methods for encoding and decoding byte streams using COBS algorithm
 * The input for decoding is given with the sync byte and 
 * the output for the encoding is given with the sync byte,
 * so that the call of this method doesn't require any processing before.
 * @author afa
 */
public class Cobs {

    /**
     * Encode a byte array using COBS algorithm
     * @param input the array to be encoded
     * @return the ecoded byte stream with the sync byte 0 at the end
     */
    public static byte[] encode(final byte[] input) {
        byte output[] = null;
        byte buffer[] = new byte[2 * input.length];
        int rdindex = 0;
        int wrindex = 1;
        int code_index = 0;
        short code = 0x01;
        
        while (rdindex < input.length) {
            if (input[rdindex] == 0x00) {
                buffer[code_index] = (byte) code;
                code_index = wrindex++;
                code = 0x01;
                rdindex++;
            } else {
                buffer[wrindex++] = input[rdindex++];
                code++;
                if (code == 0xff) {
                    buffer[code_index] = (byte) code;
                    code_index = wrindex++;
                    code = 0x01;
                }
            }
        }
        buffer[code_index] = (byte) code;
        output = new byte[wrindex+1];
        System.arraycopy(buffer, 0, output, 0, wrindex);
        //add terminaison
        output[wrindex] = 0;
        return output;
    }

    /**
     * Decode a byte array using COBS algorithm
     * @param input the array to be decoded includind the sync byte 0
     * @return 
     */
    public static byte[] decode(final byte[] input, int length) {
        byte output[] = null;
        byte buffer[] = new byte[2 * length];
        int rdindex = 0;
        int wrindex = 0;
        
        while (rdindex < length-1) {
            int code = input[rdindex++] & 0xff;        /*Process code as unsigned byte*/
            if (code > 1) {
                for (int copydecount = code - 1; copydecount > 0; --copydecount) {
                    buffer[wrindex++] = input[rdindex++];
                }
            }
            if (code < 0xff) {
                buffer[wrindex++] = 0x00;
            }
        }
        if (wrindex-1<0)
            return null;
        
        
        output = new byte[wrindex-1];
        System.arraycopy(buffer, 0, output, 0, output.length);
        return output;
    }
};