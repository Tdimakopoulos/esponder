package eu.esponder.fru.measurement;

public enum MeasurementProcessingType {

	Average,
	Maximum,
	Minimum,
	Stdev,
	Autocorrelation
	
}
