/*
 * 
 */
package eu.esponder.controller.crisis.weatherstation;

import javax.ejb.Remote;

import eu.esponder.dto.model.crisis.resource.WeatherStationDTO;

@Remote
public interface WeatherStationRemoteService {

	public WeatherStationDTO findWeatherStationDTOById(Long weatherStationID);

	public WeatherStationDTO findWeatherStationByTitleRemote(String wsTitle,
			Long userId);

	public WeatherStationDTO createWeatherStationRemote(
			WeatherStationDTO weatherStationDTO, Long userID);

	public void deleteWeatherStationRemote(Long weatherStationID, Long userID);

	public WeatherStationDTO updateWeatherStationRemote(
			WeatherStationDTO weatherStationDTO, Long userID);

}
