/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.esponder.events;

import java.io.Serializable;


/**
 *
 * @author Thomas
 */
public class EventsDTO implements Serializable {
 private String etimestamp;
 private String etype;
 private String esource;
 private String eaddinfo;
 private String eseverity;

    /**
     * @return the etimestamp
     */
    public String getEtimestamp() {
        return etimestamp;
    }

    /**
     * @param etimestamp the etimestamp to set
     */
    public void setEtimestamp(String etimestamp) {
        this.etimestamp = etimestamp;
    }

    /**
     * @return the etype
     */
    public String getEtype() {
        return etype;
    }

    /**
     * @param etype the etype to set
     */
    public void setEtype(String etype) {
        this.etype = etype;
    }

    /**
     * @return the esource
     */
    public String getEsource() {
        return esource;
    }

    /**
     * @param esource the esource to set
     */
    public void setEsource(String esource) {
        this.esource = esource;
    }

    /**
     * @return the eaddinfo
     */
    public String getEaddinfo() {
        return eaddinfo;
    }

    /**
     * @param eaddinfo the eaddinfo to set
     */
    public void setEaddinfo(String eaddinfo) {
        this.eaddinfo = eaddinfo;
    }

    /**
     * @return the eseverity
     */
    public String getEseverity() {
        return eseverity;
    }

    /**
     * @param eseverity the eseverity to set
     */
    public void setEseverity(String eseverity) {
        this.eseverity = eseverity;
    }
}
