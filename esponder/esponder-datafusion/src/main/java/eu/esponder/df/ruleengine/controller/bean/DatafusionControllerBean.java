/*
 * 
 */
package eu.esponder.df.ruleengine.controller.bean;

import java.io.IOException;
import java.util.List;

import javax.ejb.Stateless;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import eu.esponder.controller.persistence.CrudRemoteService;
import eu.esponder.datafusion.hserver.COPServer;
import eu.esponder.df.eventhandler.bean.ActionEventHandlerBean;
import eu.esponder.df.eventhandler.bean.MobileServicesEventHandlerBean;
import eu.esponder.df.eventhandler.bean.SecurityEventHandlerBean;
import eu.esponder.df.eventhandler.bean.SensorMeasurmentEventHandlerBean;
import eu.esponder.df.ruleengine.controller.DatafusionControllerRemoteService;
import eu.esponder.df.ruleengine.controller.DatafusionControllerService;
import eu.esponder.df.ruleengine.core.RuleEngineGuvnorAssets;
import eu.esponder.df.ruleengine.repository.RepositoryController;
import eu.esponder.df.ruleengine.utilities.RuleLookup;
import eu.esponder.df.ruleengine.utilities.ruleresults.object.RuleResultsXML;
import eu.esponder.df.rules.profile.ProfileManager;
import eu.esponder.dto.model.ESponderEntityDTO;
import eu.esponder.event.model.ESponderEvent;
import eu.esponder.exception.EsponderCheckedException;
//import eu.esponder.model.events.entity.OsgiEventsEntity;

import eu.esponder.test.ResourceLocator;


/*UDPSend pc=new UDPSend();
UDPReceive pr=new UDPReceive();
pc.SendValue("localhost", 8899, "stomtom");
pc.SendValue("localhost", 8899, "stomdim");
pc.SendValue("localhost", 8899, "stomtom22");

pc.SendValue("localhost", 8899, "ftomtom");
pr.Receive(9999);*/

// TODO: Auto-generated Javadoc
/**
 * The Class DatafusionControllerBean.
 */
@Stateless
public class DatafusionControllerBean implements DatafusionControllerRemoteService,DatafusionControllerService {

	
	/** The crud service. */
	//private CrudRemoteService<OsgiEventsEntity> crudService = ResourceLocator.lookup("esponder/CrudBean/remote");

	/** The sz name. */
	private String szName;

	/** The sz event type sensor measurment. */
	private String szEventTypeSensorMeasurment="CreateSensorMeasurementStatisticEvent";
	
	/** The sz event type action. */
	private String szEventTypeAction="CreateActionEvent";
	
	private String szLoginRequestEvent="LoginRequestEvent";
	private String szLogoutEsponderUserEvent="LogoutEsponderUserEvent";
	private String szCreateSensorMeasEnvEvent = "CreateSensorMeasurementEnvelopeEvent";
	


	/* (non-Javadoc)
	 * @see eu.esponder.df.ruleengine.controller.DatafusionControllerRemoteService#GetRuleResults()
	 */
	public List<RuleResultsXML> GetRuleResults() throws Exception
	{
		RuleLookup plookup = new RuleLookup();
		plookup.Initialize2();
		return plookup.ReadXMLFileWithReturn();	
	}
	
	/* (non-Javadoc)
	 * @see eu.esponder.df.ruleengine.controller.DatafusionControllerRemoteService#ReinitializeRepository()
	 */
	@SuppressWarnings("unused")
	public void ReinitializeRepository()
	{
		ProfileManager pManager=new ProfileManager();
		RuleEngineGuvnorAssets dAssets= new RuleEngineGuvnorAssets();
		String[] rules=dAssets.PopulateLocalRepositoryForCategory(pManager.GetProfileNameForSensor().getSzProfileName());//"Esponder.Sensors");
		String[] rules1=dAssets.PopulateLocalRepositoryForCategory(pManager.GetProfileNameForAction().getSzProfileName());//"Esponder.Action");
		String[] rules2=dAssets.PopulateLocalRepositoryForPackage(pManager.GetProfileNameForEsponder().getSzProfileName());//"esponder");
	}
	
	/* (non-Javadoc)
	 * @see eu.esponder.df.ruleengine.controller.DatafusionControllerRemoteService#SetToLocal()
	 */
	public void SetToLocal()
	{
		RepositoryController pController = new RepositoryController();
		pController.SetRepositoryToLocal();
	}
	
	/* (non-Javadoc)
	 * @see eu.esponder.df.ruleengine.controller.DatafusionControllerRemoteService#SetToLive()
	 */
	public void SetToLive()
	{
		RepositoryController pController = new RepositoryController();
		pController.SetRepositoryToGuvnor();
	}
	
	/* (non-Javadoc)
	 * @see eu.esponder.df.ruleengine.controller.DatafusionControllerRemoteService#DeleteRepository()
	 */
	public void DeleteRepository()
	{
		RepositoryController pController = new RepositoryController();
		pController.DeleteRepository();
	}
	
	/* (non-Javadoc)
	 * @see eu.esponder.df.ruleengine.controller.DatafusionControllerRemoteService#EsponderEventReceivedHandler(eu.esponder.event.model.ESponderEvent)
	 */
	@Override
	public void EsponderEventReceivedHandler(ESponderEvent<?> pEvent) {
		szName = pEvent.getClass().getSimpleName();

		PersistEvent(pEvent);
		
		DatafusionEventController pController=new DatafusionEventController();
		
		if(szName.equalsIgnoreCase(szCreateSensorMeasEnvEvent)) {
			System.out.println("------> Location Sensor Event Processor");
			SensorMeasurmentEventHandlerBean pHandler = new SensorMeasurmentEventHandlerBean();
			pHandler.ProcessEvent(pEvent);
			
		}
		
		if (szName.equalsIgnoreCase(szEventTypeSensorMeasurment))
		{
			System.out.println("------> Sensor Event Processor");
			SensorMeasurmentEventHandlerBean pHandler = new SensorMeasurmentEventHandlerBean();
			pHandler.ProcessEvent(pEvent);
		}

		if (szName.equalsIgnoreCase("LoginRequestEvent"))
		{
			System.out.println("------> Login Event Processor");
			//System.out.println("------> Security event received");
			//System.out.println("------> Message : "+pEvent.getJournalMessage());
			SecurityEventHandlerBean pHandler = new SecurityEventHandlerBean();
			pHandler.ProcessEvent(pEvent);
		}
		
		if (szName.equalsIgnoreCase(szLogoutEsponderUserEvent))
		{
			//System.out.println("------> Logout Event Processor");
			SecurityEventHandlerBean pHandler = new SecurityEventHandlerBean();
			pHandler.ProcessEvent(pEvent);
		}

		if (szName.equalsIgnoreCase(szEventTypeAction))
		{
			System.out.println("------> Action Event Processor");
			ActionEventHandlerBean pHandler = new ActionEventHandlerBean();
			pHandler.ProcessEvent(pEvent);
		}
		
		
		if(pController.IsMobileEvent(szName))
		{
//			LockManager pm= new LockManager();
//			try {
//				while(pm.isLocked())
//				{
//					try {
//						Thread.sleep(100);
//					} catch (InterruptedException e) {
//					System.out.println("Error on DatafusionControllerBean Lock"+e.getMessage());
//					}
//				}
//			} catch (IOException e) {
//				System.out.println("Error on DatafusionControllerBean Lock"+e.getMessage());
//			}
//			pm.enableLock();
			MobileServicesEventHandlerBean pHandler = new MobileServicesEventHandlerBean();
			pHandler.ProcessEvent(pEvent);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				System.out.println("---------------- Error in sleep on Mobile event ---------------");
			}
//			pm.disableLock();

		}
		//System.out.println("---------------- EVENT RECEIVED Ends ---------------");
 	}
	
	
	/**
	 * Persist event.
	 *
	 * @param event the event
	 */
	private void PersistEvent(ESponderEvent<? extends ESponderEntityDTO> event) {
		
//	//	OsgiEventsEntity dEntity = new OsgiEventsEntity();
//
//		dEntity.setJournalMsg(event.getJournalMessage());
//
//		dEntity.setSeverity(event.getEventSeverity().toString());
//
//		dEntity.setSourceid(event.getEventSource().getId());
//
//		dEntity.setTimeStamp(event.getEventTimestamp().getTime());
//		
//		ObjectMapper mapper = new ObjectMapper();
////		mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_CONCRETE_AND_ARRAYS);
////		mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//		String attachment = null;
//		try {
//			attachment = mapper.writeValueAsString(event.getEventAttachment());
//		} catch (JsonGenerationException e) {
//			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
//			
//		} catch (JsonMappingException e) {
//			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
//		} catch (IOException e) {
//			new EsponderCheckedException(this.getClass(),"ESponder Exception : "+e.getMessage());
//		}
//
//		dEntity.setAttachment(attachment);
//		
//		dEntity.setSource(event.getEventSource().toString());
//		
//		if(crudService != null){
//			@SuppressWarnings("unused")
//			OsgiEventsEntity entityPersisted = crudService.create(dEntity);
//		}
//		else
//			System.out.println("Null error, cannot persist event...");
	}
	
	/**
	 * Prints the event.
	 *
	 * @param event the event
	 */
	@SuppressWarnings("unused")
	private void printEvent(ESponderEvent<? extends ESponderEntityDTO> event) {
		//System.out.println("########### DF EVENT Handler - EVENT RECEIVED #############");
		//System.out.println("########### EVENT DETAILS START #############");
		//System.out.println("CSSE # " + event.toString());
		//System.out.println("CSSE attachment # " + event.getEventAttachment().toString());
		//System.out.println("CSSE severity # " + event.getEventSeverity());
		//System.out.println("CSSE source # " + event.getEventSource());
		//System.out.println("CSSE timestamp# " + event.getEventTimestamp());
		//System.out.println("########### EVENT DETAILS END #############");

	}
}
