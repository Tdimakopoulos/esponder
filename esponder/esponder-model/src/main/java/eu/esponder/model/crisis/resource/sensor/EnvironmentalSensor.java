/*
 * 
 */
package eu.esponder.model.crisis.resource.sensor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


// TODO: Auto-generated Javadoc
/**
 * The Class EnvironmentalSensor.
 */
@Entity
@DiscriminatorValue("ENVIRONMENTAL")
public abstract class EnvironmentalSensor extends Sensor {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 909177965785563306L;

}
