package InterfChecker;

import java.util.Date;
import java.util.Vector;

import InterfChecker.data.ListA;
import InterfChecker.data.ListB;


public class ListAThread extends Thread {

	private volatile boolean stop = false;//variable if thread needs to close--If true then we are stopping the thread
	private String info=" INFO ";
	static int state=1;
	protected double T;//xronos twn metrisewn dil.time2-time1
	protected static double totalTime;//Synolikos xronos dil.time3-time1
	protected static boolean changed = false;//Metavliti gia to an vrikame allagi
	protected  Date time1;//Xronos stin arxi tou iteration tou thread
	protected  Date time2;//Xronos meta tis leitourgies/elegxous tou thread
	protected  Date time3;//Xronos meta to timeout
	protected double timeout=Application.timeout;//To timeout pou tha kanei sleep to thread -- Arxikopoieitai stin timi tou property file
	protected int counterNoSuccess;//H metavliti gia tis anepitixeis epanalispeis
	int states;//H metavliti gia tis katastaseis
	protected static double changeRate;//Syntelestis metavolis x
	double totalTimeTimeout=Application.timeout;//To timeout pou yplogizei kai stelnei pisw h markov -- Arxikopoieitai stin timi tou property file
	
	public ListAThread(String str) {
		super(str);
	}
	
	  public String toString() {//print message
		    return "[Thread-" +getId()+ "] ";
		  }
	
	  public String monitorInterf() {//print message
		  return this+info+"- "+this.getName()+" -ListAThread- " +
		  		"State is "+(Application.states - states+1)+" Timeout is:"+timeout+"ms"+" Counter of No Success:"+counterNoSuccess+" Change Rate:"+changeRate;
		  }
	
	  public void run() {
		  this.states =Application.states; //Initializing states
		  this.counterNoSuccess=Application.counterNoSuccess; //Initializing counter of No Success
		  changeRate=Application.changeRate; //Initializing change Rate
		  while(!stop) {//If stop is true then stop
			  try {
				  System.out.println(monitorInterf());
				  time1 = new Date();//initializing timer
				  changed=ischanged();//checking if we have a change
                                  InterfaceThread.dataCheckerThread.changed=changed;
				  if(changed){//If we have changes we need to update the Lists with the values
					  InterfaceThread.activeListA=new Vector<ListA>();
					  InterfaceThread.activeListB=new Vector<ListB>();
					  for (int k = 0; k < InterfaceThread.interfVectorA.size(); k++){//Iterating vector A of new Interfaces 
							ListA threadUpdated = InterfaceThread.interfVectorA.elementAt(k);
							for (int i = 0; i < InterfaceThread.threadListA.size(); i++){//Checking acive threads
								ListAThread test = InterfaceThread.threadListA.elementAt(i);
								if(threadUpdated.getName().compareToIgnoreCase(test.getName())==0){//If names match
									InterfaceThread.activeListA.add(threadUpdated);//Add new Element
								}
							}
					  }
					  for (int k = 0; k < InterfaceThread.interfVectorB.size(); k++){//Doing the same for Vector B
							ListB threadUpdated = InterfaceThread.interfVectorB.elementAt(k);
							for (int i = 0; i < InterfaceThread.threadListB.size(); i++){
								ListBThread test = InterfaceThread.threadListB.elementAt(i);
								if(threadUpdated.getName().compareToIgnoreCase(test.getName())==0){
									InterfaceThread.activeListB.add(threadUpdated);
								}
							}
					  }
				  }
				  time2 = new Date();//xronos meta apo leitourgies kai elegxous
				  sleep((int)(timeout));

				  time3 = new Date();//Xronos meta to sleep
				  timeout = markovSleepTime(time1,time2,time3,changed);//calculate timeout
			  }
			  catch (Exception e)
			  {
				  e.printStackTrace();
				  return;
			  }
			  if (stop)
				  System.out.println("Stoppping thread "+ this);
		  }
	  }


	  //Method to stop thread
	  public void requestStop() {
		    stop = true;
	  }
	  
	  //Method Markov to calculate new timeout according state and counter of no success
	  public double markovSleepTime(Date time1,Date time2,Date time3,boolean changed) throws Exception{
		  if(time2!=null){//Tin prwti fora den exw parei xronous ara den ypologizw timeout
			  long l1 = time1.getTime();
			  long l2 = time2.getTime();
			  long l3 = time3.getTime();
			  T = (l2 - l1);
			  totalTime = (l3 - l1);
			  if(this.changed){//An vrw allages tote ta arxikopoiw ola
                              InterfaceThread.dataCheckerThread.changed=changed;
				  this.states=Application.states;
				  this.totalTimeTimeout =Double.parseDouble(Application.sleepTime.trim())-T;//Afairw xrono leitoourgias/elegxwn
				  this.changed=false;
				  this.counterNoSuccess=Application.counterNoSuccess;
				  return this.totalTimeTimeout;
			  }
			  else{//An den vrw allages,tote meiwnw to metriti anepityxwn epanalipsewn(c)mexri na ftasei sto 1--Otan ftasei sto 1 ypologizw to neo timeout
				  if(this.counterNoSuccess>1){
					  this.counterNoSuccess--;
					  this.totalTimeTimeout=(((Application.states-this.states+1)*Application.timeout)-T);
				  }
				  else if(this.counterNoSuccess==1){
					  if(this.states>0){
						  this.states--;
						  this.totalTimeTimeout=(((Application.states-this.states+1)*Application.timeout)-T);
						  this.counterNoSuccess=Application.counterNoSuccess;
					  }
				  }
			  }
		  }
		  return this.totalTimeTimeout;
	  }
		//Method to check for changes in states
		public boolean ischanged() throws Exception
		{
			if(!changed){
				for (int k = 0; k < InterfaceThread.activeListA.size(); k++){//Iterating active ListA Interfaces to check if we have changes with the newer values
					ListA activeListAInterf = InterfaceThread.activeListA.elementAt(k);
					for (int i = 0; i < InterfaceThread.interfVectorA.size(); i++){//Iterating updated ListA Interfaces

						ListA test = InterfaceThread.interfVectorA.elementAt(i);
						if(activeListAInterf.getName().compareToIgnoreCase(test.getName())==0){//If names match in both lists
							if(activeListAInterf.getName().compareToIgnoreCase(this.getName())==0){//and the name is the same with the thread
								if(activeListAInterf.getIpAddress()!=null){//Checking IP in both lists

									if((test.getIpAddress()==null)||!(activeListAInterf.getIpAddress().equalsIgnoreCase(test.getIpAddress()))){
										System.out.println(this.getName()+" IP CHANGED - INITIALIZING STATE AND TIMEOUT");
										return true;
									}
								}
								else if(test.getIpAddress()!=null){
									if((activeListAInterf.getIpAddress()==null)||!(activeListAInterf.getIpAddress().equalsIgnoreCase(test.getIpAddress()))){
										System.out.println(this.getName()+" IP CHANGED - INITIALIZING STATE AND TIMEOUT");
										return true;
									}
								}
								else if(activeListAInterf.getMacAddress()!=null){//Checking MAC in both lists

									if((test.getMacAddress()==null)||!(activeListAInterf.getMacAddress().equalsIgnoreCase(test.getMacAddress()))){
										System.out.println(this.getName()+" MAC CHANGED - INITIALIZING STATE AND TIMEOUT");
										return true;
									}
								}
								else if(test.getMacAddress()!=null){
									if((activeListAInterf.getMacAddress()==null)||!(activeListAInterf.getMacAddress().equalsIgnoreCase(test.getMacAddress()))){
										System.out.println(this.getName()+" MAC CHANGED - INITIALIZING STATE AND TIMEOUT");
										return true;
									}
								}
								else if(activeListAInterf.getGateway()!=null){//Checking Gateway in both lists

									if((test.getGateway()==null)||!(activeListAInterf.getGateway().equalsIgnoreCase(test.getGateway()))){
										System.out.println(this.getName()+" GATEWAY CHANGED - INITIALIZING STATE AND TIMEOUT");
										return true;
									}
								}
								else if(test.getGateway()!=null){
									if((activeListAInterf.getGateway()==null)||!(activeListAInterf.getGateway().equalsIgnoreCase(test.getGateway()))){
										System.out.println(this.getName()+" GATEWAY CHANGED - INITIALIZING STATE AND TIMEOUT");
										return true;
									}
								}
								else if(activeListAInterf.getNetMask()!=null){//Checking Subnet mask in both lists

									if((test.getNetMask()==null)||!(activeListAInterf.getNetMask().equalsIgnoreCase(test.getNetMask()))){
										System.out.println(this.getName()+" SUBNET MASK CHANGED - INITIALIZING STATE AND TIMEOUT");
										return true;
									}
								}
								else if(test.getNetMask()!=null){
									if((activeListAInterf.getNetMask()==null)||!(activeListAInterf.getNetMask().equalsIgnoreCase(test.getNetMask()))){
										System.out.println(this.getName()+" SUBNET MASK CHANGED - INITIALIZING STATE AND TIMEOUT");
										return true;
									}
								}
								else if(test.getCurPercLineRate()!=null&&activeListAInterf.getCurPercLineRate()!=null){//Checking the Change of Rate according to value from property file in both lists
									if(test.getCurPercLineRate()>(activeListAInterf.getCurPercLineRate()+Application.changeRate)){
										System.out.println(this.getName()+" CURRENT PERCENT LINE RATE CHANGED OVER SPECIFIED VALUE - INITIALIZING STATE AND TIMEOUT");
										return true;
									}
								}
								else if(test.getPacketErrorRate()!=null&&activeListAInterf.getPacketErrorRate()!=null){//Checking the Change of PER according to value from property file in both lists
									if(test.getPacketErrorRate()>(activeListAInterf.getPacketErrorRate()+Application.changeRate)){
										System.out.println(this.getName()+" PACKET ERROR RATE CHANGED OVER SPECIFIED VALUE - INITIALIZING STATE AND TIMEOUT");
										return true;
									}
								}
								else
									return false;
							}
						}
					}
				}
				for (int k = 0; k < InterfaceThread.activeListB.size(); k++){//Iterating active ListB Interfaces as above
					ListB activeListBInterf = InterfaceThread.activeListB.elementAt(k);
					for (int i = 0; i < InterfaceThread.interfVectorB.size(); i++){

						ListB test = InterfaceThread.interfVectorB.elementAt(i);
						if(activeListBInterf.getName().compareToIgnoreCase(test.getName())==0){
							if(activeListBInterf.getName().compareToIgnoreCase(this.getName())==0){
								if(activeListBInterf.getTxPower()!=null){

									if((test.getTxPower()==null)||!(activeListBInterf.getTxPower().equalsIgnoreCase(test.getTxPower()))){//Checking tx power in both lists
										System.out.println("Tx Power CHANGED - INITIALIZING STATE AND TIMEOUT");
										return true;
									}
								}
								else if(test.getTxPower()!=null){
									if((activeListBInterf.getTxPower()==null)||!(activeListBInterf.getTxPower().equalsIgnoreCase(test.getTxPower()))){
										System.out.println("Tx Power CHANGED - INITIALIZING STATE AND TIMEOUT");
										return true;
									}
								}
								else if(activeListBInterf.getLinkLevelNoise()!=null){//Checking Link Level Noise in both lists

									if((test.getLinkLevelNoise()==null)||!(activeListBInterf.getLinkLevelNoise().equalsIgnoreCase(test.getLinkLevelNoise()))){
										System.out.println("Link Level Noise CHANGED - INITIALIZING STATE AND TIMEOUT");
										return true;
									}
								}
								else if(test.getLinkLevelNoise()!=null){
									if((activeListBInterf.getLinkLevelNoise()==null)||!(activeListBInterf.getLinkLevelNoise().equalsIgnoreCase(test.getLinkLevelNoise()))){
										System.out.println("Link Level Noise CHANGED - INITIALIZING STATE AND TIMEOUT");
										return true;
									}
								}
								else if(activeListBInterf.getLinkQuality()!=null){//Checking Link Quality in both lists

									if((test.getLinkQuality()==null)||!(activeListBInterf.getLinkQuality().equalsIgnoreCase(test.getLinkQuality()))){
										System.out.println("Link Quality CHANGED - INITIALIZING STATE AND TIMEOUT");
										return true;
									}
								}
								else if(test.getLinkQuality()!=null){
									if((activeListBInterf.getLinkQuality()==null)||!(activeListBInterf.getLinkQuality().equalsIgnoreCase(test.getLinkQuality()))){
										System.out.println("Link Quality CHANGED - INITIALIZING STATE AND TIMEOUT");
										return true;
									}
								}
								else
									return false;
							}
						}
					}
				}
			}
			return false;
		}
}
